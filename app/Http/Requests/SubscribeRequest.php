<?php namespace Okuma\Http\Requests;

use Okuma\Http\Requests\Request;

class SubscribeRequest extends Request
{
    /**
     * The key to be used for the view error bag.
     *
     * @var string
     */
    protected $errorBag = 'subscription';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subscription-email' => 'required|email',
            'subscription-firstname' => 'required',
            'subscription-lastname' => 'required',

            'subscription-username' => 'honeypot',
            'subscription-time' => 'required|honeytime:' . rand(3, 5),
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'subscription-email.required' => '<i>OOPS!</i> You need to enter your <i>Email Address</i>.',
            'subscription-email.email' => '<i>OOPS!</i> The <i>Email Address</i> seems to be invalid.',
            'subscription-firstname.email' => '<i>OOPS!</i> The <i>Email Address</i> seems to be invalid.',
            'subscription-lastname.email' => '<i>OOPS!</i> The <i>Email Address</i> seems to be invalid.',
            'subscription-time.honeytime' => '<em>WOAH!</em> Please wait a little then try again.',
        ];
    }
    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [];
    }
}
