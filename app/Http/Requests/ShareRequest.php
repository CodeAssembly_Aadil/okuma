<?php namespace Okuma\Http\Requests;

use Okuma\Http\Requests\Request;

class ShareRequest extends Request
{
    /**
     * The key to be used for the view error bag.
     *
     * @var string
     */
    protected $errorBag = 'share';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sender-name' => 'required',
            'sender-email' => 'required|email',
            'recipient-email' => 'required|email',
            'sender-surname' => 'honeypot',
            'share-time' => 'required|honeytime:' . rand(2, 4),
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'sender-name.required' => '<em>OOPS!</em> Let your friend know who sent this share',

            'sender-email.required' => '<em>OOPS!</em> Your Email address is required.',
            'sender-email.email' => '<em>OOPS!</em> The <em>Email Address</em> doesn\'t seem to be valid.',

            'recipient-email.required' => '<em>OOPS!</em> Who are you sharing this with?',
            'recipient-email.email' => '<em>OOPS!</em> The <em>Email Address</em> doesn\'t seem to be valid.',

            'share-time.honeytime' => '<em>WOAH!</em> Please wait a litte while before submitting your message again.',
        ];
    }
    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [];
    }
}
