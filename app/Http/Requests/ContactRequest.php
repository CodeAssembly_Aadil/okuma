<?php namespace Okuma\Http\Requests;

use Okuma\Http\Requests\Request;

class ContactRequest extends Request
{
    /**
     * The key to be used for the view error bag.
     *
     * @var string
     */
    protected $errorBag = 'contact';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact-name' => 'required',
            'contact-email' => 'required|email',
            'contact-subject' => 'required|min:3|max:70', // 'Okuma: ' 78-8 chars single length subject line
            'contact-message' => 'required|min:11',

            'contact-lastname' => 'honeypot',
            'contact-time' => 'required|honeytime:' . rand(9, 15),
            // 'g-recaptcha-response' => 'required|recaptcha',
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'contact-name.required' => '<em>OOPS!</em> Please tell us who you are.',

            'contact-email.required' => '<em>OOPS!</em> Let us know how to get in touch with you.',
            'contact-email.email' => '<em>OOPS!</em> The <em>Email Address</em> doesn\'t seem to be valid.',

            'contact-subject.required' => '<em>OOPS!</em> Please give your message a brief <em>Subject</em>.',
            'contact-subject.min' => '<em>OOPS!</em> Please be a bit more descriptive with the <em>Subject</em> line.',
            'contact-subject.max' => '<em>WOOPS!</em> The <em>Subject</em> line seems a bit too long.',

            'contact-message.required' => '<em>OOPS!</em> Please tell us whats on your mind.',
            'contact-message.min' => '<em>OOPS!</em> Please tell us a bit more.',
            'contact-time.honeytime' => '<em>WOAH!</em> Please wait a litte while before submitting your message again.',

            // 'g-recaptcha-response.required' => '<em>OOPS!</em> You have to promise you aren\'t a robot.',
            // 'g-recaptcha-response.recaptcha' => '<em>OOPS!</em> You have to promise you aren\'t a robot.',
        ];
    }
    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [];
    }
}
