<?php namespace Okuma\Http\Requests;

use Okuma\Http\Requests\DashboardRequest;

class AdminUserRequest extends DashboardRequest
{

    /**
     * The input keys that should not be flashed on redirect.
     *
     * @var array
     */
    protected $dontFlash = ['password', 'new_password', 'new_password_confirmation'];

    public function rules()
    {
        return [
            'password' => 'required',
            'new_password' => 'required|string|min:8|confirmed',
            'new_password_confirmation' => 'required|string|min:8',
        ];
    }
}
