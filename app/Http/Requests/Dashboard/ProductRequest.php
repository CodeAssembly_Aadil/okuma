<?php namespace Okuma\Http\Requests\Dashboard;

use Okuma\Http\Requests\DashboardRequest;

class ProductRequest extends DashboardRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [

            'name' => 'sometimes|required|string|max:219',
            // 'description' => 'sometimes',
            'published_at' => 'date',
            'snippet' => 'string|max:255',

            'specification' => 'sometimes|required|array',
            'categories' => 'sometimes|required|array',
            'features' => 'sometimes|required|string|json_schema:orderables_schema.json',
        ];

        foreach ($this->request->get('categories', []) as $key => $val) {
            $rules['items.' . $key] = 'integer|min:1|exists:categories,id';
        }

        // foreach ($this->request->get('feature_ids', []) as $key => $val) {
        //     $rules['items.' . $key] = 'integer|min:1|exists:features,id';
        // }

        return $rules;
    }

    // /**
    //  * Get the validator instance for the request.
    //  *
    //  * @return \Illuminate\Contracts\Validation\Validator
    //  */
    // protected function getValidatorInstance()
    // {
    //     $validator = parent::getValidatorInstance();

    //     $validator->sometimes('category_ids', 'valid_date', function ($input) {
    //         return apply_regex($input->dob) === true;
    //     });

    //     return $validator;

    //     // $factory = $this->container->make('Illuminate\Validation\Factory');

    //     // if (method_exists($this, 'validator')) {
    //     //     return $this->container->call([$this, 'validator'], compact('factory'));
    //     // }

    //     // return $factory->make(
    //     //     $this->all(), $this->container->call([$this, 'rules']), $this->messages(), $this->attributes()
    //     // );
    // }

}
