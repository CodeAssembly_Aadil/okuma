<?php namespace Okuma\Http\Requests\Dashboard;

use Okuma\Http\Requests\DashboardRequest;

class SiteFeatureRequest extends DashboardRequest
{
    protected $columnSizes = [
        'site-hero',
        'large-4',
        'large-6',
        'large-8',
        'large-12',
    ];

    protected $displayClasses = [
        '',
        'dark',
    ];
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'string|max:255',
            'link' => 'required|URL|max:2000',
            'column_size' => 'required|in:' . implode(',', $this->columnSizes),
            'display_class' => 'in:' . implode(',', $this->displayClasses),
            'published_at' => 'date',
            'button_label' => 'string|max:64',
        ];
    }
}
