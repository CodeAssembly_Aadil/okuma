<?php namespace Okuma\Http\Requests\Dashboard;

use Illuminate\Support\Facades\Config;
use Okuma\Http\Requests\DashboardRequest;

class UpdateImageRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $imageID = $this->route()->parameters()['image'];

        $useOriginal = $this->input('use-original', false);

        $this->merge(['use_original' => $useOriginal]);

        $rules = [
            'title' => 'sometimes|required|string|max:255',
            'image-file' => 'sometimes|required|mimes:jpeg,png|max:5120',
            'imageable-type' => 'required_with:image-file|exists:images,imageable_type,id,' . $imageID,
            'type' => 'required_with:image-file|required|string|max:255',
            'use-original' => 'boolean',
        ];

        $imageableType = $this->input('imageable-type');
        $type = $this->input('type');

        $image_types = Config::get("okuma.image_types.{$imageableType}");

        if (isset($image_types)) {
            $rules['type'] .= '|in:' . implode(',', $image_types);
        }

        return $rules;
    }
}
