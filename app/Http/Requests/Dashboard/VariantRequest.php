<?php namespace Okuma\Http\Requests\Dashboard;

use Okuma\Http\Requests\DashboardRequest;

class VariantRequest extends DashboardRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'model' => 'required|string|max:32',
            'sku' => 'required|string|max:32',
            // 'specification' => 'required',
        ];
    }
}
