<?php namespace Okuma\Http\Requests\Dashboard;

use Illuminate\Support\Facades\Config;
use Okuma\Http\Requests\DashboardRequest;

class StoreImageRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $useOriginal = $this->input('use-original', 0);

        $this->merge(['use_original' => $useOriginal]);

        $rules = [
            'imageable-type' => 'required|string|max:255|in:' . implode(',', Config::get('okuma.imageable_types')),
            'imageable-id' => 'required|integer|min:1',
            'type' => 'required|string|max:255',
            'image-file' => 'required|mimes:jpeg,png|max:' . Config::get('okuma.max_file_size'),
            'title' => 'required|max:255',
            'use-original' => 'boolean',
        ];

        $imageableType = $this->input('imageable-type');
        $type = $this->input('type');

        $image_types = Config::get("okuma.image_types.{$imageableType}");

        if (isset($image_types)) {
            $rules['type'] .= '|in:' . implode(',', $image_types);
        }

        switch ($imageableType) {

            case 'Okuma\Models\Category':
                $rules['imageable-id'] .= '|exists:categories,id';
                break;

            case 'Okuma\Models\Feature':
                $rules['imageable-id'] .= '|exists:features,id';
                break;

            case 'Okuma\Models\Product':
                $rules['imageable-id'] .= '|exists:products,id';
                break;

            case 'Okuma\Models\SiteFeature':
                $rules['imageable-id'] .= '|exists:site_features,id';
                break;
        }

        return $rules;
    }
}
