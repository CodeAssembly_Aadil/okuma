<?php namespace Okuma\Http\Requests\Dashboard;

use Okuma\Http\Requests\DashboardRequest;

class CategoryRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required|string|max:219',
            'description' => 'string',
            'snippet' => 'string|max:255',
            'products' => 'sometimes|required|string|json_schema:orderables_schema.json',
        ];
    }
}
