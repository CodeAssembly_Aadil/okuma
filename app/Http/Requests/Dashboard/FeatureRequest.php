<?php namespace Okuma\Http\Requests\Dashboard;

use Okuma\Http\Requests\DashboardRequest;

class FeatureRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:219',
            'description' => 'required|string',
            'feature_set_id' => 'sometimes|integer|min:1|exists:feature_sets,id',
        ];
    }
}
