<?php namespace Okuma\Http\Requests\Dashboard;

use Okuma\Http\Requests\DashboardRequest;

class ReorderTreeRequest extends DashboardRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'tree' => 'required|string|json_schema:tree_schema.json',
        ];

        return $rules;
    }
}
