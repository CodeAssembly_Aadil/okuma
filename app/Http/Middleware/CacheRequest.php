<?php namespace Okuma\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Str;

class CacheRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $key = Str::slug($request->url());

        if (Cache::tags('routes')->has($key)) {
            $content = Cache::get($key);
            return response($content);
        }

        return $next($request);
    }

    // protected function cacheKey($url)
    // {
    //     return 'route_' . Str::slug($url);
    // }

}
