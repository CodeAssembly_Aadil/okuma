<?php namespace Okuma\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Str;

class CacheResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        $key = Str::slug($request->url());

        if (!Cache::has($key)) {
            Cache::tags('routes')->put($key, $response->getContent(), 1);
        }

        return $response;
    }

    // protected function cacheKey($url)
    // {
    //     return 'route_' . Str::slug($url);
    // }
}
