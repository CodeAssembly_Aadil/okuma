<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', 'HomePageController@index')->name('home');

Route::get('catalog', 'CategoryController@catalog')->name('catalog');

Route::get('category/{slug}', 'CategoryController@category')->name('category')->where('slug', '[a-z0-9-]+');

// Route::get('categories/{slug}', 'CategoryController@category')->name('categories')->where('slug', '[a-z0-9-]+');

Route::get('product/{slug}', 'ProductController@show')->name('product')->where('slug', '[a-z0-9-]+');

Route::get('article/{slug}', 'ArticleController@show')->name('article')->where('slug', '[a-z0-9-]+');

Route::get('search', 'SearchController@search')->name('search');

// Route::get('luckycarp/terms', 'LuckyCarpCompetitionController@terms')->name('luckycarp.terms');
// Route::get('luckycarp/{source?}', 'LuckyCarpCompetitionController@index')->name('luckycarp.landing')->where('source', '[a-z0-9-]+');

Route::group(['prefix' => 'share'],
    function () {
        Route::get('{type}/{id}', 'ShareController@form')->name('share.form')->where('type', '(product)')->where('id', '[0-9]+');
        Route::post('send/{type}/{id}', 'ShareController@share')->name('share.request')->where('type', '(product)')->where('id', '[0-9]+');
    });

Route::group(['prefix' => 'contact'],
    function () {
        Route::get('/', 'ContactController@index')->name('contact.index');
        Route::post('/send', 'ContactController@send')->name('contact.request');
    });

Route::group(['prefix' => 'newsletter'],
    function () {
        Route::get('/subscribe', 'SubscriptionController@showSubscribe')->name('newsletter.subscribe');
        Route::get('/unsubscribe', 'SubscriptionController@showUnsubscribe')->name('newsletter.unsubscribe');
        Route::post('/subscribe', 'SubscriptionController@subscribe')->name('newsletter.action.subscribe');
        Route::post('/unsubscribe', 'SubscriptionController@unsubscribe')->name('newsletter.action.unsubscribe');
    });

Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.', 'middleware' => 'nocache', 'namespace' => 'Auth'],
    function () {
        Route::get('/login', ['as' => 'login', 'uses' => 'AuthController@login']);
        Route::post('/authenticate', ['as' => 'authenticate', 'uses' => 'AuthController@authenticate']);
        Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
    });

// Route::group(['prefix' => 'combo-strike'],
//     function () {
//         // Route::get('', 'CompetitionController@index')->name('combostrike.competition');
//         Route::get('', 'CompetitionController@index')->name('combostrike.index');
//         Route::get('terms', 'CompetitionController@terms')->name('combostrike.terms');
//         Route::post('enter', 'CompetitionController@processEntry')->name('combostrike.process');
//     });

Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.', 'middleware' => 'auth', 'namespace' => 'Dashboard'],
    function () {

        // Dashboard Index
        Route::get('/', ['as' => 'home', 'uses' => 'DashboardController@index']);

        /////////////////////////////////////////////////////
        // Site Admistrator
        Route::get('administrator', ['as' => 'administrator.index', 'uses' => 'AdminUserController@index']);

        // Images
        Route::match(['PUT', 'PATCH'], 'image/reorder', ['as' => 'image.reorder', 'uses' => 'ImageController@reorder']);

        /////////////////////////////////////////////////////
        // SiteFeature
        Route::get('sitefeature/datatable', ['as' => 'sitefeature.datatable', 'uses' => 'SiteFeatureController@dataTable']);
        Route::get('sitefeature/layout', ['as' => 'sitefeature.layout', 'uses' => 'SiteFeatureController@layout']);
        Route::match(['PUT', 'PATCH'], 'sitefeature/{sitefeature}/restore', ['as' => 'sitefeature.restore', 'uses' => 'SiteFeatureController@restore'])->where(['sitefeature' => '[0-9]+']);
        Route::match(['PUT', 'PATCH'], 'sitefeature/reorder', ['as' => 'sitefeature.reorder', 'uses' => 'SiteFeatureController@reorder']);
        Route::match(['PUT', 'PATCH'], 'sitefeature/{sitefeature}/togglepublish', ['as' => 'sitefeature.togglepublish', 'uses' => 'SiteFeatureController@togglePublish'])->where(['sitefeature' => '[0-9]+']);

        // SiteFeature Backgorund Image
        Route::get('sitefeature/{sitefeature}/image/create/background', ['as' => 'sitefeature.image.create.background', 'uses' => 'SiteFeatureController@createBackground'])->where(['sitefeature' => '[0-9]+']);
        Route::get('sitefeature/{sitefeature}/image/edit/background', ['as' => 'sitefeature.image.edit.background', 'uses' => 'SiteFeatureController@editBackground'])->where(['sitefeature' => '[0-9]+']);

        /////////////////////////////////////////////////////
        // Cateory
        Route::get('category/datatable', ['as' => 'category.datatable', 'uses' => 'CategoryController@dataTable']);
        Route::get('category/tree', ['as' => 'category.tree', 'uses' => 'CategoryController@tree']);
        Route::get('category/search', ['as' => 'category.search', 'uses' => 'CategoryController@search']);
        Route::match(['PUT', 'PATCH'], 'category/{category}/restore', ['as' => 'category.restore', 'uses' => 'CategoryController@restore'])->where(['category' => '[0-9]+']);
        Route::match(['PUT', 'PATCH'], 'category/reorder', ['as' => 'category.reorder', 'uses' => 'CategoryController@reorder']);
        // Cateory Hero Banner Images
        Route::get('category/{category}/image/create/hero-banner', ['as' => 'category.image.create.hero', 'uses' => 'CategoryController@createHeroBanner'])->where(['category' => '[0-9]+']);
        // Category Products
        Route::get('category/{category}/product', ['as' => 'category.product.index', 'uses' => 'CategoryController@products'])->where(['category' => '[0-9]+']);

        /////////////////////////////////////////////////////
        // FeatureSet
        Route::get('featureset/datatable', ['as' => 'featureset.datatable', 'uses' => 'FeatureSetController@dataTable']);
        Route::get('featureset/search', ['as' => 'featureset.search', 'uses' => 'FeatureSetController@search']);
        Route::get('featureset/sort', ['as' => 'featureset.sort', 'uses' => 'FeatureSetController@sort']);
        Route::match(['PUT', 'PATCH'], 'featureset/{featureset}/restore', ['as' => 'featureset.restore', 'uses' => 'FeatureSetController@restore'])->where(['featureset' => '[0-9]+']);
        Route::match(['PUT', 'PATCH'], 'featureset/reorder', ['as' => 'featureset.reorder', 'uses' => 'FeatureSetController@reorder']);
        // FeatureSet Features
        Route::get('featureset/{featureset}/feature', ['as' => 'featureset.feature.index', 'uses' => 'FeatureSetController@features'])->where(['featureset' => '[0-9]+']);
        Route::match(['PUT', 'PATCH'], 'featureset/{featureset}/feature/reorder', ['as' => 'featureset.feature.reorder', 'uses' => 'FeatureSetController@reorderFeatures'])->where(['featureset' => '[0-9]+']);

        /////////////////////////////////////////////////////
        // Product
        Route::get('product/datatable', ['as' => 'product.datatable', 'uses' => 'ProductController@dataTable']);
        Route::get('product/search', ['as' => 'product.search', 'uses' => 'ProductController@search']);
        Route::match(['PUT', 'PATCH'], 'product/{product}/restore', ['as' => 'product.restore', 'uses' => 'ProductController@restore'])->where(['product' => '[0-9]+']);
        Route::match(['PUT', 'PATCH'], 'product/{product}/togglepublish', ['as' => 'product.togglepublish', 'uses' => 'ProductController@togglePublish'])->where(['product' => '[0-9]+']);
        // Product Images
        Route::get('product/{product}/image', ['as' => 'product.image.index', 'uses' => 'ProductController@images'])->where(['product' => '[0-9]+']);
        Route::get('product/{product}/image/create/large', ['as' => 'product.image.create.large', 'uses' => 'ProductController@createLargeImage'])->where(['product' => '[0-9]+']);
        Route::get('product/{product}/image/create/thumbnail', ['as' => 'product.image.create.thumbnail', 'uses' => 'ProductController@createDisplayThumbnail'])->where(['product' => '[0-9]+']);
        Route::get('product/{product}/image/create/shareImage', ['as' => 'product.image.create.shareimage', 'uses' => 'ProductController@createShareImage'])->where(['product' => '[0-9]+']);
        // Product Categories
        Route::get('product/{product}/category', ['as' => 'product.category.index', 'uses' => 'ProductController@categories'])->where(['product' => '[0-9]+']);
        // Product Features
        Route::get('product/{product}/feature', ['as' => 'product.feature.index', 'uses' => 'ProductController@features'])->where(['product' => '[0-9]+']);

        /////////////////////////////////////////////////////
        // Variants
        Route::match(['PUT', 'PATCH'], 'product/{product}/variant/reorder', ['as' => 'product.variant.reorder', 'uses' => 'VariantController@reorder'])->where(['procuct' => '[0-9]+']);
        Route::get('product/{product}/variant/datatable', ['as' => 'product.variant.datatable', 'uses' => 'VariantController@dataTable'])->where(['procuct' => '[0-9]+']);
        Route::match(['PUT', 'PATCH'], 'product/{product}/variant/{variant}/restore', ['as' => 'product.variant.restore', 'uses' => 'VariantController@restore'])->where(['procuct' => '[0-9]+', 'variant' => '[0-9]+']);

        /////////////////////////////////////////////////////
        // Feature
        Route::get('feature/search', ['as' => 'feature.search', 'uses' => 'FeatureController@search']);
        Route::get('feature/datatable', ['as' => 'feature.datatable', 'uses' => 'FeatureController@dataTable']);
        // Route::match(['PUT', 'PATCH'], 'feature/reorder', ['as' => 'feature.reorder', 'uses' => 'FeatureController@reorder']);
        Route::get('feature/{feature}/image/create/icon', ['as' => 'feature.image.create.icon', 'uses' => 'FeatureController@createIcon'])->where(['feature' => '[0-9]+']);
        Route::match(['PUT', 'PATCH'], 'feature/{feature}/restore', ['as' => 'feature.restore', 'uses' => 'FeatureController@restore'])->where(['feature' => '[0-9]+']);

        /////////////////////////////////////////////////////
        // Contact
        Route::get('contact', ['as' => 'contact', 'uses' => 'DashboardController@index']);

        /////////////////////////////////////////////////////
        // Backup
        Route::get('backup', ['as' => 'backup.index', 'uses' => 'BackupController@index']);
        Route::post('backup/create', ['as' => 'backup.create', 'uses' => 'BackupController@create']);
        Route::delete('backup/destroy/{file}', ['as' => 'backup.destroy', 'uses' => 'BackupController@destroy']);
        Route::delete('backup/clean', ['as' => 'backup.clean', 'uses' => 'BackupController@clean']);
        Route::get('backup/download/{file}', ['as' => 'backup.download', 'uses' => 'BackupController@download']);

        /////////////////////////////////////////////////////
        // Error Logs
        Route::get('/logs', ['as' => 'logs.index', 'uses' => 'LogViewerController@index']);
        Route::get('/log-viewer', ['as' => 'logviewer', 'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index']);
        Route::get('/phpinfo', ['as' => 'phpinfo', 'uses' => 'DashboardController@phpinfo']);
    });

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth', 'namespace' => 'Dashboard'],
    function () {
        Route::resource('sitefeature', SiteFeatureController::class, ['except' => ['show']]);
        Route::resource('category', CategoryController::class, ['except' => ['show']]);
        Route::resource('product', ProductController::class);
        Route::resource('product.variant', VariantController::class, ['except' => ['show']]);
        Route::resource('feature', FeatureController::class, ['except' => ['show']]);
        Route::resource('featureset', FeatureSetController::class, ['except' => ['show']]);
        Route::resource('image', ImageController::class, ['except' => ['index', 'show', 'create']]);
    });
