<?php namespace Okuma\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;

class LogViewerController extends Controller
{

    public function index()
    {
        $view = View::make('dashboard.logs.index');

        return $view;
    }
}
