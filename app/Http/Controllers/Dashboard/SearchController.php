<?php namespace Okuma\Http\Controllers\Dashboard;

use Carbon\Carbon;
use Elasticsearch\Client;
use Elasticsearch\Common\Exceptions\ElasticsearchException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;
use Okuma\Http\Requests\DashboardRequest;
use Okuma\Models\Product;
use Okuma\Search\SearchIndexManager;

class SearchController extends Controller
{

    protected $elasticSearch;
    protected $searchIndexManager;
    protected $queue;

    public function __construct(Client $elasticSearch, SearchIndexManager $searchIndexManager)
    {
        $this->queue = 'queues:' . Config::get('okuma.queues.search_index');
        $this->elasticSearch = $elasticSearch;
        $this->searchIndexManager = $searchIndexManager;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $queueLength = Redis::llen($this->queue);

        $updatedAt = Carbon::now();

        $view = View::make('dashboard.search.index')->with(['queueLength' => $queueLength, 'updatedAt' => $updatedAt]);

        return $view;
    }

    public function refresh(DashboardRequest $request)
    {
        try {
            $params['index'] = Config::get('elasticsearch.index');
            $this->elasticSearch->indices()->refresh($params);
        } catch (ElasticsearchException $exception) {
            if ($request->ajax()) {
                return Response::json(['level' => 'warning', 'icon' => 'fa fa-fw fa-lg fa-exclamation-triangle', 'title' => 'Warning: ', 'message' => $exception->getMessage()], $exception->getCode());
            }
        }

        if ($request->ajax()) {
            return response()->json(['level' => 'success', 'icon' => 'fa fa-fw fa-lg fa-check-circle-o', 'title' => 'Success: ', 'message' => "<em>Search Index</em> refreshed"], 200);
        }

        return redirect()->route('dashboard.search');
    }

    public function build(DashboardRequest $request)
    {
        $this->searchIndexManager->clearQueue();

        try {
            $params['index'] = Config::get('elasticsearch.index');
            $this->elasticSearch->indices()->delete($params);
            // $this->elasticSearch->indices()->create(Config::get('elasticsearch.indexConfig'));
        } catch (ElasticsearchException $exception) {
        }

        $products = Product::withDrafts()
            ->whereNotNull('published_at')
            ->get(['id', 'published_at']);

        foreach ($products as $product) {
            $this->searchIndexManager->add($product);
        }

        $this->searchIndexManager->runQueue();

        if ($request->ajax()) {
            return Response::json(['level' => 'success', 'icon' => 'fa fa-fw fa-lg fa-check-circle-o', 'title' => 'Success: ', 'message' => "<em>Search Index</em> is rebuild started."], 200);
        }

        return redirect()->route('dashboard.search');
    }

    public function destroy(DashboardRequest $request)
    {
        $this->searchIndexManager->clearQueue();

        try {
            $params['index'] = Config::get('elasticsearch.index');
            $this->elasticSearch->indices()->delete($params);
        } catch (ElasticsearchException $exception) {
            if ($request->ajax()) {
                Response::json(['level' => 'warning', 'icon' => 'fa fa-fw fa-lg fa-exclamation-triangle', 'title' => 'Warning: ', 'message' => $exception->getMessage()], $exception->getCode());
            }
        }

        if ($request->ajax()) {
            return Response::json(['level' => 'success', 'icon' => 'fa fa-fw fa-lg fa-check-circle-o', 'title' => 'Success: ', 'message' => "<em>Search Index</em> destroyed"], 200);
        }

        return redirect()->route('dashboard.search');
    }

    public function queueLength()
    {
        return Response::json([
            'length' => Redis::llen($this->queue),
            'timestamp' => Carbon::now()->toDateTimeString(),
        ]);

    }
}
