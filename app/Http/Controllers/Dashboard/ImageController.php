<?php namespace Okuma\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Okuma\Facades\ImageManager;
use Okuma\Http\Controllers\Controller;
use Okuma\Http\Requests\DashboardRequest;
use Okuma\Http\Requests\Dashboard\ReorderRequest;
use Okuma\Http\Requests\Dashboard\StoreImageRequest;
use Okuma\Http\Requests\Dashboard\UpdateImageRequest;
use Okuma\Models\Image;

class ImageController extends Controller
{

    protected $modelSingular = 'Image';
    protected $modelPlural = 'Images';

    public function store(StoreImageRequest $request)
    {

        $imageableType = $request->input('imageable-type');
        $imageableID = $request->input('imageable-id');
        $type = $request->input('type');
        $title = $request->input('title');
        $useOriginal = $request->input('use-original');
        $imageFile = $request->file('image-file');

        if (!$imageFile->isValid()) {
            return redirect()->back()
                ->withErrors(['image-file' => 'Upload failed. Invalid file.'])
                ->withInput($request->except(['image-file']));
        } else {

            $position = DB::table('images')->where('imageable_type', '=', $imageableType)
                ->where('imageable_id', '=', $imageableID)
                ->where('type', '=', $type)
                ->max('position');
            // Increment the position by 1
            $position++;

            $dimentions = Config::get("okuma.image_sizes.{$type}");
            $width = $dimentions['width'];
            $height = $dimentions['height'];

            $image = new Image();

            $image->title = $title;
            $image->imageable_id = $imageableID;
            $image->imageable_type = $imageableType;
            $image->type = $type;
            $image->position = $position;

            $image->save();

            try {
                $directory = str_replace('{ID}', $imageableID, Config::get("okuma.image_directories.{$imageableType}"));

                if (!Storage::exists('public/' . $directory)) {
                    Storage::makeDirectory('public/' . $directory);
                }

                $fileExtention = $imageFile->getClientOriginalExtension();
                $fileName = $image->id . '.' . $fileExtention;

                $processedImage = ImageManager::make($imageFile);

                if ($useOriginal) {
                    $width = $processedImage->width();
                    $height = $processedImage->height();
                    $imageFile->move(public_path($directory), $fileName);
                } else {
                    $processedImage->fit($width, $height)
                        ->save(public_path($directory . $fileName), 95);
                }

                $image->width = $width;
                $image->height = $height;
                $image->path = $directory . $fileName;
                $image->save();

            } catch (Exception $exception) {
                $image->destroy();

                return redirect()->back()
                    ->withErrors(['image-file' => 'Upload failed. There was an error processing the file.'])
                    ->withInput($request->except(['image-file']));
            }

            $redirectRoute = $request->session()->pull('redirect', 'dashboard');

            return redirect($redirectRoute);
        }
    }

    public function edit($id)
    {
        if (URL::previous() !== URL::current()) {
            Session::put('redirect', URL::previous());
        }

        $image = null;

        try {
            $image = Image::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $view = View::make('dashboard.image.edit')->with('image', $image);
        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateImageRequest $request, $id)
    {
        $image = null;

        try {
            $image = Image::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $image->title = $request->input('title');

        $imageFile = $request->file('image-file');

        if (isset($imageFile)) {

            if (!$imageFile->isValid()) {
                return redirect()->back()
                    ->withErrors(['image-file' => 'Upload failed. Invalid file.'])
                    ->withInput($request->except(['image-file']));
            }

            try {

                if (Storage::exists('public/' . $image->path)) {
                    Storage::delete('public/' . $image->path);
                }

                $type = $request->input('type');
                $dimentions = Config::get("okuma.image_sizes.{$type}");
                $width = $dimentions['width'];
                $height = $dimentions['height'];
                $useOriginal = $request->input('use-original');

                $directory = str_replace('{ID}', $image->imageable_id, Config::get("okuma.image_directories.{$image->imageable_type}"));

                if (!Storage::exists('public/' . $directory)) {
                    Storage::makeDirectory('public/' . $directory);
                }

                $fileExtention = $imageFile->getClientOriginalExtension();
                $fileName = $image->id . '.' . $fileExtention;

                $processedImage = ImageManager::make($imageFile);

                if ($useOriginal) {
                    $width = $processedImage->width();
                    $height = $processedImage->height();
                    $imageFile->move(public_path($directory), $fileName);
                } else {
                    $processedImage->fit($width, $height)
                        ->save(public_path($directory . $fileName), 95);
                }

                $image->path = $directory . $fileName;
                $image->width = $width;
                $image->height = $height;

            } catch (Exception $exception) {

                return redirect()->back()
                    ->withErrors(['image-file' => 'Upload failed. There was an error processing the file.'])
                    ->withInput($request->except(['image-file']));
            }
            // $image->save();
        }

        $image->save();

        if ($request->session()->has('redirect')) {
            return redirect($request->session()->pull('redirect'));
        }

        return redirect()->route('dashboard.home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(DashboardRequest $request, $id)
    {
        $image = null;
        // return $id;

        try {
            $image = Image::with('relatedImages')->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            if ($request->ajax()) {
                return $this->notFoundResponse($id);
            } else {
                return redirect()->back();
            }
        }

        foreach ($image->relatedImages as $relatedImage) {
            $relatedImage->delete();
        }

        $image->delete();

        if ($request->ajax()) {
            return response()->json(['status' => 'success', 'level' => 'info', 'icon' => 'fa fa-check-circle-o', 'title' => 'Success: ', 'message' => "{$this->modelSingular} <em>#{$image->id}</em> has been permanently deleted!"], 200);
        } else {
            return redirect()->back();
        }

    }

    /**
     * Update the resouce order in storage.
     *
     * @param  ReorderRequest $request
     * @return Response
     */
    public function reorder(ReorderRequest $request)
    {
        $orderableList = json_decode($request->input('orderables'));

        $ids = [];
        $positions = [];

        foreach ($orderableList->orderables as $entry) {
            array_push($ids, $entry->id);
            $positions[$entry->id] = $entry->position;
        }

        $images = Image::whereIn('id', $ids)->get();

        if (count($images) != count($ids)) {
            return redirect()->back();
        }

        foreach ($images as $image) {
            $image->position = $positions[$image->id];
            $image->save();
        }

        return redirect()->back();
    }

    protected function notFoundResponse($id)
    {
        return response()->json(['level' => 'warning', 'icon' => 'fa fa-fw fa-lg fa-exclamation-triangle', 'title' => 'Warning: ', 'message' => "{$this->modelSingular} <em>#{$id}</em> could not be found!"], 404);
    }

    protected function badRequestResponse()
    {
        return response()->json(['status' => 'error', 'level' => 'warning', 'icon' => 'fa fa-fw fa-lg fa-exclamation-triangle', 'title' => 'Warning: ', 'message' => "Bad request. Mismatched parameters."], 422);
    }
}
