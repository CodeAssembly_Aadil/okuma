<?php namespace Okuma\Http\Controllers\Dashboard;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Okuma\Http\Requests\Dashboard\ProductRequest;
use Okuma\Models\Product;
use Yajra\Datatables\Datatables;

class ProductController extends ResourceController
{

    protected $modelClass = Product::class;
    protected $modelSingular = 'Product';
    protected $modelPlural = 'Products';
    protected $routeName = 'dashboard.product';
    protected $searchSelect = ['id', 'name', 'published_at', 'deleted_at'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $dataTable = $this->htmlBuilder
            ->parameters(['pageLength' => 15, "lengthMenu" => [15, 30, 45, 60, 90], 'stateSave' => true])
            ->ajax(['url' => URL::route($this->routeName . '.datatable'), 'type' => 'GET'])
            ->addColumn(['data' => 'id', 'name' => 'id', 'title' => '#'])
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Name'])
            ->addColumn(['data' => 'slug', 'name' => 'slug', 'title' => 'Slug', 'defaultContent' => '', 'searchable' => false])
            ->addColumn(['data' => 'features_count', 'name' => 'features_count', 'title' => 'Total Features', 'defaultContent' => 0, 'searchable' => false, 'orderable' => false])
            ->addColumn(['data' => 'variants_count', 'name' => 'variants_count', 'title' => 'Total Variants', 'defaultContent' => 0, 'searchable' => false, 'orderable' => false])
            ->addColumn(['data' => 'categories', 'name' => 'categories', 'title' => 'Categories', 'defaultContent' => '', 'searchable' => false, 'orderable' => false])
            ->addColumn(['data' => 'published_at', 'name' => 'published_at', 'title' => 'Published At', 'searchable' => false])
            ->addColumn(['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At', 'defaultContent' => '', 'searchable' => false])
            ->addAction();

        $displayVariables = [
            'pageTitle' => $this->modelPlural,
            'pageHeader' => $this->modelPlural,
            'baseRoute' => $this->routeName,
            'modelName' => $this->modelSingular,
        ];

        $view = View::make('dashboard.common.datatable')->with($displayVariables)->with('dataTable', $dataTable);

        return $view;
    }

    public function dataTable()
    {
        $products = Product::select('id', 'name', 'slug', 'published_at', 'updated_at', 'deleted_at')
            ->withTrashed()
            ->withDrafts()
            ->with([
                'variantsCount' =>
                function ($query) {
                    return $query->withTrashed();
                },
                'featuresCount' =>
                function ($query) {
                    return $query->withTrashed();
                },
                'categories' =>
                function ($query) {
                    return $query->select('categories.id', 'categories.name')
                        ->withTrashed();
                },
            ]);

        $now = Carbon::now();

        $dataTable = Datatables::of($products)
            ->editColumn('id', '<b>{!! $id !!}</b>')
            ->editColumn('published_at', '@if(is_null($published_at))<i class="fa fa-fw fa-eye-slash text-muted"></i> Draft @elseif(Carbon::now()->gte($published_at))<i class="fa fa-fw fa-eye text-success" ></i> {!! $published_at !!}@else<i class="fa fa-fw fa-clock-o text-info" ></i> {!! $published_at !!}@endif')
            ->editColumn('variants_count', '{!! $variants_count !!}')
            ->editColumn('features_count', '{!! $features_count !!}')
            ->editColumn('categories', '@forelse($categories as $category)
            <span class="label label-info">{{ $category->name }}</span>
                @empty
                    <span class="label label-default">None</span>
                @endforelse
            ');

        $this->addControlsColumn($dataTable, $this->routeName, 'product');

        return $dataTable->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return Response
     */
    public function show($slug)
    {
        $product = Product::withDrafts()
            ->withTrashed()
            ->select('products.id', 'products.name', 'products.description', 'products.specification', 'products.deleted_at', 'products.published_at')
            ->with([
                'images' => function ($query) {
                    $query->select(['images.id', 'images.imageable_id', 'images.imageable_type', 'images.title', 'images.type', 'images.path', 'images.width', 'images.height', 'images.position'])
                        ->orderBy('images.position', 'ASC');
                },
                'variants' => function ($query) {
                    $query->select(['variants.id', 'variants.model', 'variants.specification', 'variants.position', 'variants.product_id'])
                        ->orderBy('variants.position', 'ASC');
                },
                'features' => function ($query) {
                    $query->select(['features.id', 'features.name', 'features.description', 'features.deleted_at', 'product_features.feature_position'])
                        ->orderBy('product_features.feature_position', 'ASC');
                },
                'features.icon' => function ($query) {
                    $query->select(['images.id', 'images.imageable_id', 'images.imageable_type', 'images.title', 'images.type', 'images.path', 'images.width', 'images.height']);
                },
            ])
            ->where('slug', '=', $slug)
            ->first();

        if (is_null($product)) {
            App::abort(404);
        } else if (!Cache::tags('product')->has($slug)) {
            $cacheDuration = Config::get('okuma.cache_duration');
        }

        Cache::rememberForever('okuma.default_images.feature_icon', function () {
            return Config::get('okuma.default_images.feature_icon');
        });

        $view = View::make('pages.product')->with('product', $product);

        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $view = View::make('dashboard.product.create');
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ProductRequest $request)
    {

        $product = new Product();

        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->snippet = $request->input('snippet');

        if ($request->has('published_at') && $request->input('published_at') !== "") {
            $product->published_at = $request->input('published_at', null);
        } else {
            $product->published_at = null;
        }

        $product->save();

        return redirect()->route('dashboard.product.edit', ['product' => $product->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $product = null;

        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $view = View::make('dashboard.product.edit.product')->with('product', $product);

        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(ProductRequest $request, $id)
    {
        $product = null;

        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        if ($request->has('name')) {

            $product->name = $request->input('name');
            $product->description = $request->input('description');
            $product->snippet = $request->input('snippet');

            if ($request->has('published_at') && $request->input('published_at') !== "") {
                $product->published_at = $request->input('published_at', null);
            } else {
                $product->published_at = null;
            }

            $slug = $product->assignSlug($request->input('slug'));

            if ($slug != null) {
                $product->slug = $slug;
                // $product->save();
            } else {
                $product->sluggify(true);
            }

            $product->save();

            return redirect()->route('dashboard.product.edit', ['product' => $id]);
        }

        if ($request->has('specification')) {
            $product->specification = json_encode($request->input('specification'), true);
            $product->save();

            return redirect()->route('dashboard.product.variant.index', ['product' => $id]);
        }

        if ($request->has('categories')) {
            $product->categories()->sync($request->input('categories'));

            $product->touch();
            $product->categories()->touch();

            return redirect()->route('dashboard.product.category.index', ['product' => $id]);
        }

        if ($request->has('features')) {
            $orderableList = json_decode($request->input('features'));

            $syncFeatures = [];

            foreach ($orderableList->orderables as $entry) {
                $syncFeatures[$entry->id] = ['feature_position' => $entry->position];
            }

            $updatedFeatures = $product->features()->withTrashed()->sync($syncFeatures);

            $product->touch();

            return redirect()->route('dashboard.product.feature.index', ['product' => $id]);
        }

        return redirect()->back();
    }

    public function images($id)
    {
        $product = null;

        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->with([
                    'thumbnail' => function ($query) {
                        $query->addSelect('updated_at');
                    },
                    'shareImage' => function ($query) {
                        $query->addSelect('updated_at');
                    },
                    'images' =>
                    function ($query) {
                        $query->where('type', '=', 'product_thumbnail')
                            ->orderby('position', 'ASC');
                    },

                    'images.relatedImages',
                ])
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $view = View::make('dashboard.product.edit.images')->with(['product' => $product]);
        return $view;
    }

    public function features($id)
    {
        $product = null;

        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->with([
                    'features' => function ($query) {
                        $query->withTrashed()
                            ->orderBy('feature_position', 'ASC');

                    },
                ])
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $list = [];
        $selected = [];

        foreach ($product->features as $feature) {
            $list[$feature->id] = $feature->name;
            array_push($selected, $feature->id);
        }
        $view = View::make('dashboard.product.edit.features')->with(['product' => $product, 'list' => $list, 'selected' => $selected]);
        return $view;
    }

    public function categories($id)
    {
        $product = null;

        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->with([
                    'categories' => function ($query) {
                        $query->withTrashed();
                    },
                ])
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $view = View::make('dashboard.product.edit.categories')->with(['product' => $product]);
        return $view;
    }

    public function createDisplayThumbnail($id)
    {
        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $type = 'product_display_thumbnail';
        $dimentions = Config::get("okuma.image_sizes.{$type}");

        $params = [
            'formTitle' => 'Product Display Thumbnail',
            'infoTable' => View::make('dashboard.components.infotable', ['id' => $product->id, 'name' => $product->name])->render(),
            'imageableType' => get_class($product),
            'imageableID' => $id,
            'type' => $type,
            'width' => $dimentions['width'],
            'height' => $dimentions['height'],
        ];

        Session::put('redirect', URL::route('dashboard.product.image.index', ['product' => $id]));

        $view = View::make('dashboard.image.create')->with($params);
        return $view;
    }

    public function createLargeImage($id)
    {
        $product = null;

        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $type = 'product_large';
        $dimentions = Config::get("okuma.image_sizes.{$type}");

        $params = [
            'formTitle' => 'Product Slider Image',
            'infoTable' => View::make('dashboard.components.infotable', ['id' => $product->id, 'name' => $product->name])->render(),
            'imageableType' => get_class($product),
            'imageableID' => $id,
            'type' => $type,
            'width' => $dimentions['width'],
            'height' => $dimentions['height'],
        ];

        Session::put('redirect', URL::route('dashboard.product.image.index', ['product' => $id]));

        $view = View::make('dashboard.image.create')->with($params);
        return $view;
    }

    public function createShareImage($id)
    {
        $product = null;

        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $type = 'share_image';
        $dimentions = Config::get("okuma.image_sizes.{$type}");

        $params = [
            'formTitle' => 'Product Share Image',
            'infoTable' => View::make('dashboard.components.infotable', ['id' => $product->id, 'name' => $product->name])->render(),
            'imageableType' => get_class($product),
            'imageableID' => $id,
            'type' => $type,
            'width' => $dimentions['width'],
            'height' => $dimentions['height'],
        ];

        Session::put('redirect', URL::route('dashboard.product.image.index', ['product' => $id]));

        $view = View::make('dashboard.image.create')->with($params);
        return $view;
    }

    public function togglePublish($id)
    {
        $product = null;

        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return $this->notFoundResponse($id);
        }

        if ($product->isPublished()) {
            $product->unpublish();
            return response()->json(['level' => 'success', 'icon' => 'fa fa-check-circle-o', 'title' => 'Success: ', 'message' => " {$this->modelSingular} <em>#{$id}</em> unpublised"], 200);
        } else {
            $product->publish();
            return response()->json(['level' => 'success', 'icon' => 'fa fa-check-circle-o', 'title' => 'Success: ', 'message' => " {$this->modelSingular} <em>#{$id}</em> published"], 200);
        }
    }

    protected function modifyControls(Model $model, array &$buttons, $routeName, array $routeParams)
    {
        array_unshift($buttons, View::make('dashboard.components.buttons.view', ['url' => URL::route($routeName . '.show', [$model->slug])])->render());

        if ($model->isPublished()) {
            array_splice($buttons, 1, 0, View::make('dashboard.components.buttons.unpublish', ['url' => URL::route($routeName . '.togglepublish', $routeParams)])->render());
        } else {
            array_splice($buttons, 1, 0, View::make('dashboard.components.buttons.publish', ['url' => URL::route($routeName . '.togglepublish', $routeParams)])->render());
        }
    }

    protected function removeQueryScopes($query)
    {
        return $query->withDrafts()
            ->withTrashed();
    }

    protected function searchQueryScopes($query)
    {
        // return $query->with('thumbnail');

        return $query->with(['thumbnail' =>
            function ($query) {
                // $query->select('id', 'path', 'type','imageable_id', 'imageable_type')->get('path');
                $query->select('id', 'path', 'type','imageable_id', 'imageable_type');//->pluck('path');
            },
        ]);
    }
}
