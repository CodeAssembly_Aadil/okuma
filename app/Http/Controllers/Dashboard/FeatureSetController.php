<?php namespace Okuma\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Okuma\Http\Requests\Dashboard\FeatureSetRequest;
use Okuma\Http\Requests\Dashboard\ReorderRequest;
use Okuma\Models\Feature;
use Okuma\Models\FeatureSet;
use Yajra\Datatables\Datatables;

class FeatureSetController extends ResourceController
{

    protected $modelClass = FeatureSet::class;
    protected $modelSingular = 'Feature Set';
    protected $modelPlural = 'Feature Sets';
    protected $routeName = 'dashboard.featureset';
    protected $searchSelect = ['id', 'name', 'deleted_at'];

    public function index()
    {
        $dataTable = $this->htmlBuilder
            ->parameters(['pageLength' => 15, "lengthMenu" => [15, 30, 45, 60, 90], 'stateSave' => true])
            ->ajax(['url' => URL::route($this->routeName . '.datatable'), 'type' => 'GET'])
            ->addColumn(['data' => 'id', 'name' => 'id', 'title' => '#'])
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Name'])
            ->addColumn(['data' => 'slug', 'name' => 'slug', 'title' => 'Slug', 'defaultContent' => '', 'searchable' => false])
            ->addColumn(['data' => 'position', 'name' => 'position', 'title' => 'Position', 'searchable' => false])
            ->addColumn(['data' => 'features_count.value', 'name' => 'features_count', 'title' => 'Total Features', 'defaultContent' => 0, 'searchable' => false, 'orderable' => false])
            ->addColumn(['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At', 'defaultContent' => '', 'searchable' => false])
            ->addAction();

        $displayVariables = [
            'pageTitle' => $this->modelPlural,
            'pageHeader' => $this->modelPlural,
            'baseRoute' => $this->routeName,
            'modelName' => $this->modelSingular,
            'tabs' => 'dashboard.featureset.index.tabs',
        ];

        $view = View::make('dashboard.common.tab.datatable')->with($displayVariables)->with('dataTable', $dataTable);

        return $view;
    }

    public function dataTable()
    {

        $featureSets = FeatureSet::select('id', 'name', 'slug', 'position', 'updated_at', 'deleted_at')
            ->withTrashed()
            ->with(['featuresCount' =>
                function ($query) {
                    return $query->withTrashed();
                },
            ]);

        $dataTable = Datatables::of($featureSets)
            ->editColumn('id', '<b>{!! $id !!}</b>');
        $this->addControlsColumn($dataTable, $this->routeName, 'featureset');

        return $dataTable->make(true);
    }

    public function sort()
    {
        $featureSets = FeatureSet::select('id', 'name', 'position', 'deleted_at')
            ->withTrashed()
            ->orderBy('position', 'ASC')
            ->get();

        $displayVariables = [
            'pageTitle' => $this->modelPlural,
            'pageHeader' => $this->modelPlural,
            'baseRoute' => $this->routeName,
            'modelName' => $this->modelSingular,
            'tabs' => 'dashboard.featureset.index.tabs',
        ];

        $view = View::make('dashboard.featureset.index.sort')->with($displayVariables)->with('featureSets', $featureSets);
        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (URL::previous() !== URL::current()) {
            Session::put('redirect', URL::previous());
        }

        $view = View::make('dashboard.featureset.create');
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(FeatureSetRequest $request)
    {
        $featureSet = new FeatureSet();
        $featureSet->name = $request->input('name');
        $featureSet->save();

        if (Session::has('redirect')) {
            return redirect(Session::pull('redirect'));
        }

        return redirect()->route('dashboard.featureset.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if (URL::previous() !== URL::current()) {
            Session::put('redirect', URL::previous());
        }

        $featureSet = null;

        try {
            $featureSet = FeatureSet::withTrashed()
                ->with([
                    'featuresCount' => function ($query) {
                        $query->withTrashed();
                    },
                ])
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $view = View::make('dashboard.featureset.edit.featureset')->with('featureSet', $featureSet);
        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(FeatureSetRequest $request, $id)
    {
        $featureSet = null;

        try {
            $featureSet = FeatureSet::withTrashed()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        if ($request->has('name')) {
            $featureSet->name = $request->input('name');
            $featureSet->save();
        }

        if ($request->has('features')) {
            $orderableList = json_decode($request->input('features'));

            $features = [];

            foreach ($orderableList->orderables as $entry) {
                $features[$entry->id] = ['product_position' => $entry->position];
                $feature = Feature::findOrFail($entry->id);
                $feature->position = $entry->position;
                $feature->featureSet()->associate($featureSet->id);
                $feature->save();
            }

            return redirect()->route('dashboard.featureset.feature.index', ['category' => $id]);
        }

        if (Session::has('redirect')) {
            return redirect(Session::pull('redirect'));
        }

        return redirect()->route('dashboard.featureset.index');
    }

    public function reorder(ReorderRequest $request)
    {
        $orderableList = json_decode($request->input('orderables'));

        $ids = [];
        $positions = [];

        foreach ($orderableList->orderables as $entry) {
            array_push($ids, $entry->id);
            $positions[$entry->id] = $entry->position;
        }

        $featureSets = FeatureSet::whereIn('id', $ids)->get();

        if (count($featureSets) != count($ids)) {
            return redirect()->back();
        }

        foreach ($featureSets as $featureSet) {
            $featureSet->position = $positions[$featureSet->id];
            $featureSet->save();
        }

        return redirect()->back();
    }

    public function features($id)
    {
        $featureSet = null;

        try {
            $featureSet = FeatureSet::withTrashed()
                ->with([
                    'features' => function ($query) {
                        $query->withTrashed()
                            ->orderBy('features.position', 'ASC');
                    },
                    'featuresCount' => function ($query) {
                        $query->withTrashed();
                    },
                ])
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $view = View::make('dashboard.featureset.edit.features')->with('featureSet', $featureSet);
        return $view;
    }

    public function reorderFeatures(ReorderRequest $request, $id)
    {
        $featureSet = null;

        try {
            $featureSet = FeatureSet::withTrashed()
                ->with([
                    'features' => function ($query) {
                        $query->withTrashed();
                    },
                ])
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $orderableList = json_decode($request->input('orderables'));

        $ids = [];
        $positions = [];

        foreach ($orderableList->orderables as $entry) {
            array_push($ids, $entry->id);
            $positions[$entry->id] = $entry->position;
        }

        $featureIDs = $featureSet->features
            ->lists('id')
            ->toArray();

        $ids = array_merge($ids, $featureIDs);

        $features = Feature::withTrashed()
            ->whereIn('id', $ids)
            ->get();

        foreach ($features as $feature) {
            if (isset($positions[$feature->id])) {
                $feature->position = $positions[$feature->id];
                $feature->feature_set_id = $featureSet->id;

            } else {
                $feature->position = 0;
                $feature->feature_set_id = null;
            }

            $feature->save();
        }

        return redirect()->back();
    }

    protected function removeQueryScopes($query)
    {
        return $query->withTrashed();
    }

}
