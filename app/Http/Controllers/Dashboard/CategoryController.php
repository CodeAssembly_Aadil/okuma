<?php namespace Okuma\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Okuma\Http\Requests\Dashboard\CategoryRequest;
use Okuma\Http\Requests\Dashboard\ReorderTreeRequest;
use Okuma\Models\Category;
use Okuma\Models\TreeCategory;
use Yajra\Datatables\Datatables;

class CategoryController extends ResourceController
{
    protected $modelClass = Category::class;
    protected $modelSingular = 'Category';
    protected $modelPlural = 'Categories';
    protected $routeName = 'dashboard.category';
    protected $searchSelect = ['id', 'name', 'deleted_at'];

    public function index()
    {
        $dataTable = $this->htmlBuilder
            ->parameters(['pageLength' => 15, "lengthMenu" => [15, 30, 45, 60, 90], 'stateSave' => true])
            ->ajax(['url' => URL::route($this->routeName . '.datatable'), 'type' => 'GET'])
            ->addColumn(['data' => 'id', 'name' => 'id', 'title' => '#'])
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Name'])
            ->addColumn(['data' => 'description', 'name' => 'description', 'title' => 'Description', 'defaultContent' => '', 'searchable' => false, 'orderable' => false])
            ->addColumn(['data' => 'slug', 'name' => 'slug', 'title' => 'Slug', 'searchable' => false])
            ->addColumn(['data' => 'products_count', 'name' => 'products_count', 'title' => 'Total Products', 'defaultContent' => '0', 'searchable' => false, 'orderable' => false])
            ->addColumn(['data' => 'direct_ancestor.name', 'name' => 'direct_ancestor', 'title' => 'Parent', 'defaultContent' => '<i class="fa fa-fw fa-sitemap"></i> Root', 'searchable' => false, 'orderable' => false])
            ->addColumn(['data' => 'real_depth', 'name' => 'real_depth', 'title' => 'Depth'])
            ->addAction();

        $displayVariables = [
            'pageTitle' => $this->modelPlural,
            'pageHeader' => $this->modelPlural,
            'baseRoute' => $this->routeName,
            'modelName' => $this->modelSingular,
            'tabs' => 'dashboard.category.index.tabs',
        ];

        $view = View::make('dashboard.common.tab.datatable')->with($displayVariables)->with('dataTable', $dataTable);

        return $view;
    }

    public function dataTable()
    {
        $categories = Category::select('categories.id', 'categories.name', 'categories.description', 'categories.slug', 'categories.parent_id', 'categories.real_depth', 'categories.updated_at', 'categories.deleted_at')
            ->withTrashed()
            ->with([
                'directAncestor' => function ($query) {
                    $query->withTrashed();
                },
                'productsCount' => function ($query) {
                    $query->withDrafts()
                        ->withTrashed();
                },
            ]);

        $dataTable = Datatables::of($categories)
            ->editColumn('id', '<b>{!! $id !!}</b>')
            ->editColumn('description', '{!! str_limit($description, 60) !!}')
            ->editColumn('products_count', '{!! $products_count !!}');

        $this->addControlsColumn($dataTable, $this->routeName, 'category');

        return $dataTable->make(true);
    }

    public function tree()
    {
        $categories = Category::getTree();

        $displayVariables = [
            'pageTitle' => $this->modelPlural,
            'pageHeader' => $this->modelPlural,
            'baseRoute' => $this->routeName,
            'modelName' => $this->modelSingular,
            'tabs' => 'dashboard.category.index.tabs',
        ];

        $view = View::make('dashboard.category.index.tree')->with($displayVariables)->with('tree', $categories);

        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (URL::previous() !== URL::current()) {
            Session::put('redirect', URL::previous());
        }

        $view = View::make('dashboard.category.create');
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CategoryRequest $request)
    {
        Session::put('previousAction', 'category.store');

        $category = new TreeCategory();

        $category->name = $request->input('name');
        $category->description = $request->input('description');
        $category->snippet = $request->input('snippet');

        $category->save();

        return redirect()->route('dashboard.category.edit', ['category' => $category->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $category = null;

        try {
            $category = Category::withTrashed()
                ->with([
                    'heroBanner',
                    'directAncestor' => function ($query) {
                        $query->withTrashed();
                    },
                    'productsCount' => function ($query) {
                        $query->withDrafts()
                            ->withTrashed();
                    },
                ])
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        if (URL::previous() !== URL::current() && Session::pull('previousAction') !== 'category.store') {
            Session::put('redirect', URL::previous());
        }

        $view = View::make('dashboard.category.edit.category')->with('category', $category);
        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $category = null;

        try {
            $category = Category::withTrashed()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        if ($request->has('name')) {
            $category->name = $request->input('name');
            $category->description = $request->input('description');
            $category->snippet = $request->input('snippet');
            $category->save();

            return redirect()->route('dashboard.category.edit', ['category' => $id]);
        }

        if ($request->has('products')) {

            $orderableList = json_decode($request->input('products'));

            $syncProducts = [];

            foreach ($orderableList->orderables as $entry) {
                $syncProducts[$entry->id] = ['product_position' => $entry->position];
            }

            $category->products()->sync($syncProducts);

            return redirect()->route('dashboard.category.product.index', ['category' => $id]);
        }

        return redirect()->back();
    }

    /**
     * Update the entire tree in storage.
     *
     * @param  ReorderTreeRequest $request
     * @return Response
     */
    public function reorder(ReorderTreeRequest $request)
    {
        $tree = json_decode($request->input('tree'))->tree;

        foreach ($tree as $node) {

            $id = $node->id;
            $position = $node->position;
            $parentID = $node->parent_id === 0 ? null : $node->parent_id;

            $category = TreeCategory::find($id);

            if ($category->parent_id !== $parentID) {
                $category->moveTo($position, $parentID);
            } else if ($category->position !== $position) {
                $category->position = $position;
                $category->save();
            }
        }

        return redirect()->back();
    }

    public function products($id)
    {
        $category = null;

        try {
            $category = Category::withTrashed()
                ->with([
                    'products' => function ($query) {
                        $query->withTrashed()
                            ->withDrafts()
                            ->orderBy('product_position', 'ASC');
                    },
                    'products.thumbnail',
                    'productsCount' => function ($query) {
                        $query->withDrafts()
                            ->withTrashed();
                    },
                ])
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        // $default_product_display_thumbnail = Cache::tags(['site', 'defaults'])->rememberForever('default_product_display_thumbnail', function () {
        //     return (object) array(
        //         'path' => Config::get('okuma.default_images.product_display_thumbnail'),
        //         'width' => Config::get('okuma.image_sizes.product_display_thumbnail.width'),
        //         'height' => Config::get('okuma.image_sizes.product_display_thumbnail.height'),
        //     );
        // });

        // $view = View::make('dashboard.category.edit.products')->with(['category' => $category, 'default_product_display_thumbnail' => $default_product_display_thumbnail]);
        $view = View::make('dashboard.category.edit.products')->with('category', $category);
        return $view;
    }

    public function createHeroBanner($id)
    {
        $category = null;

        try {
            $category = Category::withTrashed()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $type = 'category_hero';
        $dimentions = Config::get("okuma.image_sizes.{$type}");

        $params = [
            'formTitle' => 'Category Hero Banner',
            'infoTable' => View::make('dashboard.components.infotable', ['id' => $category->id, 'name' => $category->name])->render(),
            'imageableType' => get_class($category), //'Feature',
            'imageableID' => $id,
            'type' => $type,
            'width' => $dimentions['width'],
            'height' => $dimentions['height'],
        ];

        Session::put('redirect', URL::route('dashboard.category.edit', ['category' => $id]));

        $view = View::make('dashboard.image.create')->with($params);
        return $view;
    }

    protected function removeQueryScopes($query)
    {
        return $query->withTrashed();
    }
}
