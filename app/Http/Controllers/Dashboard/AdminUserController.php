<?php namespace Okuma\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;
use Okuma\Http\Requests\DashboardRequest;

class AdminUserController extends Controller
{

    public function index()
    {
        $view = View::make('dashboard.adminuser.index');
        return $view;
    }

    public function updatePassword(DashboardRequest $request)
    {

    }

}
