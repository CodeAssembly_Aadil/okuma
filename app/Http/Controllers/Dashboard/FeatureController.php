<?php namespace Okuma\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Okuma\Http\Requests\Dashboard\FeatureRequest;
use Okuma\Http\Requests\Dashboard\ReorderRequest;
use Okuma\Models\Feature;
use Yajra\Datatables\Datatables;

class FeatureController extends ResourceController
{

    protected $modelClass = Feature::class;
    protected $modelSingular = 'Feature';
    protected $modelPlural = 'Features';
    protected $routeName = 'dashboard.feature';
    protected $searchSelect = ['id', 'name', 'deleted_at'];

    public function index()
    {
        $dataTable = $this->htmlBuilder
            ->parameters(['pageLength' => 15, "lengthMenu" => [15, 30, 45, 60, 90], 'stateSave' => true])
            ->ajax(['url' => URL::route($this->routeName . '.datatable'), 'type' => 'GET'])
            ->addColumn(['data' => 'id', 'name' => 'id', 'title' => '#'])
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Name'])
            ->addColumn(['data' => 'description', 'name' => 'description', 'title' => 'Description', 'defaultContent' => '', 'orderable' => false, 'searchable' => false])
            ->addColumn(['data' => 'slug', 'name' => 'slug', 'title' => 'Slug', 'defaultContent' => '', 'searchable' => false])
            ->addColumn(['data' => 'feature_set.name', 'name' => 'feature_set', 'title' => 'Feature Set', 'searchable' => false, 'defaultContent' => ''])
            ->addColumn(['data' => 'products_count', 'name' => 'products_count', 'title' => 'Total Products', 'searchable' => false, 'defaultContent' => '0'])
            ->addColumn(['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At', 'defaultContent' => '', 'searchable' => false])
            ->addAction();

        //addColumn(['data' => 'action', 'name' => 'actions', 'title' => 'Actions', 'defaultContent' => '', 'orderable' => false, 'searchable' => false]);

        $displayVariables = [
            'pageTitle' => $this->modelPlural,
            'pageHeader' => $this->modelPlural,
            'baseRoute' => $this->routeName,
            'modelName' => $this->modelSingular,
        ];

        $view = View::make('dashboard.common.datatable')->with($displayVariables)->with('dataTable', $dataTable);

        return $view;
    }

    public function dataTable()
    {
        $features = Feature::select('features.id', 'features.name', 'features.description', 'features.slug', 'features.updated_at', 'features.deleted_at', 'features.feature_set_id')
            ->withTrashed()
            ->with([
                'featureSet' => function ($query) {
                    $query->withTrashed();
                },
                'productsCount' => function ($query) {
                    $query->withDrafts()
                        ->withTrashed();
                },
            ]);
        $dataTable = Datatables::of($features)
            ->editColumn('id', '<b>{!! $id !!}</b>')
            ->editColumn('description', '{!! str_limit($description, 30) !!}')
            ->editColumn('products_count', '{!! $products_count !!}');

        $this->addControlsColumn($dataTable, $this->routeName, 'feature');

        return $dataTable->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $view = View::make('dashboard.feature.create');
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(FeatureRequest $request)
    {
        $feature = new Feature();

        $feature->name = $request->input('name');
        $feature->description = $request->input('description');
        $feature->feature_set_id = $request->input('feature_set_id');

        $feature->save();

        return redirect()->route('dashboard.feature.edit', ['feature' => $feature->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $feature = null;

        try {
            $feature = Feature::withTrashed()
                ->with('icon')
                ->with([
                    'featureSet' => function ($query) {
                        $query->withTrashed();
                    },
                ])
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $view = View::make('dashboard.feature.edit')->with('feature', $feature);
        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(FeatureRequest $request, $id)
    {
        $feature = null;

        try {
            $feature = Feature::withTrashed()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $feature->name = $request->input('name');
        $feature->description = $request->input('description');
        $feature->feature_set_id = $request->input('feature_set_id');

        $feature->save();

        return redirect()->route('dashboard.feature.edit', ['feature' => $feature->id]);
    }

    public function createIcon($id)
    {
        $feature = null;

        try {
            $feature = Feature::withTrashed()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $type = 'feature_icon';

        $dimentions = Config::get("okuma.image_sizes.{$type}");

        $params = [
            'formTitle' => 'Feature Icon',
            'infoTable' => View::make('dashboard.components.infotable', ['id' => $feature->id, 'name' => $feature->name])->render(),
            'imageableType' => get_class($feature),
            'imageableID' => $id,
            'type' => $type,
            'width' => $dimentions['width'],
            'height' => $dimentions['height'],
        ];

        Session::put('redirect', URL::route('dashboard.feature.edit', ['feature' => $id]));

        $view = View::make('dashboard.image.create')->with($params);
        return $view;
    }

    public function reorder(ReorderRequest $request)
    {
        $orderableList = json_decode($request->input('orderables'));

        $ids = [];
        $positions = [];

        foreach ($orderableList->orderables as $entry) {
            array_push($ids, $entry->id);
            $positions[$entry->id] = $entry->position;
        }

        $features = Feature::whereIn('id', $ids)->get();

        if (count($features) != count($ids)) {
            return redirect()->back();
        }

        foreach ($features as $feature) {
            $feature->position = $positions[$feature->id];
            $feature->save();
        }

        return redirect()->back();
    }

    protected function removeQueryScopes($query)
    {
        return $query->withTrashed();
    }
}
