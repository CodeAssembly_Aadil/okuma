<?php namespace Okuma\Http\Controllers\Dashboard;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;
use Okuma\Http\Requests\Dashboard\ReorderRequest;
use Okuma\Http\Requests\Dashboard\VariantRequest;
use Okuma\Models\Product;
use Okuma\Models\Variant;

class VariantController extends Controller
{

    protected $modelClass = Variant::class;
    protected $modelSingular = 'Variant';
    protected $modelPlural = 'Variants';
    protected $routeName = 'dashboard.product.variant';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($productID)
    {
        $product = null;

        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->with(['variants' =>
                    function ($query) {
                        $query->select('id', 'model', 'sku', 'specification', 'position', 'updated_at', 'deleted_at', 'product_id')
                        ->withTrashed()
                        ->orderBy('position');
                    }])
                ->findOrFail($productID);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $view = View::make('dashboard.product.edit.variants')->with('product', $product);

        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($productID)
    {
        $product = null;

        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->findOrFail($productID);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $view = View::make('dashboard.product.variant.create')->with(['product' => $product]);
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(VariantRequest $request, $productID)
    {
        $product = null;

        try {
            $product = Product::withTrashed()
                ->withDrafts()
                ->findOrFail($productID);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $variantSpec = [];

        $productSpec = json_decode($product->specification, true);

        if (!empty($productSpec)) {
            foreach ($productSpec as $property) {
                $name = str_slug('spec-' . $property);

                $value = $request->input($name, '');

                $variantSpec[$property] = $value;
            }
        }

        $variant = new Variant();
        $variant->model = $request->input('model');
        $variant->sku = $request->input('sku');
        $variant->specification = json_encode($variantSpec);

        // $variant->model = $request->input('model');
        // $variant->sku = $request->input('sku');
        // $variant->product_id = $product->id;

        $product->variants()->save($variant);

        return redirect()->route('dashboard.product.variant.index', ['product' => $product->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($productID, $id)
    {
        $product = null;
        $variant = null;

        try {

            $product = Product::withTrashed()
                ->withDrafts()
                ->findOrFail($productID);

            $variant = Variant::withTrashed()
                ->findOrFail($id);

        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $view = View::make('dashboard.product.variant.edit')->with(['product' => $product, 'variant' => $variant]);

        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(VariantRequest $request, $productID, $id)
    {
        $product = null;
        $variant = null;

        try {

            $product = Product::withTrashed()
                ->withDrafts()
                ->findOrFail($productID);

            $variant = Variant::withTrashed()
                ->findOrFail($id);

        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $variantSpec = [];

        $productSpec = json_decode($product->specification);

        if (!empty($productSpec)) {
            foreach ($productSpec as $property) {
                $name = str_slug('spec-' . $property);

                $value = $request->input($name, '');

                $variantSpec[$property] = $value;
            }
        }

        $variant->model = $request->input('model');
        $variant->sku = $request->input('sku');
        $variant->specification = json_encode($variantSpec);

        $variant->save();

        return redirect()->route('dashboard.product.variant.index', ['product' => $productID]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($productID, $id)
    {
        $variant = null;

        try {
            Product::withTrashed()
                ->withDrafts()
                ->findOrFail($productID);

            $variant = Variant::withTrashed()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return $this->notFoundResponse($id);
        }

        if ($variant->trashed()) {
            $variant->forceDelete();
            return response()->json(['level' => 'info', 'icon' => 'fa fa-fw fa-lg fa-check-circle-o', 'title' => 'Success: ', 'message' => "{$this->modelSingular} <em>#{$id}</em> has been permanently deleted!"], 200);
        } else {
            $variant->delete();
            return response()->json(['level' => 'info', 'icon' => 'fa fa-fw fa-lg fa-check-circle-o', 'title' => 'Success: ', 'message' => "{$this->modelSingular} <em>#{$id}</em> trashed"], 200);
        }
    }

    /**
     * Restore the specified resource to storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function restore($productID, $id)
    {
        $variant = null;

        try {
            Product::withTrashed()
                ->withDrafts()
                ->findOrFail($productID);
            $variant = Variant::withTrashed()->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return $this->notFoundResponse($id);
        }

        $variant->restore();

        return response()->json(['level' => 'success', 'icon' => 'fa fa-fw fa-lg fa-check-circle-o', 'title' => 'Success: ', 'message' => " {$this->modelSingular} <em>#{$id}</em> restored"], 200);
    }

    public function reorder(ReorderRequest $request)
    {
        $orderableList = json_decode($request->input('orderables'));

        $ids = [];
        $positions = [];

        foreach ($orderableList->orderables as $entry) {
            array_push($ids, $entry->id);
            $positions[$entry->id] = $entry->position;
        }

        $variants = Variant::whereIn('id', $ids)->get();

        if (count($variants) != count($ids)) {
            return redirect()->back();
        }

        foreach ($variants as $variant) {
            $variant->position = $positions[$variant->id];
            $variant->save();
        }

        return redirect()->back();
    }

    protected function notFoundResponse($id)
    {
        return response()->json(['level' => 'warning', 'icon' => 'fa fa-fw fa-lg fa-exclamation-triangle', 'title' => 'Warning: ', 'message' => "{$this->modelSingular} <em>#{$id}</em> could not be found!"], 404);
    }
}
