<?php namespace Okuma\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function index()
    {
        $view = View::make('dashboard.master');
        return $view;
    }

    public function phpinfo()
    {
        $view = View::make('dashboard.pages.phpinfo');
        return $view;
    }

}
