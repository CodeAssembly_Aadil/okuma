<?php namespace Okuma\Http\Controllers\Dashboard;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;
use Okuma\Http\Requests\DashboardRequest;
use Yajra\Datatables\Html\Builder;

class ResourceController extends Controller
{

    protected $htmlBuilder;
    protected $modelClass;
    protected $modelSingular;
    protected $modelPlural;
    protected $routeName;
    protected $searchSelect = ['*'];

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }

    protected function removeQueryScopes($query)
    {
        return $query;
    }

    protected function searchQueryScopes($query)
    {
        return $query;
    }

    /**
     * A basic model search
     *
     * @param  Request  $request
     * @return Response
     */
    public function search(DashboardRequest $request)
    {
        $query = $request->input('query', '*');
        $page = $request->input('page', '0');

        $modelClass = $this->modelClass;

        $searchQuery = $modelClass::select($this->searchSelect);

        $searchQuery = $this->removeQueryScopes($searchQuery);
        $searchQuery = $this->searchQueryScopes($searchQuery);

        if ($query != '*') {
            if (intval($query)) {
                $searchQuery->where('id', 'LIKE', "{$query}%")
                    ->orderBy('id', 'ASC');
            } else {
                $searchQuery->search($query);
            }
        }

        $searchResult = $searchQuery
            ->simplePaginate(10);

        return $searchResult->toJson();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $model = null;

        try {

            $modelClass = $this->modelClass;
            $modelQuery = $modelClass::query();
            $modelQuery = $this->removeQueryScopes($modelQuery);
            $model = $modelQuery->findOrFail($id);

        } catch (ModelNotFoundException $exception) {
            return $this->notFoundResponse($id);
        }

        if ($model->trashed()) {
            $this->onDestroy($model);
            $model->forceDelete();
            return response()->json(['level' => 'info', 'icon' => 'fa fa-fw fa-lg fa-check-circle-o', 'title' => 'Success: ', 'message' => "{$this->modelSingular} <em>#{$id}</em> has been permanently deleted!"], 200);
        } else {
            $this->onTrash($model);
            $model->delete();
            return response()->json(['level' => 'info', 'icon' => 'fa fa-fw fa-lg fa-check-circle-o', 'title' => 'Success: ', 'message' => "{$this->modelSingular} <em>#{$id}</em> trashed"], 200);
        }
    }

    /**
     * Restore the specified resource to storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function restore($id)
    {
        $model = null;

        try {

            $modelClass = $this->modelClass;
            $modelQuery = $modelClass::query();
            $modelQuery = $this->removeQueryScopes($modelQuery);
            $model = $modelQuery->findOrFail($id);

        } catch (ModelNotFoundException $exception) {
            return $this->notFoundResponse($id);
        }

        // return $model->toJson();
        $this->onRestore($model);
        $model->restore();

        return response()->json(['level' => 'success', 'icon' => 'fa fa-fw fa-lg fa-check-circle-o', 'title' => 'Success: ', 'message' => " {$this->modelSingular} <em>#{$id}</em> restored"], 200);

    }

    protected function onTrash(Model $model)
    {

    }

    protected function onDestroy(Model $model)
    {

    }

    protected function onRestore(Model $model)
    {

    }

    protected function addControlsColumn($dataTable, $routeName, $routeParam, $routeParams = [])
    {

        $routeParams[$routeParam] = 0;

        $dataTable->addColumn('action',
            function ($model) use ($routeName, $routeParam, $routeParams) {

                $routeParams[$routeParam] = $model->id;

                $buttons = [];

                array_push($buttons, View::make('dashboard.components.buttons.edit', ['url' => URL::route($routeName . '.edit', $routeParams)])->render());

                if ($model->trashed()) {
                    array_push($buttons, View::make('dashboard.components.buttons.destroy', ['url' => URL::route($routeName . '.destroy', $routeParams)])->render());
                    array_push($buttons, View::make('dashboard.components.buttons.restore', ['url' => URL::route($routeName . '.restore', $routeParams)])->render());

                } else {
                    array_push($buttons, View::make('dashboard.components.buttons.trash', ['url' => URL::route($routeName . '.destroy', $routeParams)])->render());
                }

                $this->modifyControls($model, $buttons, $routeName, $routeParams);

                return View::make('dashboard.components.model-actions', ['buttons' => $buttons])->render();

            }, false
        );

        return $dataTable;
    }

    protected function modifyControls(Model $model, array &$buttons, $routeName, array $routeParams)
    {

    }

    protected function notFoundResponse($id)
    {
        return response()->json(['level' => 'warning', 'icon' => 'fa fa-fw fa-lg fa-exclamation-triangle', 'title' => 'Warning: ', 'message' => "{$this->modelSingular} <em>#{$id}</em> could not be found!"], 404);
    }
}
