<?php namespace Okuma\Http\Controllers\Dashboard;

use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;

class BackupController extends Controller
{

    public function index()
    {
        $directory = Config::get('laravel-backup.backup.name');

        $disk = Storage::disk('localbackup');

        $files = $disk->files($directory);
        $files = array_reverse($files);

        $backups = [];

        $now = Carbon::now();

        foreach ($files as $file) {

            if (ends_with($file, '.zip')) {

                $filename = basename($file, '.zip');

                $date = Carbon::createFromFormat('Y-m-d-His', $filename);

                $backup = [
                    'filename' => $filename,
                    'age' => $date->diffForHumans($now, true),
                    'size' => $this->formatBytes($disk->size($file)),
                    'fullpath' => basename($file),
                ];

                array_push($backups, $backup);
            }
        }

        $view = View::make('dashboard.backup.index')->with(['backups' => $backups]);

        return $view;
    }

    protected function formatBytes($bytes, $precision = 2)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        // $bytes /= pow(1024, $pow);
        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    public function destroy($file)
    {
        $directory = Config::get('laravel-backup.backup.name');

        if (Storage::disk('localbackup')->exists($directory . '/' . $file)) {
            Storage::disk('localbackup')->delete($directory . '/' . $file);
        }

        return Response::json(['level' => 'success', 'icon' => 'fa fa-fw fa-lg fa-check-circle-o', 'title' => 'Success: ', 'message' => "<em>Backup</em> destroyed."], 200);
    }

    public function create()
    {
        $exitCode = Artisan::call('backup:run');

        Session::flash('status', 'Backup started. Please be patient.');

        return redirect()->route('dashboard.backup.index');
    }

    public function clean()
    {
        $exitCode = Artisan::call('backup:clean');

        Session::flash('status', 'Backups queued for cleaning.');

        return redirect()->route('dashboard.backup.index');
    }

    public function download($file)
    {
        $directory = Config::get('laravel-backup.backup.name');

        $disk = Storage::disk('localbackup');

        if ($disk->exists($directory . '/' . $file)) {

            $path = $disk->getDriver()->getAdapter()->getPathPrefix();

            $size = $disk->size($directory . '/' . $file);

            return Response::download($path . $directory . '/' . $file, $file, ['Content-Length: ' . $size]);
        }

        abort(404);
    }
}
