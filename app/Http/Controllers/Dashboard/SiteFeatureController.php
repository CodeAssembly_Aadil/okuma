<?php namespace Okuma\Http\Controllers\Dashboard;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Okuma\Http\Requests\Dashboard\ReorderRequest;
use Okuma\Http\Requests\Dashboard\SiteFeatureRequest;
use Okuma\Models\SiteFeature;
use Yajra\Datatables\Datatables;

class SiteFeatureController extends ResourceController
{

    protected $modelClass = SiteFeature::class;
    protected $modelSingular = 'Site Feature';
    protected $modelPlural = 'Site Features';
    protected $routeName = 'dashboard.sitefeature';

    protected $columnSizes = [
        'site-hero' => 'Site Hero',
        'large-4' => 'One Third Width (4/12 Columns)',
        'large-6' => 'Half Width (6/12 Columns)',
        'large-8' => 'Two Thirds Width (8/12 Columns)',
        'large-12' => 'Full Width (12/12 Columns)',
    ];

    protected $displayClasses = [
        '' => 'None',
        'dark' => 'Dark Title',
    ];

    public function index()
    {
        $dataTable = $this->htmlBuilder
            ->parameters(['pageLength' => 15, "lengthMenu" => [15, 30, 45, 60, 90], 'stateSave' => true])
            ->ajax(['url' => URL::route($this->routeName . '.datatable'), 'type' => 'GET'])
            ->addColumn(['data' => 'id', 'name' => 'id', 'title' => '#'])
            ->addColumn(['data' => 'title', 'name' => 'title', 'title' => 'Title', 'orderable' => false])
            ->addColumn(['data' => 'body', 'name' => 'Body', 'title' => 'Body', 'defaultContent' => '', 'orderable' => false, 'searchable' => false])
            ->addColumn(['data' => 'link', 'name' => 'link', 'title' => 'Link', 'searchable' => false, 'defaultContent' => ''])
            ->addColumn(['data' => 'column_size', 'name' => 'column_size', 'title' => 'Column Size', 'defaultContent' => '', 'searchable' => false])
            ->addColumn(['data' => 'position', 'name' => 'position', 'title' => 'Position'])
            ->addColumn(['data' => 'published_at', 'name' => 'published_at', 'title' => 'Published At', 'defaultContent' => '<span class="label label-default"> Draft </span>', 'searchable' => false])
            ->addColumn(['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At', 'defaultContent' => '', 'searchable' => false])
            ->addAction();

        //addColumn(['data' => 'action', 'name' => 'actions', 'title' => 'Actions', 'defaultContent' => '', 'orderable' => false, 'searchable' => false]);

        $displayVariables = [
            'pageTitle' => $this->modelPlural,
            'pageHeader' => $this->modelPlural,
            'baseRoute' => $this->routeName,
            'modelName' => $this->modelSingular,
            'tabs' => 'dashboard.sitefeature.index.tabs',
        ];

        $view = View::make('dashboard.common.tab.datatable')->with($displayVariables)->with('dataTable', $dataTable);

        return $view;
    }

    public function dataTable()
    {
        $siteFeatures = SiteFeature::select('id', 'title', 'body', 'link', 'column_size', 'position', 'published_at', 'updated_at', 'deleted_at')
            ->withTrashed()
            ->withDrafts();

        // ->select('id', 'name', 'slug', 'position', 'description', 'created_at', 'updated_at', 'deleted_at', 'feature_set_id'); //->take(5);

        $dataTable = Datatables::of($siteFeatures)
            ->editColumn('id', '<b>{!! $id !!}</b>')
            ->editColumn('title', '{!! str_limit($title, 25) !!}')
            ->editColumn('body', '{!! str_limit($body, 25) !!}')
            ->editColumn('link', '{!! str_limit($link, 25) !!}')
            ->editColumn('published_at', '@if(is_null($published_at))<i class="fa fa-fw fa-eye-slash"></i> DRAFT @elseif(Carbon::now()->gte($published_at))<i class="fa fa-fw fa-eye" ></i> {!! $published_at !!}@else<i class="fa fa-fw fa-clock-o" ></i> {!! $published_at !!}@endif');

        $this->addControlsColumn($dataTable, $this->routeName, 'sitefeature');

        return $dataTable->make(true);
    }

    public function layout()
    {

        $hero = SiteFeature::where('column_size', '=', 'site-hero')
            ->withDrafts()
            ->with('background')
            ->orderBy('position', 'DESC')
            ->first();

        $siteFeatures = SiteFeature::where('column_size', '!=', 'site-hero')
            ->withDrafts()
            ->with('background')
            ->orderBy('position', 'ASC')
            ->get();

        $displayVariables = [
            'pageTitle' => $this->modelPlural,
            'pageHeader' => $this->modelPlural,
            'baseRoute' => $this->routeName,
            'modelName' => $this->modelSingular,
            'tabs' => 'dashboard.sitefeature.index.tabs',
        ];

        $view = View::make('dashboard.sitefeature.index.layout')->with($displayVariables)->with(['hero' => $hero, 'siteFeatures' => $siteFeatures]);
        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (URL::previous() !== URL::current()) {
            Session::put('redirect', URL::previous());
        }

        $view = View::make('dashboard.sitefeature.create')->with('columnSizes', $this->columnSizes)->with('displayClasses', $this->displayClasses);
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(SiteFeatureRequest $request)
    {
        $siteFeature = new SiteFeature();

        $siteFeature->title = $request->input('title', '');
        $siteFeature->body = $request->input('body', '');
        $siteFeature->link = $request->input('link');
        $siteFeature->button_label = $request->input('button_label', 'View More');
        $siteFeature->display_class = $request->input('display_class', '');
        $siteFeature->position = DB::table('site_features')->select('id', 'position')->max('position') + 1;

        if ($request->has('published_at') && $request->input('published_at') !== "") {
            $siteFeature->published_at = $request->input('published_at', null);
        } else {
            $siteFeature->published_at = null;
        }

        $columnSize = $request->input('column_size');

        switch ($columnSize) {
            case 'large-4':
                $siteFeature->column_size = 'small-12 medium-6 large-4';
                break;

            case 'large-6':
                $siteFeature->column_size = 'small-12 medium-6 large-6';
                break;

            case 'large-8':
                $siteFeature->column_size = 'small-12 medium-12 large-8';
                break;

            case 'large-12':
                $siteFeature->column_size = 'small-12 medium-12 large-12';
                break;

            default:
                $siteFeature->column_size = $columnSize;
                break;
        }

        $siteFeature->save();

        return redirect()->route('dashboard.sitefeature.edit', ['siteFeature' => $siteFeature->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        if (URL::previous() !== URL::current() && Session::pull('previousAction') !== 'category.store') {
            Session::put('redirect', URL::previous());
        }

        $siteFeature = null;

        try {
            $siteFeature = SiteFeature::withTrashed()
                ->withDrafts()
                ->with('background')
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $view = View::make('dashboard.sitefeature.edit')->with(['siteFeature' => $siteFeature, 'columnSizes' => $this->columnSizes])->with('displayClasses', $this->displayClasses);

        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(SiteFeatureRequest $request, $id)
    {
        $siteFeature = null;

        try {
            $siteFeature = SiteFeature::withTrashed()
                ->withDrafts()
            // ->with('background')
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $siteFeature->title = $request->input('title', '');
        $siteFeature->body = $request->input('body', '');
        $siteFeature->link = $request->input('link');
        $siteFeature->display_class = $request->input('display_class', '');
        $siteFeature->button_label = $request->input('button_label', 'View More');

        if ($request->has('published_at') && $request->input('published_at') !== "") {
            $siteFeature->published_at = $request->input('published_at', null);
        } else {
            $siteFeature->published_at = null;
        }

        $columnSize = $request->input('column_size');

        switch ($columnSize) {
            case 'large-4':
                $siteFeature->column_size = 'small-12 medium-6 large-4';
                break;

            case 'large-6':
                $siteFeature->column_size = 'small-12 medium-6 large-6';
                break;

            case 'large-8':
                $siteFeature->column_size = 'small-12 medium-12 large-8';
                break;

            case 'large-12':
                $siteFeature->column_size = 'small-12 medium-12 large-12';
                break;

            default:
                $siteFeature->column_size = $columnSize;
                break;
        }

        $siteFeature->save();

        return redirect()->route('dashboard.sitefeature.edit', ['sitefeature' => $siteFeature->id]);
    }

    public function createBackground($id)
    {
        $siteFeature = null;

        try {
            $siteFeature = SiteFeature::withTrashed()
                ->withDrafts()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $type = $siteFeature->getImageType();

        $dimentions = Config::get("okuma.image_sizes.{$type}");
        // dd($dimentions);

        // $type = 'site_feature_bg';

        $params = [
            'formTitle' => 'Site Feature Background',
            'infoTable' => View::make('dashboard.components.infotable', ['id' => $siteFeature->id, 'name' => "<span class=\"truncate\">{$siteFeature->title}</span>"])->render(),
            'imageableType' => get_class($siteFeature),
            'imageableID' => $id,
            'type' => $type,
            'width' => $dimentions['width'],
            'height' => $dimentions['height'],
        ];

        Session::put('redirect', URL::route('dashboard.sitefeature.edit', ['sitefeature' => $id]));

        $view = View::make('dashboard.image.create')->with($params);
        return $view;
    }

    public function editBackground($id)
    {
        $siteFeature = null;

        try {
            $siteFeature = SiteFeature::withTrashed()
                ->withDrafts()
                ->with('background')
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $image = $siteFeature->background;

        if (!isset($image)) {
            abort(404);
        }

        $type = $siteFeature->getImageType();

        $dimentions = Config::get("okuma.image_sizes.{$type}");

        $view = View::make('dashboard.image.edit')->with([
            'image' => $image,
            'type' => $type,
            'width' => $dimentions['width'],
            'height' => $dimentions['height'],
        ]);

        Session::put('redirect', URL::route('dashboard.sitefeature.edit', ['sitefeature' => $id]));

        return $view;
    }

    public function togglePublish($id)
    {
        $siteFeature = null;

        try {
            $siteFeature = SiteFeature::withTrashed()
                ->withDrafts()
                ->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return $this->notFoundResponse($id);
        }

        if ($siteFeature->isPublished()) {
            $siteFeature->unpublish();
            return response()->json(['level' => 'success', 'icon' => 'fa fa-check-circle-o', 'title' => 'Success: ', 'message' => " {$this->modelSingular} <em>#{$id}</em> unpublised"], 200);
        } else {
            $siteFeature->publish();
            return response()->json(['level' => 'success', 'icon' => 'fa fa-check-circle-o', 'title' => 'Success: ', 'message' => " {$this->modelSingular} <em>#{$id}</em> published"], 200);
        }
    }

    /**
     * Update the resouce order in storage.
     *
     * @param  ReorderRequest $request
     * @return Response
     */
    public function reorder(ReorderRequest $request)
    {
        $orderableList = json_decode($request->input('orderables'));

        foreach ($orderableList->orderables as $entry) {

            $siteFeature = SiteFeature::withDrafts()
                ->find($entry->id);

            $siteFeature->position = $entry->position;

            $siteFeature->save();

        }

        return redirect()->back();
    }

    protected function modifyControls(Model $model, array &$buttons, $routeName, array $routeParams)
    {
        if ($model->isPublished()) {
            array_splice($buttons, 1, 0, View::make('dashboard.components.buttons.unpublish', ['url' => URL::route($routeName . '.togglepublish', $routeParams)])->render());
            // array_push($buttons, View::make('dashboard.components.buttons.unpublish', ['url' => URL::route($routeName . '.togglepublish', $routeParams)])->render());
        } else {
            array_splice($buttons, 1, 0, View::make('dashboard.components.buttons.publish', ['url' => URL::route($routeName . '.togglepublish', $routeParams)])->render());
            // array_push($buttons, View::make('dashboard.components.buttons.publish', ['url' => URL::route($routeName . '.togglepublish', $routeParams)])->render());
        }
    }

    protected function removeQueryScopes($query)
    {
        return $query->withDrafts()
            ->withTrashed();
    }

}
