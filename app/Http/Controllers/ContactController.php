<?php namespace Okuma\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Okuma\Http\Requests\ContactRequest;

class ContactController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $view = View::make('pages.contact');
        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function send(ContactRequest $request)
    {

        $name = $request->input('contact-name');
        $email = $request->input('contact-email');
        $subject = $request->input('contact-subject');
        $message = $request->input('contact-message');

        $messageBody = preg_split("/\R/", $message);

        $data = array(
            'title' => 'Message Received',
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
            'messageBody' => $messageBody,
            'timestamp' => Carbon::now()->format('l jS \\of F Y H:i'),
            'responseEmail' => Config::get('okuma.contact.email'),
            'responseName' => Config::get('okuma.contact.name'),
        );

        $queue = Config::get('okuma.queues.email');

        // Send message to sender
        Mail::queueOn($queue, 'emails.contact_notify_sender', $data,
            function ($message) use ($data) {
                $message->from($data['responseEmail'], $data['responseName']);
                $message->to($data['email'], $data['name']);
                $message->replyTo($data['responseEmail'], $data['responseName']);
                $message->subject('Okuma: ' . $data['subject']);
            });

        // Send message to okuma
        Mail::queueOn($queue, 'emails.contact_received', $data,
            function ($message) use ($data) {
                $message->from($data['email'], $data['name']);
                $message->to($data['responseEmail'], $data['responseName']);
                $message->replyTo($data['email'], $data['name']);
                $message->subject('Okuma: ' . $data['subject']);
            });

        if ($request->ajax()) {
            return response()->json([
                'contact-response' => 'sent',
            ], 200);
        }

        Session::flash('contact-response', 'sent');

        return redirect()->route('contact.index');
    }
}
