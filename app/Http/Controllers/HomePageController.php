<?php namespace Okuma\Http\Controllers;

use Okuma\Repositories\SiteFeatureRepository;

class HomePageController extends Controller
{
    protected $siteFeatureRepository;

    public function __construct(SiteFeatureRepository $siteFeatureRepository)
    {
        $this->siteFeatureRepository = $siteFeatureRepository;
    }

    public function index()
    {

        $hero = $this->siteFeatureRepository->getHero();
        $siteFeatures = $this->siteFeatureRepository->getFeatures();

        $view = view('pages.homepage')->with(['hero' => $hero, 'siteFeatures' => $siteFeatures]);

        return $view;
    }
}
