<?php namespace Okuma\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Okuma\Http\Requests\SubscribeRequest;
use Okuma\Http\Requests\UnsubscribeRequest;

class SubscriptionController extends Controller
{

    protected $guzzleClient;
    protected $listID;

    public function __construct()
    {
        $base_uri = Config::get('services.mailchimp.base_uri');
        $username = Config::get('services.mailchimp.username');
        $api_key = Config::get('services.mailchimp.secret');
        $this->listID = Config::get('services.mailchimp.list_id');

        $this->guzzleClient = new Client([
            // hw_DocByAnchor(connection, anchorID)ase URI is used with relative requests
            'base_uri' => $base_uri,
            // You can set any number of default request options.
            'timeout' => 2.0,
            'auth' => [$username, $api_key],
            'http_errors' => false,
        ]);
    }

    public function showSubscribe()
    {
        return View::make('pages.newsletter.subscribe');
    }

    public function showUnsubscribe()
    {
        return View::make('pages.newsletter.unsubscribe');
    }

    public function subscribe(SubscribeRequest $request)
    {

        $email = $request->input('subscription-email');

        $data = array(
            'email_address' => $email,
            'status' => 'pending',

            'merge_fields' => array(
                'FNAME' => $request->input('subscription-firstname'),
                'LNAME' => $request->input('subscription-lastname'),
            ),
        );

        $list_id = $this->listID;

        $response = $this->guzzleClient->post("lists/{$list_id}/members", ['json' => $data]);

        $code = $response->getStatusCode(); // 200
        // $reason = $response->getReasonPhrase(); // OK

        if ($code === 200) {
            return View::make('pages.newsletter.response')->with('status', 'subscribed');
        } else {
            return redirect()->back()->withInput()->withErrors(['action' => '<em>WOOPS!</em> There was an error with your request. Please try again.'], 'subscription');
        }
    }

    public function unsubscribe(UnsubscribeRequest $request)
    {
        $email = $request->input('subscription-email');

        $member_id = md5(strtolower($email));

        $data = array(
            'status' => 'unsubscribed',
        );

        $list_id = $this->listID;
        $response = $this->guzzleClient->patch("lists/{$list_id}/members/{$member_id}", ['json' => $data]);

        $code = $response->getStatusCode(); // 200
        // $reason = $response->getReasonPhrase(); // OK

        if ($code === 200) {
            return View::make('pages.newsletter.response')->with('status', 'unsubscribed');
        } else {
            return redirect()->back()->withInput()->withErrors(['action' => '<em>WOOPS!</em> There was an error with your request. Please try again.'], 'subscription');
        }
    }
}
