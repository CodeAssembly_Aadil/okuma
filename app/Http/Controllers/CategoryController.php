<?php namespace Okuma\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;
use Okuma\Models\Category;
use Okuma\Models\FeatureSet;
use Okuma\Models\Product;
use Okuma\Repositories\CategoryRepository;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function catalog()
    {
        $catalog = $this->categoryRepository->getCatalog();

        if (is_null($catalog)) {
            App::abort(404);
        }

        $view = View::make('pages.catalog')->with('catalog', $catalog);
        return $view;
    }

    public function category(Request $request, $slug)
    {

        $category = Category::select('id', 'name', 'description', 'slug')
            ->where('slug', '=', $slug)
            // ->with(['heroBanner', 'decendants', 'rootParent'])
            ->with([
                'heroBanner' => function ($query) {
                    $query->select('id', 'imageable_id', 'imageable_type', 'path', 'width', 'height');
                },
                'decendants' => function ($query) {
                    $query->select('id');
                }
                , 'rootParent' => function ($query) {
                    $query->select('id', 'name', 'slug');
                },
            ])
            ->first();

        if (is_null($category)) {
            App::abort(404);
        }

        $categoryIDs = $category
            ->decendants
            ->lists('id');

        $products = Product::select('products.id', 'products.name', 'products.slug')
            ->with('thumbnail', 'variantsCount')
            ->join('product_categories', 'product_categories.product_id', '=', 'products.id')
            ->join('categories', 'product_categories.category_id', '=', 'categories.id')
            ->whereNull('categories.deleted_at')
            ->whereIn('categories.id', $categoryIDs);

        $filterBy = $request::all();

        $hasFilters = false;

        $filterCategories = [];

        if (isset($filterBy['categories'])) {

            $filterCategorySlugs = $filterBy['categories'];

            if (count($filterCategorySlugs) > 0) {

                $filterCategories = Category::select('id', 'name', 'slug')
                    ->with([
                        'decendants' => function ($query) {
                            $query->select('id');
                        }
                        , 'rootParent' => function ($query) {
                            $query->select('id', 'name', 'slug');
                        },
                    ])
                    ->whereIn('categories.slug', $filterCategorySlugs)
                    ->get();

                $keyedByRootParent = [];

                foreach ($filterCategories as $filterCategory) {
                    if ($filterCategory->rootParent->id !== $filterCategory->id) {

                        $branch = $filterCategory->decendants->lists('id');

                        $products->whereHas('categories',
                            function ($query) use ($branch) {
                                $query->whereIn('categories.id', $branch);
                            }, '>=', 1);

                        $keyedByRootParent[$filterCategory->rootParent->name] = $filterCategory;
                        $hasFilters = true;
                    }
                }

                $filterCategories = $keyedByRootParent;
            }
        }

        $filterFeatureSlugs = [];

        if (isset($filterBy['features'])) {

            $hasFilters = true;

            $filterFeatureSlugs = $filterBy['features'];

            $products->whereHas('features',
                function ($query) use ($filterFeatureSlugs) {
                    $query->whereIn('features.slug', $filterFeatureSlugs);
                }, '>=', count($filterFeatureSlugs));

        }

        $products = $products->groupBy('products.id')
            ->orderBy('categories.real_depth', 'ASC')
            ->orderBy('categories.position', 'ASC')
            ->orderBy('product_categories.product_position', 'ASC')
            ->get();

        $productIDs = $products->lists('id');

        $featureSets = FeatureSet::select('feature_sets.id', 'feature_sets.name', 'feature_sets.slug', 'feature_sets.position')
            ->with(['features' =>
                function ($query) use ($productIDs) {

                    $query->selectWithCountForMenu($productIDs)

                    ->whereHas('products',
                        function ($query) use ($productIDs) {
                            $query->whereIn('products.id', $productIDs);
                        }, '>=', 1);

                    $query->orderBy('features.position');

                },
            ])

            ->orderBy('feature_sets.position', 'ASC')
            ->get();

        $perPage = 18;
        $pageName = 'page';
        $total = $products->count();
        $page = ($total < $perPage) ? 1 : (int) Request::input($pageName, 1);

        $results = $products->slice(($page - 1) * $perPage, $perPage);

        $paginatedProducts = new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);

        if (Request::ajax()) {

            $viewData = [
                'category' => $category->toArray(),
                'filterCategories' => $filterCategories,
                'featureSets' => $featureSets->toArray(),
                'products' => $paginatedProducts->toArray(),
                'productCount' => $total,
                'hasFilters' => $hasFilters,
            ];

            return Response::json($viewData);
        }

        $viewData = [
            'category' => $category,
            'filterCategories' => $filterCategories,
            'featureSets' => $featureSets,
            'products' => $paginatedProducts,
            'productCount' => $total,
            'hasFilters' => $hasFilters,
            // 'default_product_display_thumbnail' => $default_product_display_thumbnail,
        ];

        Request::flash();

        $view = View::make('pages.category', $viewData);

        return $view;
    }
}
