<?php namespace Okuma\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\URL;
use Okuma\Models\Category;
use Okuma\Models\Product;

class SearchController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function search(Request $request)
    {

        $find = $request->input('find', null);

        $searchResults = null;

        if ($request->ajax()) {

            if (is_null($find)) {
                return Response::json([]);
            }

            $productSearch = Product::search($find, ['name' => 20, 'slug' => 5], true)
                ->select('id', 'name', 'slug')
                ->selectRaw('"Product" as type')
                ->take(4)
                ->get();

            foreach ($productSearch as $product) {
                $product->setAttribute('url', URL::route('product', ['slug' => $product->slug]));
            }

            $categorySearch = Category::search($find, ['name' => 20, 'slug' => 5], true)
                ->select('id', 'name', 'slug')
                ->selectRaw('"Category" as type')
                ->take(2)
                ->get();

            foreach ($categorySearch as $category) {
                $category->setAttribute('url', URL::route('category', ['slug' => $category->slug]));
            }

            $searchResults = $productSearch->toBase()->merge($categorySearch->toBase());

            return Response::json($searchResults);
        }

        if (!is_null($find)) {

            // ->simplePaginate( 2 );

            // foreach ( $productSearch as $product ) {
            //  $product->setAttribute( 'url', URL::route( 'product', [ 'slug' => $product->slug ] ) );
            // }

            $categorySearch = Category::search($find, ['name' => 20, 'slug' => 5], true)
                ->select('id', 'name', 'slug', 'snippet')
                ->selectRaw('"Category" as type')
                ->simplePaginate(4);

            $productSearch = Product::search($find, ['name' => 20, 'slug' => 5], true)
                ->select('id', 'name', 'slug', 'snippet')
                ->selectRaw('"Product" as type')
                ->with([
                    'thumbnail' => function ($query) {
                        $query->select('id', 'path','type', 'position','imageable_id', 'imageable_type') ;
                    },
                ])
                ->simplePaginate(8);


            // ->simplePaginate(4);

            // foreach ($searchResults as $result) {

            //     if ($result->type == 'Product') {
            //         $result->setAttribute('url', URL::route('product', ['slug' => $result->slug]));
            //     } else if ($result->type == 'Category') {
            //         $result->setAttribute('url', URL::route('category', ['slug' => $result->slug]));
            //     }
            // }

            $searchResults = $productSearch->toBase()->merge($categorySearch->toBase());


            $productIsPaginated = !empty($productSearch) && $productSearch->hasPages();
            $categoryIsPaginated = !empty($categorySearch) && $categorySearch->hasPages();

            $pagination = null;

            if ($productIsPaginated) {
                $pagination = $productSearch;
            } else if ($categoryIsPaginated) {
                $pagination = $categorySearch;
            }

            if ($productIsPaginated && $productSearch->hasMorePages()) {
                $pagination = $productSearch;
            } else if ($categoryIsPaginated && $categorySearch->hasMorePages()) {
                $pagination = $categorySearch;
            }
        }

            // dd($searchResults, $pagination, $productSearch, $categorySearch);
        // $view = View::make('pages.search')->with(['query' => $find, 'searchResults' => $searchResults]);
        $view = View::make('pages.search')->with(['query' => $find, 'searchResults' => $searchResults, 'pagination' => $pagination]);

        return $view;
    }
}
