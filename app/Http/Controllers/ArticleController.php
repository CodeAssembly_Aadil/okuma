<?php namespace Okuma\Http\Controllers;

use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return Response
     */
    public function show($slug)
    {
        $view = View::make('pages.article');

        return $view;
    }
}
