<?php namespace Okuma\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;

class AuthController extends Controller
{

    private $redirectTo = 'dashboard';

    public function login()
    {
        $view = View::make('dashboard.authentication.login');
        return $view;
    }

    public function logout()
    {
        Auth::logout();
        return redirect($this->redirectTo);
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {

        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $this->validate($request, $rules);

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            // Authentication passed...
            return redirect()->intended($this->redirectTo);
        } else {
            return redirect()->back()->with(['notice' => 'Invalid login.', 'noticeLevel' => 'danger']);
        }
    }
}
