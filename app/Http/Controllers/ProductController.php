<?php namespace Okuma\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;
use Okuma\Repositories\ProductRepository;

class ProductController extends Controller
{

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return Response
     */
    public function show($slug)
    {
        $product = $this->productRepository->getProductBySlug($slug);

        if (is_null($product)) {
            App::abort(404);
        }

        $view = View::make('pages.product')->with('product', $product);

        return $view;
    }
}
