<?php namespace Okuma\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Okuma\Http\Requests\ContactRequest;

class LuckyCarpCompetitionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($source= null)
    {
        $view = View::make('pages.luckycarp.landing');
        return $view;
    }

    // public function app()
    // {
    //     return View::make('pages.carpstikre.app');
    // }

    // public function entry(CarpStrikeCompRequest $request) {

    // }

    public function terms() {

        return View::make('pages.luckycarp.terms');
    }
}
