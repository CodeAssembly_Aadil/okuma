<?php namespace Okuma\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Okuma\Http\Controllers\Controller;
use Okuma\Http\Requests\ShareRequest;
use Okuma\Repositories\ProductRepository;

class ShareController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function form($type, $id)
    {
        $view = View::make('pages.share')->with(['type' => $type, 'id' => $id]);
        return $view;
    }

    public function share(ShareRequest $request, $type, $id)
    {
        $share = null;

        if (strtolower($type) === 'product') {
            $share = $this->productRepository->getProductForShare($id);
        }

        if (is_null($share)) {
            Session::flash('status', 'error');
            return redirect()->back();
        }

        $senderName = $request->input('sender-name');
        $senderEmail = $request->input('sender-email');
        $recipientEmail = $request->input('recipient-email');

        $data = array(
            'title' => $share->getTitle(),
            'senderName' => $senderName,
            'senderEmail' => $senderEmail,
            'recipientEmail' => $recipientEmail,
            'subject' => $share->getTitle(),
            'messageBody' => $share->getDescription(),
            'image' => $share->getImage(),
            'link' => $share->getLink(),
            'responseEmail' => Config::get('okuma.contact.email'),
            'responseName' => Config::get('okuma.contact.name'),
        );

        Mail::queueOn(Config::get('okuma.queues.email'), 'emails.share', $data,
            function ($message) use ($data) {

                $message->from($data['responseEmail'], $data['responseName']);

                // $message->from($data['senderEmail'], $data['senderName']);
                $message->to($data['recipientEmail']);
                $message->cc($data['senderEmail'], $data['senderName']);
                $message->replyTo($data['responseEmail'], $data['responseName']);

                $message->subject('Okuma: ' . $data['subject']);
            });

        Session::flash('share-response', 'shared');

        return redirect()->back();
    }

}
