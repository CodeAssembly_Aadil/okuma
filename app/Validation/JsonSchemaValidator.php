<?php namespace Okuma\Validation;

use JsonSchema\Uri\UriRetriever;
use JsonSchema\Validator;

class JsonSchemaValidator
{

    public function validate($attribute, $value, $parameters)
    {
        if (isset($parameters['schema'])) {

            $schemaFile = $parameters['schema'];

            $retriever = new UriRetriever();
            $path = base_path() . '/schema/' . $schemaFile;
            $schema = $retriever->retrieve('file://' . $path);

            $validator = new Validator();
            $validator->check($value, $schema);

            return $validator->isValid();
        }

        return true;
    }

}
