<?php
namespace Okuma\ViewComposers\Navigation;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class DefaultImagesViewComposer
{

    protected $default_images;

    public function __construct()
    {

        $this->default_images = Cache::tags('site')
            ->rememberForever('default_images', function () {

                $default_product_display_thumbnail = (object) array(
                    'path' => Config::get('okuma.default_images.product_display_thumbnail'),
                    'width' => Config::get('okuma.image_sizes.product_display_thumbnail.width'),
                    'height' => Config::get('okuma.image_sizes.product_display_thumbnail.height'),
                );

                $default_feature_icon = (object) array(
                    'path' => Config::get('okuma.default_images.feature_icon'),
                    'width' => Config::get('okuma.image_sizes.feature_icon.width'),
                    'height' => Config::get('okuma.image_sizes.feature_icon.height'),
                );

                $default_search_thumbnail = (object) array(
                    'path' => Config::get('okuma.default_images.search_thumbnail'),
                    'width' => Config::get('okuma.image_sizes.search_thumbnail.width'),
                    'height' => Config::get('okuma.image_sizes.search_thumbnail.height'),
                );

                $default_images = array(
                    'default_product_display_thumbnail' => $default_product_display_thumbnail,
                    'default_feature_icon' => $default_feature_icon,
                    'default_search_thumbnail' => $default_search_thumbnail,
                );

                return $default_images;

            });
    }

    /**
     * Bind data to the view.
     *
     * @param  Illuminate\Support\Facades\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with($this->default_images);
    }

}
