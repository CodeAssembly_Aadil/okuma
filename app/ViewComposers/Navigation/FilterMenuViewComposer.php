<?php
namespace Okuma\ViewComposers\Navigation;

use GrahamCampbell\HTMLMin\Facades\HTMLMin;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View as ViewManager;
use Okuma\Models\Category;

class FilterMenuViewComposer
{

    protected $categoryMenuTrees;

    public function __construct()
    {

        $this->categoryMenuTrees = Cache::tags(['site', 'category'])
            ->rememberForever('category.tree-html',
                function () {

                    $categoryTree = Category::getTree();
                    $productCounts = Category::fetchAllTotalProductCounts();

                    $categoryMenuTrees = [];

                    foreach ($categoryTree as $tree) {

                        if (isset($productCounts[$tree->id]) && $productCounts[$tree->id] > 0) {

                            $compiledTree = ViewManager::make('components.filtermenu.root_category', ['category' => $tree])->render();

                            foreach ($tree->getChildren() as $category) {
                                $compiledTree .= ViewManager::make('components.filtermenu.category_tree', ['category' => $category, 'productCounts' => $productCounts])->render();
                            }

                            $categoryMenuTrees[$tree->name] = HTMLMin::html($compiledTree);
                        }
                    }

                    return $categoryMenuTrees;
                });
    }

    /**
     * Bind data to the view.
     *
     * @param  Illuminate\Support\Facades\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('categoryMenus', $this->categoryMenuTrees);
    }

}
