<?php
namespace Okuma\ViewComposers\Navigation;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cache;
use Okuma\Models\Category;

class CategoryMenusViewComposer
{

    protected $categoryTree;
    protected $categoryProductCounts;

    public function __construct()
    {
        // DB::connection()->enableQueryLog();

        // Cache::tags('category')->flush();

        $this->categoryTree = Cache::tags(['site', 'category'])
            ->rememberForever('category.tree',
                function () {
                    return Category::getTree();
                });

        $this->categoryProductCounts = Cache::tags(['site', 'category'])
            ->rememberForever('category.product-counts',
                function () {
                    return Category::fetchAllTotalProductCounts();
                });

        // $this->categoryTree = Cache::tags('catalog')->rememberForever('category-tree',
        //     function () {
        //         return Category::getTreeWhere('real_depth', '<', 2);
        //     });

        // dd($this->categoryTree);

        // $this->categoryTree = Cache::tags('catalog')->rememberForever('category-tree',
        //     function () {
        //         return Category::getTree();
        //     });

    }

    /**
     * Bind data to the view.
     *
     * @param  Illuminate\Support\Facades\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(['categoryTree' => $this->categoryTree, 'categoryProductCounts' => $this->categoryProductCounts]);
    }

}
