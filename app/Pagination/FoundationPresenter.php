<?php namespace Okuma\Pagination;

use Illuminate\Contracts\Pagination\Paginator as PaginatorContract;
use Illuminate\Contracts\Pagination\Presenter as PresenterContract;
use Illuminate\Pagination\UrlWindow;
use Illuminate\Support\Facades\View;

class FoundationPresenter implements PresenterContract
{

    use PreviousNextTrait, LinksTrait;

    /**
     * The paginator implementation.
     *
     * @var \Illuminate\Contracts\Pagination\Paginator
     */
    protected $paginator;

    /**
     * The URL window data structure.
     *
     * @var array
     */
    protected $window;

    /**
     * @param PaginatorContract $paginator
     * @param UrlWindow $window
     */
    public function __construct(PaginatorContract $paginator, UrlWindow $window = null)
    {
        $this->paginator = $paginator;
        $this->window = is_null($window) ? UrlWindow::make($paginator) : $window->get();
    }

    /**
     * Render the given paginator.
     *
     * @return string
     */
    public function render()
    {
        if (!$this->hasPages()) {
            return '';
        }

        $data = [
            'current' => $this->paginator->currentPage(),
            'previous' => $this->getPrevious(),
            'links' => $this->getLinks(),
            'next' => $this->getNext(),
        ];

        return View::make('components.pagination.pagination', $data)->render();
    }

    /**
     * Determine if the underlying paginator being presented has pages to show.
     *
     * @return bool
     */
    public function hasPages()
    {
        return $this->paginator->hasPages();
    }
}
