<?php namespace Okuma\Database\Eloquent;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Okuma\Database\Eloquent\PublishedScope;

trait Published
{
    /**
     * Boot the soft deleting trait for a model.
     *
     * @return void
     */
    public static function bootPublished()
    {
        static::addGlobalScope(new PublishedScope);
    }

    /**
     * Restore a soft-deleted model instance.
     *
     * @return bool|null
     */
    public function publish()
    {
        if ($this->fireModelEvent('publishing') === false) {
            return false;
        }

        $this->{$this->getPublishedAtColumn()} = Carbon::now(); //$this->freshTimestamp();

        $result = $this->save();

        $this->fireModelEvent('published', false);

        return $result;
    }

    /**
     * Restore a soft-deleted model instance.
     *
     * @return bool|null
     */
    public function publishOn(Carbon $date)
    {
        if ($this->fireModelEvent('publishing_on') === false) {
            return false;
        }

        $this->{$this->getPublishedAtColumn()} = $date;

        $result = $this->save();

        if ($date->lt(Carbon::now())) {
            $this->fireModelEvent('published', false);
        } else {
            $this->fireModelEvent('publish_on', false);
        }

        return $result;
    }

    /**
     * Restore a soft-deleted model instance.
     *
     * @return bool|null
     */
    public function unpublish()
    {
        if ($this->fireModelEvent('unpublishing') === false) {
            return false;
        }

        $this->{$this->getPublishedAtColumn()} = null;

        $result = $this->save();

        $this->fireModelEvent('unpublished', false);

        return $result;
    }
    /**
     * Determine if the model instance has been soft-deleted.
     *
     * @return bool
     */
    public function isPublished()
    {
        $column = $this->getPublishedAtColumn();

        if (is_null($this->{$column})) {
            return false;
        }

        // dd(Carbon::now()->gte($this->{$column}));

        return Carbon::now()->gte($this->{$column});
    }
    /**
     * Get a new query builder that includes soft deletes.
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public static function withDrafts()
    {
        $instance = new static;

        return $instance->newQueryWithoutScope(new PublishedScope);
    }

    /**
     * Get a new query builder that only includes soft deletes.
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public static function onlyDrafts()
    {
        $instance = new static;

        $column = $instance->getQualifiedPublishedAtColumn();

        return $instance->newQueryWithoutScope(new PublishedScope)
            ->whereNull($column)
            ->whereDate($column, '>', Carbon::now(), 'OR');
    }

    /**
     * Register a restoring model event with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function publishing($callback)
    {
        static::registerModelEvent('publishing', $callback);
    }

    /**
     * Register a restored model event with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function published($callback)
    {
        static::registerModelEvent('published', $callback);
    }

    /**
     * Register a restored model event with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function publishing_on($callback)
    {
        static::registerModelEvent('publishing_on', $callback);
    }

    /**
     * Register a restored model event with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function published_on($callback)
    {
        static::registerModelEvent('published_on', $callback);
    }

    /**
     * Register a restoring model event with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function unpublishing($callback)
    {
        static::registerModelEvent('unpublishing', $callback);
    }

    /**
     * Register a restored model event with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function unpublished($callback)
    {
        static::registerModelEvent('unpublished', $callback);
    }

    /**
     * Get the name of the "published at" column.
     *
     * @return string
     */
    public function getPublishedAtColumn()
    {
        return defined('static::PUBLISHED_AT') ? static::PUBLISHED_AT : 'published_at';
    }

    /**
     * Get the fully qualified "published at" column.
     *
     * @return string
     */
    public function getQualifiedPublishedAtColumn()
    {
        return $this->getTable() . '.' . $this->getPublishedAtColumn();
    }

    /**
     * Get the next publish date.
     *
     * @return Carbon
     */
    public static function getNextPublishDate()
    {
        $now = Carbon::now();

        $instance = new static;
        $column = $instance->getPublishedAtColumn();

        DB::enableQueryLog();

        $nextPublishDate = $instance
            ->newQueryWithoutScope(new PublishedScope)
            ->whereDate($column, '>', $now)
            ->orderBy($column, 'ASC')
            ->first();

        if (isset($nextPublishDate)) {
            return new Carbon($nextPublishDate->published_at);
        }

        return null;
    }
}
