<?php
namespace Okuma\Database\Eloquent;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Sofa\GlobalScope\GlobalScope;

class PublishedScope extends GlobalScope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $column = $model->getQualifiedPublishedAtColumn();

        $builder->whereDate($column, '<=', Carbon::now());

        $this->addPublish($builder);
        $this->addUnpublish($builder);

        $this->addWithDrafts($builder);
        $this->addOnlyDrafts($builder);
    }

    /**
     * Get the "published_at" column for the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return string
     */
    protected function getPublishedAtColumn(Builder $builder)
    {
        if (count($builder->getQuery()->joins) > 0) {
            return $builder->getModel()->getQualifiedPublishedAtColumn();
        } else {
            return $builder->getModel()->getPublishedAtColumn();
        }
    }

    /**
     * Add the force delete extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addPublish(Builder $builder, Carbon $time = null)
    {
        $builder->macro('publish', function (Builder $builder) use ($time) {
            $builder->withDrafts();

            $model = $builder->getModel();
            $column = $model->getPublishedAtColumn();
            $time = isset($time) ? $time : Carbon::now();

            return $builder->update([$column => $time]);
        });
    }

    /**
     * Add the restore extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addUnpublish(Builder $builder)
    {
        $builder->macro('unpublish', function (Builder $builder) {

            $model = $builder->getModel();

            $column = $column->getPublishedAtColumn();

            return $builder->update([$column => null]);
        });
    }

    /**
     * Add the with-drafts extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addWithDrafts(Builder $builder)
    {
        $builder->macro('withDrafts', function (Builder $builder) {

            $model = $builder->getModel();

            $this->remove($builder, $model);

            return $builder;
        });
    }

    /**
     * Add the only-drafts extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addOnlyDrafts(Builder $builder)
    {
        $builder->macro('onlyDrafts', function (Builder $builder) {

            $model = $builder->getModel();

            $column = $model->getQualifiedPublishedAtColumn();

            $this->remove($builder, $model);

            $builder->getQuery()
            ->whereNull($column)
            ->whereDate($column, '>', Carbon::now(), 'OR');

            return $builder;
        });
    }
}
