<?php namespace Okuma\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Okuma\Models\SiteFeature;

class SiteFeatureRepository
{
    private static $CACHE_TAGS = ['site', 'sitefeature'];

    public function getHero()
    {
        $hero = Cache::tags(self::$CACHE_TAGS)->get('sitefeatures.hero',
            function () {
                return SiteFeature::where('column_size', '=', 'site-hero')
                ->with('background')
                ->orderBy('position', 'DESC')
                ->first();
            });

        if (isset($hero)) {
            if (!Cache::tags(self::$CACHE_TAGS)->has('sitefeatures.hero')) {

                $cacheDuration = SiteFeature::getNextPublishDate();

                if (is_null($cacheDuration)) {
                    $cacheDuration = Config::get('okuma.cache_duration');
                }

                Cache::tags(self::$CACHE_TAGS)->add('sitefeatures.hero', $hero, $cacheDuration);
            }
        }

        return $hero;
    }

    public function getFeatures()
    {
        $siteFeatures = Cache::tags(self::$CACHE_TAGS)->get('sitefeatures.features',
            function () {
                return SiteFeature::where('column_size', '!=', 'site-hero')
                ->with('background')
                ->orderBy('position', 'ASC')
                ->get();
            });

        if (isset($siteFeatures)) {
            if (!Cache::tags(self::$CACHE_TAGS)->has('sitefeatures.features')) {

                $cacheDuration = SiteFeature::getNextPublishDate();

                if (is_null($cacheDuration)) {
                    $cacheDuration = Config::get('okuma.cache_duration');
                }

                Cache::tags(self::$CACHE_TAGS)->add('sitefeatures.features', $siteFeatures, $cacheDuration);
            }
        }

        return $siteFeatures;
    }
}
