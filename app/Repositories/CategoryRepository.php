<?php namespace Okuma\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Okuma\Models\Category;
use Okuma\Models\Product;

class CategoryRepository
{

    private static $CACHE_TAGS = ['site', 'category'];

    public function getCatalog()
    {
        $catalog = Cache::tags(self::$CACHE_TAGS)
            ->get('category.catalog',
                function () {
                    $catalog = Category::getRoots()->first();

                    if (is_null($catalog)) {
                        return $catalog;
                    }

                    $productCounts = Category::fetchAllTotalProductCounts();

                    $topLevelCategories = Category::where('real_depth', '=', 1)->get();

                    foreach ($topLevelCategories as $category) {

                        $categoryID = $category->id;
                        $productCount = isset($productCounts[$category->id]) ? $productCounts[$category->id] : 0;

                        if ($productCount === 0) {
                            $topLevelCategories->forget($category->getKey());
                        } else {

                            $products = Product::distinct()
                            ->join('product_categories', 'product_categories.product_id', '=', 'products.id')
                            ->join('category_closure', 'category_closure.descendant', '=', 'product_categories.category_id')
                            ->join('categories', 'category_closure.descendant', '=', 'categories.id')
                            ->with([
                                'thumbnail',
                                'variantsCount',
                            ])
                            ->where('category_closure.ancestor', '=', $categoryID)
                            ->orderBy('product_categories.product_position')
                            ->orderBy('category_closure.depth')
                            ->orderBy('categories.position')
                            ->limit(4)
                            ->getForCatalog();

                            $category->setRelation('products', $products);
                            $category->setAttribute('totalProductsCount', $productCount);
                        }
                    }

                    $catalog->load('heroBanner');
                    $catalog->setRelation('children', $topLevelCategories);

                    return $catalog;
                });

        if (isset($catalog)) {
            if (!Cache::tags(self::$CACHE_TAGS)->has('category.catalog')) {
                $cacheDuration = Config::get('okuma.cache_duration');
                Cache::tags(self::$CACHE_TAGS)->add('category.catalog', $catalog, $cacheDuration);
            }
        }

        return $catalog;
    }

}
