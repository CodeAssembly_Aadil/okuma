<?php namespace Okuma\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Okuma\Models\Product;
use Okuma\Models\Share;

class ProductRepository
{

    private static $CACHE_TAGS = ['site', 'product'];

    public function getProductBySlug($slug)
    {
        $product = Cache::tags(self::$CACHE_TAGS)
            ->get('product' . $slug,
                function () use ($slug) {
                    $product = Product::select('products.id', 'products.name', 'products.description', 'products.snippet', 'products.specification', 'products.deleted_at', 'products.published_at')
                        ->with([
                            'sliderImages',
                            'sliderThumbnails',
                            'shareImage',
                            'variants' => function ($query) {
                                $query->select(['variants.id', 'variants.model', 'variants.specification', 'variants.position', 'variants.deleted_at', 'variants.product_id'])
                                    ->orderBy('variants.position', 'ASC');
                            },
                            'features' => function ($query) {
                                $query->select(['features.id', 'features.name', 'features.description', 'features.deleted_at', 'product_features.feature_position'])
                                    ->orderBy('product_features.feature_position', 'ASC');
                            },
                            'features.icon' => function ($query) {
                                $query->select(['images.id', 'images.imageable_id', 'images.imageable_type', 'images.title', 'images.type', 'images.path', 'images.width', 'images.height']);
                            },
                        ])
                        ->where('slug', '=', $slug)
                        ->first();

                    return $product;
                });

        if (isset($product)) {
            if (!Cache::tags(self::$CACHE_TAGS)->has('product' . $slug)) {
                $cacheDuration = Config::get('okuma.cache_duration');
                Cache::tags(self::$CACHE_TAGS)->add('product' . $slug, $product, $cacheDuration);
            }
        }

        return $product;
    }

    public function getProductForShare($id)
    {
        $product = Product::select('products.id', 'products.slug', 'products.name', 'products.snippet', 'products.deleted_at', 'products.published_at')
            ->with('shareImage')
            ->find($id);

        $share = null;

        if (isset($product)) {

            $title = $product->name;
            $description = $product->snippet;
            $type = 'Product';
            $image = (isset($product->shareImage)) ? URL::asset($product->shareImage->path) : null;
            $link = URL::route('product', ['slug' => $product->slug]);

            $share = new Share($title, $description, $type, $image, $link);
        }

        return $share;
    }
}
