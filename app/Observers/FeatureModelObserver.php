<?php namespace Okuma\Observers;

use Illuminate\Support\Facades\DB;
use Okuma\Models\Feature;

class FeatureModelObserver
{
    public function saved(Feature $feature)
    {

        if (!isset($feature->feature_set_id)) {
            $this->remove($feature);
            return true;
        }

        $products = DB::table('product_features')
            ->select('product_id')
            ->where('feature_id', '=', $feature->id)
            ->groupBy('product_id')
            ->lists('product_id');

        if (empty($products)) {
            $this->remove($feature);
            return true;
        }

        $categories = DB::table('product_categories')
            ->select('category_id')
            ->whereIn('product_id', $products)
            ->groupBy('category_id')
            ->lists('category_id');

        if (empty($categories)) {
            $this->remove($feature);
            return true;
        }

        $categoriesWithFeature = DB::table('category_features')
            ->select('category_id')
            ->whereIn('category_id', $categories)
            ->where('feature_id', '=', $feature->id)
            ->lists('category_id');

        if ($feature->isDirty('feature_set_id') && !empty($categoriesWithFeature)) {

            $updateResult = DB::table('category_features')
                ->whereIn('category_id', $categoriesWithFeature)
                ->where('feature_id', '=', $feature->id)
                ->update(
                    [
                        'feature_set_id' => $feature->feature_set_id,
                    ]);
        }

        $categoriesWithoutFeature = array_diff($categories, $categoriesWithFeature);

        $insert = [];

        foreach ($categoriesWithoutFeature as $category) {
            array_push($insert, [
                'category_id' => $category,
                'feature_id' => $feature->id,
                'feature_set_id' => $feature->feature_set_id,
            ]);
        }

        if (!empty($insert)) {
            DB::table('category_features')->insert($insert);
        }

        return true;
    }

    // public function updated(Feature $feature)
    // {

    //     if (!isset($feature->feature_set_id)) {
    //         $this->remove($feature);
    //         return true;
    //     }

    //     $products = DB::table('product_features')
    //         ->select('product_id')
    //         ->where('feature_id', '=', $feature->id)
    //         ->groupBy('product_id')
    //         ->lists('product_id');

    //     if (empty($products)) {
    //         $this->remove($feature);
    //         return true;
    //     }

    //     $categories = DB::table('product_categories')
    //         ->select('category_id')
    //         ->whereIn('product_id', $products)
    //         ->groupBy('category_id')
    //         ->lists('category_id');

    //     if (empty($categories)) {
    //         $this->remove($feature);
    //         return true;
    //     }

    //     $categoriesWithFeature = DB::table('category_features')
    //         ->select('category_id')
    //         ->whereIn('category_id', $categories)
    //         ->where('feature_id', '=', $feature->id)
    //         ->lists('category_id');

    //     if ($feature->isDirty('feature_set_id') && !empty($categoriesWithFeature)) {

    //         $updateResult = DB::table('category_features')
    //             ->whereIn('category_id', $categoriesWithFeature)
    //             ->where('feature_id', '=', $feature->id)
    //             ->update(
    //                 [
    //                     'feature_set_id' => $feature->feature_set_id,
    //                 ]);
    //     }

    //     $categoriesWithoutFeature = array_diff($categories, $categoriesWithFeature);

    //     $insert = [];

    //     foreach ($categoriesWithoutFeature as $category) {
    //         array_push($insert, [
    //             'category_id' => $category,
    //             'feature_id' => $feature->id,
    //             'feature_set_id' => $feature->feature_set_id,
    //         ]);
    //     }

    //     if (!empty($insert)) {
    //         DB::table('category_features')->insert($insert);
    //     }

    //     return true;
    // }

    public function deleted(Feature $feature)
    {
        $exists = DB::table($feature->getTable())
            ->where($feature->getQualifiedKeyName(), '=', $feature->getKey())
            ->exists();

        if (!$exists) {

            $icon = $feature->icon()
                ->get();

            foreach ($icon as $image) {
                $image->forceDelete();
            }
        }

        return true;
    }

    protected function remove($feature)
    {
        DB::table('category_features')
            ->where('feature_id', '=', $feature->id)
            ->delete();
    }

}
