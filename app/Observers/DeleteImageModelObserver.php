<?php namespace Okuma\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Okuma\Models\Image;

class DeleteImageModelObserver
{

    /*
     * Delete all images related to this model if it does not exist
     */

    public function deleted(Model $model)
    {

        $exists = DB::table($model->getTable())
            ->where($model->getQualifiedKeyName(), '=', $model->getKey())
            ->exists();

        if (!$exists) {

            $images = Image::where('imageable_type', '=', $model->getMorphClass())
                ->where('imageable_id', '=', $model->getKey())
                ->get();

            foreach ($images as $image) {
                $image->delete();
            }
        }

        return true;
    }
}
