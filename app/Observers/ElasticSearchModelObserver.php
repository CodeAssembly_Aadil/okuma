<?php namespace Okuma\Observers;

use Illuminate\Database\Eloquent\Model;
use Okuma\Search\Searchable;
use Okuma\Search\SearchIndexManager;

class ElasticSearchModelObserver
{

    private $serachIndexManager;

    public function __construct(SearchIndexManager $serachIndexManager)
    {
        $this->serachIndexManager = $serachIndexManager;
    }

    public function saved(Model $model)
    {

        if ($model instanceof Searchable && $model->searchableCanIndex()) {

            $this->serachIndexManager->add($model);
        }

        return true;
    }

    public function deleted(Model $model)
    {
        if ($model instanceof Searchable) {
            $this->serachIndexManager->remove($model);
        }

        return true;
    }
}
