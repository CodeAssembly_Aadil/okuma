<?php namespace Okuma\Observers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Okuma\Models\Product;

class ProductModelObserver
{

    public function created(Product $product)
    {
        $path = 'public/' . str_replace('{ID}', $product->id, Config::get('okuma.image_directories.' . get_class($product)));

        if (Storage::exists($path)) {
            Storage::deleteDirectory($path);
        }

        Storage::makeDirectory($path);

        return true;
    }

    public function deleted(Product $product)
    {

        $productExists = DB::table('products')
            ->where('id', '=', $product->id)
            ->exists();

        if (!$productExists) {

            $variants = $product->variants()
                ->withTrashed()
                ->get();

            foreach ($variants as $variant) {
                $variant->forceDelete();
            }

            // $images = $product->images()
            //     ->get();

            // foreach ($images as $image) {
            //     $image->delete();
            // }

            $path = 'public/' . str_replace('{ID}', $product->id, Config::get('okuma.image_directories.' . get_class($product)));

            if (Storage::exists($path)) {
                Storage::deleteDirectory($path);
            }
        }

        return true;
    }

}
