<?php namespace Okuma\Observers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Okuma\Facades\ImageManager;
use Okuma\Models\Image;

class ImageModelObserver
{

    public function saved(Image $image)
    {

        if ($image->type === 'product_large' && isset($image->path)) {

            if ($image->getOriginal('path') == null) {
                return $this->createProductThumbnail($image);
            } else if ($image->getDirty('path') !== null) {
                return $this->updateProductThumbnail($image);
            } else if ($image->getDirty('title') !== null) {
                return $this->updateProductThumbnailTitle($image);
            }
        }

        return true;
    }

    public function deleted(Image $image)
    {
        $path = trim($image->path);

        try {
            $filePath = 'public/' . $image->path;

            if (isset($path) && !empty($path) && Storage::exists($filePath)) {
                Storage::delete($filePath);
            }

        } catch (Exception $exception) {
            return false;
        }

        return true;
    }

    protected function createProductThumbnail($image)
    {
        $dimentions = Config::get("okuma.image_sizes.product_thumbnail");
        $width = $dimentions['width'];
        $height = $dimentions['height'];

        $thumbnail = new Image();

        $thumbnail->title = $image->title;
        $thumbnail->imageable_id = $image->imageable_id;
        $thumbnail->imageable_type = $image->imageable_type;
        $thumbnail->type = 'product_thumbnail';
        $thumbnail->position = $image->position;
        $thumbnail->width = $width;
        $thumbnail->height = $height;

        $thumbnail->save();

        try {
            $directory = str_replace('{ID}', $thumbnail->imageable_id, Config::get("okuma.image_directories.{$image->imageable_type}"));

            $fileName = $thumbnail->id . '.jpg';

            $thumbnail->path = $directory . $fileName;
            $thumbnail->save();

            if (!Storage::exists('public/' . $directory)) {
                Storage::makeDirectory('public/' . $directory);
            }
            // Create Thumbnail
            $imageFile = ImageManager::make(public_path($image->path))
                ->fit($width, $height)
                ->save(public_path($directory . $fileName), 95);

            $thumbnail->relatedImages()->attach($image->id, ['relation' => 'large']);
            $image->relatedImages()->attach($thumbnail->id, ['relation' => 'thumbnail']);

        } catch (Exception $exception) {
            // $thumbnail->destroy();
            return false;
        }

        return true;
    }

    protected function updateProductThumbnail($image)
    {
        try {

            $thumbnail = $image->relatedImages()->first();

            // Delete existing files
            $thumbnailPath = trim($thumbnail->path);
            try {
                $filePath = 'public/' . $thumbnail->path;

                if (isset($thumbnailPath) && !empty($thumbnailPath) && Storage::exists($filePath)) {
                    Storage::delete($filePath);
                }

            } catch (Exception $exception) {
                // return false;
            }

            // if (Storage::exists('public/' . $thumbnail->path)) {
            //     Storage::delete('public/' . $thumbnail->path);
            // }

            //Get Directory
            $directory = str_replace('{ID}', $thumbnail->imageable_id, Config::get("okuma.image_directories.{$image->imageable_type}"));

            $fileName = $thumbnail->id . '.jpg';

            $thumbnail->title = $image->title;
            $thumbnail->path = $directory . $fileName;

            $thumbnail->save();

            if (!Storage::exists('public/' . $directory)) {
                Storage::makeDirectory('public/' . $directory);
            }

            $dimentions = Config::get("okuma.image_sizes.product_thumbnail");
            $width = $dimentions['width'];
            $height = $dimentions['height'];

            $imageFile = ImageManager::make(public_path($image->path))
                ->fit($width, $height)
                ->save(public_path($directory . $fileName), 95);

        } catch (Exception $exception) {
            return false;
        }

        return true;
    }

    protected function updateProductThumbnailTitle($image)
    {
        try {

            $thumbnail = $image->relatedImages()->first();

            $thumbnail->title = $image->title;

            $thumbnail->save();

        } catch (Exception $exception) {
            return false;
        }

        return true;
    }
}
