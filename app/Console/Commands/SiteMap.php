<?php namespace Okuma\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Okuma\Models\Category;
use Okuma\Models\Product;

// use Okuma\Tasks\Generate

class SiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a sitemap for the website';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $now = Carbon::now();

        $this->info('Sitemap: Create ' . $now);

        $timestamp = $now->toW3cString();

        // create new sitemap object
        $sitemap = App::make("sitemap");

        $homeLastUpdate = DB::table('site_features')
            ->whereNull('deleted_at')
            ->whereDate('published_at', '<', $now)
            ->max('published_at');

        $homeLastUpdate = (is_null($homeLastUpdate)) ? $timestamp : $homeLastUpdate;

        // add items to the sitemap (url, date, priority, freq)
        $sitemap->add(URL::route('home'), $homeLastUpdate, '1', 'weekly');

        $sitemap->add(URL::route('catalog'), $timestamp, '0.7', 'monthly');

        $categories = Category::getTree();

        $productCounts = Category::fetchAllTotalProductCounts();

        foreach ($categories as $category) {
            $this->addCategoryToSiteMap($sitemap, $category, $timestamp, $productCounts);
        }

        $products = Product::select('id', 'slug', 'updated_at')
            ->has('categories', '>=', 1)
            ->get();

        foreach ($products as $product) {
            $sitemap->add(URL::route('product', ['product' => $product->slug]), $product->updated_at->toW3cString(), '1.0', 'monthly');
        }

        $sitemap->add(URL::route('contact.index'), $timestamp, '0.8', 'yearly');
        $sitemap->add(URL::route('newsletter.subscribe'), $timestamp, '0.8', 'yearly');
        $sitemap->add(URL::route('newsletter.unsubscribe'), $timestamp, '0.5', 'yearly');

        $sitemap->add(URL::route('search'), $timestamp, '0.5', 'yearly');

        $sitemap->store('xml', 'sitemap');

        $this->info('Sitemap: Complete');
    }

    protected function addCategoryToSiteMap($sitemap, $category, $timestamp, $productCounts)
    {
        if (isset($productCounts[$category->id])) {
            $sitemap->add(URL::route('category', ['category' => $category->slug]), $timestamp, '0.9', 'monthly');

            foreach ($category->getChildren() as $node) {
                $this->addCategoryToSiteMap($sitemap, $node, $timestamp, $productCounts);
            }
        }
    }
}
