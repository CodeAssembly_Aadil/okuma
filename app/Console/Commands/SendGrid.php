<?php namespace Okuma\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\URL;
use Mail;
use Validator;

// use Okuma\Tasks\Generate

class SendGrid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendgrid:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $sendTo = $this->ask('Send to Email Address');

        if (is_null($sendTo)) {
            $this->error('Recipient Email is required.');
            return;
        }

        $subject = $this->ask('Subject');

        if (is_null($subject)) {
            $this->error('Subject is required.');
            return;
        }

        $messageBody = $this->ask('Message');

        if (is_null($messageBody)) {
            $messageBody = '';
        }

        $time = Carbon::now();

        $message = "
                    ------------ \r\n
                    From: Okuma | Inspired Fishing <info@okuma.co.za> \r\n
                    To: {$sendTo} \r\n
                    Subject: {$subject} \r\n
                    Time: {$time} \r\n
                    Reply To: Sensational Angling Supplies <sensationtackle@gmail.com> \r\n
                    ------------ \r\n\r\n
                ";

        $message .= $messageBody;

        Mail::raw($message, function ($message) use ($sendTo, $subject) {
            $message->from('info@okuma.co.za', 'Okuma | Inspired Fishing');
            $message->replyTo('sensationtackle@gmail.com', 'Sensational Angling Supplies');
            $message->to($sendTo);
            $message->subject($subject);
        });
    }
}
