<?php namespace Okuma\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Okuma\Models\AdminUser;

class Dashboard extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dashboard:admin {action=list}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manage Dashboard aministrators.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        switch ($this->argument('action')) {
            case 'list':
                $this->listAdmins();
                break;

            case 'new':
                $this->createAdmin();
                break;

            case 'delete':
                $this->deleteAdmin();
                break;

            case 'name':
                $this->changeName();
                break;

            case 'email':
                $this->changeEmail();
                break;

            case 'password':
                $this->changePassword();
                break;

            default:
                $this->info('Invalid action.');
                break;
        }

    }

    /**
     * Register new AdminUser
     */
    protected function createAdmin()
    {
        $name = $this->ask('Name:');
        if (is_null($name)) {
            return;
        }

        $email = $this->ask('Email Address:');
        if (is_null($email)) {
            return;
        }

        $emailConfirm = $this->ask('Confirm Email Address:');
        if (is_null($emailConfirm)) {
            return;
        }

        $password = $this->ask('Password:');
        if (is_null($password)) {
            return;
        }

        $passwordConfirm = $this->ask('Confirm Password:');
        if (is_null($passwordConfirm)) {
            return;
        }

        $validator = Validator::make(
            [
                'name' => $name,

                'email' => $email,
                'email_confirmation' => $emailConfirm,

                'password' => $password,
                'password_confirmation' => $passwordConfirm,
            ],
            [
                'name' => 'required|max:25',
                'email' => 'required|email|confirmed|max:255',
                'password' => 'required|between:8,60|confirmed',
            ],
            [
                'name.required' => 'No Name provided',
                'name.max' => 'Name is too long',

                'email.required' => 'No Email Address provided',
                'email.email' => 'Invalid Email Address.',
                'email.confirmed' => 'Email Address confirmation failed ',
                'email.max' => 'Email Address is too long ',

                'password.required' => 'No Password provided',
                'password.between' => 'Password must be between 8 and 60 characters',
                'password.confirmed' => 'Password confirmation failed ',

            ]);

        if ($validator->fails()) {

            if ($validator->errors()->has('name')) {
                $this->info('Error: ' . $validator->errors()->first('name'));
            }

            if ($validator->errors()->has('email')) {
                $this->info('Error: ' . $validator->errors()->first('email'));
            }

            if ($validator->errors()->has('password')) {
                $this->info('Error: ' . $validator->errors()->first('password'));
            }

            return;
        }

        try
        {
            AdminUser::create([
                'name' => $name,
                'email' => $email,
                'password' => bcrypt($password),
            ]);
        } catch (Exception $e) {
            $this->error('Error: Dashboard Administrator could not be created.');
            return;
        }

        $this->info('Success: Dashboard Administrator: ' . $email . ' was successfully created!');
    }

    /**
     * Get all AdminUsers
     * @return array
     */
    protected function getAdmins()
    {
        $admins = AdminUser::orderBy('id', 'asc')->get();

        $result = [];

        foreach ($admins as $admin) {
            $result[$admin->id] = '<info>' . $admin->name . '</info>: ' . $admin->email;
        }

        return $result;
    }

    /**
     * Render AdminUsers list
     */
    protected function listAdmins()
    {

        $admins = $this->getAdmins();

        foreach ($admins as $admin) {
            $this->line($admin);
        }

    }

    /**
     * Delete AdminUser
     */
    protected function deleteAdmin()
    {
        $id = $this->selectAdminUser('Select Dashboard Administrator to Delete:');

        $confirm = $this->confirm('Are you sure want to delete Dashboard Administrator with id ' . $id . '?', false);
        if (!$confirm) {
            return;
        }

        AdminUser::destroy($id);
        $this->info('Success: Dashboard Administrator with id ' . $id . ' was deleted.');
    }

    /**
     * Rename AdminUser
     */
    protected function changeName()
    {
        $id = $this->selectAdminUser('Select Dashboard Administrator to chnage Name:');

        $name = $this->ask('Name:');
        if (is_null($username)) {
            return;
        }

        $validator = Validator::make(
            ['name' => $name],
            ['name' => 'required|max:25'],
            [
                'name.required' => 'No Name provided',
                'name.max' => 'Name is too long',
            ]);

        if ($validator->fails()) {
            $this->info('Error: ' . $validator->errors()->first('name'));
            return;
        }

        $AdminUser = AdminUser::find($id);
        $AdminUser->name = $name;
        $AdminUser->save();

        $this->info('Success: Name changed.');
    }

    /**
     * Change AdminUser's password
     */
    protected function changePassword()
    {
        $id = $this->selectAdminUser('Select Dashboard Administrator to change Password:');

        $password = $this->ask('New Password:');
        if (is_null($password)) {
            return;
        }

        $passwordConfirm = $this->ask('Confirm Password:');
        if (is_null($passwordConfirm)) {
            return;
        }

        if ($password !== $passwordConfirm) {
            $this->error('Error: Password confirmation failed.');
            return;
        }

        $validator = Validator::make(
            [
                'password' => $password,
                'password_confirmation' => $passwordConfirm,
            ],
            ['password' => 'required|between:8,60|confirmed'],
            [
                'password.required' => 'No Password provided',
                'password.between' => 'Password must be between 8 and 60 characters',
                'password.confirmed' => 'Password confirmation failed ',
            ]);

        if ($validator->fails()) {
            $this->info('Error: ' . $validator->errors()->first('password'));
            return;
        }

        $AdminUser = AdminUser::find($id);
        $AdminUser->password = bcrypt($password);
        $AdminUser->save();

        $this->info('Success: Password changed.');
    }

    /**
     * Change AdminUser's password
     */
    protected function changeEmail()
    {
        $id = $this->selectAdminUser('Select Dashboard Administrator to change Email Address:');

        $email = $this->ask('New Email Address:');
        if (is_null($email)) {
            return;
        }

        $emailConfirm = $this->ask('Confirm Email Address:');
        if (is_null($emailConfirm)) {
            return;
        }

        $validator = Validator::make(
            [
                'email' => $email,
                'email_confirmation' => $emailConfirm,
            ],
            ['email' => 'required|email|confirmed|max:255'],
            [
                'email.required' => 'No Email Address provided',
                'email.email' => 'Invalid Email Address.',
                'email.confirmed' => 'Email Address confirmation failed ',
                'email.max' => 'Email Address is too long ',
            ]);

        if ($validator->fails()) {
            $this->info('Error: ' . $validator->errors()->first('email'));
            return;
        }

        $AdminUser = AdminUser::find($id);
        $AdminUser->email = $email;
        $AdminUser->save();

        $this->info('Success: Email Address changed.');
    }

    /**
     * Select AdminUser
     * @return int
     */
    protected function selectAdminUser($message)
    {
        $admins = $this->getAdmins();

        $result = $this->choice($message, $admins);

        $flipped = array_flip($admins);
        $id = $flipped[$result];

        return $id;
    }

}
