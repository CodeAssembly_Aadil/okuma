<?php namespace Okuma\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Okuma\Models\Category;
use Okuma\Models\Feature;
use Okuma\Models\Image;
use Okuma\Models\Product;
use Okuma\Models\SiteFeature;
use Okuma\Observers\DeleteImageModelObserver;
use Okuma\Observers\ImageModelObserver;
use Okuma\Observers\ProductModelObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        Product::observe($this->app->make(ProductModelObserver::class));
        Image::observe($this->app->make(ImageModelObserver::class));

        Category::observe($this->app->make(DeleteImageModelObserver::class));
        Feature::observe($this->app->make(DeleteImageModelObserver::class));
        SiteFeature::observe($this->app->make(DeleteImageModelObserver::class));

        $events->listen(['eloquent.saved*', 'eloquent.deleted*'],
            function (Model $model) {
                Cache::tags('site')->flush();
            });
    }
}
