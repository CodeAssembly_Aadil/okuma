<?php namespace Okuma\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Okuma\ViewComposers\Navigation\CategoryMenusViewComposer;
use Okuma\ViewComposers\Navigation\DefaultImagesViewComposer;
use Okuma\ViewComposers\Navigation\FilterMenuViewComposer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('components.sidebar_fitler_menu', FilterMenuViewComposer::class);
        // View::composer(['components.menubar', 'components.pushmenu'], 'Okuma\ViewComposers\Navigation\CategoryMenusViewComposer');
        View::composer([
            'components.menubar',
            'components.pushmenu',
        ], CategoryMenusViewComposer::class);

        View::composer([
            'components.menubar',
            'pages.catalog',
            'pages.category',
            'pages.product',
            'dashboard.category.edit.products',
        ], DefaultImagesViewComposer::class);
        // View::composer(['components.menubar', 'components.pushmenu'], 'Okuma\ViewComposers\Navigation\CategoryMenusViewComposer');

        Validator::extend('json_schema', 'Okuma\Validation\JsonSchemaValidator@validate');

        // $default_product_display_thumbnail = Cache::tags(['site', 'defaults'])->rememberForever('default_product_display_thumbnail', function () {
        //     return (object) array(
        //         'path' => Config::get('okuma.default_images.product_display_thumbnail'),
        //         'width' => Config::get('okuma.image_sizes.product_display_thumbnail.width'),
        //         'height' => Config::get('okuma.image_sizes.product_display_thumbnail.height'),
        //     );
        // });

        // $default_feature_icon = Cache::tags(['site', 'defaults'])->rememberForever('default_feature_icon', function () {
        //     return (object) array(
        //         'path' => Config::get('okuma.default_images.feature_icon'),
        //         'width' => Config::get('okuma.image_sizes.feature_icon.width'),
        //         'height' => Config::get('okuma.image_sizes.feature_icon.height'),
        //     );
        // });

        // view()->share('default_product_display_thumbnail', $default_product_display_thumbnail);
        // view()->share('default_feature_icon', $default_feature_icon);

        // Cache::rememberForever('okuma.default_images.product_thumbnail', function () {
        //     return Config::get('okuma.default_images.product_thumbnail');
        // });

        // view()->share('default_search_thumbnail', ['path'=> , 'widht'=> 'height'=>]);
        // view()->share('default_feature_icon', ['path'=> , 'widht'=> 'height'=>]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
