<?php namespace Okuma\Providers;

use Illuminate\Support\ServiceProvider;
use Okuma\Repositories\CategoryRepository;
use Okuma\Repositories\ProductRepository;
use Okuma\Repositories\SiteFeatureRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CategoryRepository::class,
            function ($app) {
                return new CategoryRepository();
            });

        $this->app->singleton(ProductRepository::class,
            function ($app) {
                return new ProductRepository();
            });

        $this->app->singleton(SiteFeatureRepository::class,
            function ($app) {
                return new SiteFeatureRepository();
            });
    }
}
