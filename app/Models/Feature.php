<?php namespace Okuma\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
// use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

class Feature extends Model implements SluggableInterface
{
    use Eloquence;
    // use SearchableTrait;
    use SluggableTrait;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'features';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The class name to be used in polymorphic relations.
     *
     * @var string
     */
    // protected $morphClass = 'Feature';

    /**
     * All of the relationships to be touched.
     * would result in too many updates
     * @var array
     */
    // does not work when null
    // protected $touches = ['featureSet'];

    /**
     * The atrributes that detfine this Model's Slug management
     *
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'name',
        'on_update' => true,
    );

    /**
     * Searchable rules.
     * nicolaslopezj/searchable
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'name' => 10,
        ],
    ];

    /**
     * Searchable rules.
     * jarektkaczyk/eloquence
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 20,
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_features', 'feature_id', 'product_id', 'id')->withPivot(['feature_position']);
    }

    public function icon()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function featureSet()
    {
        return $this->belongsTo(FeatureSet::class, 'feature_set_id', 'id');
    }

    public function scopeGetBasicColums($query)
    {
        return $query->get(['features.id', 'features.feature_set_id', 'features.name', 'features.description', 'features.slug', 'features.position']);
    }

    public function scopeGetForMenu($query)
    {
        return $query->get(['features.id', 'features.feature_set_id', 'features.name', 'features.slug', 'features.position']);
    }

    public function productsCount()
    {
        return $this->belongsToMany(Product::class, 'product_features', 'feature_id', 'product_id', 'id')
            ->select('feature_id')
            ->selectRaw('IFNULL(COUNT(product_id), 0) AS value')
            ->groupBy('feature_id');
    }

    public function getProductsCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!array_key_exists('productsCount', $this->relations)) {
            $this->load('productsCount');
        }

        $related = $this->getRelation('productsCount')->first();

        // then return the count directly
        return is_null($related) ? 0 : (int) $related->value;
    }

    public function scopeSelectWithCountForMenu($query, $products = [])
    {
        $query->select('features.id', 'features.name', 'features.slug', 'features.position', 'features.feature_set_id')
        // $query->select('*')
        // ->selectRaw('COUNT(okuma_products.id) AS product_count')
            ->selectRaw('COUNT(product_id) AS product_count')
            ->join('product_features', 'product_features.feature_id', '=', 'features.id')
        // ->join('products', 'products.id', '=', 'product_features.product_id')
        // ->whereNull('products.deleted_at')
        // ->whereDate('products.published_at', '<=', Carbon::now())
            ->whereIn('product_features.product_id', $products)
        // ->whereIn('products.id', $products)
            ->groupBy('features.id')
            ->orderBy('features.position', 'ASC');
    }

    public function scopeSelectFilterForMenu($query, array $productIDs, array $featureSlugs, $activeFeatureSetID = null)
    {

        $bindingsString = implode(',', array_fill(0, count($featureSlugs), '?'));

        $query->select('features.id', 'features.feature_set_id', 'features.name', 'features.slug', 'features.position')
            ->selectRaw('COUNT(okuma_products.id) AS product_count')
            ->selectRaw("IF (okuma_features.slug IN ({$bindingsString}), TRUE, FALSE) AS filter_active", $featureSlugs)
            ->join('product_features', 'product_features.feature_id', '=', 'features.id')
            ->join('products', 'products.id', '=', 'product_features.product_id')
            ->whereNull('products.deleted_at')
            ->whereIn('product_features.product_id', $productIDs)
            ->orWhereIn('features.slug', $featureSlugs);

        if (isset($activeFeatureSetID)) {
            $query->orWhere('features.feature_set_id', '=', $activeFeatureSetID);
        }

        $query->groupBy('features.id')
            ->orderBy('features.position', 'ASC');

        // if (isset($featureSet)) {
        //     $queryWhere
        // }
    }

}
