<?php namespace Okuma\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Okuma\Database\Eloquent\Published;
use Okuma\Models\Image;

class SiteFeature extends Model
{
    use Published;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'site_features';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'published_at', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['published_at', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The class name to be used in polymorphic relations.
     *
     * @var string
     */
    // protected $morphClass = 'SiteFeature';

    public function background()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    // /**
    //  * Get the column size for all screen resolutions.
    //  *
    //  * @param  string  $value
    //  * @return string
    //  */
    // public function getFirstNameAttribute($value)
    // {
    //     switch ($value) {
    //         case 'large-4':
    //             return 'small-12 medium-6 large-4';
    //             break;

    //         case 'large-6':
    //             return 'small-12 medium-6 large-6';
    //             break;

    //         case 'large-8':
    //             return 'small-12 medium-12 large-8';
    //             break;

    //         case 'large-12':
    //             return 'small-12 medium-12 large-12';
    //             break;

    //         default:
    //             return $value;
    //             break;
    //     }
    // }

    public function getImageType()
    {
        $columnSize = $this->column_size;

        $columnClasses = explode(' ', $columnSize);

        $columnSize = array_pop($columnClasses);

        switch ($columnSize) {
            case 'large-4':
                return 'site_feature_4_bg';
                break;

            case 'large-6':
                return 'site_feature_6_bg';
                break;

            case 'large-8':
                return 'site_feature_8_bg';
                break;

            case 'large-12':
                return 'site_feature_12_bg';
                break;

            default:
                return 'site_feature_hero_bg';
                break;
        }
    }

    //     public function calcBGImageWidth()
    // {
    //     $columnSize = $this->column_size;

    //     $columnClasses = $columnSize . split(' ');

    //     $columnSize = array_pop($columnClasses);

    //     switch ($columnSize) {
    //         case 'large-4':
    //             return 400;
    //             break;

    //         case 'large-6':
    //             return 600;
    //             break;

    //         case 'large-8':
    //             return 800;
    //             break;

    //         case 'large-12':
    //             return 1200;
    //             break;

    //         default:
    //             return 1920;
    //             break;
    //     }
    // }

    public function bootstrapColumnSize()
    {

        $bootstrap = $this->column_size;

        $bootstrap = str_replace('small-', 'col-sm-', $bootstrap);
        $bootstrap = str_replace('medium-', 'col-md-', $bootstrap);
        $bootstrap = str_replace('large-', 'col-lg-', $bootstrap);

        return $bootstrap;
    }

}
