<?php namespace Okuma\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Okuma\Database\Eloquent\Published;
use Okuma\Models\Category;
use Okuma\Models\Feature;
use Okuma\Models\Variant;
use Sofa\Eloquence\Eloquence;

// use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
// use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class Product extends Model implements SluggableInterface//, HasMediaConversions

{
    use Eloquence;
    use Published;
    use SluggableTrait;
    use SoftDeletes;

    // use HasMediaTrait;

    public function registerMediaConversions()
    {
        // Perform a resize and filter on images from the images- and anotherCollection collections

        // and save them as png files.
        $this->addMediaConversion('sliderThumbnail')
            ->setManipulations(['w' => 320, 'h' => 197, 'fit' => 'max', 'fm' => 'png'])
            ->performOnCollections('slider-images')
            ->nonQueued();

        $this->addMediaConversion('sliderLarge')
            ->setManipulations(['w' => 1200, 'h' => 740, 'fit' => 'max'])
            ->performOnCollections('slider')
            ->nonQueued();

        $this->addMediaConversion('displayThumbnail')
            ->setManipulations(['w' => 320, 'h' => 320, 'fit' => 'max', 'fm' => 'png'])
            ->performOnCollections('slider')
            ->nonQueued();
    }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'published_at', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['published_at', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The class name to be used in polymorphic relations.
     *
     * @var string
     */
    // protected $morphClass = 'Product';

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    // protected $touches = ['features'];

    /**
     * Searchable rules.
     * jarektkaczyk/eloquence
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 20,
        'slug' => 10
    ];

    /**
     * The atrributes that detfine this Model's Slug management
     *
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'name',
        'on_update' => false,
    );

    public function variants()
    {
        return $this->hasMany(Variant::class, 'product_id', 'id');
    }

    public function features()
    {
        return $this->belongsToMany(Feature::class, 'product_features', 'product_id', 'feature_id')->withPivot('feature_position');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id')->withPivot('product_position');
        // return $this->morphToMany('Okuma\Models\Category', 'categorizable', 'categorizables')->withPivot('position');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function thumbnail()
    {
        return $this->morphOne(Image::class, 'imageable')
            ->select('images.id', 'images.title', 'images.path', 'images.type', 'images.width', 'images.height', 'images.position', 'images.imageable_id', 'images.imageable_type')
            ->where('images.type', '=', 'product_display_thumbnail');
    }

    public function shareImage()
    {
        return $this->morphOne(Image::class, 'imageable')
            ->select('images.id', 'images.title', 'images.path', 'images.type', 'images.width', 'images.height', 'images.position', 'images.imageable_id', 'images.imageable_type')
            ->where('images.type', '=', 'share_image');
    }

    public function sliderImages()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->select('images.id', 'images.title', 'images.path', 'images.type', 'images.width', 'images.height', 'images.position', 'images.imageable_id', 'images.imageable_type')
            ->where('images.type', '=', 'product_large')
            ->orderBy('images.position', 'ASC');
    }

    public function sliderThumbnails()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->select('images.id', 'images.title', 'images.path', 'images.type', 'images.width', 'images.height', 'images.position', 'images.imageable_id', 'images.imageable_type')
            ->where('images.type', '=', 'product_thumbnail')
            ->orderBy('images.position', 'ASC');
    }

    public function variantsCount()
    {
        return $this->hasOne(Variant::class, 'product_id', 'id')
            ->selectRaw('product_id, COUNT(id) AS value')
            ->groupBy('product_id');
    }

    public function getVariantsCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!array_key_exists('variantsCount', $this->relations)) {
            $this->load('variantsCount');
        }

        $related = $this->getRelation('variantsCount');

        // then return the count directly
        return ($related) ? (int) $related->value : 0;
    }

    public function featuresCount()
    {
        return $this->belongsToMany(Feature::class, 'product_features', 'product_id', 'feature_id')
            ->select('product_id')
            ->selectRaw('IFNULL(COUNT(feature_id), 0) AS value')
            ->groupBy('product_id');
    }

    public function getFeaturesCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!array_key_exists('featuresCount', $this->relations)) {
            $this->load('featuresCount');
        }

        $related = $this->getRelation('featuresCount')->first();

        // then return the count directly
        return is_null($related) ? 0 : (int) $related->value;
    }

    public function scopeGetForCatalog($query)
    {
        return $query->get(['products.id', 'products.name', 'products.slug']);
    }

    public function scopeInCategory(Builder $query, $categoryID)
    {
        return $query->distinct()
            ->join('product_categories', 'product_categories.product_id', '=', 'products.id')
            ->join('categories',
                function ($join) {
                    $join->on('product_categories.category_id', '=', 'categories.id')
                        ->whereNull('categories.deleted_at');
                })
            ->join('category_closure',
                function ($join) use ($categoryID) {
                    $join->on('category_closure.descendant', '=', 'categories.id')
                        ->where('category_closure.ancestor', '=', $categoryID);
                })
            ->orderBy('categories.real_depth', 'ASC')
            ->orderBy('categories.position', 'ASC')
            ->orderBy('product_categories.product_position', 'ASC');
    }

    public function assignSlug($source)
    {
        $config = $this->getSluggableConfig();
        $save_to = $config['save_to'];
        $max_length = $config['max_length'];

        $slug = $this->generateSlug($source);

        if (is_string($slug) && $max_length) {
            $slug = substr($slug, 0, $max_length);
        }

        $slugExists = DB::table('products')
            ->where('id', '!=', $this->id)
            ->where($save_to, '=', $slug)
            ->exists();
        if ($slugExists) {
            return null;
        }

        return $slug;
    }
}
