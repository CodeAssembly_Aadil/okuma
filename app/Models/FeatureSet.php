<?php
namespace Okuma\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Nicolaslopezj\Searchable\SearchableTrait;
use Sofa\Eloquence\Eloquence;

class FeatureSet extends Model implements SluggableInterface
{

    use Eloquence;
    // use SearchableTrait;
    use SluggableTrait;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'feature_sets';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
    // protected $guarded = ['id', 'deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    // protected $dates = ['deleted_at'];

    /**
     * The class name to be used in polymorphic relations.
     *
     * @var string
     */
    // protected $morphClass = 'FeatureSet';

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    // protected $touches = [];

    /**
     * The atrributes that detfine this Model's Slug management
     *
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'name',
        'on_update' => true,
    );

    /**
     * Searchable rules.
     * nicolaslopezj/searchable
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'name' => 10,
        ],
    ];

    /**
     * Searchable rules.
     * jarektkaczyk/eloquence
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 20,
    ];

    /**
     * ClosureTable model instance.
     *
     * @var FeatureSetClosure
     */
    protected $closure = FeatureSetClosure::class;

    public function features()
    {
        return $this->hasMany(Feature::class, 'feature_set_id', 'id');
        // ->orderBy('position', 'ASC');
    }

    // public function categories()
    // {
    //     return $this->hasMany(Category::class, 'feature_set_id', 'id');
    // }

    // public function ancestors()
    // {
    //     return $this->belongsToMany(FeatureSet::class, 'feature_set_closure', 'descendant', 'ancestor')
    //                 ->withPivot(['ancestor', 'descendant', 'depth']);
    //     // ->where('depth', '>', 0);
    // }

    // public function decendants()
    // {
    //     return $this->belongsToMany(FeatureSet::class, 'feature_set_closure', 'ancestor', 'descendant')
    //                 ->withPivot(['ancestor', 'descendant', 'depth']);
    //     // ->where('depth', '>', 0);
    // }

    // public function directAncestor()
    // {
    //     return $this->belongsTo(FeatureSet::class, 'parent_id', 'id');
    // }

    // public function directDecendants()
    // {
    //     return $this->hasMany(FeatureSet::class, 'parent_id', 'id');
    //     // ->orderBy('position', 'ASC');
    // }

    public function featuresCount()
    {
        return $this->hasOne(Feature::class, 'feature_set_id', 'id')
            ->selectRaw('feature_set_id, COUNT(id) AS value')
            ->groupBy('feature_set_id');

        // return $this->hasOne(Variant::class)
        //     ->selectRaw('product_id, COUNT(id) AS value')
        //     ->groupBy('product_id');

        // dd($query->toSql());
    }

    public function getFeaturesCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!array_key_exists('featuresCount', $this->relations)) {
            $this->load('featuresCount');
        }

        $related = $this->getRelation('featuresCount');

        // then return the count directly
        return ($related) ? (int) $related->value : 0;
    }

    public function scopeGetChildrenForMenu($query, array $featureSetSlugs)
    {

        $bindingsString = implode(',', array_fill(0, count($featureSetSlugs), '?'));

        return $this->hasMany('Okuma\Models\FeatureSet', 'parent_id', 'id')
            ->select('*')
            ->selectRaw("IF (okuma_feature_sets.slug IN ({$bindingsString}), TRUE, FALSE) AS filter_active", $featureSetSlugs)
            ->orderBy('position', 'ASC')
            ->get();
        // ->orderBy('position', 'ASC');
    }
}
