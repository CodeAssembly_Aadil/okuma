<?php namespace Okuma\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The relationships that should be touched on save.
     *
     * @var array
     */
    protected $touches = ['imageable'];
    // protected $touches = ['product', 'feature', 'sitefeature'];

    /**
     * The class name to be used in polymorphic relations.
     *
     * @var string
     */
    // protected $morphClass = 'Image';

    public function imageable()
    {
        return $this->morphTo();
        // return $this->morphTo('imageable');
    }

    public function relatedImages()
    {
        return $this->belongsToMany(Image::class, 'related_images', 'image_id', 'related_image_id')->withPivot('relation', 'position');
    }

    // public function images()
    // {
    //     return $this->morphMany(Image::class, 'imageable');
    // }

    public function scopeGetBasicColumns($query)
    {
        return $query->get(['images.id', 'images.title', 'images.path', 'images.type', 'images.width', 'images.height', 'images.position']);
    }

    // public function product()
    // {
    //     return $this->morphTo(Product::class, 'imageable');
    // }

    // public function feature()
    // {
    //     return $this->morphTo(Feature::class, 'imageable');
    // }

    // public function sitefeature()
    // {
    //     return $this->morphTo(SiteFeature::class, 'imageable');
    // }

    //  public function feature()
    // {
    //     return $this->morphTo(Feature::class, 'imageable');
    // }
}
