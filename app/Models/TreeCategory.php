<?php
namespace Okuma\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Franzose\ClosureTable\Contracts\EntityInterface;
use Franzose\ClosureTable\Models\Entity;
use Okuma\Models\Category;

/*
    this model is  an ugly hack it excludes the Sofa/Eloquence package to
    avoide conflicts with franzose/ClosureTable with throws erros on the builder extentiosn
    it is currently used to create new Categories and reorder them.
    It targets the same DB entries as Models/Category.php
*/

class TreeCategory extends Entity implements EntityInterface, SluggableInterface
{

    use SluggableTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    // protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $guarded = ['id', 'deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $dates = ['deleted_at'];

    /**
     * The atrributes that detfine this Model's Slug management
     *
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'name',
        'on_update' => true,
    );

    /**
     * ClosureTable model instance.
     *
     * @var CategoryClosure
     */
    protected $closure = CategoryClosure::class;
}
