<?php
namespace Okuma\Models;

// use EspadaV8\ClosureTable\Contracts\ClosureTableInterface;
// use EspadaV8\ClosureTable\Models\ClosureTable;
use Franzose\ClosureTable\Contracts\ClosureTableInterface;
use Franzose\ClosureTable\Models\ClosureTable;

class CategoryClosure extends ClosureTable implements ClosureTableInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category_closure';
}
