<?php namespace Okuma\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Nicolaslopezj\Searchable\SearchableTrait;
use Okuma\Database\Eloquent\Published;
use Sofa\Eloquence\Eloquence;

class Article extends Model implements SluggableInterface
{
    use Eloquence;
    use Published;
    // use SearchableTrait;
    use SluggableTrait;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'articles';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'published_at', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['published_at', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The class name to be used in polymorphic relations.
     *
     * @var string
     */
    // protected $morphClass = 'Article';

    /**
     * Searchable rules.
     * nicolaslopezj/searchable
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'title' => 10,
        ],
    ];

    /**
     * Searchable rules.
     * jarektkaczyk/eloquence
     *
     * @var array
     */
    protected $searchableColumns = [
        'title' => 20,
    ];

    /**
     * The atrributes that detfine this Model's Slug management
     *
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'title',
        'on_update' => true,
    );

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function searchThumbnail()
    {
        return $this->morphOne(Image::class, 'imageable')
            ->select('images.id', 'images.title', 'images.path', 'images.type', 'images.width', 'images.height', 'images.position', 'images.imageable_id', 'images.imageable_type')
            ->where('images.type', '=', 'article_search_thumbnail');
    }

    public function heroBanner()
    {
        return $this->morphOne(Image::class, 'imageable')
            ->select('images.id', 'images.title', 'images.path', 'images.type', 'images.width', 'images.height', 'images.position', 'images.imageable_id', 'images.imageable_type')
            ->where('images.type', '=', 'article_hero_banner');
    }
}
