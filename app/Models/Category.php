<?php
namespace Okuma\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Franzose\ClosureTable\Contracts\EntityInterface;
use Franzose\ClosureTable\Models\Entity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Okuma\Models\Category;
use Okuma\Models\Feature;
use Okuma\Models\FeatureSet;
use Okuma\Models\Product;
// use Nicolaslopezj\Searchable\SearchableTrait;
use Sofa\Eloquence\Eloquence;

class Category extends Entity implements EntityInterface, SluggableInterface
{

    use Eloquence;
    // use SearchableTrait;
    use SluggableTrait;
    use SoftDeletes;

    protected $productCount = 0;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    // protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $guarded = ['id', 'deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $dates = ['deleted_at'];

    /**
     * The class name to be used in polymorphic relations.
     *
     * @var string
     */
    // protected $morphClass = 'Category';

    /**
     * All of the relationships to be touched.
     * would result in too many updates
     * @var array
     */
    // protected $touches = ['products'];

    /**
     * The atrributes that detfine this Model's Slug management
     *
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'name',
        'on_update' => true,
    );

    /**
     * Searchable rules.
     * jarektkaczyk/eloquence
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 20,
        'slug' => 10
    ];

    /**
     * ClosureTable model instance.
     *
     * @var CategoryClosure
     */
    protected $closure = CategoryClosure::class;

    public function heroBanner()
    {
        return $this->morphOne(Image::class, 'imageable');
        // ->select('images.id', 'images.title', 'images.path', 'images.type', 'images.width', 'images.height', 'images.position', 'images.imageable_id', 'images.imageable_type')
        // ->where('images.type', '=', 'category_hero');
        // ->orderBy('images.position', 'asc');
    }

    public function searchThumbnail()
    {
        return $this->morphOne(Image::class, 'imageable')
            ->select('images.id', 'images.title', 'images.path', 'images.type', 'images.width', 'images.height', 'images.position', 'images.imageable_id', 'images.imageable_type')
            ->where('images.type', '=', 'category_search_thumbnail');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_categories', 'category_id', 'product_id')
            ->withPivot(['product_position']);
    }

    public function featureSets()
    {
        return $this->belongsToMany(FeatureSet::class, 'category_features', 'category_id', 'feature_set_id');
    }

    public function features()
    {
        return $this->belongsToMany(Feature::class, 'category_features', 'category_id', 'feature_id');
    }

    public function ancestors()
    {
        return $this->belongsToMany(Category::class, 'category_closure', 'descendant', 'ancestor')
            ->withPivot(['ancestor', 'descendant', 'depth']);
        // ->where('depth', '>', 0);
    }

    public function decendants()
    {
        return $this->belongsToMany(Category::class, 'category_closure', 'ancestor', 'descendant')
            ->withPivot(['ancestor', 'descendant', 'depth']);
    }

    public function rootParent()
    {
        return $this->belongsToMany(Category::class, 'category_closure', 'descendant', 'ancestor')
            ->where('categories.real_depth', '=', 0)
            ->limit(1);
    }

    public function getRootParentAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!array_key_exists('RootParent', $this->relations)) {
            $this->load('RootParent');
        }

        return $this->getRelation('RootParent')->first();
    }

    public function directAncestor()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function directDecendants()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
        // ->orderBy('position', 'ASC');
    }

    public function productsCount()
    {
        return $this->belongsToMany(Product::class, 'product_categories', 'category_id', 'product_id')
            ->select('category_id')
            ->selectRaw('IFNULL(COUNT(product_id), 0) AS value')
            ->groupBy('category_id');
    }

    public function getProductsCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!array_key_exists('productsCount', $this->relations)) {
            $this->load('productsCount');
        }

        $related = $this->getRelation('productsCount')->first();

        // then return the count directly
        return is_null($related) ? 0 : (int) $related->value;
    }

    public function getTotalProductsCountAttribute()
    {
        return $this->productCount;
    }

    public function setTotalProductsCountAttribute($value)
    {
        if (isset($value) && is_int($value) && $value > 0) {
            $this->productCount = $value;
        }
    }

    public function fetchTotalProductsCount()
    {
        $id = $this->id;

        $productCount = DB::table('categories')
            ->selectRaw('COUNT(DISTINCT okuma_products.id) AS product_count')
            ->join('category_closure', 'category_closure.descendant', '=', 'categories.id')
            ->join('product_categories', 'categories.id', '=', 'product_categories.category_id')
            ->join('products', 'products.id', '=', 'product_categories.product_id')
            ->where('category_closure.ancestor', '=', $id)
            ->whereNull('categories.deleted_at')
            ->whereNull('products.deleted_at')
            ->whereDate('products.published_at', '<=', Carbon::now())
            ->first();

        $this->setTotalProductsCountAttribute($productCount->product_count);

        return $productCount->product_count;
    }

    public static function fetchAllTotalProductCounts()
    {
        $results = DB::table('products')
            ->select('category_closure.ancestor AS category_id', DB::raw('COUNT(DISTINCT okuma_products.id) AS product_count'))
            ->join('product_categories', 'product_categories.product_id', '=', 'products.id')
            ->join('categories', 'product_categories.category_id', '=', 'categories.id')
            ->join('category_closure', 'category_closure.descendant', '=', 'categories.id')
            ->whereNull('categories.deleted_at')
            ->whereNull('products.deleted_at')
            ->whereDate('products.published_at', '<=', Carbon::now())
            ->groupBy('category_closure.ancestor')
            ->get();

        $categoryProductCounts = [];

        foreach ($results as $row) {
            $categoryProductCounts[$row->category_id] = $row->product_count;
        }

        return $categoryProductCounts;
    }
}
