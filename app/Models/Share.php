<?php namespace Okuma\Models;

class Share
{
    protected $title = "";
    protected $description = "";
    protected $type = "";
    protected $image = "";
    protected $link = "";

    public function __construct($title, $description, $type, $image, $link)
    {
        if (isset($title)) {
            $this->title = $title;
        }

        if (isset($description)) {
            $this->description = $description;
        }

        if (isset($type)) {
            $this->type = $type;
        }

        if (isset($image)) {
            $this->image = $image;
        }

        if (isset($link)) {
            $this->link = $link;
        }
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function getImage()
    {
        return $this->image;
    }
}
