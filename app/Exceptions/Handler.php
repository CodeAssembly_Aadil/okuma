<?php

namespace Okuma\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Config;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

        if ($this->isHttpException($e)) {

            if (view()->exists('errors.' . $e->getStatusCode())) {
                return $this->renderHttpException($e);
            } else if (!Config::get('app.debug')) {
                return response()->view('errors.500', [], 500);
            }

            return $this->renderHttpException($e);
        }

        if ($e instanceof TokenMismatchException) {
            return redirect($request->fullUrl())->withError('csrf_error', "Opps! Seems you couldn't submit form for a longtime. Please try again");
        }

        if (!Config::get('app.debug')) {
            return response()->view('errors.500', [], 500);
        }

        return parent::render($request, $e);
    }
}
