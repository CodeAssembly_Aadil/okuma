
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
     */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'mailchimp' => [
        'secret' => env('MAILCHIMP_SECRET', ''),
        'base_uri' => env('MAILCHIMP_BASE_URI', ''),
        'username' => env('MAILCHIMP_USERNAME', 'okuma'),
        'list_id' => env('MAILCHIMP_LIST_ID', ''),
    ],

    // 'grecaptcha' => [
    //     'secret' => '6LeNBgkTAAAAAKJldbaMDfEI1bio_coSH2Hh4QCc',
    //     'key' => '6LeNBgkTAAAAAMmCYoLU7Ps8o7-pARhJ9gNJxLpn',
    // ],

    'ses' => [
        'key' => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model' => Okuma\User::class,
        'key' => '',
        'secret' => '',
    ],

];
