<?php

return [
    // Minutes
    'cache_duration' => env('CACHE_DURATION', 30),
    // Kilobytes
    'max_file_size' => env('MAX_FILE_SIZE', 2000),

    'contact' => [
        'email' => 'info@okuma.co.za',
        'error' => 'admin@okuma.co.za',
        'tel' => '+27 12 327 1301',
        // 'name' => 'Okuma | Inspired Fishing Southern Africa',
        // 'name' => 'Okuma | Southern Africa',
        'name' => 'Okuma | Inspired Fishing ',
    ],

    'default_images' => [
        'feature_icon' => 'images/feature/default_feature_icon.png',
        'product_display_thumbnail' => 'images/product/default_product_thumb.png',
        // 'product_thumbnail' => 'images/product/default_product_thumb_320x320.png',
        // 'product_thumbnail' => 'images/product/default_product_thumb.png',
        'search_thumbnail' => 'images/search/default_search_thumb.png',
    ],

    'image_directories' => [
        'Okuma\Models\Category' => 'images/category/',
        'Okuma\Models\Feature' => 'images/feature/',
        'Okuma\Models\Product' => 'images/product/{ID}/',
        'Okuma\Models\SiteFeature' => 'images/sitefeature/',
        'search_thumbnail' => 'images/search/',
    ],

    'imageable_types' => [
        'Okuma\Models\Category',
        'Okuma\Models\Feature',
        'Okuma\Models\Product',
        'Okuma\Models\SiteFeature',

        // 'search_thumbnail',
    ],

    'image_types' => [
        'Okuma\Models\Category' => ['category_hero'],
        'Okuma\Models\Feature' => ['feature_icon'],
        'Okuma\Models\Product' => ['product_display_thumbnail', 'product_large', 'share_image'], //, 'product_thumbnail'],
        'Okuma\Models\SiteFeature' => ['site_feature_hero_bg', 'site_feature_12_bg', 'site_feature_8_bg', 'site_feature_6_bg', 'site_feature_4_bg'],
        // 'search_thumbnail',
    ],

    // Map images sizes to image type

    'image_sizes' => [
        'category_hero' => ['width' => 1920, 'height' => 560],

        'feature_icon' => ['width' => 320, 'height' => 320],

        // 'product_display_thumbnail' => ['width' => 320, 'height' => 197],
        'product_display_thumbnail' => ['width' => 320, 'height' => 320],
        'product_large' => ['width' => 1200, 'height' => 740],
        'product_thumbnail' => ['width' => 320, 'height' => 197],

        'share_image' => ['width' => 1200, 'height' => 900],

        'search_thumbnail' => ['width' => 70, 'height' => 70],

        'site_feature_hero_bg' => ['width' => 1920, 'height' => 714],
        'site_feature_12_bg' => ['width' => 1200, 'height' => 400],
        'site_feature_8_bg' => ['width' => 800, 'height' => 400],
        'site_feature_6_bg' => ['width' => 600, 'height' => 400],
        'site_feature_4_bg' => ['width' => 400, 'height' => 400],
    ],

    'queues' => [
        'email' => 'email_queue',
    ],
];
