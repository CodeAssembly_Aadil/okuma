//https://github.com/kriskbx/whatsapp-sharing

(function() {

    if ($(".share-bar").length === 1) {
        init();
    }

    function init() {
        if (isMobile()) {
            initWhatsAppButton();
        }

        $(".share-bar").css("display", "block");
        $(".share-bar").on("click", "a.share-button", share);
    }

    function paramReplace(uri, name, value) {
        // Find the param with regex
        // Grab the first character in the returned uri (should be ? or &)
        // Replace our href uri with our new value, passing on the name and delimeter

        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        var matches = regex.exec(uri);
        var newString;

        if (matches === null) {
            // if there are no params, append the parameter
            newString = uri + '?' + name + '=' + value;
        } else {
            var delimeter = matches[0].charAt(0);
            newString = uri.replace(regex, delimeter + name + "=" + value);
        }

        return newString;
    }

    function share(event) {
        event.preventDefault();

        var self = $(this);
        var url = self.attr("href");

        if (self.data("nc") === 1) {
            url = paramReplace(url, "nc", new Date().getTime());
        }

        popupCenter(url, self.find(".title").html(), 580, 540);
    }

    function popupCenter(url, title, w, h) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 3) - (h / 3)) + dualScreenTop;

        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    };

    function isMobile() {
        return ((navigator.userAgent.match(/Android|iPhone/i) && !navigator.userAgent.match(/iPod|iPad/i)) ? true : false);
    }

    function initWhatsAppButton() {
        var button = $(".share-bar a.share-button.whatsapp");
        // use the title as the message text
        var data = "?text=" + encodeURIComponent("Check this out: " + $("title").html());
        // use the current url as the link
        data += "%20%0A%0A" + encodeURIComponent(document.URL);

        button.attr("href", button.attr("href") + data);
        button.attr("target", "_blank");

        button.css("display", "inline-block");
    }

})();
