(function() {

    var currentFrame = null;
    var nextFrame = null;
    var progress = 1;

    function init() {
        if ($("#start").length !== 0) {
            // console.log("start");

            currentFrame = $("#start");

            $(".progress-bar").on("click", "a", onChangeSlide);
            $("main").on("click", ".next-btn", onChangeSlide);
        } else {
            console.log("end");
        }
    }

    function onChangeSlide(event) {
        console.log(event);
        var button = $(event.currentTarget);
        var frameID = button.attr("href");

        gotoFrame(frameID);
    }

    function gotoFrame(frame) {

        nextFrame = $(frame);


        nextFrame.css({
            // "left": "100%",
            // "top": "-100%",
            "display": "block",
            // "z-index": "100",
            // "opacity": "0"
        });

        // var queue = "q_" + (new Date()).valueOf();

        // currentFrame.slideUp(300);
        // nextFrame.fadeOut(0).slideUp(0).slideDown(300);
        currentFrame.slideDown(0).slideUp({
            "duration" : 300,
            // "queue": queue
        });

        // currentFrame.slideUp(300);

        nextFrame.slideUp(0).delay(300).slideDown({
            "duration" : 300,
            // "queue": queue,
            "complete": transitionComplete

        });


        // nextFrame.slideUp(0).slideDown(300);

        // nextFrame.animate({
        //     "opacity": "1"
        // }, 300, 'linear', transitionComplete);
    }

    function transitionComplete() {

        currentFrame.css({
            "display": "none"
        });

        currentFrame = nextFrame;
        nextFrame = null;

        setProgressBar(currentFrame.attr("id"));
    }

    function setProgressBar(frameID) {
        console.log(frameID);
        $(".progress-bar .active").removeClass('active');
        $(".progress-bar a[href=\"#" + frameID + "\"]").addClass("active");
    }

    init();

})();
