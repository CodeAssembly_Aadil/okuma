// Contact page inputs

(function() {


    var _Request = null;
    var _TimeOut = null;

    var _ErrorMessages = {
        "contact-name.required": "<em>OOPS!</em> Please tell us who you are",

        "contact-email.required": "<em>OOPS!</em> Let us know how to get in touch with you.",
        "contact-email.email": "<em>OOPS!</em> The <em>Email Address</em> doesn\"t seem to be valid.",

        "contact-subject.required": "<em>OOPS!</em> Please give your message a brief <em>Subject</em>.",
        "contact-subject.min": "<em>OOPS!</em> Please be a bit more descriptive with the <em>Subject</em> line.",
        "contact-subject.max": "<em>WOOPS!</em> The <em>Subject</em> line seems a bit too long.",

        "contact-message.required": "<em>OOPS!</em> Please tell us whats on your mind.",
        "contact-message.min": "<em>OOPS!</em> Please tell us a bit more.",

        "server-error": "<em>WOOPS!</em> the server encouneted an error so your message could not be sent. Please try again.",
    };

    var _ErrorContainers = {
        "contact-form": "#error-form",
        "contact-name": "#error-name",
        "contact-email": "#error-email",
        "contact-subject": "#error-subject",
        "contact-message": "#error-message",
        "contact-time": "#error-form",
    };

    if ($(".contact-page").length === 1) {
        init();
    }

    function init() {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
            }
        });

        $("#contact-form .input-field").each(
            function() {
                if ($.trim($(this).val()) !== "") {
                    $(this).addClass("isFilled");
                }
            }
        );

        $("#contact-form .input-field").on("focus",
            function(event) {
                $(event.target).addClass("isFilled");
            }
        );

        $("#contact-form .input-field").on("blur",
            function(event) {
                if ($.trim($(event.target).val()) === "") {
                    $(event.target).removeClass("isFilled");
                }
            });

        // $("#contact-submit").on("click", submitForm);
        // $(".button").on("click", Foundation.utils.debounce(submitForm, 300, true));
        $("#contact-form").on("submit", Foundation.utils.debounce(submitForm, 300, true));
        // $(".button").on("click", Foundation.utils.debounce(submitForm, 300, true));

        $("#contact-name").on("blur", validateName);
        $("#contact-email").on("blur", validateEmail);
        $("#contact-subject").on("blur", validateSubject);
        $("#contact-message").on("blur", validateMessage);

        $("#contact-form .input-field").on("focus", onFocus);
    }

    function submitForm(event) {

        event.cancelBubble = true;
        event.returnValue = false;
        event.stopPropagation();
        event.preventDefault();

        if (validateName() && validateEmail() && validateSubject() && validateMessage()) {

            $("#contact-submit").addClass("progress");

            clearAllMessages();

            var url = $("#contact-form").attr("action");
            var method = $("#contact-form").attr("method");
            var data = $("#contact-form").serialize();

            submitRequest(url, method, data);

        } else {
            $("#contact-submit").removeClass("shake animated");

            $("#contact-submit").addClass("shake animated").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                function(event) {
                    $(this).removeClass("shake animated");
                });
        }

        return true;
    }

    function onFocus(event) {
        var input = $(event.target);
        var value = input.val();

        clearMessage(_ErrorContainers[input.attr("name")]);
    }

    function validateName(event) {
        var value = $("#contact-name").val();
        if (isNullOrWhitespace(value)) {
            if (!(event && event.type === "blur")) {
                showMessage("#error-name", _ErrorMessages["contact-name.required"]);
            }
            return false;
        }

        clearMessage("#error-name");
        return true;
    }

    function validateEmail(event) {
        var value = $("#contact-email").val();

        if (isNullOrWhitespace(value)) {
            if (!(event && event.type === "blur")) {
                showMessage("#error-email", _ErrorMessages["contact-email.required"]);
            }
            return false;
        } else if (!(/.+@.+\..+/i).test(value)) {
            showMessage("#error-email", _ErrorMessages["contact-email.email"]);
            return false;
        }

        clearMessage("#error-email");
        return true;
    }

    function validateSubject(event) {
        var value = $("#contact-subject").val();

        if (isNullOrWhitespace(value)) {
            if (!(event && event.type === "blur")) {
                showMessage("#error-subject", _ErrorMessages["contact-subject.required"]);
            }
            return false;
        } else if (value.length < 3) {
            showMessage("#error-subject", _ErrorMessages["contact-subject.min"]);
            return false;
        } else if (value.length > 70) {
            showMessage("#error-subject", _ErrorMessages["contact-subject.max"]);
            return false;
        }

        clearMessage("#error-subject");
        return true;
    }

    function validateMessage(event) {
        var value = $("#contact-message").val();

        if (isNullOrWhitespace(value)) {
            if (!(event && event.type === "blur")) {
                showMessage("#error-message", _ErrorMessages["contact-message.required"]);
            }
            return false;
        } else if (value.length < 11) {
            showMessage("#error-message", _ErrorMessages["contact-message.min"]);
            return false;
        }

        clearMessage("#error-message");
        return true;
    }

    function submitRequest(url, method, data) {

        if (_Request !== null)
            _Request.abort();

        _Request = $.ajax({
            type: method,
            url: url,
            data: data,
            dataType: "json",
            cache: false,
            error: onAjaxError,
            success: onAjaxSuccess,
            complete: onAjaxComplete
        });
    }

    function onAjaxSuccess(data, textStatus, jqXHR) {

        clearAllMessages();

        if (window.console) {
            console.log("onAjaxSuccess( data:", data, ", textStatus:", textStatus, ", jqXHR:", jqXHR, ")");
        }

        if (textStatus == "success") {
            $(".isFilled").removeClass("isFilled").val("");
            clearAllMessages();
            submitButtonSuccess();
        }
    }

    function onAjaxComplete(jqXHR, textStatus) {
        $("#contact-submit").removeClass("progress");
        clearTimeout(_TimeOut);
        _TimeOut = setTimeout(submitButtonReset, 5000);
    }

    function onAjaxError(jqXHR, textStatus, errorThrown) {

        if (textStatus === "error") {

            if (window.console) {
                console.log("onAjaxError( jqXHR:", jqXHR, ", textStatus:", textStatus, ", errorThrown:", errorThrown, ")");
            }

            switch (jqXHR.status) {
                case 422:
                    var errors = jqXHR.responseJSON;

                    for (var fieldName in errors) {
                        var message = errors[fieldName][0];
                        var container = _ErrorContainers[fieldName];

                        showMessage(container, message);
                    }
                    break;

                default:
                    showMessage("#error-form", _ErrorMessages["server-error"]);
                    break;
            }

            submitButtonError();
        }
    }

    function showMessage(container, message) {
        $(container).html("<span class=\"feedback-message\"><i class=\"icon-attention-circle text-alert\"></i> " + message + "</span>");
    }

    function submitButtonReset() {
        $("#contact-submit .icon").attr("class", "icon");
        $("#contact-submit").removeClass("progress success error");
    }

    function submitButtonProgress() {
        submitButtonReset();
        $("#contact-submit").addClass("progress");
    }

    function submitButtonError() {
        submitButtonReset();
        $("#contact-submit .icon").addClass("icon-cross");
        $("#contact-submit").addClass("error");
    }

    function submitButtonSuccess() {
        submitButtonReset();
        $("#contact-submit .icon").addClass("icon-tick");
        $("#contact-submit").addClass("success");
    }

    function clearMessage(container) {
        $(container).html("");
    }

    function clearAllMessages(container) {
        for (var fieldName in _ErrorContainers) {
            var container = _ErrorContainers[fieldName];
            clearMessage(container);
        }
    }

    function isNullOrWhitespace(value) {
        if (value == null)
            return true;

        return !(/\S/.test(value));
    }

})();
