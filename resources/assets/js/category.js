(function() {

    var _FilterForm = null;
    var _Request = null;
    var _TimeOut = null;
    var _Method = null;

    var _CategoryRoute = null;
    var _ProductRoute = null;
    var _BasePath = null;

    var _ProductBlockTemplate = "";
    var _DefaultImage = "";

    if ($(".category-page").length === 1) {
        init();
    }

    function init() {

        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
            }
        });

        _ProductBlockTemplate += "<article class=\"small-6 medium-4 columns\" itemscope itemtype=\"http://schema.org/Product\">";
        _ProductBlockTemplate += "   <a href=\"{productURL}\" class=\"product-thumb\" itemprop=\"url\">";
        _ProductBlockTemplate += "      <img itemprop=\"image\" src=\"{imagePath}\" alt=\"{imageTitle}\" width=\"{imageWidht}\" height=\"{imageHeight}\">";
        _ProductBlockTemplate += "      <div class=\"product-thumb-details\">";
        _ProductBlockTemplate += "         <h1 itemprop=\"name\" class=\"product-thumb-title\">{productName}</h1>";
        _ProductBlockTemplate += "         <div class=\"product-thumb-models\" style=\"display:{showVariants}\">{numVariants} Models</div>";
        _ProductBlockTemplate += "      </div>";
        _ProductBlockTemplate += "   </a>";
        _ProductBlockTemplate += "</article>";

        _CategoryRoute = $("meta[name=\"category-route\"]").attr("content");
        _ProductRoute = $("meta[name=\"product-route\"]").attr("content");

        _DefaultImage = {
            path: $("meta[name=\"default-thumbnail\"]").attr("content"),
            width: $("meta[name=\"default-thumbnail\"]").data("width"),
            height: $("meta[name=\"default-thumbnail\"]").data("height"),
        };

        _BasePath = $("meta[name=\"default-thumbnail\"]").data("base-path");

        _FilterForm = $("#product-filter-form");
        _FilterForm.find("button[type=\"submit\"]").hide();

        $(".accordion-navigation .group-header").on("click", toogleAccordion);
        _FilterForm.on("click", "input[type=\"checkbox\"]", Foundation.utils.debounce(filter, 300, true))

        window.getData = getData;

    }

    function toogleAccordion(event) {
        var header = $(event.currentTarget);
        var content = header.next();

        if (content.hasClass("active")) {
            content.slideUp(250);
        } else {
            content.slideDown(250);
        }
    }

    function filter(event) {
        _FilterForm.submit();
    }


    function getData() {
        submitRequest(buildUrl(), 'GET', null);
    }

    function buildUrl() {
        var url = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname;

        return url;
    }


    function submitRequest(url, method, data) {

        if (_Request !== null)
            _Request.abort();

        _Request = $.ajax({
            type: method,
            url: url,
            data: data,
            dataType: "json",
            cache: false,
            error: onAjaxError,
            success: onAjaxSuccess,
            complete: onAjaxComplete
        });
    }

    function onAjaxSuccess(data, textStatus, jqXHR) {

        if (window.console) {
            console.log("onAjaxSuccess( data:", data, ", textStatus:", textStatus, ", jqXHR:", jqXHR, ")");
        }

        removeProducts();
        setTimeout(addProducts, 300, data.products.data);
    }

    function onAjaxComplete(jqXHR, textStatus) {
        // $("#contact-submit").removeClass("progress");
        // clearTimeout(_TimeOut);
        // _TimeOut = setTimeout(submitButtonReset, 5000);
    }

    function onAjaxError(jqXHR, textStatus, errorThrown) {

        if (textStatus === "error") {

            if (window.console) {
                console.log("onAjaxError( jqXHR:", jqXHR, ", textStatus:", textStatus, ", errorThrown:", errorThrown, ")");
            }

            // switch (jqXHR.status) {
            //     case 422:
            //         var errors = jqXHR.responseJSON;

            //         for (var fieldName in errors) {
            //             var message = errors[fieldName][0];
            //             var container = _ErrorContainers[fieldName];

            //             showMessage(container, message);
            //         }
            //         break;

            //     default:
            //         showMessage("#error-form", _ErrorMessages["server-error"]);
            //         break;
            // }

            // submitButtonError();
        }
    }

    function removeProducts() {
        var rows = $("#category-products .row");
        var products = $("#category-products article");

        for (var i = products.length - 1; i >= 0; i--) {
            var product = $(products.get(i));
            product.fadeOut(149 * Math.random() + 150);
        };

        setTimeout(function() {
            $("#category-products .row").remove();
        }, 299);
    }

    function addProducts(products) {

        var html = "";

        for (var i = 0, l = products.length; i < l; i++) {

            var product = createProductBlock(products[i]);
            // product.fadeOut(75 * Math.random() + 75);

            var m = i % 3;

            if (m === 0) {
                html += "<div class=\"row\">"
            }

            html += product;

            if (m === 2 || i === l - 1) {
                html += "</div>"
            }
        }

        $("#category-products").append(html);

        var products = $("#category-products article");

        for (var i = products.length - 1; i >= 0; i--) {
            var product = $(products.get(i));
            product.fadeOut(0).fadeIn(150 * Math.random() + 150);
        };
    }

    function createProductBlock(product) {

        var template = _ProductBlockTemplate;

        var productURL = _ProductRoute.replace(new RegExp("-slug-", "gm"), product.slug);

        template = template.replace(new RegExp("{productURL}", "gm"), productURL);
        template = template.replace(new RegExp("{productName}", "gm"), product.name);

        if (product.variants_count !== null && product.variants_count.value > 1) {
            template = template.replace(new RegExp("{numVariants}", "gm"), product.variants_count.value);
            template = template.replace(new RegExp("{showVariants}", "gm"), "block");
        } else {
            template = template.replace(new RegExp("{showVariants}", "gm"), "none");
        }

        if (product.thumbnail !== null) {
            template = template.replace(new RegExp("{imagePath}", "gm"), _BasePath + product.thumbnail.path);
            template = template.replace(new RegExp("{imageTitle}", "gm"), product.thumbnail.title);
            template = template.replace(new RegExp("{imageWidth}", "gm"), product.thumbnail.width);
            template = template.replace(new RegExp("{imageHeight}", "gm"), product.thumbnail.height);
        } else {
            template = template.replace(new RegExp("{imagePath}", "gm"), _DefaultImage.path);
            template = template.replace(new RegExp("{imageTitle}", "gm"), product.name);
            template = template.replace(new RegExp("{imageWidth}", "gm"), _DefaultImage.width);
            template = template.replace(new RegExp("{imageHeight}", "gm"), _DefaultImage.height);
        }

        return template;
    }


})();
