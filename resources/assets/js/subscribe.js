// Contact page inputs

(function() {

    $("#subscription-form .input-field").each(
        function() {
            if ($.trim($(this).val()) !== "") {
                $(this).addClass("isFilled");
            }
        }
    );

    $("#subscription-form .input-field").on("focus",
        function(event) {
            $(event.target).addClass("isFilled");
        }
    );

    $("#subscription-form .input-field").on("blur",
        function(event) {
            if ($.trim($(event.target).val()) === "") {
                $(event.target).removeClass("isFilled");
            }
        }
    );
})();
