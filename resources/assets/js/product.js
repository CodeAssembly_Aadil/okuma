// Slick slider config

(function() {

    var NEXT;
    var PREVIOUS;
    var SLIDER;
    var SLIDER_NAV;
    var SLIDE_THUMB_CACHE;

    if ($(".product-page").length === 1) {
        init();
    }

    function init() {

        if ($(".product-page .product-image-slider").length === 0)
            return;

        SLIDER = $(".product-image-slider").slick({
            arrows: true,
            asNavFor: ".product-image-slider-nav",
            centerMode: false,
            dots: false,
            infinite: true,
            lazyLoad: "ondemand",
            nextArrow: $(".slider-control.next"),
            prevArrow: $(".slider-control.previous"),
            slide: ".product-image-slider .product-image-slide",
            slidesToScroll: 1,
            slidesToShow: 1,

            responsive: [{
                breakpoint: 642,
                settings: {
                    // slidesToShow: 3,
                    // slidesToScroll: 3,
                    dots: true
                }
            }]
        });

        SLIDER_NAV = $(".product-image-slider-nav").slick({
            asNavFor: ".product-image-slider",
            centerMode: false,
            dots: false,
            focusOnSelect: true,
            lazyLoad: "progressive",
            slide: ".product-image-slider-nav .product-image-slide",
            slidesToScroll: 1,
            slidesToShow: 5,
        });

        SLIDE_THUMB_CACHE = $(".product-image-slider-nav .product-image-slide img.product-image");

        if (SLIDE_THUMB_CACHE.length > 1) {
            NEXT = $(".slider-control.next span");
            PREVIOUS = $(".slider-control.previous span");
            updateControls(null, null, 0);
            $(".product-image-slider").on("afterChange", updateControls);

            onResize();

            $(window).on('resize', Foundation.utils.throttle(onResize, 300));
        }
    }

    function updateControls(event, slick, currentSlide) {

        var nextIdx = currentSlide + 1;
        var previousIdx = currentSlide - 1;

        if (nextIdx >= SLIDE_THUMB_CACHE.length) {
            nextIdx = 0;
        }

        if (previousIdx < 0) {
            previousIdx = SLIDE_THUMB_CACHE.length - 1;
        }

        var nextImage = $(SLIDE_THUMB_CACHE.get(nextIdx));
        NEXT.css("background-image", "url(" + (nextImage.attr("src") || nextImage.attr("data-lazy")) + ")");

        var previousImage = $(SLIDE_THUMB_CACHE.get(previousIdx));
        PREVIOUS.css("background-image", "url(" + (previousImage.attr("data-lazy") || previousImage.attr("src")) + ")");
    }

    function onResize(event) {
        var slides = SLIDER.find(".product-image-slide");
        var slideWidth = $(slides.get(0)).css("width");
        var slideHeight = parseInt(slideWidth) / 1200 * 740;
        slides.css("height", slideHeight + "px");
    }

})();
