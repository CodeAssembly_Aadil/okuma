(function() {

    if ($("#published_at_datepicker").length > 0) {
        edit();
    } else if ($("#sitefeatures-reorder-form").length > 0) {
        layout()
    }

    function edit() {

        $("#published_at_datepicker").datetimepicker({
            format: "YYYY-MM-DD HH:mm:ss", //2015-07-17 16:54:10
            icons: {
                time: "fa fa-fw fa-lg fa-clock-o",
                date: "fa fa-fw fa-lg fa-calendar",
                up: "fa fa-fw fa-lg fa-chevron-up",
                down: "fa fa-fw fa-lg fa-chevron-down",
                previous: "fa fa-fw fa-lg fa-chevron-left",
                next: "fa fa-fw fa-lg fa-chevron-right",
                today: "fa fa-fw fa-lg fa-crosshairs",
                clear: "fa fa-fw fa-lg fa-trash-o",
            },
            showTodayButton: true,
            showClear: true,
        });
    }

    function layout() {

        $("#sitefeatures-reorder-form button").click(
            function(event) {

                var orderables = $("#sitefeature-container .sortable-item");

                var data = {
                    "orderables": []
                };

                for (var i = 0, l = orderables.length; i < l; i++) {

                    var item = $(orderables[i]);
                    var id = item.data("id");
                    // var relatedID = item.data("related-id");
                    var position = i + 1;

                    data.orderables.push({
                        "id": id,
                        "position": position
                    });
                }

                $("#sitefeatures-reorder-form input[name=\"orderables\"]").val(JSON.stringify(data));

                return true;
            });
    }


})();
