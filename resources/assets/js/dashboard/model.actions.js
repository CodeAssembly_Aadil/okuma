(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("meta[name=\"csrf-token\"]").attr('content')
        }
    });

    var _Request = null;
    var _Button;
    var _ProcessingAction;

    var _NotifySettings = {
        element: 'body',
        position: 'fixed',
        type: 'info',
        allow_dismiss: true,
        newest_on_top: true,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        offset: {
            x: 20,
            y: 70
        },
        spacing: 10,
        z_index: 1031,
        delay: 3500,
        timer: 1000,
        url_target: '_blank',
        mouse_over: 'pause',
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}">' + '<button type="button" class="close" data-notify="dismiss"><span class="fa fa-times"></span></button>' + '<span data-notify="icon"></span>' + '<span data-notify="title"> <strong>{1}</strong></span>' + '<span data-notify="message">{2}</span>' + '</div>'
    };

    $("#content").on("click", ".model-actions .btn", modelActionsButtonHandler);

    $("button[data-action=\"datatable-refresh\"]").click(
        function(event) {
            if ($("#dataTableBuilder").length > 0) {
                var table = $("#dataTableBuilder").DataTable();
                table.ajax.reload(null, false);
            }
        });

    $("button[data-action=\"datatable-reset\"]").click(
        function(event) {
            if ($("#dataTableBuilder").length > 0) {

                var table = $("#dataTableBuilder").DataTable();
                table.state.clear();
                table.ajax.reload(null);
            }
        });

    function showMessage(icon, title, message, type) {
        _NotifySettings.type = type;

        $.notify({
            icon: icon,
            title: title,
            message: message,
            url: null,
            target: null
        }, _NotifySettings);
    }

    function modelActionsButtonHandler(event) {

        _Button = $(event.target);

        var action = _Button.data("action");
        var process = _Button.data("process");

        if (!process) {
            // if (process === 'false') {
            if (_Request !== null) {
                _Request.abort();
            }
        } else {
            event.stopPropagation();
            event.preventDefault();

            if (!_ProcessingAction) {
                // if (!_ProcessingAction  === 'true') {

                if (action === 'destroy') {

                    bootbox.confirm({
                        message: "Permanently Delete? This action cannot be undone.",
                        buttons: {
                            confirm: {
                                label: 'Delete',
                            }
                        },
                        callback: onConfrimDelete
                    });

                } else {
                    showButtonProgress(_Button);
                    submitRequest(_Button.attr('href'), _Button.data("method"), null);
                }
            }
        }
    }

    function onConfrimDelete(result) {
        if (result) {
            showButtonProgress(_Button);
            submitRequest(_Button.attr('href'), _Button.data("method"), null);
        }
    }

    function showButtonProgress(button) {
        var url = button.attr('href');
        var method = button.data("method");
        var icon = button.children('.fa');

        icon.data('default-class', icon.attr('class'));
        icon.attr('class', 'fa fa-fw fa-lg fa-cog fa-spin');
        button.addClass('disabled');
    }

    function resetButtonProgress(button) {
        var url = button.attr('href');
        var method = button.data("method");
        var icon = button.children('.fa');

        icon.attr('class', icon.data('default-class'));
        button.removeClass('disabled');
    }

    function submitRequest(url, method, data) {

        if (_Request !== null)
            _Request.abort();

        _Request = $.ajax({
            type: method,
            url: url,
            data: data,
            dataType: "json",
            cache: false,
            error: onAjaxError,
            success: onAjaxSuccess,
            complete: onAjaxComplete
        });

        _ProcessingAction = true;
    }

    function onAjaxSuccess(data, textStatus, jqXHR) {
        showMessage(data.icon, data.title, data.message, data.level);

        if ($("#dataTableBuilder").length) {
            var table = $("#dataTableBuilder").DataTable();
            table.ajax.reload(null, false);
        } else {
            window.location.reload(true);
        }
    }

    function onAjaxComplete(jqXHR, textStatus) {
        resetButtonProgress(_Button);
        _ProcessingAction = false;
    }

    function onAjaxError(jqXHR, textStatus, errorThrown) {

        if (textStatus == "error") {
            if (window.console) {
                console.log("onAjaxError( jqXHR:", jqXHR, ", textStatus:", textStatus, ", errorThrown:", errorThrown, ")");
            }

            if (jqXHR.status === 500) {
                showMessage("fa fa-exclamation-triangle", "Internal Server Error", "The application has encounted an <strong>Internal Server Error</strong> if you continue to experience this error, please report it to your Systems Admninistrator", "danger");
            } else {
                // var response = jqXHR.responseJSON;
                showMessage("fa fa-exclamation-triangle", "Error: " + jqXHR.status, " " + jqXHR.statusText, "warning");
            }
        }
    }

})();
