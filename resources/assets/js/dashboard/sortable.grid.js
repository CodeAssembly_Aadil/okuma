(function() {

    var placeholderStyle = $("<style type=\"text/css\"></style>");

    $("head").append(placeholderStyle);

    $(".sortable-grid").sortable({
        group: "",
        draggedClass: "is-dragging",
        containerSelector: ".sortable-grid",
        delay: 100,
        nested: false,
        vertical: false,
        itemSelector: ".sortable-item",
        placeholderClass: "sortable-item-placeholder",
        placeholder: "<div class=\"sortable-item-placeholder\"></div>",
        pullPlaceholder: true,
        onDragStart: onDragStart,
        // onDrop: onDrop,
        tolerance: -5,
    });

    function onDragStart(item, container, _super, event) {
        placeholderStyle.html(".sortable-item-placeholder { width:" + item.outerWidth() + "px; height:" + item.outerHeight() + "px; margin:" + item.css('margin') + ";");
        _super(item, container, _super, event);
    }
})();
