(function() {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
        }
    });

    switch ($('.nav.nav-tabs').data('active-tab')) {

        case 'images':
            images();
            break;

        case 'variants':
            variants();
            break;

        case 'features':
            features();
            break;

        case 'categories':
            categories();
            break;

        default:
            // case 'product': or product.create
            product();
            break;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Products
    ///////////////////////////////////////////////////////////////////////////

    function product() {
        tinymce.init({
            selector: "#description",
            plugins: "link, code, visualblocks",
            body_class: "tiny-mce-body",
            menubar: false,
            toolbar: "formatselect, removeformat | bold, italic, underline | link | bullist numlist outdent indent | strikethrough, subscript, superscript | undo, redo | code, visualblocks",
            browser_spellcheck: true,
            content_css :  "/css/okuma/main.css"
        });

        $('#snippet').counter({
            container_class: "text-right",
            type: "char",
            count: "down",
            goal: "255",
            text: false,
            append: true,
            // target: '#snippet-counter',
        });

        $("#snippet_counter").addClass("text-right");
        $("#snippet_count").addClass("badge");

        $("#published_at_datepicker").datetimepicker({
            format: "YYYY-MM-DD HH:mm:ss", //2015-07-17 16:54:10
            icons: {
                time: "fa fa-fw fa-lg fa-clock-o",
                date: "fa fa-fw fa-lg fa-calendar",
                up: "fa fa-fw fa-lg fa-chevron-up",
                down: "fa fa-fw fa-lg fa-chevron-down",
                previous: "fa fa-fw fa-lg fa-chevron-left",
                next: "fa fa-fw fa-lg fa-chevron-right",
                today: "fa fa-fw fa-lg fa-crosshairs",
                clear: "fa fa-fw fa-lg fa-trash-o",
            },
            showTodayButton: true,
            showClear: true,
        });
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Images
    ///////////////////////////////////////////////////////////////////////////

    function images() {

        $("#images-reorder-form button").click(
            function(event) {

                var orderables = $("#images-container .sortable-item");

                var data = {
                    "orderables": []
                };

                for (var i = 0, l = orderables.length; i < l; i++) {

                    var item = $(orderables[i]);
                    var id = item.data("id");
                    var relatedID = item.data("related-id");
                    var position = i + 1;

                    data.orderables.push({
                        "id": id,
                        "position": position
                    });

                    data.orderables.push({
                        "id": relatedID,
                        "position": position
                    });
                }

                $("#images-reorder-form input[name=\"orderables\"]").val(JSON.stringify(data));

                return true;
            });
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Variants
    ///////////////////////////////////////////////////////////////////////////

    function variants() {

        $("#variants-reorder-form button").click(
            function(event) {

                var orderables = $("#variant-table .sortable-row");

                var data = {
                    "orderables": []
                };

                for (var i = 0, l = orderables.length; i < l; i++) {

                    var item = $(orderables[i]);
                    var id = item.data("id");
                    var position = i + 1;

                    data.orderables.push({
                        "id": id,
                        "position": position
                    });
                }

                $("#variants-reorder-form input[name=\"orderables\"]").val(JSON.stringify(data));

                return true;
            });

        var inputGroup = $("#product-specification-form table td:first-of-type");
        var inputGroup = inputGroup.clone(false);
        inputGroup.find("input").removeAttr("value");

        $("#product-specification-form table").on("click", ".btn",
            function(event) {

                var btn = $(event.target);
                var btnAction = btn.data("action");

                if (btnAction === "add") {
                    btn.closest("td").before(inputGroup.clone());
                } else if (btnAction === "remove") {
                    btn.closest("td").remove()
                }

            });
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Features
    ///////////////////////////////////////////////////////////////////////////

    function features() {
        var featureSearch = $("#feature-search");

        featureSearch.select2({
            placeholder: "Search for Features",
            allowClear: true,
            theme: "bootstrap",
            ajax: {
                delay: 250,
                url: featureSearch.data("url"),
                dataType: "json",
                data: function(params) {
                    var queryParameters = {
                        query: params.term,
                        page: params.page,
                    }

                    return queryParameters;
                },

                processResults: function(data, page) {

                    var parsedData = [];

                    var results = data.data;

                    for (var i = 0, l = results.length; i < l; i++) {
                        var feature = results[i];

                        parsedData[i] = {
                            id: feature.id,
                            text: feature.name,
                            data: feature,
                            // value: category.id,
                        }
                    }

                    return {
                        results: parsedData,
                        pagination: {
                            more: (data.next_page_url !== null)
                        }
                    };
                },
                cache: true,

            },

            minimumInputLength: 1,
            templateResult: templateResult,
            templateSelection: templateSelection,
        });

        $("#feature-search-open").on("click",
            function(event) {
                $("#feature-search").select2("open")

            });


        featureSearch.on("select2:select",
            function(event) {

                featureSearch.val(null).trigger("change");

                var feature = event.params.data;

                if ($("#feature-list .sortable-item[data-id=\"" + feature.id + "\"]").length == 0) {

                    var featureElem = String($("#feature-template").html());

                    var label = (feature.data.deleted_at === null) ? "label-success" : "label-warning";

                    featureElem = featureElem.replace(new RegExp("{feature_id}", "gm"), feature.id);
                    featureElem = featureElem.replace(new RegExp("{feature_name}", "gm"), feature.text);
                    featureElem = featureElem.replace(new RegExp("{feature_trashed}", "gm"), label);

                    $("#feature-list .sortable-list").append(featureElem);
                }
            });

        $("#feature-list").on("click", ".model-actions .btn",
            function(event) {

                var btn = $(event.target);
                var btnAction = btn.data("action");

                if (btnAction === "remove") {
                    event.preventDefault();
                    event.stopPropagation();
                    btn.closest(".sortable-item").remove();
                }
            }
        );

        $("#features-reorder-form button").click(
            function(event) {

                var orderables = $("#feature-list .sortable-item");

                var data = {
                    "orderables": []
                };

                for (var i = 0, l = orderables.length; i < l; i++) {

                    var item = $(orderables[i]);
                    var id = item.data("id");
                    var position = i + 1;

                    data.orderables.push({
                        "id": id,
                        "position": position
                    });
                }

                $("#features-reorder-form input[name=\"features\"]").val(JSON.stringify(data));

                return true;
            });
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Categories
    ///////////////////////////////////////////////////////////////////////////

    function categories() {

        $("#categories").select2({
            placeholder: "Search for Categories",
            allowClear: false,
            theme: "bootstrap",
            ajax: {
                delay: 250,
                url: $("#categories").data("url"),
                dataType: "json",
                data: function(params) {
                    var queryParameters = {
                        query: params.term,
                        page: params.page,
                    }

                    return queryParameters;
                },

                processResults: function(data, page) {

                    var parsedData = [];

                    var results = data.data;

                    for (var i = 0, l = results.length; i < l; i++) {
                        var category = results[i];

                        parsedData[i] = {
                            id: category.id,
                            text: category.name,
                            data: category,
                        }
                    }

                    return {
                        results: parsedData,
                        pagination: {
                            more: (data.next_page_url !== null)
                        }
                    };
                },
                cache: true,

            },

            minimumInputLength: 1,
            templateResult: templateResult,
            templateSelection: templateSelection,
        });

        $("#categories-clear").on("click",
            function(event) {
                $("#categories").val(null).trigger("change");

            });
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Common
    ///////////////////////////////////////////////////////////////////////////

    function templateSelection(selection) {

        var trashed = "label-primary";

        if (selection.data !== undefined) {
            trashed = (selection.data.deleted_at === null) ? "label-success" : "label-warning";
        } else if ($(selection.element).data('trashed') !== undefined) {
            trashed = ($(selection.element).data('trashed')) ? "label-warning" : "label-success";
        }

        return $("<span><small class=\"selected-tag \"><span class=\"label " + trashed + "\">#" + ((selection.id) ? selection.id : "") + "</span></small> " + selection.text + "</span>");
    }

    function templateResult(result) {

        var trashed = "label-primary";

        if (result.data !== undefined) {
            trashed = (result.data.deleted_at === null) ? "label-success" : "label-warning";
        }

        return $("<span><span class=\"label " + trashed + "\">#" + ((result.id) ? result.id : "") + "</span> " + result.text + "</span>");
    }

    if ($("#published_at_datepicker").length > 0) {

    }

})();
