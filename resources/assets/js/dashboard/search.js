(function() {


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("meta[name=\"csrf-token\"]").attr('content')
        }
    });

    var _Request = null;

    var ELEMENT = $("#queue-length");
    var URL = ELEMENT.data("url");
    var METHOD = ELEMENT.data("method");

    pollQueueLength();

    function pollQueueLength() {

        if (_Request !== null)
            _Request.abort();

        _Request = $.ajax({
            type: METHOD,
            url: URL,
            data: null,
            dataType: "json",
            cache: false,
            error: onAjaxError,
            success: onAjaxSuccess,
            // complete: onAjaxComplete
        });
    }


    function onAjaxSuccess(data, textStatus, jqXHR) {
         $(ELEMENT.find('td')[0]).html(data.length);
         $(ELEMENT.find('td')[1]).html(data.timestamp);
         setTimeout(pollQueueLength, 3000);
    }

    function onAjaxComplete(jqXHR, textStatus) {
    }

    function onAjaxError(jqXHR, textStatus, errorThrown) {

        if (textStatus == "error") {
            if (window.console) {
                console.log("onAjaxError( jqXHR:", jqXHR, ", textStatus:", textStatus, ", errorThrown:", errorThrown, ")");
            }
        }
    }
})();
