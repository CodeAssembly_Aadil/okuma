(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("meta[name=\"csrf-token\"]").attr('content')
        }
    });

    if ($("#category-reorder-form button").length > 0) {
        tree();
    }
    if ($("#snippet").length > 0) {
        category();
    } else if ($("#product-search").length > 0) {
        products()
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Category Tree
    ///////////////////////////////////////////////////////////////////////////

    function tree() {
        $("#category-reorder-form button").click(
            function(event) {
                $("#category-reorder-form input[name=\"tree\"]").val(JSON.stringify(serialize()));
                return true;

            });
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Category
    ///////////////////////////////////////////////////////////////////////////

    function category() {
        $('#snippet').counter({
            container_class: "text-right",
            type: "char",
            count: "down",
            goal: "255",
            text: false,
            append: true,
            // target: '#snippet-counter',
        });

        $("#snippet_counter").addClass("text-right");
        $("#snippet_count").addClass("badge");
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Products
    ///////////////////////////////////////////////////////////////////////////

    function products() {

        var productSearch = $("#product-search");

        productSearch.select2({
            placeholder: "Search for Products",
            allowClear: true,
            theme: "bootstrap",
            ajax: {
                delay: 250,
                url: productSearch.data("url"),
                dataType: "json",
                data: function(params) {
                    var queryParameters = {
                        query: params.term,
                        page: params.page
                    }

                    return queryParameters;
                },

                processResults: function(data, page) {

                    var parsedData = [];

                    var results = data.data;

                    for (var i = 0, l = results.length; i < l; i++) {
                        var product = results[i];

                        parsedData[i] = {
                            id: product.id,
                            text: product.name,
                            data: product,
                            image: product.thumbnail ? product.thumbnail.path : null,
                            // value: featureSet.id,
                        }
                    }

                    return {
                        results: parsedData,
                        pagination: {
                            more: (data.next_page_url !== null)
                        }
                    };
                },
                cache: true,
            },

            templateSelection: templateSelection,
            templateResult: templateResult,
            minimumInputLength: 1
        });

        $("#product-search-open").on("click",
            function(event) {
                $("#product-search").select2("open")

            });


        var productTemplate = $("#product-template").html();
        var defaultImagePath = $("#product-template").data("default-image");

        productSearch.on("select2:select",
            function(event) {

                productSearch.val(null).trigger("change");

                var product = event.params.data;

                if ($("#product-grid .sortable-item[data-id=\"" + product.id + "\"]").length == 0) {

                    if (!product.image) {
                        product.image = $("#product-template").data("default-image");
                    }

                    var productTrashed = (product.data.deleted_at === null) ? "label-success" : "label-warning";
                    var productPublished = "label-default";
                    var productPublishedIcon = "fa-eye-slash";
                    var productPublishedAt = "Draft";

                    if (product.data.published_at !== null) {

                        productPublishedAt = product.data.published_at;

                        var publishedDate = new Date(product.data.published_at);
                        var currentDate = new Date();

                        if (publishedDate <= currentDate) {
                            productPublished = "label-success";
                            productPublishedIcon = "fa-eye";
                        } else {
                            productPublished = "label-info";
                            productPublishedIcon = "fa-clock-o";
                        }
                    }

                    (product.data.published_at === null) ? "label-default" : "label-warning";

                    var productElem = String($("#product-template").html());

                    productElem = productElem.replace(new RegExp("{product_id}", "gm"), product.id);
                    productElem = productElem.replace(new RegExp("{product_name}", "gm"), product.text);
                    productElem = productElem.replace(new RegExp("{product_trashed}", "gm"), productTrashed);
                    productElem = productElem.replace(new RegExp("{product_published}", "gm"), productPublished);
                    productElem = productElem.replace(new RegExp("{product_published_icon}", "gm"), productPublishedIcon);
                    productElem = productElem.replace(new RegExp("{product_published_at}", "gm"), productPublishedAt);
                    productElem = productElem.replace(new RegExp("{image_path}", "gm"), "//" + window.location.hostname + "/" + product.image);

                    $("#product-grid .sortable-grid").append(productElem);
                }
            });

        $("#product-grid").on("click", ".model-actions .btn",
            function(event) {


                var btn = $(event.target);
                var btnAction = btn.data("action");

                if (btnAction === "remove") {
                    event.preventDefault();
                    event.stopPropagation();

                    btn.closest(".sortable-item").remove();
                }
            }
        );

        $("#products-reorder-form button").click(
            function(event) {

                var orderables = $("#product-grid .sortable-item");

                var data = {
                    "orderables": []
                };

                for (var i = 0, l = orderables.length; i < l; i++) {

                    var item = $(orderables[i]);
                    var id = item.data("id");
                    var position = i + 1;

                    data.orderables.push({
                        "id": id,
                        "position": position
                    });
                }

                $("#products-reorder-form input[name=\"products\"]").val(JSON.stringify(data));

                return true;
            });
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Common
    ///////////////////////////////////////////////////////////////////////////


    function templateSelection(selection) {

        var trashed = "label-primary";
        var published = " <i class=\"fa fa-fw fa-lg fa-eye-slash text-muted\"></i> ";

        if (selection.data !== undefined) {

            trashed = (selection.data.deleted_at === null) ? "label-success" : "label-warning";

            if (selection.data.published_at !== null) {

                var publishedDate = new Date(selection.data.published_at);
                var currentDate = new Date();

                if (publishedDate <= currentDate) {
                    published = " <i class=\"fa fa-fw fa-lg fa-eye text-success\"></i> ";
                } else {
                    published = " <i class=\"fa fa-fw fa-lg fa-clock-o text-info\"></i> ";
                }
            }
        }

        return $("<span><small class=\"selected-tag\"><span class=\"label " + trashed + "\">#" + ((selection.id) ? selection.id : "") + "</span> " + published + " </small> " + selection.text + "</span>");
    }

    function templateResult(result) {

        var trashed = "label-primary";
        var published = " <i class=\"fa fa-fw fa-lg fa-eye-slash text-muted\"></i> ";

        if (result.data !== undefined) {

            trashed = (result.data.deleted_at === null) ? "label-success" : "label-warning";


            if (result.data.published_at !== null) {

                var publishedDate = new Date(result.data.published_at);
                var currentDate = new Date();

                if (publishedDate <= currentDate) {
                    published = " <i class=\"fa fa-fw fa-lg fa-eye text-success\"></i> ";
                } else {
                    published = " <i class=\"fa fa-fw fa-lg fa-clock-o text-info\"></i> ";
                }
            }
        }

        return $("<span><span class=\"label " + trashed + "\">#" + ((result.id) ? result.id : "") + "</span> " + published + result.text + "</span>");
    }


    function serialize() {

        var data = {
            "tree": []
        };

        var rootNodes = $("#category-tree .sortable-list > .sortable-item");

        for (var rootIndex = 0, numRoots = rootNodes.length; rootIndex < numRoots; rootIndex++) {

            var rootNode = $(rootNodes[rootIndex]);
            var rootNodeID = rootNode.data('id');

            var childNodes = rootNode.find('> .sub-list > .sortable-item');

            for (var childIndex = 0, numChildren = childNodes.length; childIndex < numChildren; childIndex++) {
                var node = $(childNodes[childIndex]);
                processNode(data.tree, node, childIndex, rootNodeID);
            }
        }

        return data;
    }

    function processNode(tree, node, position, parentID) {

        node = $(node);

        var id = node.data("id");

        tree.push({
            "id": id,
            "position": position,
            "parent_id": parentID,
            // "name": node.find("> .sortable-item-wrapper .title").text()
        });

        var children = node.find('> .sub-list > .sortable-item');

        for (var i = 0, l = children.length; i < l; i++) {
            var child = $(children[i]);

            processNode(tree, child, i, id);
        }
    }

})();
