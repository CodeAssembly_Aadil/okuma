(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("meta[name=\"csrf-token\"]").attr('content')
        }
    });

    $("#feature_set_id").select2({
        placeholder: "Select a Feature Set",
        allowClear: false,
        theme: "bootstrap",
        ajax: {
            delay: 250,
            url: $("select[name=feature_set_id]").data("url"),
            dataType: "json",
            data: function(params) {
                var queryParameters = {
                    query: params.term,
                    page: params.page,
                    depth: 2
                }

                return queryParameters;
            },

            processResults: function(data, page) {

                var parsedData = [];

                var results = data.data;

                for (var i = 0, l = results.length; i < l; i++) {
                    var featureSet = results[i];

                    parsedData[i] = {
                        id: featureSet.id,
                        text: featureSet.name,
                        data: featureSet
                            // value: featureSet.id,
                    }
                }

                return {
                    results: parsedData,
                    pagination: {
                        more: (data.next_page_url !== null)
                    }
                };
            },
            cache: true,

        },

        minimumInputLength: 1,
        templateResult: templateResult,
        templateSelection: templateSelection,
    });

    $("#feature-set-clear").on("click",
        function(event) {
            $("#feature_set_id").val(null).trigger("change");

        });

   function templateSelection(selection) {

        var trashed = "label-primary";

        if (selection.data !== undefined) {
            trashed = (selection.data.deleted_at === null) ? "label-success" : "label-warning";
        } else if ($(selection.element).data('trashed') !== undefined) {
            trashed = ($(selection.element).data('trashed')) ? "label-warning" : "label-success";
        }

        return $("<span><small class=\"selected-tag \"><span class=\"label " + trashed + "\">#" + ((selection.id) ? selection.id : "") + "</span></small> " + selection.text + "</span>");
    }

    function templateResult(result) {

        var trashed = "label-primary";
        
        if (result.data !== undefined) {
            trashed = (result.data.deleted_at === null) ? "label-success" : "label-warning";
        }

        return $("<span><span class=\"label " + trashed + "\">#" + ((result.id) ? result.id : "") + "</span> " + result.text + "</span>");
    }



})();
