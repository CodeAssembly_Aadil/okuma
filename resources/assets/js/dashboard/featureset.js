(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("meta[name=\"csrf-token\"]").attr('content')
        }
    });

    if ($("#feature-search").length > 0)
    {
        features();
    }
    else if ($("#featureset-reorder-form button").length > 0)

    function sort() {
        $("#featureset-reorder-form button").click(
            function(event) {

                var orderables = $("#featureset-list .sortable-item");

                var data = {
                    "orderables": []
                };

                for (var i = 0, l = orderables.length; i < l; i++) {

                    var item = $(orderables[i]);
                    var id = item.data("id");
                    var position = i + 1;

                    data.orderables.push({
                        "id": id,
                        "position": position
                    });
                }

                $("#featureset-reorder-form input[name=\"orderables\"]").val(JSON.stringify(data));

                return true;
            });
    }

    function features() {

        var featureSearch = $("#feature-search");

        featureSearch.select2({
            placeholder: "Search for Features",
            allowClear: true,
            theme: "bootstrap",
            ajax: {
                delay: 250,
                url: featureSearch.data("url"),
                dataType: "json",
                data: function(params) {
                    var queryParameters = {
                        query: params.term,
                        page: params.page,
                    }

                    return queryParameters;
                },

                processResults: function(data, page) {

                    var parsedData = [];

                    var results = data.data;

                    for (var i = 0, l = results.length; i < l; i++) {
                        var feature = results[i];

                        parsedData[i] = {
                            id: feature.id,
                            text: feature.name,
                            data: feature,
                            // value: category.id,
                        }
                    }

                    return {
                        results: parsedData,
                        pagination: {
                            more: (data.next_page_url !== null)
                        }
                    };
                },
                cache: true,

            },

            minimumInputLength: 1,
            templateResult: templateResult,
            templateSelection: templateSelection,
        });



        featureSearch.on("select2:select",
            function(event) {

                featureSearch.val(null).trigger("change");

                var feature = event.params.data;

                if ($("#feature-list .sortable-item[data-id=\"" + feature.id + "\"]").length == 0) {

                    var featureElem = String($("#feature-template").html());

                    var label = (feature.data.deleted_at === null) ? "label-success" : "label-warning";

                    featureElem = featureElem.replace(new RegExp("{feature_id}", "gm"), feature.id);
                    featureElem = featureElem.replace(new RegExp("{feature_name}", "gm"), feature.text);
                    featureElem = featureElem.replace(new RegExp("{feature_trashed}", "gm"), label);

                    $("#feature-list .sortable-list").append(featureElem);
                }
            });

        $("#feature-search-open").on("click",
            function(event) {
                $("#feature-search").select2("open")

            });

        $("#feature-list").on("click", ".model-actions .btn",
            function(event) {

                var btn = $(event.target);
                var btnAction = btn.data("action");

                if (btnAction === "remove") {
                    event.preventDefault();
                    event.stopPropagation();

                    btn.closest(".sortable-item").remove();
                }
            }
        );

        $("#features-reorder-form button").click(
            function(event) {

                var orderables = $("#feature-list .sortable-item");

                var data = {
                    "orderables": []
                };

                for (var i = 0, l = orderables.length; i < l; i++) {

                    var item = $(orderables[i]);
                    var id = item.data("id");
                    var position = i + 1;

                    data.orderables.push({
                        "id": id,
                        "position": position
                    });
                }

                $("#features-reorder-form input[name=\"orderables\"]").val(JSON.stringify(data));

                return true;
            });
    }



    function templateSelection(selection) {

        var trashed = "label-primary";

        if (selection.data !== undefined) {
            trashed = (selection.data.deleted_at === null) ? "label-success" : "label-warning";
        } else if ($(selection.element).data('trashed') !== undefined) {
            trashed = ($(selection.element).data('trashed')) ? "label-warning" : "label-success";
        }

        return $("<span><small class=\"selected-tag \"><span class=\"label " + trashed + "\">#" + ((selection.id) ? selection.id : "") + "</span></small> " + selection.text + "</span>");
    }

    function templateResult(result) {

        var trashed = "label-primary";

        if (result.data !== undefined) {
            trashed = (result.data.deleted_at === null) ? "label-success" : "label-warning";
        }

        return $("<span><span class=\"label " + trashed + "\">#" + ((result.id) ? result.id : "") + "</span> " + result.text + "</span>");
    }

})();
