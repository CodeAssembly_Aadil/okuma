(function() {

    var placeholderStyle = $("<style type=\"text/css\"></style>");

    $("head").append(placeholderStyle);

    var colSpan = $(".sortable-table tbody tr:first-of-type td").length;

    $(".sortable-table").sortable({
        draggedClass: "is-dragging",
        containerSelector: "table",
        itemPath: "> tbody",
        delay: 100,
        nested: false,
        vertical: true,
        itemSelector: ".sortable-row",
        placeholderClass: "sortable-row-placeholder",
        placeholder: "<tr class=\"sortable-row-placeholder\"><td colspan=\"" + colSpan + "\"></td></tr>",
        pullPlaceholder: true,
        onDragStart: onDragStart,
        onDrop: onDrop,
        tolerance: -5,
    });

    function onDragStart(item, container, _super, event) {
        placeholderStyle.html(".sortable-row-placeholder { height:" + (item.outerHeight() - 4) + "px;");
        _super(item, container, _super, event);

        item.css({
            height: item.outerHeight(),
            width: "auto"
        })
    }

    function onDrop(item, container, _super, event) {
        _super(item, container, _super, event);
    }

})();
