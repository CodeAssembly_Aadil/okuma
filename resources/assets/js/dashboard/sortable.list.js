(function() {


    // $("#tree").sortable({
    //     // group: ".sortable-list",
    //     // afterMove: *,
    //     // containerPath:">",
    //     containerSelector: ".sortable-list",
    //     distance: 0,
    //     delay: 100,
    //     handle: "",
    //     itemPath: "",
    //     itemSelector: ".sortable-item",
    //     bodyClass: "dragging",
    //     draggedClass: "is-dragging",
    //     // isValidTarget: *
    //     // onCancel: *
    //     // onDragStart: onDragStart,
    //     // onDrop: onDrop,
    //     // onMousedown: *,
    //     placeholderClass: "sortable-item-placeholder",
    //     placeholder: "<li class=\"sortable-item-placeholder\"></li>",
    //     pullPlaceholder: true,
    //     // serialize: *,
    //     tolerance: -5,
    //     // nested: true,
    //     vertical: true

    // });

    $(".sortable-list").sortable({
        delay: 100,
        draggedClass: "is-dragging",
        itemSelector: ".sortable-item",
        placeholder: "<li class=\"sortable-item-placeholder\"><i class=\"fa fa-2x fa-arrow-right text-info\"></i></li>",
        placeholderClass: "sortable-item-placeholder",
        tolerance: -5,
        vertical: true,
        onDragStart: onDragStart
    });

    function onDragStart(item, container, _super, event) {
        // var itemWrapper = $(item).find('.sortable-item-wrapper');
        // placeholderStyle.html(".sortable-item-placeholder .wrapper { width:" + itemWrapper.outerWidth() + "px; height:" + itemWrapper.outerHeight() + "px;");

        _super(item, container, _super, event);
        item.css({
                height: item.outerHeight(),
                width: "auto"
            })
            // item.css({
            //     height: item.outerHeight(),
            //     width: item.outerWidth()
            // })
            // item.addClass(container.group.options.draggedClass)
            // ("body").addClass(container.group.options.bodyClass)
    }
// function onDrop(item, container, _super, event) {
//     _super(item, container, _super, event);
// }


})();
