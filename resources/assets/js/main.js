// @koala-append "subscribe.js"

$(document).foundation();

(function() {

    var BODY = $("body");
    var MENU_BAR = $("#menubar");
    var PUSH_MENU = $("#pushmenu");
    var DROP_DOWN = $("#product-categories-dropdown");
    var MENU_BAR_NAV = $("#menubar .menubar-nav");
    var SEARCH_BOX = $("#menubar-searchform input[name=\"find\"]");
    var SPINNER = $("<div class=\"columns small-12 end \" id=\"search-spinner\"><div class=\"search-spinner\"></div></div>");
    var RESULT_CONTAINER = $("#menubar .menubar-drawer .menubar-search-results");
    var SEARCH_RESULT_TEMPLATE = "";
    var DEFAULT_THUMB = $(".menubar-search-results").data("default-thumb");

    var SEARCH_URL = $("#menubar-searchform").attr("action");

    var DID_SCROLL = false;
    var CHANGE_ON = 0.5 * (MENU_BAR.height() - 60);
    var SEARCH_TIMEOUT = 250;

    var BASE_URL = null;

    var _Request = null;
    var _TimeOutID = null;

    // var _CurrentResult;
    // var _SearchResults;
    var _FocusIndex = 0;

    var _DropDownOpen = false;

    /*---------------------------------------------------------------------------*/
    /* Init */
    /*---------------------------------------------------------------------------*/
    // $.ajaxSetup({
    //     headers: {
    //         "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
    //     }
    // });


    SEARCH_RESULT_TEMPLATE += "<div class=\"columns small-12 medium-6 large-4 end result\">"
    SEARCH_RESULT_TEMPLATE += "    <div class=\"search-result\">"
    SEARCH_RESULT_TEMPLATE += "        <a href=\"{url}\">"
    SEARCH_RESULT_TEMPLATE += "            <img class=\"search-thumbnail\" src=\"{thumbnail}\" width=\"60\" height=\"60\">"
    SEARCH_RESULT_TEMPLATE += "            <span class=\"search-title\">{title}</span>"
    SEARCH_RESULT_TEMPLATE += "            <span class=\"search-type\">{type}</span>"
    SEARCH_RESULT_TEMPLATE += "        </a>"
    SEARCH_RESULT_TEMPLATE += "    </div>"
    SEARCH_RESULT_TEMPLATE += "</div>"

    scrollPage();

    RESULT_CONTAINER.append(SPINNER);
    SPINNER.slideUp(0);
    RESULT_CONTAINER.slideUp(0);

    // PUSH_MENU.perfectScrollbar();
    /*---------------------------------------------------------------------------*/
    /* Fucntions */
    /*---------------------------------------------------------------------------*/

    function initSearch() {
        clearResults();

        SPINNER.slideUp(0);
        RESULT_CONTAINER.slideUp(0);

        MENU_BAR.addClass("menubar-search");

        SEARCH_BOX.val("");
        SEARCH_BOX.on("input propertychange", onSearchBoxInput);
        $("html").on("keydown", navigateSearch);

        SEARCH_BOX.focus();

        _TimeOutID = setTimeout(function() {
            SEARCH_BOX.focus()
        }, 350);
    }

    function clearSearch() {
        clearTimeout(_TimeOutID);

        SEARCH_BOX.off("input propertychange", onSearchBoxInput);
        // $("#masthead").off("keydown", navigateSearch);
        $("html").off("keydown", navigateSearch);

        // _SearchResults = null;

        if (_Request !== null) {
            _Request.abort();
        }

        RESULT_CONTAINER.slideUp(150,
            function() {
                clearResults();
                SPINNER.slideUp(0);

                SEARCH_BOX.val("");

                MENU_BAR.removeClass("menubar-search");
            });
    }

    function scrollPage() {
        var scrollPosY = window.pageYOffset || MENU_BAR.scrollTop();

        if (scrollPosY >= CHANGE_ON) {
            MENU_BAR.addClass("menubar-shrink");
        } else {
            MENU_BAR.removeClass("menubar-shrink");
        }

        DID_SCROLL = false;
    }

    function search(query) {

        if (_Request !== null) {
            _Request.abort();
        }

        SPINNER.slideDown(200);
        RESULT_CONTAINER.slideDown(200);

        _Request = $.ajax({
            type: "GET",
            url: SEARCH_URL,
            data: {
                find: query
            },
            dataType: "json",
            cache: false,
            error: onAjaxError,
            success: onAjaxSuccess,
            complete: onAjaxComplete
        });

    }

    function onAjaxSuccess(data, textStatus, jqXHR) {

        RESULT_CONTAINER.slideUp(150,
            function() {
                SPINNER.slideUp(0);
                clearResults();
                populateResults(data);
            });

        // if (data.length > 0) {
        // }
        RESULT_CONTAINER.delay(50).slideDown(200);
    }

    function onAjaxComplete(jqXHR, textStatus) {}

    function onAjaxError(jqXHR, textStatus, errorThrown) {

        if (textStatus == "error") {
            if (window.console) {
                console.log("onAjaxError( jqXHR:", jqXHR, ", textStatus:", textStatus, ", errorThrown:", errorThrown, ")");
            }

            RESULT_CONTAINER.slideUp(150,
                function() {
                    SPINNER.slideUp(0);
                    clearResults();
                });
        }
    }

    function clearResults() {
        _CurrentResult = null;
        RESULT_CONTAINER.find(".result").remove();
    }

    function populateResults(results) {

        var numResults = results.length;

        if (numResults == 0) {
            var searchResult = "<div class=\"columns small-12 result\">"
            searchResult += "    <div class=\"search-result\">"
            searchResult += "        <span class=\"search-title\">Sorry, no quick search results could be found.</span>"
            searchResult += "        <span class=\"search-type\">Please try another search.</span>"
            searchResult += "    </div>"
            searchResult += "</div>"

            searchResult = $(searchResult);

            RESULT_CONTAINER.append(searchResult);

        } else {

            numResults = numResults <= 6 ? numResults : 6;

            for (var i = 0; i < numResults; i++) {

                var result = results[i];

                var searchResult = SEARCH_RESULT_TEMPLATE;
                searchResult = searchResult.replace("{title}", result.name);
                searchResult = searchResult.replace("{type}", result.type);
                searchResult = searchResult.replace("{url}", result.url);
                if (result.image) {
                    searchResult = searchResult.replace("{thumbnail}", result.url);
                } else {
                    searchResult = searchResult.replace("{thumbnail}", DEFAULT_THUMB);
                }

                searchResult = $(searchResult);

                RESULT_CONTAINER.append(searchResult);
            }
        }

        _FocusIndex = 0;
    }

    function setFocusTo(index) {
        var searchResults = $(".search-result a");
        var totalResults = searchResults.length;

        searchResults.removeClass("active");

        if (index < 0) {
            index = totalResults;
        } else if (index > totalResults) {
            index = 0;
        }

        if (index === 0) {
            SEARCH_BOX.focus();
            return 0;
        } else {
            $(searchResults.get(index - 1)).addClass("active").focus();
            return index;
        }

        index = 0;
    }

    /*---------------------------------------------------------------------------*/
    /* Event Handlers */
    /*---------------------------------------------------------------------------*/


    $(window).on("scroll", onWindowScroll);
    $("#menubar .menu-link").on("click", togglePushMenu);
    $("#menubar .search-link, #menubar .close-search").on("click", toggleSearch);
    $("#products-link").on("click", openDropDown);

    function openDropDown(event) {

        event.preventDefault();
        event.stopPropagation();

        if (DROP_DOWN.hasClass("show")) {
            closeDropDown();
            return false;
        }

        DROP_DOWN.show(0);
        DROP_DOWN.addClass("show");

        BODY.on("click", closeDropDown);
    }

    function closeDropDown(event) {

        if (event) {

            if ($.contains(DROP_DOWN.get(0), event.target)) {
                return true;
            }

            event.preventDefault();
        }

        DROP_DOWN.removeClass("show");
        DROP_DOWN.delay(300).hide(0);

        BODY.off("click", closeDropDown);

        return false;
    }

    function togglePushMenu(event) {
        PUSH_MENU.toggleClass("pushmenu-open");
        return true;
    }

    function toggleSearch(event) {
        event.stopPropagation();
        event.preventDefault();

        if (MENU_BAR.hasClass("menubar-search")) {
            clearSearch();
        } else {
            initSearch();
        }

        return false;
    }

    function onWindowScroll(event) {
        if (!DID_SCROLL) {
            DID_SCROLL = true;
            setTimeout(scrollPage, 250);
        }
    }

    function onSearchBoxInput(event) {
        clearTimeout(_TimeOutID);

        var query = SEARCH_BOX.val();

        if ($.trim(query).length >= 3) {
            _TimeOutID = setTimeout(search, SEARCH_TIMEOUT, query);
        }
    }

    function navigateSearch(event) {

        switch (event.which) {

            // Backspace
            case 8:
                if (document.activeElement !== SEARCH_BOX.get(0)) {
                    _FocusIndex = setFocusTo(0);
                    event.preventDefault();
                }
                break;

                // Enter
            case 13:
                // event.preventDefault();
                break;

                // ESC
            case 27:
                // event.preventDefault();
                clearSearch();
                break;

                // Up
            case 38:
                event.preventDefault();
                _FocusIndex = setFocusTo(_FocusIndex - 1);
                break;

                // Down
            case 40:
                event.preventDefault();
                _FocusIndex = setFocusTo(_FocusIndex + 1);
                break;
        }
    }

})();
