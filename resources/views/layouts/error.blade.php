@extends('layouts.master')

@section('metaTags')
<meta name="robots" content="noindex, nofollow, noimageindex">
@endsection

@section('pageTitle', 'Error')
@section('pageDescription', 'Error')

@section('bodyClass', 'error-page')

@section('mastheadContent')
<div class="page-header-strip">
    <div class="row valign-middle">
        <div class="small-12 medium-4 columns">
            <h1>@yield('errorTitle')</h1>
        </div>
        <div class="medium-8 columns hidden-for-small-only">
            <p>@yield('errorDescription')</p>
        </div>
    </div>
</div>
@endsection

@section('bodyContent')
<div class="row">
    <div class="small-12 medium-4 large-3 small-centered medium-uncentered text-center columns">
        <img src="{{ URL::asset('images/okuma/life_saver.png') }}" width="300" height="300" />
    </div>
    <div class="small-12 medium-8 large-9  columns">
        @yield('errorContent')
    </div>
</div>
@endsection
