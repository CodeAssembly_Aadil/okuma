<html>

<head>
    <title>Okuma is down for maintenance.</title>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>

    <style type="text/css">
        body {
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        color: #95A5A6;
        display: table;
        font-weight: 300;
        font-family: "Open Sans";
    }

    .container {
        text-align: center;
        display: table-cell;
        vertical-align: middle;
    }

    .content {
        text-align: center;
        display: inline-block;
    }

    .title {
        font-size: 4.5rem;
        margin-bottom: 40px;
    }

    .sub-title {
        font-size: 1.75rem;
        display: block;
    }
    </style>
</head>

<body>
    <div class="container">
        <div class="content">
            <div class="title">
                Gone fishing.
                <small class="sub-title">Okuma will be back soon...</small>
            </div>

        </div>
    </div>
</body>

</html>
