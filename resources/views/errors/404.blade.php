@extends('layouts.error')

@section('pageTitle', 'Error 404 Page not Found')
@section('pageDescription', 'Oh No! The page you were looking for could not be found.')

@section('errorTitle', 'Error 404')
@section('errorDescription', 'Page Not Found')

@section('errorContent')
<h2><em>Woops!</em> Looks like this one got away</h2>
<p>The page you were looking for could not be found. Use the search bar above to find what you're looking for, or click the link below to go back to the previous page.</p>
<p><a href="{{ URL::previous() }}" class="button"><strong>Go Back</strong></a></p>
@endsection
