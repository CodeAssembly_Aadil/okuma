@extends('layouts.error')

@section('pageTitle', 'Error 403 Method not allowed')
@section('pageDescription', 'Woah There! You aren\'t allowed to do that.')

@section('errorTitle', 'Error 403')
@section('errorDescription', 'Method not allowed')

@section('errorContent')
<h2><em>Woah There!</em> You aren't allowed to do taht.</h2>
<p>Looks like you tried to do something that's not allowed or we hit a snag, but that's OK! Use the search bar above to find what you're looking for, or click the link below to go back to the previous page.</p>
<p><a href="{{ URL::previous() }}" class="button"><strong>Go Back</strong></a></p>
@endsection
