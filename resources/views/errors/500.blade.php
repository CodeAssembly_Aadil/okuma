@extends('layouts.error')

@section('pageTitle', 'Error 500 Internal Server Error')
@section('pageDescription', 'Man over board!')

@section('errorTitle', 'Error 500')
@section('errorDescription', 'Internal Server Error')

@section('errorContent')
<h2><em>OH NO!</em> Man over board!</h2>
<p>There was an Internal Server Error and we aren't sure what went wrong. Should you continue to receive this error please <a href="mailto:{!! Config::get('okuma.contact.error') !!}?subject=Server Error 500">let us know</a>.</p>
<p><a href="{{ URL::previous() }}" class="button"><strong>Go Back</strong></a></p>
@endsection
