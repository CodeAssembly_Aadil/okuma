@extends('layouts.error')

@section('pageTitle', 'Error 400 Bad Request')
@section('pageDescription', 'Bummer, that was a bad request!')

@section('errorTitle', 'Error 400')
@section('errorDescription', 'Bad Request')

@section('errorContent')
<h2><em>Doah!</em> You caught a boot</h2>
<p>There seems to be an error with the request and it could not be completed.  Should you continue to receive this error please <a href="mailto:{!! Config::get('okuma.contact.error') !!}?subject=Server Error 500">let us know</a>.</p>
<p><a href="{{ URL::previous() }}" class="button"><strong>Go Back</strong></a></p>
@endsection
