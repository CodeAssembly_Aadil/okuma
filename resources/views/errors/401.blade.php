@extends('layouts.error')

@section('pageTitle', 'Error 401 Unauthorized ')
@section('pageDescription', 'Looks like you aren\'t supposed to be here!')

@section('errorTitle', 'Error 401')
@section('errorDescription', 'Unauthorized ')

@section('errorContent')
<h2><em>Woops!</em> Looks like you took a wrong turn.</h2>
<p>You are Unauthorized to view the requested resource. Click the link below to go to the home page.</p>
<p><a href="{{ route('home') }}" class="button"><strong>Go Home</strong></a></p>
@endsection
