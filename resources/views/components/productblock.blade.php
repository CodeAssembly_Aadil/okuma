<article class="{{ $columnSize }} columns" itemscope itemtype="http://schema.org/Product">
	<a href="{{ route('product', ['slug'=>$product->slug]) }}" class="product-thumb" itemprop="url">
		@if(isset($product->thumbnail))
		<img itemprop="image" src="{{ URL::asset($product->thumbnail->path) }}" alt="{{ $product->thumbnail->title }}" width="{{ $product->thumbnail->width }}" height="{{ $product->thumbnail->height }}" />
		@else
		<img itemprop="image" src="{{ URL::asset($default_product_display_thumbnail->path) }}" alt="{{ $product->name }}" width="{{ $default_product_display_thumbnail->width }}" height="{{ $default_product_display_thumbnail->height }}" />
		@endif
		<div class="product-thumb-details">
			<h1 itemprop="name" class="product-thumb-title">{{ $product->name }}</h1>
			@if($product->variants_count > 1)
			<div class="product-thumb-models">{{ $product->variants_count }} Models</div>
			@endif
		</div>
	</a>
</article>
