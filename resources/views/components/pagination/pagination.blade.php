<ul class="pagination">
    @if ($previous)
        <li class="arrow"><a href="{{ $previous }}"><span class="icon-left"></span>PREV</a></li>
    @else
        <li class="arrow unavailable"><a href="#!"><span class="icon-left"></span>PREV</a></li>
    @endif
    @foreach ($links as $page => $url)
        @if ($page == $current)
            <li class="current"><a href="#!">{{ $page }}</a></li>
        @elseif($url)
            <li><a href="{{ $url }}">{{ $page }}</a></li>
        @else
            <li class="unavailable"><a href="#!">{{ $page }}</a></li>
        @endif
    @endforeach
    @if ($next)
        <li class="arrow"><a href="{{ $next }}">NEXT<span class="icon-right"></span></a></li>
    @else
        <li class="arrow unavailable"><a href="#!">NEXT<span class="icon-right"></span></a></li>
    @endif
</ul>
