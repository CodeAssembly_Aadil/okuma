<div class="share-bar">
	<span class="bar-title">Share</span>

	<a class="share-button facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}"  target="_blank">
		<span class="icon icon-facebook">
			{{-- <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid" width="100%" height="100%" viewBox="0 0 100 100">
				<path d="M38.08 22.43v12.39H29v15.15h9.08V95h18.65V49.98H69.24c0 0 1.17-7.26 1.74-15.21H56.8v-10.36c0-1.55 2.03-3.63 4.04-3.63H71V5H57.19C37.62 5 38.08 20.17 38.08 22.43"/>
			</svg> --}}
		</span>
		<span class="action">Share</span>
		<span class="title">facebook</span>
	</a>

	<a class="share-button whatsapp" href="whatsapp://send" target="_blank">
		<span class="icon icon-whatsapp">
			{{-- <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid" width="100%" height="100%" viewBox="0 0 100 100">
				<path d="M95 49.25c0 24.21-19.78 43.84-44.18 43.84 -7.75 0-15.02-1.98-21.36-5.45L5 95.41l7.98-23.52c-4.02-6.61-6.34-14.35-6.34-22.64 0-24.21 19.78-43.84 44.18-43.84C75.22 5.41 95 25.03 95 49.25M50.82 12.39c-20.48 0-37.15 16.54-37.15 36.86 0 8.07 2.63 15.54 7.08 21.61l-4.64 13.69 14.28-4.54c5.87 3.85 12.89 6.1 20.44 6.1 20.48 0 37.15-16.53 37.15-36.86C87.96 28.92 71.3 12.39 50.82 12.39M73.13 59.34c-0.27-0.45-0.99-0.72-2.08-1.25 -1.08-0.54-6.41-3.14-7.4-3.49 -0.99-0.36-1.72-0.54-2.44 0.54 -0.72 1.08-2.8 3.5-3.43 4.21 -0.63 0.72-1.26 0.81-2.35 0.27 -1.08-0.54-4.57-1.67-8.71-5.33 -3.22-2.85-5.39-6.36-6.02-7.44 -0.63-1.07-0.07-1.66 0.48-2.19 0.49-0.48 1.08-1.25 1.63-1.88 0.54-0.63 0.72-1.07 1.08-1.79 0.36-0.72 0.18-1.34-0.09-1.88 -0.27-0.54-2.44-5.82-3.34-7.98 -0.9-2.15-1.8-1.79-2.44-1.79 -0.63 0-1.35-0.09-2.08-0.09 -0.72 0-1.9 0.27-2.89 1.34 -0.99 1.08-3.79 3.68-3.79 8.96 0 5.29 3.88 10.4 4.42 11.11 0.54 0.72 7.49 11.92 18.5 16.22 11.01 4.3 11.01 2.87 13 2.69 1.98-0.18 6.41-2.6 7.31-5.11C73.4 61.94 73.4 59.79 73.13 59.34"/>
			</svg> --}}
		</span>
		<span class="action">Message</span>
		<span class="title">WhatsApp</span>
	</a>

	<a class="share-button email"  target="_blank" href="{!! $shareLink !!}" data-nc="1">
		<span class="icon icon-email">
			{{-- <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid" width="100%" height="100%" viewBox="0 0 100 100">
				<g>
					<polygon points="95 80 95 23.23 64.27 46.63 80.23 64.77 79.77 65.23 61.27 48.92 50 57.5 38.73 48.92 20.24 65.23 19.77 64.77 35.7 46.63 5 23.28 5 80 "/>
					<polygon points="93.06 20 6.88 20 50 52.79 "/>
				</g>
			</svg> --}}
		</span>
		<span class="action">Send</span>
		<span class="title">Email</span>
	</a>
</div>
