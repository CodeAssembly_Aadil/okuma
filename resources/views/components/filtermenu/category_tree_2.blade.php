@if(isset($categoryProductCounts[$category->id]) && $categoryProductCounts[$category->id] > 0)

  <li>
    <a href="{{ URL::route('category', ['category' => $category->slug]) }}" data-slug="{{ $category->slug }}">{{ $category->name }}</a>
  </li>

  @if($childCategory->hasChildren())

    <li>
      <ul class="sidebar-links category-link-list">
        @foreach ($category->getChildren() as $childCategory)
          @include('components.filtermenu.category_tree', ['category'=>$childCategory, 'categoryProductCounts' => $categoryProductCounts])
        @endforeach
      </ul>
    </li>

  @endif

@endif
