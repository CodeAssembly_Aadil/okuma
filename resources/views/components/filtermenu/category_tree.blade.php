@if(isset($productCounts[$category->id]) && $productCounts[$category->id] > 0)
  <li><a href="{{ URL::route('category', ['category' => $category->slug]) }}" data-slug="{{ $category->slug }}">{{ $category->name }}</a></li>

  @if($category->hasChildren())
  <li>
    <ul class="sidebar-links category-link-list">
      @foreach($category->getChildren() as $child)
        @include('components.filtermenu.category_tree', ['category'=>$child])
      @endforeach
    </ul>
  </li>
  @endif
@endif
