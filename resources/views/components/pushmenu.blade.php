<nav class="pushmenu" id="pushmenu">
		<?php $category = $categoryTree->first();?>

		<a href="{{ route('catalog') }}" class="pushmenu-item nav-link">
			{{ $category->name }}
		</a>

		@if ($category->hasChildren())
			<ul class="pushmenu-item nav-link-list">
				@foreach($category->getChildren() as $subCategory)
					@if(isset($categoryProductCounts[$subCategory->id]) && $categoryProductCounts[$subCategory->id] > 0)
						<li><a href="{{ route('category', ['category'=>$subCategory->slug]) }}" class="nav-link">{{ $subCategory->name }}</a></li>
					@endif
				@endforeach
			</ul>
		@endif

		<a href="" class="pushmenu-item nav-link">
			About Okuma
		</a>

		<a href="{{ route('contact.index') }}" class="pushmenu-item nav-link">
			Contact Us
		</a>
</nav>
