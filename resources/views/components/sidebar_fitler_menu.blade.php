<div class="sidebar sticky-menu">
	<div class="sidebar-menu">

		<div class="sidebar-menu-header">
			<div class="product-count">Displaying <span >{{ $productCount }}</span>
				@if ($hasFilters)
					<a href="{{ route('category', [$category->slug]) }}"><span class="icon-cancel-circle"></span> Clear filters</a>
				@else
					Products
				@endif
			</div>
		</div>

		<nav class="sidebar-category-nav">

			<?php $rootParent = $category->rootParent->first();?>
			<?php $categoryMenuTitle = $rootParent->name;?>
			<?php $categoryMenu = $categoryMenus[$categoryMenuTitle];?>
			<?php unset($categoryMenus[$categoryMenuTitle]);?>

			<ul class="accordion box-border" data-accordion data-options="multi_expand:true;toggleable:true">
				<li class="accordion-navigation">
					<a href="#{{ $categoryMenuTitle }}" class="group-header"><span class="group-title">{{ $categoryMenuTitle }}:</span>
						<span class="active-category">
							@if($category->slug === $rootParent->slug)
								ALL
							@else
								{{ $category->name }}
							@endif
						</span>

						<span class="icon_accordian icon-plus"></span>
					</a>

					<div id="{{ $categoryMenuTitle }}" class="content">
						<ul class="sidebar-links sidebar-category-links">
							{!! $categoryMenu !!}
						</ul>
					</div>
				</li>
			</ul>

			@foreach ($categoryMenus as $categoryMenuTitle => $categoryMenu)

				<?php $url = Request::url()?>

				<ul class="accordion box-border" data-accordion data-options="multi_expand:true;toggleable:true">
					<li class="accordion-navigation">
						<a href="#{{ $categoryMenuTitle }}" class="group-header">

							<span class="group-title">{{ $categoryMenuTitle }}:</span>
							<?php $filterCategory = isset($filterCategories[$categoryMenuTitle]) ? $filterCategories[$categoryMenuTitle] : null;?>

							<span class="active-category">
								@if(!is_null($filterCategory))
									{{ $filterCategory->name }}
								@else
									ALL
								@endif
							</span>

							<span class="icon_accordian icon-plus"></span>
						</a>
						<div id="{{ $categoryMenuTitle }}" class="content">
							<ul class="sidebar-links sidebar-category-links">
								{!! preg_replace('/(\/category\/){1}([a-z0-9-]+){1}/', '$1'.$category->slug.'?categories[]=$2', $categoryMenu); !!}
							</ul>
						</div>
					</li>
				</ul>
			@endforeach

		</nav>

		@if(isset($featureSets) && !empty($featureSets))

			<div class="sidebar-menu-filter-header">
				<strong>FILTER FEATURES</strong>
			</div>

			<div class="sidebar-feature-nav">

				<form id="product-filter-form" action="{{ route(Route::currentRouteName(), ['slug' => $category->slug]) }}#main">
					<?php $categories = Request::get('categories', [])?>

					@foreach ($categories as $filterCategory)
						<input type="hidden" name="categories[]" value="{{ $filterCategory }}">
					@endforeach

					<ul class="accordion box-border" data-accordion data-options="multi_expand:true;toggleable:true">

						<?php $baseURL = Request::fullUrl();?>
						<?php $checkedFeatures = Request::old('features');?>
						<?php $checkedFeatures = is_null($checkedFeatures) ? [] : $checkedFeatures;?>
						<?php $baseURL .= (strpos($baseURL, '?') === false) ? '?' : '';?>

						@foreach ($featureSets as $featureSet)
							@if(!$featureSet->features->isEmpty())

								<li class="accordion-navigation">
									<a href="#{{ $featureSet->slug }}" class="group-header"><span class="group-title">{{ $featureSet->name }}</span> <span class="icon_accordian icon-plus"></span></a>

									<div id="{{ $featureSet->slug }}" class="content @if($featureSet->filter_active) active @endif">

										<ul class="sidebar-links sidebar-feature-links">

											@foreach ($featureSet->features as $feature)
											<li>
												<label>
													<input type="checkbox" name="features[]" value="{{ $feature->slug }}" @if(in_array($feature->slug, $checkedFeatures)) checked @endif />
													<i class="icon_checkbox icon-uncheck-circle"></i>
													{{ $feature->name }} <span class="product-count">{{ $feature->product_count }}</span>
												</label>

											</li>
											@endforeach

										</ul>

									</div>
								</li>

							@endif
						@endforeach
					</ul>

					<button type="submit" class="expand">Apply Filters</button>

				</form>

			</div>
		@endif
	</div>
</div>
