<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<meta name="google-site-verification" content="MHVzz7EiwDdt2WvgzcrS5pnujt5_gu7_-Pqd_R7RIt8" />

<link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ URL::asset('icons/apple-touch-icon-57x57.png') }}" />
{{-- <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ URL::asset('icons/apple-touch-icon-60x60.png') }}" /> --}}
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::asset('icons/apple-touch-icon-72x72.png') }}" />
{{-- <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ URL::asset('icons/apple-touch-icon-76x76.png') }}" /> --}}
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::asset('icons/apple-touch-icon-114x114.png') }}" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ URL::asset('icons/apple-touch-icon-120x120.png') }}" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::asset('icons/apple-touch-icon-144x144.png') }}" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ URL::asset('icons/apple-touch-icon-152x152.png') }}" />

<link rel="icon" type="image/png" href="{{ URL::asset('icons/favicon-196x196.png') }}" sizes="196x196" />
<link rel="icon" type="image/png" href="{{ URL::asset('icons/favicon-32x32.png') }}" sizes="32x32" />
<link rel="icon" type="image/png" href="{{ URL::asset('icons/favicon-16x16.png') }}" sizes="16x16" />
<link rel="icon" type="image/png" href="{{ URL::asset('icons/favicon-128.png') }}" sizes="128x128" />

<meta name="application-name" content="Okuma | Inspired Fishing"/>

<meta name="msapplication-TileColor" content="#001755" />
<meta name="msapplication-TileImage" content="{{ URL::asset('mstile-144x144.png') }}" />
<meta name="msapplication-square70x70logo" content="{{ URL::asset('mstile-70x70.png') }}" />
<meta name="msapplication-wide310x150logo" content="{{ URL::asset('mstile-310x150.png') }}" />
<meta name="msapplication-square310x310logo" content="{{ URL::asset('mstile-310x310.png') }}" />

<meta name="theme-color" content="#001755">

<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/4.1.1/normalize.min.css" />
<link type="text/css" rel="stylesheet" href="{{ elixir('css/okuma/main.css') }}" />

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
