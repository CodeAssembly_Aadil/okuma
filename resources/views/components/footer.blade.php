<div class="container row">
	<div class="links hide-for-small-only medium-3 large-4 columns">
		<nav class="site-footer-nav">
			<h2 class="header">Site Links</h2>
			<ul>
				<li>
					<a href="{{ route('catalog') }}" class="animated-link bg-block bg-scale-anim">Products</a>
				</li>
				<li>
					<a href="{{ route('article', ['slug' => 'about']) }}" class="animated-link bg-block bg-scale-anim">About Okuma</a>
				</li>
				<li>
					<a href="{{ route('contact.index') }}" class="animated-link bg-block bg-scale-anim">Contact Us</a>
				</li>
			</ul>
		</nav>
	</div>

	<div class="small-12 medium-3 large-4 columns">
		<div class="site-footer-logo"></div>
	</div>

	<div class="small-12 medium-6 large-4 columns">
		<div class="newsletter-sign-up-box">

			<div class="row">
				<div class="small-12 columns">
				</div>
			</div>

			<div class="row">
				<div class="small-12 columns">
					<p><b>Get more form Okuma</b>.<br/> Sign up for our newsletter and get freshly caught <i>Product News</i>, <i>Updates</i> and <i>Special Content</i> straight to your inbox.</p>
					<a class="button subscribe-button" href="{{ route('newsletter.subscribe') }}">SIGN UP</a>
					<p class="unsubscribe-link">You may <a href="{{ route('newsletter.unsubscribe') }}">unsubscribe</a> at any time</p>
				</div>
			</div>

			<div class="row">
				<div class="small-12 columns">
				</div>
			</div>

		</div>

		<div class="row">
			<div class="site-footer-social-bar small-12 columns">
				Follow us: <a href="https://facebook.com/OkumaFishingAfrica" target="_blank" class="social-link"> <span class="icon-facebook"></span></a>
			</div>
		</div>

	</div>
</div>

<div class="site-footer-pedestal">
	<div class="row">
		<div class="small-12 columns">
		<div class="copyright-notice">
  			Okuma is distributed under licence by <br class="hide-for-medium-up"><a href="http://www.sensationtackle.co.za" target="_blank" title="Sensational Angling Supplies" class="sensational-angling-supplies">Sensational Angling Supplies</a>
  			<br>
  			Copyright &copy; {{ date("Y") }} Okuma. All Rights Reserved. E&amp;OE.
  		</div>
		</div>
	</div>
</div>
