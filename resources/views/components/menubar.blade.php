<div class="menubar" id="menubar">

	<div class="menubar-wrapper row">

		<a class="menubar-item menu-link"><span>MENU</span></a>

		<h1 class="menubar-item logo">
			<a href="{{ URL::route('home') }}">
				<span class="icon-okuma-bear"></span>
				<span class="icon-okuma"></span>
			</a>
		</h1>

		<nav class="menubar-nav">

			<a href="{{ route('catalog') }}" class="menubar-item nav-link" id="products-link">
				<span class="animated-link bg-block">Products</span>
			</a>

			<a href="{{ route('article', ['slug' => 'about']) }}" class="menubar-item nav-link">
				<span class="animated-link bg-block">About Okuma</span>
			</a>

			<a href="{{ route('contact.index') }}" class="menubar-item nav-link">
				<span class="animated-link bg-block">Contact Us</span>
			</a>

			<a href="{{ route('search') }}" class="menubar-item search-link">
				<i class="icon icon-search"></i>
			</a>

		</nav>

		<form method="GET" action="{{ route('search') }}" class="" accept-charset="UTF-8" id="menubar-searchform" enctype="application/x-www-form-urlencoded">
			<div class="close-search"><span class='icon-cross'></span></div>
			<input type="text" autocomplete="off" placeholder="SEARCH" name="find"/>
			<button class="button expand"><i class="icon icon-search"></i></button>
		</form>
	</div>

	<div class="menubar-drawer">
		<div class="menubar-search-results row" data-default-thumb="{!! URL::asset($default_search_thumbnail->path) !!}">
		</div>
	</div>

	<nav id="product-categories-dropdown">
		<div class="row product-categories">
			@foreach($categoryTree as $tree)
					@if(isset($categoryProductCounts[$tree->id]) && $categoryProductCounts[$tree->id] > 0)
						<div class="small-4 columns">
							<h4><a href="{{ route('catalog') }}" class="animated-link bg-block bg-scale-anim">{{ $tree->name }}</a></h4>

							@if($tree->hasChildren())
								<ul>
									@foreach ($tree->getChildren() as $category)
										<li>
											<a href="{{ route('category', ['category'=>$category->slug]) }}" class="animated-link bg-block bg-scale-anim">{{ $category->name }}</a>
										</li>
									@endforeach
								</ul>
							@endif

						</div>
					@endif
			@endforeach
		</div>
	</nav>
</div>
