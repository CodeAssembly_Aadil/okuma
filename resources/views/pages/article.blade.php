@extends('layouts.master')

@section('pageTitle', 'About')

@section('bodyClass', 'article-page')

@section('mastheadContent')
<div class="page-header-strip">
	<div class="row valign-middle">
		<div class="small-12 columns">
			<h1 >About Okuma</h1>
		</div>
	</div>
</div>
@endsection

@section('bodyContent')
	<div class="row">
		<div class="small-12 large-9 small-centered columns"  itemprop="articleBody">

			<article class="product" itemscope itemtype="http://schema.org/BlogPosting">

				<header>
					<div class="row">
						<div class="small-12 large-9 small-centered columns"  itemprop="articleBody">
							<h2 itemprop="headline">About Okuma</h2>
						</div>
					</div>
				</header>

				<section>
						<p>The jolting sound of the clicker; the subtle take of the fly; the crushing strike that comes on a fast-retrieved swimbait-- fishing rewards the senses in so many ways. From mountain lake to offshore waters, at every turn Okuma Fishing Tackle embraces your passion for the sport of fishing.</p>

						<p>Our 2015 introductions continue our commitment to dynamic and innovative rods and reels for both freshwater and saltwater anglers. Within each, our goal is to advance your skills and deliver a better experience from every day on the water. We accomplish this with advanced materials, cutting-edge design and by having one ear to the ground at all times. Our customers are the source of our most valuable input and it's your needs, goals and desires the drive Okuma product development.</p>
				</section>
				<section>
				</section>

			</article>

		</div>
	</div>
@endsection

@section('scripts')
@endsection
