<html>

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>Okuma Lucky Carp Competition : Terms and Conditions</title>

	<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/4.1.1/normalize.min.css" />

	<style>
		.tandc {
			max-width: 1024px;
			margin: 0 auto;
		}
	</style>
</head>

<body>
    <article class="tandc">

        <header class="tandc-header">
            <h1>Okuma Lucky Carp Competition : Terms and Conditions</h1>
        </header>

        <section class="tandc-section">
            <h2>GENERAL</h2>
            <p>No purchase or payment of any kind is necessery to enter or win.</p>
            <p>The <b>Lucky Carp Competition</b> <i>(“Competition”)</i> is sponsored by <a href="okuma.co.za">okuma.co.za</a> (<a href="www.sensationtackle.co.za">Sensational Angling Supplies</a>) <i>(“Sponsor”)</i> This contest is governed by these official rules <i>(“Official Rules”)</i>. By participating in the contest, each entrant agrees to abide by these Official Rules, including all eligibility requirements, and understands that the results of the contest, as determined by Sponsor and its agents, are final in all respects. The contest is subject to all federal, state and local laws and regulations and is void where prohibited by law.</p>

            <p>This promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook. Any questions, comments or complaints regarding the promotion will be directed to Sponsor, not Facebook.</p>
        </section>

        <section class="tandc-section">
            <h2>ELIGIBILITY</h2>
            <p>The Contest is open to legal residents of the Rebublic of South Africa where not prohibited by law, who are sixteen (16) years of age or older at the time of entry who have Internet access and a valid e-mail account prior to the beginning of the Contest Period. Sponsor has the right to verify the eligibility of each entrant.</p>

            <p>Competition is not open to employees, agencies, prize sponsors or contractors of Sensational Angling Supplies, or any person directly or indirectly involved in the organisation or running of the competition, or their immediate family members.</p>
        </section>

        <section class="tandc-section">
            <h2>COMPETITION PERIOD</h2>
            <p>The Competitionbegins at Wednesday 18 May 2016, 08:00 SAST, ends at Wednesday 01 June 2016 at 12:00 SAST. <i>(“Competition Period”)</i>. All entries <i>(“submissions”)</i> must be received on or before the time stated during that submission period. Sponsor reserves the right to extend or shorten the contest at their sole discretion.</p>
        </section>

        <section class="tandc-section">
            <h2>HOW TO ENTER</h2>
            <p>You can enter the Competition through the Sponsor Facebook Page. Comment with your answer on the sticky Competition post. Entrants with the correct responses will receive one (1) entry into the drawing.</p>
            <p>You may enter as many times as you wish.</p>
        </section>

        <section class="tandc-section">
            <h2>WINNER SELECTION</h2>
            <p>All eligible entries received during the Submission Period will gathered into a database at the end of the Submission Period. A winner will be chosen at random. The winners will be announced on or about 02 June 2016 on or about 12:00 SAST. Announcement and instructions for prize will be sent to the e-mail address supplied on the potential prize winner’s entry form. Each entrant is responsible for monitoring his/her e-mail account for prize notification and receipt or other communications related to this sweepstakes. If a potential prize winner cannot be reached by Administrator (or Sponsor) within fifteen (15) days, using the contact information provided at the time of entry, or if the prize is returned as undeliverable, that potential prize winner shall forfeit the prize. Upon the request of the Sponsor, the potential winner may be required to return an Affidavit of Eligibility, Release and Prize Acceptance Form. If a potential winner fails to comply with these official rules, that potential winner will be disqualified. Prizes may not be awarded if an insufficient number of eligible entries are received.</p>
        </section>

        <section class="tandc-section">
            <h2>PRIZES</h2>
            <p>Terms and conditions may apply. Incidental expenses and all other costs and expenses which are not specifically listed as part of a prize in these Official Rules and which may be associated with the award, acceptance, receipt and use of all or any portion of the awarded prize are solely the responsibility of the respective prize winner. ALL FEDERAL, STATE AND LOCAL TAXES ASSOCIATED WITH THE RECEIPT OR USE OF ANY PRIZE IS SOLELY THE RESPONSIBILITY OF THE WINNER.</p>
            <h3>First Prize</h3>
            <p>Okuma Avenger ABF-B Series ABF-55 Baitfeeder Reel and Okuma Avenger 12ft Carp Rod.</p>
            <h3>Second Prize</h3>
            <p>Okuma LongBow-II 50 Baitfeeer Reel and Okuma DynCarp 12ft Carp Rod.</p>
        </section>

        <section class="tandc-section">
            <h2>ADDITIONAL LIMITATIONS</h2>
            <p>Prize is non-transferable. No substitution or cash equivalent of prizes is permitted. Sponsor and its respective parent, affiliate and subsidiary companies, agents, and representatives are not responsible for any typographical or other errors in the offer or administration of the Sweepstakes, including, but not limited to, errors in any printing or posting or these Official Rules, the selection and announcement of any winner, or the distribution of any prize. Any attempt to damage the content or operation of this Competitionis unlawful and subject to possible legal action by Sponsor. Sponsor reserves the right to terminate, suspend or amend the Sweepstakes, without notice, and for any reason, including, without limitation, if Sponsor determines that the Competitioncannot be conducted as planned or should a virus, bug, tampering or unauthorized intervention, technical failure or other cause beyond Sponsor’s control corrupt the administration, security, fairness, integrity or proper play of the Sweepstakes. In the event any tampering or unauthorized intervention may have occurred, Sponsor reserves the right to void suspect entries at issue. Sponsor and its respective parent, affiliate and subsidiary companies, agents, and representatives, and any telephone network or service providers, are not responsible for incorrect or inaccurate transcription of entry information, or for any human error, technical malfunction, lost or delayed data transmission, omission, interruption, deletion, line failure or malfunction of any telephone network, computer equipment or software, the inability to access any website or online service or any other error, human or otherwise.</p>
        </section>

        <section class="tandc-section">
            <h2>INDEMNIFICATION AND LIMITATION OF LIABILITY</h2>
            <p>BY ENTERING THE COMPETTION, EACH ENTRANT AGREES TO INDEMNIFY, RELEASE AND HOLD HARMLESS SPONSOR AND ITS PARENT, AFFILIATE AND SUBSIDIARY COMPANIES, THE FACEBOOK PLATFORM, ADMINISTRATOR, ADVERTISING AND PROMOTIONAL AGENCIES, AND ALL THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES, REPRESENTATIVES AND AGENTS FROM ANY LIABILITY, DAMAGES, LOSSES OR INJURY RESULTING IN WHOLE OR IN PART, DIRECTLY OR INDIRECTLY, FROM THAT ENTRANT’S PARTICIPATION IN THE COMPETITION AND THE ACCEPTANCE, USE OR MISUSE OF ANY PRIZE THAT MAY BE WON. SPONSOR AND ITS PARENT, AFFILIATE AND SUBSIDIARY COMPANIES DO NOT MAKE ANY WARRANTIES, EXPRESS OR IMPLIED, AS TO THE CONDITION, FITNESS OR MERCHANTABILITY OF THE PRIZE. SPONSOR AND ITS PARENTS, SUBSIDIARIES, AFFILIATES, ADVERTISING AND PROMOTIONAL AGENCIES, AND ALL THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES, REPRESENTATIVES AND AGENTS DISCLAIM ANY LIABILITY FOR DAMAGE TO ANY COMPUTER SYSTEM RESULTING FROM ACCESS TO OR THE DOWNLOAD OF INFORMATION OR MATERIALS CONNECTED WITH THE SWEEPSTAKES.</p>
        </section>

        <section class="tandc-section">
            <h2 class="tandc-section-title">PUBLICITY</h2>
            <p>By participating, each entrant grants Sponsor permission to use his/her name, likeness or comments for publicity purposes without payment of additional consideration, except where prohibited by law.</p>
        </section>

        <footer class="tandc-footer">
            <h2>COMPETITION SPONSORS</h2>
            <p>This Competitionis sponsored by:</p>
            <p>
                <br/> <b>Senational Angling Supplies</b>
                <br/> Cnr. of Baralong and Sunbeam Street, Icon Industrial park
                <br/> Sunderland Ridge Industrial
                <br/> Centurion
                <br/> 0015
                <br/> South Africa
            </p>
            <p>
                +27 10 591 2987
            </p>
        </footer>
    </article>
</body>

</html>
