<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Okuma Lucky Carp Competitio. Visit our facebookbage and leave your comment to enter for a chance to win one of two fantastic prizes.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lucky Carp Competition | Okuma</title>
    <link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/4.1.1/normalize.min.css" />
    <link type="text/css" rel="stylesheet" href="{{ elixir('css/okuma/luckycarp.css') }}" />
    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-64491471-1', 'auto');
    ga('require', 'linkid', 'linkid.js');
    ga('send', 'pageview');
    </script>
    <!--[if lt IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
    <article class="luckycarp">
        <header class="luckycarp-header">
            <h1 class="logo"><img class="logo-img" src="images/luckycarp/lucky-carp-logo.svg" alt="Okuma Lucky Carp Competition" /></h1>
        </header>

        <div class="luckycarp-main">

            <section id="intro">
                <h1 class="title">Win one of two <span class="title-yellow">Okuma Carp Combos!</span></h1>
                <p class="sub-title">Visit our Facebook page to for a chance to strike it lucky, and you could win an Avenger Baitfeeder Reel and matching Avenger Carp Rod or an Okuma Longbow-II Baitfeeder Reel with an Okuma Dyna Carp Rod.</p>

                <a class="cta-btn" href="//facebook.com/OkumaFishingAfrica/">Go to Facebook</a>
                <hr class="short-divider" />
            </section>

            <section id="prizes">
                <h3 class="section-title">What's up for grabs?</h3>
                <img src="images/luckycarp/avenger-set.jpg" alt="Avenger Rod and Reel" />
                <p>The first place prize is an <a class="link" href="//okuma.co.za/product/avenger-abf-b" title="Avenger ABF-B Series Baitfeeder Reel">Okuma Avenger ABF-55 Baitfeeder Reel</a> and matching <a class="link" href="//okuma.co.za/product/avenger" title="Avenger Carp Rod">Okuma Avenger 12ft Carp Rod</a></p>
                <img src="images/luckycarp/longbow-dynacarp-set.jpg" alt="Longbow 2 Reel and Dyna Carp Rod" />
                <p>The runner up lands an <a class="link" href="//okuma.co.za/product/longbow" title="Longbow-II Baitfeeder Reel">Okuma Longbow-II 55 Baitfeeder Reel</a> and <a class="link" href="//okuma.co.za/product/dyna-carp" title="Dyna Carp Rod">Okuma Dyna Carp 12ft Rod</a>.</p>
            </section>

            <section id="how-to-enter">
                <h3 class="section-title">How to enter</h3>
                <p>Its easy to enter, visit the <a class="link" ref="//facebook.com/OkumaFishingAfrica/">Okuma Facebook Page</a> and answer one easy question <i class="comp-question">What was the furthest cast with the Avenger Rod?</i> By commenting on the top post. There's a clue in the videos on our Facebook page.{{-- <a class="link" href="#posts">videos</a> linked below. --}}</p>

                <p>Winners will be selected from the corre entries by random draw after the competition period.</p>
            </section>

             <section id="details">
                <h3 class="section-title">Competition Details</h3>
                <p>The Okuma Lucky Carp Competition runs from <b><i>Wednesday 18<sup>th</sup> May 2016, 08:00 SAST</i></b> until <b><i>Wednesday 1<sup>st</sup> June 2016, 12:00 SAST</i></b>.</p>

                <p>A winner and runner up will be selected from the correct responses by random draw and announced on <i>Thurdsay 2<sup>nd</sup> June 2016, 12:00 SAST</i>.</p>

                <p><a class="link" href="{{ URL::route('luckycarp.terms') }}">Terms and Conditions apply.</a></p>
            </section>

            {{-- <section id="posts">
                                        <h3 class="section-title">Facebook Posts</h3>
                                        <div class="link-container">
                                            <a class="fb-link">Avenger ABF-55 Baitfeeer Reel | Introduction</a>
                                        </div>
                                        <div class="link-container">
                                        </div>
                                    </section> --}}

            <section id="sponsors">
                <h3 class="section-title">Proudly Sponsored By</h3>
                <div class="sponsor-logo-container">
	                <div class="sponsor-logo-wrapper">
	                	<a class="link" href="//okuma.co.za" title="Okuma | Inspired Fishing"><img class="sponsor-logo"src="/images/luckycarp/okuma-logo.svg" alt="Okuma | Inspired Fishing" /></a>
	                </div>
	                <div class="sponsor-logo-wrapper">
	                	<a class="link" href="sensationtackle.co.za" title="Sensational Angling Supplies"><img class="sponsor-logo" src="/images/luckycarp/sensational-logo.svg" alt="Sensational Angling Supplies"/></a>
	                </div>
                </div>
            </section>

        </div>

        <footer class="luckycarp-footer">
            <a class="tandc" href="{{ URL::route('luckycarp.terms') }}" target="_blank" rel="noopener noreferrer">Terms and Conditions</a>

            <div class="notice">Okuma is distributed under licence by Sensational Angling Supplies<br/>Copyright © 2016 Okuma. All Rights Reserved. E&OE.</div>
        </footer>

    </article>

    <script type="text/javascript">
    WebFontConfig = {
        google: {
            families: ['Open+Sans:400,700,800,400italic,700italic:latin']
        }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
    </script>

</body>

</html>
