@extends('layouts.master')

@section('pageTitle', 'NewsLetter Unsubscribe')

@section('pageDescription', 'Subscribe to the Okuma Inspired Fishing newsletter, where you get access to the latest Product News , Updates and More...')

@section('bodyClass', 'subscription-page')

@section('mastheadContent')
<div class="row">
	<div class="small-12 medium-9 large-7 columns small-centered">
		<h1 class="title-header">Okuma Newsletter</h1>
	</div>
</div>
@endsection

@section('bodyContent')
<div class="subscriptionpage-content">
	<section>

		<div class="row">
			<div class="small-12 medium-9 large-7 columns small-centered">
				<div class="row">
					<div class="small-12 medium-3 columns text-center">
						<img src="{{ URL::asset('images/okuma/icon_newsletter.png') }}" />
					</div>
					<div class="small-12 medium-9 columns small-only-text-center">
						<h3>Unsubscribe</h3>
						<p>Use the form below to remove your email address from the <b class="text-okuma-blue">Okuma NewsLetter</b> mailling list.</p>

						<p>You can <a href="{{ URL::route('newsletter.subscribe') }}"><b>subscribe</b></a> to receive freshly caught <i>Product News</i>, <i>Updates</i> and <i>Special Content</i> delivered straight to your inbox.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="small-12 medium-9 large-7 columns small-centered">

				<form method="POST" action="{{ URL::route('newsletter.action.unsubscribe') }}#subscription-form" accept-charset="UTF-8" id="subscription-form" enctype="application/x-www-form-urlencoded">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="row">
						<div class="small-12 columns">
							<div class="form-input underlined animated">
								<input type="text" name="subscription-email" id="subscription-email"  class="input-field" maxlength="254"value="{{ old('subscription-email') }}" autocomplete="off" />
								<label class="input-label" for="subscription-email" >
									<div class="input-label-value" >Email <span id="error-email">{!! $errors->subscription->first('subscription-email', '<span class="feedback-message"><i class="icon-attention-circle text-alert"></i> :message</span>') !!}</span></div>
								</label>
							</div>
						</div>
					</div>

					{!! Honeypot::generate('subscription-username', 'subscription-time') !!}

					<div class="row">
						<div class="small-12 medium-4 columns small-only-text-center">
							<button type="submit" class="form-control submit button expand" id="subscription-submit"><span class="icon"></span><strong class="btn-label">Unsubscribe</strong></button>
						</div>
						<div class="small-12 medium-8 columns">
							<span id="error-form">{!! $errors->subscription->first('action', '<span class="feedback-message"><i class="icon-attention-circle text-alert"></i> :message</span>') !!}</span>
						</div>
					</div>
				</form>

			</div>
		</div>

	</section>


</div>
@endsection