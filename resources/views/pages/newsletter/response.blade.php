@extends('layouts.master')

@section('pageTitle', 'NewsLetter Subscribe')

@section('pageDescription', 'Subscribe to the Okuma Inspired Fishing newsletter, where you get access to the latest Product News , Updates and More...')

@section('bodyClass', 'subscription-page')

@section('mastheadContent')
<div class="row">
	<div class="small-12 medium-9 large-7 columns small-centered">
		<h1 class="title-header">Okuma Newsletter</h1>
	</div>
</div>
@endsection

@section('bodyContent')
<div class="subscriptionpage-content">
	<section>

		<div class="row">
			<div class="small-12 medium-9 large-7 columns small-centered">
				<div class="row">
					<div class="small-12 medium-3 columns text-center">
						<img src="{{ URL::asset('images/okuma/icon_newsletter.png') }}" />
					</div>
					<div class="small-12 medium-9 columns small-only-text-center">
					@if($status === 'subscribed')
						<h3>Thank you for signing up!</h3>
						<p>You have been succesfully subscribed the <b class="text-okuma-blue">Okuma Newsletter</b>. Please check your inbox to confirm your subscription.</p>
					@elseif($status === 'unsubscribed')
						<h3>We're sorry to see you go!</h3>
						<p>Your details have been removed from the <b class="text-okuma-blue">Okuma Newsletter</b> mailing list.</p>
					@endif
					</div>
				</div>
			</div>
		</div>
	</section>


</div>
@endsection

@section('scripts')
@endsection
