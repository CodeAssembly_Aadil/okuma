@extends('layouts.master')

@section('pageTitle', 'Inspired Fishing')

@section('bodyClass', 'home-page')

@section('mastheadContent')
	@if(isset($hero))
		<div class="hero">
			<div class="site-feature-wrapper">
				<figure class="site-feature {{ $hero->display_class }}" >

					@if(isset($hero->background))
						<div class="site-feature-bg" style="background-image: url({{ URL::asset($hero->background->path) }})"></div>
					@endif

					<figcaption>
						<div class="site-feature-content">
							@if(isset($hero->title))
								<h2 class="site-feature-title">{{ $hero->title }}</h2>
							@endif
							@if(isset($hero->body))
								<div class="site-feature-body">{{ $hero->body }}</div>
							@endif
						</div>

						@if(isset($hero->link))
							<a href="{!! $hero->link !!}" class="site-feature-link"><span>View More</span></a>
							@if(isset($hero->button_label))
								<a href="{!! $hero->link !!}" class="site-feature-button">{{ $hero->button_label }}</a>
							@endif
						@endif
					</figcaption>
				</figure>
			</div>
		</div>
	@endif
@endsection

@section('bodyContent')
	<div class="homepage-content">
		<div class="row collapse">
			@foreach ($siteFeatures as $feature)
				<div class="columns {{ $feature->column_size }}">
					<div class="site-feature-wrapper">
						<figure class="site-feature {{ $feature->display_class }}">

							@if(isset($feature->background))
								<div class="site-feature-bg" style="background-image: url({{ URL::asset($feature->background->path) }})"></div>
							@endif

							<figcaption>
								<div class="site-feature-content">
									@if(isset($feature->title))
										<h2 class="site-feature-title">{{ $feature->title }}</h2>
									@endif
									@if(isset($feature->body))
										<div class="site-feature-body">{{ $feature->body }}</div>
									@endif
								</div>

								@if(isset($feature->link))
									<a href="{!! $feature->link !!}" class="site-feature-link"><span>View More</span></a>
									@if(isset($feature->button_label))
										<a href="{!! $feature->link !!}" class="site-feature-button">{{ $feature->button_label }}</a>
									@endif
								@endif
							</figcaption>
						</figure>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endsection
