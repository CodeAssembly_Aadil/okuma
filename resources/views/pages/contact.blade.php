@extends('layouts.master')

@section('metaTags')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('pageTitle', 'Contact Us')

@section('pageDescription', 'Get in touch with us at Okuma Inspired Fishing South Africa. Contact us at info@okuma.co.za or give us a call on +27 12 327 1301.')

@section('bodyClass', 'contact-page')

@section('mastheadContent')
<div class="row">
	<div class="small-12 columns">
		<h1 class="title-header">Contact Us</h1>
	</div>
</div>
@endsection

@section('bodyContent')
<div class="contactpage-content">
	<div class="row">
		<div class="small-12 medium-4 columns">
			<section class="contact-info">
				<h3>Okuma Southern Africa</h3>
				<div class="contact-info-block">
					<div><span class="icon icon-email"></span>{{ Config::get('okuma.contact.email') }}</div>
					<div><span class="icon icon-phone"></span> +27 10 591 2987 / 6</div>
					<div><span class="icon icon-location"></span> Cnr. Baralong and Sunbeam Street<br/>
						Icon Park, Sunderland Ridge Industrial<br/>
						Centurion<br/>
						0157<br/>
						South Africa
					</div>
					<div><span class="icon icon-facebook"></span> <a href="https://facebook.com/OkumaFishingAfrica" target="_blank">/OkumaFishingAfrica</a></div>
				</div>
			</section>
		</div>

		@if(Session::has('contact-response') && Session::get('contact-response') === 'sent')
			<div class="small-12 medium-8 columns">
				<p><span class="large-icon size-45 icon-tick bg-success"></span> Thank you! Your message has been sent and we will be in touch with you soon.</p>
			</div>
		@endif

		<div class="small-12 medium-8 columns">
			<section>
				<h3>Contact Form</h3>
				<p>Complete the form below to get in touch with us <em>(All fields are required).</em></p>

				<form method="POST" action="{{ route('contact.request') }}#contact-form" accept-charset="UTF-8" id="contact-form" enctype="application/x-www-form-urlencoded">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="row">
						<div class="small-12 columns">
							<div class="form-input underlined animated">
								<input type="text" name="contact-name" id="contact-name" class="input-field" maxlength="128" value="{{ old('contact-name') }}" autocomplete="off" />
								<label class="input-label" for="contact-name">
									<div class="input-label-value" >Name <span id="error-name">{!! $errors->contact->first('contact-name', '<span class="feedback-message"><i class="icon-attention-circle text-alert"></i> :message</span>') !!}</span></div>
								</label>
							</div>
						</div>
					</div>

					{!! Honeypot::generate('contact-lastname', 'contact-time') !!}

					<div class="row">
						<div class="small-12 columns">
							<div class="form-input underlined animated">
								<input type="text" name="contact-email" id="contact-email"  class="input-field" maxlength="254"value="{{ old('contact-email') }}" autocomplete="off" />
								<label class="input-label" for="contact-email" >
									<div class="input-label-value" >Email <span id="error-email">{!! $errors->contact->first('contact-email', '<span class="feedback-message"><i class="icon-attention-circle text-alert"></i> :message</span>') !!}</span></div>
								</label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="small-12 columns">
							<div class="form-input underlined animated">
								<input type="text" name="contact-subject" id="contact-subject" class="input-field" maxlength="64"value="{{ old('contact-subject') }}" autocomplete="off" />
								<label class="input-label" for="contact-subject" >
									<div class="input-label-value" >Subject <span id="error-subject">{!! $errors->contact->first('contact-subject', '<span class="feedback-message"><i class="icon-attention-circle text-alert"></i> :message</span>') !!}</span></div>
								</label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="small-12 columns">
							<div class="form-input text-area underlined animated">
								<textarea name="contact-message" id="contact-message" class="input-field" rows="11" autocomplete="off" >{{ old('contact-message') }}</textarea>
								<label class="input-label" for="contact-message">
									<div class="input-label-value" >Message <span id="error-message">{!! $errors->contact->first('contact-message', '<span class="feedback-message"><i class="icon-attention-circle text-alert"></i> :message</span>') !!}</span></div>
								</label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-4 columns small-only-text-center">
							<button type="submit" class="form-control submit button expand ajax-progress-button" id="contact-submit"><span class="icon"></span><strong class="btn-label">Send Message</strong></button>
						</div>
						<div class="small-12 medium-8 columns">
							<span id="error-form"></span>
						</div>
					</div>
				</form>

			</section>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ elixir('js/okuma/contact.js') }}"></script>
@endsection
