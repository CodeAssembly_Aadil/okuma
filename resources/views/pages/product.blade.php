@extends('layouts.master')

@section('metaTags')

<!-- facebook share data -->
<meta property="og:url" content="{{ URL::current() }}" />
<meta property="og:title" content="Okuma | {{ $product->name }}" />
<meta property="og:description" content="{{ $product->snippet }}" />
<meta property="og:site_name" content="Okuma | Inspired Fishing" />
<meta property="og:type" content="product" />

@if(isset($product->shareImage))
	<meta property="og:image" content="{{ URL::asset($product->shareImage->path)  }}" />
	<meta property="og:image:width" content="{{ $product->shareImage->width }}" />
	<meta property="og:image:height" content="{{ $product->shareImage->height }}" />
@elseif(!$product->sliderImages->isEmpty())
	<?php $shareImage = $product->sliderImages->first();?>
	<meta property="og:image" content="{{ URL::asset($shareImage->path)  }}" />
	<meta property="og:image:width" content="{{ $shareImage->width }}" />
	<meta property="og:image:height" content="{{ $shareImage->height }}" />
@endif

@endsection

@section('pageTitle', $product->name)

@if(!is_null($product->snippet))
	@section('pageDescription', $product->snippet)
@endif

@section('bodyClass', 'product-page')

@section('mastheadContent')
	<div class="page-header-strip">
		<div class="row valign-middle">
			<div class="small-12 columns">
				<h1>{{ $product->name }}</h1>
			</div>
		</div>
	</div>
@endsection

@section('bodyContent')
	<article class="product" itemscope itemtype="http://schema.org/Product">
		@if (!$product->sliderImages->isEmpty())
			<?php $slideCount = $product->sliderImages->count();?>

			<header>
				<div class="product-slider">
					<div class="row">
						<div class="small-12 columns">
							<div class="product-image-slider">
								@if ($slideCount === 1)
								<?php $sliderImage = $product->sliderImages->first();?>
									<div class="product-image-slide">
										<img src="{{ URL::asset($sliderImage->path) }}" class="product-image" itemprop="image" contentUrl="{{  URL::asset($sliderImage->path) }}" width="{{ $sliderImage->width }}" height="{{ $sliderImage->height }}" data-title="{{ $sliderImage->title }}" />
									</div>
								@else
									@foreach ($product->sliderImages as $sliderImage)
										<?php $imageSrc = URL::asset($sliderImage->path);?>
										<div class="product-image-slide">
											<img data-lazy="{{ $imageSrc }}" class="product-image" itemprop="image" contentUrl="{{ $imageSrc }}" width="{{ $sliderImage->width }}" height="{{ $sliderImage->height }}">
										</div>
									@endforeach

									<button class="slider-control previous"><i class="icon-left"></i><span></span></button>
									<button class="slider-control next"><i class="icon-right"></i><span></span></button>
								@endif
							</div>
						</div>
					</div>

					@if($slideCount > 1)
						<div class="row visible-for-medium-up">
							<div class="small-12 columns">
								<div class="product-image-slider-nav">
									@foreach ($product->sliderThumbnails as $sliderThumbnail)
										<div class="product-image-slide">
											<img data-lazy="{{ URL::asset($sliderThumbnail->path) }}" class="product-image" width="{{ $sliderThumbnail->width }}" height="{{ $sliderThumbnail->height }}" data-title="{{ $sliderThumbnail->title }}" />
										</div>
									@endforeach
								</div>
							</div>
						</div>
					@endif
				</div>
			</header>
		@endif


		<div class="row">
			<div class="small-12 large-9 small-centered columns text-right">
				@include('components.share_buttons', ['shareLink' => URL::route('share.form', ['type'=>'product', 'id'=>$product->id])])
			</div>
		</div>

		<div class="row">
			<div class="small-12 large-9 small-centered columns">

				<section class="product-overview">
					<div class="row">

						<div class="small-12 small-centered columns">
							<h1 class="product-title"><span itemprop="brand">Okuma</span> <span  itemprop="name">{{ $product->name }}</span></h1>
						</div>
						<div class="small-12 columns">
							<h2 class="product-section-heading">Overview</h2>
						</div>

						<div class="small-12 columns" itemprop="description">
							{!!$product->description!!}
						</div>
					</div>
				</section>

				<?php $productSpec = json_decode($product->specification, true);?>

				@if(!empty($productSpec) && $product->variants->count() > 0)
				<section class="product-variants">
					<div class="row">
						<div class="small-12 columns">
							<h2 class="product-section-heading">Variants</h2>
						</div>

						<div class="small-12 columns">
							<table class="product-variants-table responsive">
								<thead>
									<th>Model</th>
									@foreach($productSpec as $property)
										<th>{{ $property }}</th>
									@endforeach
								</thead>
								<tbody>
									@foreach($product->variants as $variant)
										<tr>
											<td itemprop="model" data-label="Model">{{ $variant->model }}</td>
											<?php $specification = json_decode($variant->specification, true);?>
											@foreach ($productSpec as $property)
												<td itemprop="{{ $property }}" data-label="{{ $property }}">{{ $specification[$property] or '' }}</td>
											@endforeach
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</section>
				@endif

				@if($product->features->count() > 0)
				<section class="product-features">
					<div class="row">
						<div class="small-12 columns">
							<h2 class="product-section-heading">Features</h2>
						</div>
					</div>

					@foreach ($product->features as $feature)
						<article class="product-feature">
							<div class="row">

								<div class="small-3 medium-2 columns">
									@if(isset($feature->icon))
										<img src="{{ URL::asset($feature->icon->path) }}" width="{{ $feature->icon->width }}" height="{{ $feature->icon->height }}" alt="{{ $feature->icon->title }}">
									@else
										<img src="{{ URL::asset($default_feature_icon->path) }}" alt="{{ $feature->name }}" width="{{ $default_feature_icon->height }}" height="{{ $default_feature_icon->width }}" />
									@endif
								</div>

								<div class="small-9 medium-10 columns end">
									<h1>{{ $feature->name }}</h1>
									<p>{{ $feature->description }}</p>
								</div>
							</div>
						</article>
					@endforeach
				</section>
				@endif

			</div>
		</div>
	</article>
@endsection

@section('scripts')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
	<script type="text/javascript" src="{{ elixir('js/okuma/product.js') }}"></script>
	<script type="text/javascript" src="{{ elixir('js/okuma/share.js') }}"></script>
@endsection
