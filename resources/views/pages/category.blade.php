@extends('layouts.master')

@section('pageTitle', 'Category | '.$category->name)

@if(!is_null($category->snippet))
@section('pageDescription',$category->snippet)
@endif

@section('bodyClass', 'category-page')

<?php $hasHeroBanner = isset($category->heroBanner);?>

@if($hasHeroBanner)
@section('mastheadClass', 'has-hero-image')
@endif

@section('mastheadContent')
@if($hasHeroBanner)
	<div class="hero-image" style="background-image: url({{ url($category->heroBanner->path) }});">
	</div>
@endif

<div class="page-header-strip">
	<div class="row valign-middle">
		<div class="small-12 medium-4 columns">
			<h1 class="medium-text-right">{{ $category->name }}</h1>
		</div>
		<div class="small-12 columns show-for-medium-up">
			<p>{{ $category->description }}</p>
		</div>
	</div>
</div>
@endsection

@section('bodyContent')
<div class="catalogue-content">
	<div class="row">
		<div class="small-12 medium-3 columns">
			<aside>@include('components.sidebar_fitler_menu')</aside>
		</div>
		<div class="small-12 medium-9 columns">
			<section id="category-products">
				<div class="row">
					<?php $columnSize = 'small-6 medium-4';?>
					@foreach($products as $product)
						@include('components.productblock')
					@endforeach
				</div>
			</section>
		</div>
	</div>

	@if($products->hasPages() )
	<div class="row">
		<div class="small-12 medium-9 columns medium-push-3">
			<div class="pagination-centered">
				{{-- {!! $products->appends(Request::except('page'))->render() !!} --}}
				{!! $products->appends(Request::except('page'))->render(new Okuma\Pagination\FoundationPresenter($products) ) !!}
			</div>
		</div>
	</div>
	@endif
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ elixir('js/okuma/category.js') }}"></script>
@endsection