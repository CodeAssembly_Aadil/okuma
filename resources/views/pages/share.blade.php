<!doctype html>
<html class="no-js" lang="en">
<head>

	@include('components.head_static')

	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Okuma | Email Share</title>
	<meta name="description" content="Okuma Email Share makes it easy to share interesting information form the Okuma website with friends, directly to their inbox." />

</head>

<body class="share-page" id="sharePage">
	<div id="wrapper">
		<header>
			<div class="row">
				<div class="small-12 columns">
					<h1><span class="icon-okuma"></span></h1>
				</div>
			</div>
		</header>

		<main>
			@if(Session::has('share-response'))

			<div class="row text-center share-response">
				<div class="small-12 columns">

					@if(Session::get('share-response') === 'shared')
					<p><span class="large-icon bg-success icon-tick"></span></p>
					<h3>Thank you for sharing!</h3>
					<p class="message">
						You may close this window.
					{{-- </p> --}}
					@else
					<p>
						<span class="large-icon bg-error icon-attention-circle"></span>
					</p>
					<h3 class="title"><b><i>OOPS!</i></b> We hit a snag.</h3>
					<p class="message">
						Please close this window and try again.
					{{-- </p> --}}
					@endif
					<br/>
					{{-- <p class="message"> --}}
						Window will close in <b id="countDown">5</b> seconds.
					</p>
					<button class="button" id="closeButton"><span class="icon-cross"></span> CLOSE</button>
				</div>
			</div>

			<div class="row">
				<div class="small-12 columns text-center">
				</div>
			</div>

			@else

			<div class="row">
				<div class="small-12 columns">
					<h3>Email Share</h3>
					<p>Easily share interesting Okuma content with friends directly to their inbox.</p>
				</div>
			</div>

			<div class="row">
				<div class="small-12 columns">
					<form method="POST" action="{{ URL::route('share.request', ['type'=>request()->type,'id'=>request()->id ]) }}" accept-charset="UTF-8" id="share-form" enctype="application/x-www-form-urlencoded">

						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="row">
							<div class="small-12 columns">
								<div class="form-input underlined">
									<input type="text" name="sender-name" id="sender-name" class="input-field" maxlength="128" value="{{ old('sender-name') }}" autocomplete="off" />
									<label class="input-label" for="sender-name">
										<div class="input-label-value" >Your Name <span id="error-sender-name">{!!$errors->share->first('sender-name', '<span class="feedback-message"><i class="icon-attention-circle text-alert"></i> :message</span>')!!}</span></div>
									</label>
								</div>
							</div>
						</div>

						{!! Honeypot::generate('sender-surname', 'share-time') !!}

						<div class="row">
							<div class="small-12 columns">
								<div class="form-input underlined">
									<input type="text" name="sender-email" id="sender-email"  class="input-field" maxlength="254"value="{{ old('sender-email') }}" autocomplete="off" />
									<label class="input-label" for="sender-email" >
										<div class="input-label-value" >Your Email <span id="error-sender-email">{!! $errors->share->first('sender-email', '<span class="feedback-message"><i class="icon-attention-circle text-alert"></i> :message</span>') !!}</span></div>
									</label>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="small-12 columns">
								<div class="form-input underlined">
									<input type="text" name="recipient-email" id="recipient-email" class="input-field" maxlength="128" value="{{ old('recipient-email') }}" autocomplete="off" />
									<label class="input-label" for="recipient-email">
										<div class="input-label-value" >Friends Email <span id="error-recipient-email">{!!$errors->share->first('recipient-email', '<span class="feedback-message"><i class="icon-attention-circle text-alert"></i> :message</span>')!!}</span></div>
									</label>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="small-8 medium-4 columns">
								<button type="submit" class="form-control submit button success expand" id="share-submit"><span class="icon-email"></span> <b>Share</b></button>
							</div>
							<div class="small-4 medium-4 columns end">
								<button type="cancel" class="form-control submit button expand ajax-progress-button" id="share-cancel" onclick="window.close()"><span class="icon-cross"></span> <b>Close</b></button>
							</div>
							<div class="small-12 columns">
								<span id="error-form"></span>
							</div>
						</div>
					</form>

				</div>
			</div>

			@endif
		</main>
	</div>

	@if(Session::has('share-response'))
	<script type="text/javascript">
		var closeButton = document.getElementById("closeButton");
		var countDown = document.getElementById("countDown");
		var tick = 5;
		var intervalID;

		closeButton.addEventListener('click', closeWindow);

		function closeWindow(event) {
			window.close();
		}

		intervalID = setInterval(function() {
			countDown.innerHTML = tick;

			if (tick == 0) {
				clearInterval(intervalID);
				closeWindow();
			}

			tick--;
		}, 1000);
	</script>
	@endif

	@include('components.fonts')
	@include('components.analytics')
</body>
</html>
