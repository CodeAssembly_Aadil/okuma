@extends('layouts.master')

@section('pageTitle', 'Catalog')

@if(!is_null($catalog->snippet))
	@section('pageDescription', $catalog->snippet)
@endif

@section('bodyClass', 'catalog-page')

<?php $hasHeroBanner = isset($catalog->heroBanner);?>

@if($hasHeroBanner)
	@section('mastheadClass', 'has-hero-image')
@endif

@section('mastheadContent')
	@if($hasHeroBanner)
		<div class="hero-image" style="background-image: url({{ url($catalog->heroBanner->path) }});"></div>
	@endif

	<div class="page-header-strip">
		<div class="row valign-middle">
			<div class="small-12 columns">
				<p class="text-center">{{ $catalog->description }}</p>
			</div>
		</div>
	</div>
@endsection

@section('bodyContent')
	<div class="category-content">
		@foreach ($catalog->children as $category)
			@if ($category->total_products_count > 0)

				<div class="row"  id="category-{{ $category->slug }}">
					<div class="medium-12 large-3 columns">
						<div class="catalog-category-details">
							<h2><a href="{{ route('category', ['slug'=>$category->slug]) }}">{{ $category->name }}</a></h2>
							<a href="{{ route('category', ['slug'=>$category->slug]) }}" class="animated-link bg-block bg-skew-anim">
								View all <strong>{{ $category->product_count }}</strong> {{ $category->name }} <span class="icon-right-circle"></span>
							</a>
						</div>
					</div>

					<div class="medium-12 large-9 columns">
						<div class="row">
							<?php $columnSize = 'small-6 medium-4 large-3';?>
							@foreach ($category->products as $product)
								@include('components.productblock')
							@endforeach
						</div>
					</div>
				</div>

			@endif
		@endforeach
	</div>
@endsection