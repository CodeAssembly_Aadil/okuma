@extends('layouts.master')

@section('pageTitle', 'Search')

@section('bodyClass', 'search-page')

@section('mastheadContent')
<div class="row">
	<div class="small-12 columns">
		<h1 class="title-header">Search</h1>
	</div>
</div>
@endsection

@section('bodyContent')

	<div class="searchpage-content">

		<section>
			<div class="row">
				<div class="small-12 medium-7 small-centered columns">
					<form method="GET" action="{{ route('search') }}" accept-charset="UTF-8" id="site-search-form" enctype="application/x-www-form-urlencoded">
						<div class="bordered-input">
							<div class="row collapse">
								<div class="small-10 columns">
									<input type="text" placeholder="SEARCH" name="find" value="{{ $query }}">
								</div>
								<div class="small-2 columns">
									<button class="button expand postfix"><span class="icon-search"></span></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>

		<section class="search-results">
			@if(isset($query))

				<div class="row">
					<div class="small-12 large-9 small-centered columns">
						<h3 class="results-header">Results for: <em>{{ $query }}</em></h3>
					</div>
				</div>


				@if(!$searchResults->isEmpty())
					<div class="row">
						<div class="small-12 large-9 small-centered columns">
							@foreach($searchResults as $result)

								<article class="search-result">
									<a href="{{ URL::route( strtolower($result->type), ['slug' => $result->slug]) }}">
										<div class="row">
											@if(isset($result->thumbnail))
												<div class="small-4 medium-3 columns">
													<img src="{{ URL::asset($result->thumbnail->path) }}" />
												</div>
												<div class="small-8 medium-9 columns">
											@else
												<div class="small-12 columns">
											@endif
													<h1 class="search-title">{{ $result->name }}</h1>
													<p class="search-type">{{ $result->type }}</p>
													@if(isset($result->snippet))
														<p class="search-snippet">{{ $result->snippet }}</p>
													@endif
												</div>
										</div>
									</a>
								</article>

							@endforeach
						</div>
					</div>

					@if(!is_null($pagination))
						<div class="row">
							<div class="small-12 large-9 small-centered columns">
								<br/>
								<div class="pagination-centered">
									{!! $pagination->appends(Request::except('page'))->render(new Okuma\Pagination\SimpleFoundationPresenter($pagination) ) !!}
								</div>
							</div>
						</div>
					@endif

				@else
					<div class="row">
						<div class="small-12 large-9 small-centered columns">
							<h3 class="no-query"><strong>Gosh!</strong> We couldn't find what you were looking for.</h3>
						</div>
					</div>
				@endif
			@else
				<div class="row">
					<div class="small-12 medium-7 small-centered columns">
						<h3 class="no-query"><strong>Woops!</strong> looks like there isn't anything to find. <span>Please enter your search in the form above.</span></h3>
					</div>
				</div>
			@endif
		</section>

	</div>
@endsection
