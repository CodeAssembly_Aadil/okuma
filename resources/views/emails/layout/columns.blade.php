<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateColumns">
    <tr>
        <td align="left" valign="top" class="columnsContainer" width="50%">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateColumn">
                <tr>
                    <td valign="top" class="leftColumnContainer">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                            <tbody class="mcnCaptionBlockOuter">
                                <tr>
                                    <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">
                                        <table border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightContentOuter" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="mcnCaptionRightContentInner" style="padding:0 9px ;">
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightImageContentContainer">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="mcnCaptionRightImageContent" valign="top">
                                                                        <a href="https://facebook.com/OkumaFishingAfrica" title="Follow Okuma Fishing Africa on Facebook" class="" target="_blank">
                                                                            <img alt="" src="{{ URL::asset('images/emails/icon_facebook_128x128.png') }}" width="57" style="max-width:128px;" class="mcnImage">
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table class="mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="171">
                                                            <tbody>
                                                                <tr>
                                                                    <td valign="top" class="mcnTextContent">
                                                                        <div> Join the conversation on <a href="https://facebook.com/OkumaFishingAfrica" id="Okuma Facebook" name="Okuma Facebook" target="_blank" title="Follow Okuma Fishing Africa on Facebook">Facebook</a> and become a part of Okuma's Inspired Fishing community.</div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td align="left" valign="top" class="columnsContainer" width="50%">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateColumn">
                <tr>
                    <td valign="top" class="rightColumnContainer">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                            <tbody class="mcnCaptionBlockOuter">
                                <tr>
                                    <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">
                                        <table border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightContentOuter" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="mcnCaptionRightContentInner" style="padding:0 9px ;">
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightImageContentContainer">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="mcnCaptionRightImageContent" valign="top">
                                                                        <a href="{{ URL::route('newsletter.subscribe') }}" title="Subscribe to the Okuma Newsletter" class="" target="_blank">
                                                                            <img alt="" src="{{ URL::asset('images/emails/icon_newsletter_128x128.png') }}" width="57" style="max-width:128px;" class="mcnImage">
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table class="mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="171">
                                                            <tbody>
                                                                <tr>
                                                                    <td valign="top" class="mcnTextContent">
                                                                        <div><a href="{{ URL::route('newsletter.subscribe') }}" target="_blank" title="Subscribe to the Okuma Newsletter">Subscribe to the Okuma Newsletter</a> and get the latest Product News, Updates and More delivered straight to your inbox.</div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
