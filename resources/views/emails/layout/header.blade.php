<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
    <tr>
        <td valign="top" class="headerContainer">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock">
                <tbody class="mcnImageBlockOuter">
                    <tr>
                        <td valign="top" style="padding:0px" class="mcnImageBlockInner">
                            <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer">
                                <tbody>
                                    <tr>
                                        <td class="mcnImageContent" valign="top" style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;">
                                            <img align="center" alt="" src="{{ URL::asset('images/emails/contact_header_light.png') }}" width="600" style="max-width:600px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
