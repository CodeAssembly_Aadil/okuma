<table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader">
    <tr>
        <td valign="top" class="preheaderContainer" style="">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                                <tbody>
                                    <tr>
                                        <td valign="top" class="mcnTextContent" style="padding: 18px 18px 17px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 13px; line-height: 125%; text-align: left;">
                                            <em><strong>Okuma <span style="color:#F6B027">Inspired Fishing</span> | </strong> @yield('preHeaderTitle') </em>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
