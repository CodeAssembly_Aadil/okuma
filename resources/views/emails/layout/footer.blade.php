<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateFooter">
    <tr>
        <td valign="top" class="footerContainer" style="padding-bottom:9px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                                <tbody>
                                    <tr>
                                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                            <a href="{{ URL::route('home') }}" target="_blank" title="Okuma Inspired Fishing"><span style="font-size:16px"><span style="color:#F6B027">{{ preg_replace("#^[^:/.]*[:/]+#i", "", preg_replace("{/$}", "", urldecode(URL::route('home')))) }}</span></span></a>
                                            <br>
                                            <span style="line-height:20.7999992370605px">Okuma is distributed under licence by Sensational Angling Supplies.<br>
                                                Copyright © </span>{{ date('Y') }} <span style="line-height:20.7999992370605px">Okuma, All rights reserved. E&amp;OE.</span>
                                            <br>
                                            <a href="mailto:{!! $responseEmail !!}" style="font-size: 11px; line-height: 20.8px;" target="_blank" title="Contact Okuma"><span style="color:#001755">{!! $responseEmail !!}</span></a>
                                            <span> | </span>
                                            <a href="http://www.sensationtackle.co.za" style="font-size: 11px; line-height: 20.8px;" target="_blank" title="Sensational Angling Supplies"><span style="color:#ED1C24">www.sensationtackle.co.za</span></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
