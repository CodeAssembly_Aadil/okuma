@extends('emails.layout.master')

@section('preHeaderTitle',  $senderName . ' has shared this with you.')

@section('body')
<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
    <tr>
        <td valign="top" class="bodyContainer">

            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                                <tbody>
                                    <tr>
                                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                            <strong><span style="color:#001755">{{ $senderName }} would like you to check out <a href="{!! $link !!}" target="_blank" title="Okuma Inspired Fishing"><b><span style="color:#F6B027">{{ $title }}</span></b></a>.</span></strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            @if(isset($image))
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
                    <tbody class="mcnImageBlockOuter">
                        <tr>
                            <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                                <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                                    <tbody>
                                        <tr>
                                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">
                                                <img align="center" alt="{{ $title }}" src="{!! $image !!}" width="564" style="max-width:1200px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
             </table>
            @endif

            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                                <tbody>
                                    <tr>
                                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                            <h1>{{  $title  }}</h1>

                                            {{ $messageBody }}
                                            <br>
                                            <br>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock">
                <tbody class="mcnButtonBlockOuter">
                    <tr>
                        <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="left" class="mcnButtonBlockInner">
                            <div>
                                <!--[if mso]>
                                    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{!! $link !!}" style="height:40px;v-text-anchor:middle;width:300px;" arcsize="10%" stroke="f" fillcolor="#001755">
                                        <w:anchorlock/>
                                        <center style="color:#ffffff;font-family:sans-serif;font-size:16px;font-weight:bold;">Get more info</center>
                                    </v:roundrect>
                                <![endif]-->
                                <![if !mso]>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" width="150" height="40" bgcolor="#001755" style=";border-radius: 3px; color: #ffffff; display: block;">
                                            <a href="{!! $link !!}" style="font-size:16px; font-weight: bold; font-family:sans-serif; text-decoration: none; line-height:40px; width:100%; display:inline-block">
                                                <span style="color: #ffffff;">Get more info</span>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <![endif]>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                <tbody class="mcnDividerBlockOuter">
                    <tr>
                        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
                            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                                <tbody>
                                    <tr>
                                        <td>
                                            <span></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

@endsection
