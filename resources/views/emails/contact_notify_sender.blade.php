@extends('emails.layout.master')

@section('preHeaderTitle', 'Thank you for getting in touch with us.')

@section('body')
<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
    <tr>
        <td valign="top" class="bodyContainer">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                                <tbody>
                                    <tr>
                                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                            <h1>Hello!</h1>
                                            <h3 class="null">Thank you for getting in touch with us.</h3>
                                            <br> We're just checking in to let you know that we have received your message and will get back to you as soon as possible. Below is a copy of the message we received through our contact form at <a href="{{ URL::route('contact.index') }}" target="_blank" title="Okuma Contact Us">{{ preg_replace("#^[^:/.]*[:/]+#i", "", preg_replace("{/$}", "", urldecode(URL::route('contact.index')))) }}</a>.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock">
                <tbody class="mcnDividerBlockOuter">
                    <tr>
                        <td class="mcnDividerBlockInner" style="padding: 18px;">
                            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top-width: 2px;border-top-style: solid;border-top-color: #CCCCCC;">
                                <tbody>
                                    <tr>
                                        <td>
                                            <span></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                                <tbody>
                                    <tr>
                                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                            <h4 class="null">
                                                                                    Time: <span style="color:#34495E">{{ $timestamp }}</span><br>
                                                                                    Name: <span style="color:#34495E">{{ $name }}</span><br>
                                                                                    Subject: <span style="color:#34495E">{{ $subject }}</span><br>
                                                                                    <br>
                                                                                    Message:
                                                                            </h4> @foreach($messageBody as $paragraph)
                                            <br>{{ $paragraph }} @endforeach
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                <tbody class="mcnDividerBlockOuter">
                    <tr>
                        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
                            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #CCCCCC;">
                                <tbody>
                                    <tr>
                                        <td>
                                            <span></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
                        </td>
                    </tr>
                </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                                <tbody>
                                    <tr>
                                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                            <span style="font-size:12px">Reply directly to this email or contact us at <a href="mailto:{!! $responseEmail !!}?subject={{ $subject }}" target="_blank">{!! $responseEmail !!}</a></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                <tbody class="mcnDividerBlockOuter">
                    <tr>
                        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
                            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                                <tbody>
                                    <tr>
                                        <td>
                                            <span></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

@endsection
