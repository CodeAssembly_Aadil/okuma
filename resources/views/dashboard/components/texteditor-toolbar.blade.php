<div class="form-group @if($errors->has($field)) has-error @endif">

	{!! Form::label($field, $label. ': ' . $errors->first($field), array('class' => 'control-label')) !!}

	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="btn-toolbar texteditor-toolbar" style="display:none" data-stylesheet="{!! URL::asset('css/okuma/main.css') !!}">
			</div>
		</div>
		{!! Form::textArea($field, null , [ 'class'=>'form-control panel-body texteditor-textarea', 'placeholder'=> $placeholder, 'rows'=>15 ])!!}
	</div>

	<p class="help-block"><span class="label label-info">Required</span> <strong>Description</strong> of this Product. Appears on Product pages. Supports <code>&lt;html&gt;</code> tags.</p>
</div>
