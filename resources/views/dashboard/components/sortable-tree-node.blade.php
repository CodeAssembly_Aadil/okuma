<?php $routeParams = [$node->id];?>
<li class="sortable-item" data-id="{{ $node->id }}">
  <div class="sortable-item-wrapper">
    <span class="title"><span class="label label-default">#{{ $node->id }}</span> {{ $node->name }} </span>

    <div class="btn-group btn-group-sm model-actions">

      @include('dashboard.components.buttons.edit', ['url'=> URL::route($nodeRoute.'edit', $routeParams)])

      @if($node->trashed())
        @include('dashboard.components.buttons.destroy', ['url'=> URL::route($nodeRoute.'.destroy', $routeParams)])
        @include('dashboard.components.buttons.restore', ['url'=> URL::route($nodeRoute.'restore', $routeParams)])
      @else
        @include('dashboard.components.buttons.trash', ['url'=> URL::route($nodeRoute.'destroy', $routeParams)])
      @endif

    </div>
  </div>

  <ol class="sub-list">
    @foreach($node->getChildren() as $child)
      @include('dashboard.components.sortable-tree-node', ['node'=>$child, 'nodeRoute' => $nodeRoute])
    @endforeach
  </ol>
</li>
