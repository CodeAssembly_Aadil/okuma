@if($errors->has())
<div class="well text-danger">
	@foreach ($errors->all() as $error)
	<div>{{ $error }}</div>
	@endforeach
</div>
@endif
