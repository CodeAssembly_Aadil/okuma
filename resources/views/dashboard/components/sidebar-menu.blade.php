<?php $currentRoute = Route::currentRouteName();?>

<a class="link @if($currentRoute == 'dashboard.home') active @endif" href="{{ route('dashboard.home') }}#">
	<i class="fa fa-lg fa-fw fa-tachometer"></i> Dashboard
</a>
<a class="link @if($currentRoute == 'dashboard.sitefeature.index') active @endif" href="{{ route('dashboard.sitefeature.index') }}#">
	<i class="fa fa-lg fa-fw fa-home"></i> Homepage
</a>
<a class="link @if($currentRoute == 'dashboard.category.index') active @endif" href="{{ route('dashboard.category.index') }}">
	<i class="fa fa-lg fa-fw fa-sitemap"></i> Categories
</a>
<a class="link @if($currentRoute == 'dashboard.product.index') active @endif" href="{{ route('dashboard.product.index') }}">
	<i class="fa fa-lg fa-fw fa-rocket"></i> Products
</a>
<a class="link @if($currentRoute == 'dashboard.featureset.index') active @endif" href="{{ route('dashboard.featureset.index') }}">
	<i class="fa fa-lg fa-fw fa-th-large"></i> Feature Sets
</a>
<a class="link @if($currentRoute == 'dashboard.feature.index') active @endif" href="{{ route('dashboard.feature.index') }}">
	<i class="fa fa-lg fa-fw fa-list-ol"></i> Features
</a>
<a class="link @if($currentRoute == 'dashboard.contact') active @endif" href="{{ route('dashboard.contact') }}">
	<i class="fa fa-lg fa-fw fa-envelope"></i> Contact
</a>
<a class="link @if($currentRoute == 'dashboard.backup.index') active @endif" href="{{ route('dashboard.backup.index') }}">
	<i class="fa fa-lg fa-fw fa-database"></i> Backup
</a>
<a class="link @if($currentRoute == 'dashboard.logs.index') active @endif" href="{{ route('dashboard.logs.index') }}">
	<i class="fa fa-lg fa-fw fa-file-text"></i> Logs
</a>
<a class="link" href="{{ route('dashboard.phpinfo') }}" target="_blank">
	<i class="fa fa-lg fa-fw fa-info-circle"></i> PHP Info
</a>
