<a
	data-action="view"
	data-method="GET"
	data-process="false"

	class="btn btn-info"
	title="View"

	href="{!! $url !!}"
	target="_blank"
>
<i class="fa fa-fw fa-lg fa-external-link"></i>
</a>
