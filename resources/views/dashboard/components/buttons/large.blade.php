<a
	data-action="large"
	data-method="GET"
	data-process="false"

	class="btn btn-info"
	title="View Large"

	href="{!! $url !!}"
	target="_blank"
>
<i class="fa fa-fw fa-lg fa-expand"></i>
</a>
