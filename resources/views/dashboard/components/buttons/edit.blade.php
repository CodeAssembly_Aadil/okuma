<a
	data-action="edit"
	data-method="GET"
	data-process="false"

	class="btn btn-primary"
	title="Edit"

	href="{!! $url !!}"
>
<i class="fa fa-fw fa-lg fa-pencil"></i>
</a>
