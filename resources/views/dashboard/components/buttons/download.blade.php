<a
	data-action="download"
	data-method="GET"
	data-process="false"

	class="btn btn-info"
	title="Download"

	href="{!! $url !!}"
	target="_blank"
>
<i class="fa fa-fw fa-lg fa-download"></i>
</a>
