<a
	data-action="unpublish"
	data-method="PATCH"
	data-process="true"

	class="btn btn-default"
	title="Unpublish"

	href="{!! $url !!}"
>
<i class="fa fa-fw fa-lg fa-eye-slash"></i>
</a>
