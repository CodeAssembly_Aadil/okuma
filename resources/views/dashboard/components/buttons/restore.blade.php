<a
	data-action="restore"
	data-method="PATCH"
	data-process="true"

	class="btn btn-success"
	title="Restore"

	href="{!! $url !!}"
>
<i class="fa fa-fw fa-lg fa-undo"></i>
</a>
