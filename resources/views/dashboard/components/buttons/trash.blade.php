<a
	data-action="trash"
	data-method="DELETE"
	data-process="true"

	class="btn btn-warning"
	title="Send to Trash"

	href="{!! $url !!}"
>
<i class="fa fa-fw fa-lg fa-trash-o"></i>
</a>
