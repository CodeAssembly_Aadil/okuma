<a
	data-action="destroy"
	data-method="DELETE"
	data-process="true"

	class="btn btn-danger"
	title="Permanently Delete"

	href="{!! $url !!}"
>
<i class="fa fa-fw fa-lg fa-trash"></i>
</a>
