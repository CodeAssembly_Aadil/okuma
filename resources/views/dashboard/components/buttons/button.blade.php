<a
	data-action="{{ $dataAction }}"
	data-method="{{ $dataMethod }}"
	data-process="{{ $dataProcess }}"

	class="btn {{ $class }}"
	title="{{ $title }}"

	href="{!! $url !!}"
>
<i class="fa fa-fw fa-lg {{ $icon }}"></i>
</a>
