<a
	data-action="publish"
	data-method="PATCH"
	data-process="true"

	class="btn btn-success"
	title="Publish"

	href="{!! $url !!}"
>
<i class="fa fa-fw fa-lg fa-eye"></i>
</a>
