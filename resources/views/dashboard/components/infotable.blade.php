<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>{!! $id !!}</td>
      <td>{!! $name !!}</td>
    </tr>
  </tbody>
</table>
