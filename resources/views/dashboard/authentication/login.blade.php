<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Okuma Dashbord | Login</title>

	<!-- <link rel="stylesheet" href="{{ URL::asset('dashboard_assets/css/vendor/bootstrap.flatly.css') }}"> -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/flatly/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6 col-md-offset-3">
				<div class="panel panel-default" style="margin-top: 48px">
					<div class="panel-heading">
						<h3 class="panel-title">Okuma Dashbord</h3>
					</div>
					<div class="panel-body">

						<form class="form-horizontal" action="{{ action('Auth\AuthController@authenticate') }}" method="post">

							<input type="hidden" name="_token" value="{{ csrf_token() }}" />

							@if(session()->has('notice'))
							 <div class="alert @if(session()->has('noticeLevel')){{ 'alert-' . session()->get('noticeLevel') }}@else alert-info @endif"><span class="glyphicon glyphicon-alert"></span> {{ session()->get('notice') }}</div>
							@endif

							<div class="form-group @if($errors->has('email'))has-error @endif has-feedback">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><span class="fa fa-lg fa-fw fa-user"></span></span>
										<input type="text" class="form-control" name="email" placeholder="Email Address" autofocus>
									</div>
									<span class="glyphicon @if($errors->has('email')) glyphicon-remove @endif form-control-feedback"></span>
								</div>
							</div>

							<div class="form-group @if($errors->has('password')) has-error @endif has-feedback">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><span class="fa fa-lg fa-fw fa-key"></span></span>
										<input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off">
									</div>
									<span class="glyphicon @if($errors->has('password')) glyphicon-remove @endif form-control-feedback"></span>
								</div>
							</div>

							<div style="margin-top:10px" class="form-group">
								<!-- Button -->

								<div class="col-sm-12 controls">
									<button id="btn-login" href="#" class="btn btn-primary btn-block"><span class="fa fa-lg fa-fw fa-sign-in"></span> Login</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
