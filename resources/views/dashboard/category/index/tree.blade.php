@extends('dashboard.common.tab.index')

@section('pageTitle')
@parent
- Tree
@endsection

@section('pageHeader')
@parent
- Tree
@endsection

@section('controls')
@parent
<form action="{{ route('dashboard.category.reorder') }}" method="POST" class="inline-block" id="category-reorder-form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="PATCH" />
    <input type="hidden" name="tree" value=""/>
    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-save"></i> Save Category Tree</button>
</form>

<a href="{{ route('dashboard.category.tree') }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-undo"></i> Reset Categories</a>
@endsection

@section('tabContent')

<div class="row">

    <div class="col-sm-12">
        <p class="help-block"><i class="fa fa-fw fa-lg fa-arrows"></i> <em>Drag and drop Categories to reorder them.</em> This is the order in which Categories appear in the menu.</p>
    </div>

    <div class="col-sm-12">
    <div id="category-tree" class="sortable-tree">
            <ol class="sortable-list">
                @foreach($tree as $rootNode)
                <?php $routeParams = [$rootNode->id];?>

                @include('dashboard.components.sortable-tree-node', ['node'=>$rootNode, 'nodeRoute' => 'dashboard.category.'])
                @endforeach

            </ol>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/vendor/jquery-sortable-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/sortable.list.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/category.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
@endsection

