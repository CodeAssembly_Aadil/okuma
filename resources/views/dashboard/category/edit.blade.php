@extends('dashboard.master')

@section('pageTitle')
Category - Edit
@endsection

@section('pageHeader')
Category - Edit
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12 margin-bottom-16">
		<table class="table table-bordered">
			<thead>
				<tr class="@if($category->trashed()) warning @else info @endif" >
					<th>ID</th>
					<th>Name</th>
					<th>Slug</th>
					<th>Parent Category</th>
					<th>Position</th>
					<th>Depth</th>
					<th>Total Products</th>
					<th>Trashed</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $category->id }}</td>
					<td>{{ $category->name }}</td>
					<td>{{ $category->slug }}</td>
					<td>{!! $category->directAncestor->name or '<i class="fa fa-fw fa-sitemap"></i> Root' !!}</td>
					<td>{{ $category->position }}</td>
					<td>{{ $category->real_depth }}</td>
					<td>{{ $category->products_count }}</td>
					<td>@if($category->trashed()) <span class="label label-warning">TRUE</span> @else <span class="label label-default">FALSE</span> @endif</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-12 margin-bottom-32">

	<?php $routeName = Route::currentRouteName();?>

		<ul class="nav nav-tabs">
			<li class="@if($routeName == 'dashboard.category.edit') active @endif">
				<a href="{{ route('dashboard.category.edit', ['category'=>$category->id]) }}"><i class="fa fa-lg fa-fw fa-sitemap"></i> Category</a>
			</li>
			<li class="@if($routeName == 'dashboard.category.product.index') active @endif">
				<a href="{{ route('dashboard.category.product.index', ['category'=>$category->id]) }}"><i class="fa fa-lg fa-fw fa-rocket"></i> Products</a>
			</li>
		</ul>
	</div>
</div>

@yield('editContent')

@endsection

@section('scripts')
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/category.min.js') }}"></script>
@endsection
