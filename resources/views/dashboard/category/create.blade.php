@extends('dashboard.master')

@section('pageTitle')
Category - Create New
@endsection

@section('pageHeader')
Category - Create New
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<h3 class="text-muted">Category Details</h3>

		{!! Form::open(['route' => ['dashboard.category.store'], 'method'=> 'POST', 'id'=>'category-create-form']) !!}

		<div class="form-group @if($errors->has('name')) has-error @endif">
			{!! Form::label('name', 'Name: ' . $errors->first('name'), ['class' => 'control-label']) !!}
			{!! Form::text('name', null , ['class'=>'form-control', 'placeholder'=>'Category Name'])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Name</strong> of this Category. Appears in menus and on category pages. Root category Names appear as Menu Headers in the sidebar filter menu.</p>
		</div>

		<div class="form-group @if($errors->has('description')) has-error @endif">
			{!! Form::label('description', 'Description: ' . $errors->first('description'), ['class' => 'control-label']) !!}
			{!! Form::textArea('description', null , [ 'class'=>'form-control', 'placeholder'=>'Category Description', 'rows'=>10 ])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Description</strong> of this Category. Appears on category pages.</p>
		</div>

		<div class="form-group @if($errors->has('snippet')) has-error @endif">
			{!! Form::label('snippet', 'Snippet: ' . $errors->first('snippet'), array('class' => 'control-label')) !!}
			{!! Form::textArea('snippet', null , [ 'class'=>'form-control', 'placeholder'=>'Category Snippet', 'rows'=>4 ])!!}
			<p class="help-block"><span class="label label-info">Recommended</span> <strong>Snippet</strong> of this Category. Appears in search results, in Category page metadata and anywhere a short description may be needed.</p>
		</div>

		<a href="@if(Session::has('redirect')) {!! Session::get('redirect') !!} @else {{ route('dashboard.category.index') }} @endif" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
		<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-bolt"></i> Create</button>

		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/vendor/word-and-character-counter.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/category.min.js') }}"></script>
@endsection
