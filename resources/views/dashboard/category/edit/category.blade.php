@extends('dashboard.category.edit')

@section('pageTitle')
@parent
: Category
@endsection

@section('pageHeader')
@parent
: Category
@endsection

@section('editContent')

<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Category Hero Banner</h3>

		<div class="well well-lg">
			@if(isset($category->heroBanner))
			<img src="{{ URL::asset($category->heroBanner->path) }}" class="img-responsive" data-image-id="{{ $category->heroBanner->id }}">
			<br/>
			<div>
				<span class="label label-primary">#{{ $category->heroBanner->id }}</span>
				<span class="label label-info">{{ $category->heroBanner->updated_at }}</span>
			</div>
			<h5>{{ $category->heroBanner->title }}</h5>
			<div class="btn-group btn-group-sm model-actions">
				@include('dashboard.components.buttons.edit', ['url'=> URL::route('dashboard.image.edit', ['image'=>$category->heroBanner->id])])
				@include('dashboard.components.buttons.destroy', ['url'=> URL::route('dashboard.image.destroy', ['image'=>$category->heroBanner->id])])
			</div>
			@else
			<a class="btn btn-primary" href="{{ route('dashboard.category.image.create.hero',['category'=>$category->id]) }}"><i class="fa fa-lg fa-fw fa-picture-o"></i> Set Hero Banner</a>
			@endif
		</div>
		<p class="help-block"><span class="label label-default">Recommended</span> <strong>Hero Banner</strong> of this Category. Appears as a background on Category pages.</p>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Category Details</h3>

		{!! Form::model($category, ['route' => ['dashboard.category.update', $category->id], 'method'=> 'PATCH', 'id'=>'category-edit-form']) !!}

		<div class="form-group @if($errors->has('name')) has-error @endif">
			{!! Form::label('name', 'Name: ' . $errors->first('name', ':message'), array('class' => 'control-label')) !!}
			{!! Form::text('name', null , ['class'=>'form-control', 'placeholder'=>'Category Name'])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Name</strong> of this Category. Appears in Menus and on Category pages. Root category Names appear as Menu Headers in the sidebar filter menu.</p>
		</div>

		<div class="form-group @if($errors->has('description')) has-error @endif">
			{!! Form::label('description', 'Description: ' . $errors->first('description', ':message'), array('class' => 'control-label')) !!}
			{!! Form::textArea('description', null , [ 'class'=>'form-control', 'placeholder'=>'Category Description', 'rows'=>15 ])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Description</strong> of this Category. Appears on Category pages.</p>
		</div>

		<div class="form-group @if($errors->has('snippet')) has-error @endif">
			{!! Form::label('snippet', 'Snippet: ' . $errors->first('snippet'), array('class' => 'control-label')) !!}
			{!! Form::textArea('snippet', null , [ 'class'=>'form-control', 'placeholder'=>'Category Snippet', 'rows'=>4 ])!!}
			<p class="help-block"><span class="label label-info">Recommended</span> <strong>Snippet</strong> of this Category. Appears in search results, in Category page metadata and anywhere a short description may be needed.</p>
		</div>

		<div class="form-group">
			<a href="@if(Session::has('redirect')) {!! Session::get('redirect') !!} @else {{ route('dashboard.category.index') }} @endif" class="btn btn-default"><i class="fa fa-lg fa-fw fa-times-circle"></i> Cancel</a>
			<button type="submit" class="btn btn-primary"><i class="fa fa-lg fa-fw fa-save"></i> Save Changes</button>
		</div>

		{!! Form::close() !!}

	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/vendor/word-and-character-counter.min.js') }}"></script>
@parent
@endsection
