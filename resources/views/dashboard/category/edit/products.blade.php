@extends('dashboard.category.edit')

@section('pageTitle')
@parent
: Products
@endsection

@section('pageHeader')
@parent
: Products
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<link rel="stylesheet" href="{{ URL::asset('dashboard_assets/css/vendor/select2-bootstrap.min.css') }}">
@endsection

@section('editContent')

<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Category Products</h3>

		{!! Form::open() !!}

		<div class="form-group form-group-lg @if($errors->has('products')) has-error @endif">
			{!! Form::label(null, 'Search for Products: ' . $errors->first('products', ':message'), ['class' => 'control-label']) !!}
			<div class="input-group select2-bootstrap-append">
				{!! Form::select(null, [], null, ['id'=>'product-search', 'class'=>'form-control input-lg', 'data-url'=> route('dashboard.product.search', ['query' => '', 'page' => 1 ])] )!!}
				<span class="input-group-btn">
					<button class="btn btn-default" type="button" id="product-search-open">
						<i class="fa fa-fw fa-lg fa-search"></i>
					</button>
				</span>
			</div>
			<p class="help-block">Search for <strong>Products</strong> to add to this Category. Type <kbd>*</kbd> to see all options. Holddown <kbd>ctrl</kbd> key select multiple Products at once.</p>
		</div>

		{!! Form::close() !!}
	</div>
</div>

<div class="row">
	<div class="col-sm-12 margin-bottom-32">

		<form action="{{ route('dashboard.category.update', ['category'=>$category->id]) }}" method="POST" class="inline-block" id="products-reorder-form">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="_method" value="PATCH" />
			<input type="hidden" name="products" value=""/>
			<button class="btn btn-primary" type="submit"><i class="fa fa-lg fa-fw fa-save"></i> Save Products</button>
		</form>

		<a href="{{ route('dashboard.category.product.index', ['category'=>$category->id]) }}" class="btn btn-default"><i class="fa fa-lg fa-fw fa-undo"></i> Reset Products</a>
	</div>
</div>

<div class="row" id="product-grid">
	<div class="col-sm-12">
		<p class="help-block"><i class="fa fa-lg fa-fw fa-arrows"></i> <em>Drag and drop Products to reorder them.</em> This is the order in which items appear in this Category.</p>
		<br/>
	</div>

	<div class="col-sm-12">
		<div class="sortable-grid clearfix">
			<?php $defaultProductThumbnail = URL::asset($default_product_display_thumbnail->path)?>
			<?php $now = Carbon::now()?>

			@foreach ($category->products as $product)

			<figure class="thumbnail product-thumb sortable-item" data-id="{{ $product->id }}">
				@if(isset($product->thumbnail))
				<img src="{{ URL::asset($product->thumbnail->path) }}"  class="img-responsive" width="{{$product->thumbnail->width }}" height="$product->thumbnail->height" />
				@else
				<img src="{!! $defaultProductThumbnail !!}"  class="img-responsive" width="{{ $default_product_display_thumbnail->width }}" height="$default_product_display_thumbnail->height" />
				@endif
				<figcaption class="caption">
					<div>
						<span class="label @if($product->trashed()) label-warning @else label-success @endif">#{{ $product->id }}</span>

						@if(is_null($product->published_at))
							<span class="label label-default"><i class="fa fa-fw fa-eye-slash"></i> Draft</span>
						@elseif($now->gte($product->published_at))
							<span class="label label-success"><i class="fa fa-fw fa-eye" ></i> {!! $product->published_at !!}</span>
						@else
							<span class="label label-info"><i class="fa fa-fw fa-clock-o" ></i> {!! $product->published_at !!}</span>
						@endif
					</div>
					<h5 class="title"> {{ $product->name }}</h5>
					<div class="btn-group btn-group-sm model-actions">
						@include('dashboard.components.buttons.remove')
					</div>
				</figcaption>
			</figure>

			@endforeach
		</div>
	</div>
</div>

<div id="product-template" class="hidden" data-default-image="{!! URL::asset($default_product_display_thumbnail->path) !!}" data-toggle-publish="{!! URL::route('dashboard.product.togglepublish', ['product'=>'{product_id}']) !!}">
	<figure class="thumbnail product-thumb sortable-item" data-id="{product_id}">
		<img src="{image_path}" class="img-responsive" width="{{ $default_product_display_thumbnail->width }}" height="{{ $default_product_display_thumbnail->height }}" />
		<figcaption class="caption">
			<div>
				<span class="label {product_trashed}">#{product_id}</span>
				<span class="label {product_published}"><i class="fa fa-fw {product_published_icon}"></i> {product_published_at}</span>
			</div>
			<h5 class="title">{product_name}</h5>
			<div class="btn-group btn-group-sm model-actions">
				@include('dashboard.components.buttons.remove')
			</div>
		</figcaption>
	</figure>
</div>

@endsection

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/vendor/jquery-sortable-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/sortable.grid.min.js') }}"></script>
@parent
@endsection
