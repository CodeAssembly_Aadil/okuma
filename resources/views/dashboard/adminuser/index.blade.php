@extends('dashboard.master')

@section('pageTitle', 'Dashboard | Site Administrator')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<h1 class="page-header">Site Administrator</h1>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Administrator Details</h3>

		<div class="well well-lg">
		</div>

		<p class="help-block"><span class="label label-warning">Attention</span> This option will refresh the <strong>Search Index</strong>. Note this operation may take a long time to complete.</p>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Change password</h3>

		<div class="well well-lg">
		</div>
	</div>
</div>

@endsection

@section('scripts')
@endsection
