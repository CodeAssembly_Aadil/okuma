@extends('dashboard.master')

@section('pageTitle')
Search Manager
@endsection

@section('pageHeader')
Search Manager
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12 margin-bottom-16">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Search Index Queue Length</th>
					<th>Last Updated</th>
				</tr>
			</thead>
			<tbody>
				<tr id="queue-length" data-method="GET" data-url="{{ route('dashboard.search.queue.length') }}">
					<td>{{ $queueLength }}</td>
					<td>{{ $updatedAt }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Refresh  Search Index</h3>

		<div class="well well-lg model-actions">


			<a data-action="refresh" data-method="POST" data-process="true" class="btn btn-warning" href="{{ route('dashboard.search.refresh') }}">
				<i class="fa fa-fw fa-lg fa-refresh"></i> Refresh Search Index
			</a>

			{{-- <a class="btn btn-warning" href=""><i class="fa fa-fw fa-lg fa-wrench"></i> Rebuild Search Index</a> --}}
		</div>

		<p class="help-block"><span class="label label-warning">Attention</span> This option will refresh the <strong>Search Index</strong>. Note this operation may take a long time to complete.</p>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Rebuild Search Index</h3>

		<div class="well well-lg model-actions">


			<a data-action="rebuild" data-method="POST" data-process="true" class="btn btn-warning" href="{{ route('dashboard.search.build') }}">
				<i class="fa fa-fw fa-lg fa-wrench"></i> Rebuild Search Index
			</a>

			{{-- <a class="btn btn-warning" href=""><i class="fa fa-fw fa-lg fa-wrench"></i> Rebuild Search Index</a> --}}
		</div>

		<p class="help-block"><span class="label label-warning">Attention</span> This option will clear the <strong>Search Index</strong> and re-process all Search Documents. Note this operation may take a long time to complete.</p>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Delete Search Index</h3>

		<div class="well well-lg model-actions">

			<a data-action="destroy" data-method="DELETE" data-process="true" class="btn btn-danger" href="{{ route('dashboard.search.destroy') }}">
				<i class="fa fa-fw fa-lg fa-trash-o"></i> Delete Search Index
			</a>
			{{-- <a class="btn btn-danger" href=""><i class="fa fa-fw fa-lg fa-trash-o"></i> Delete Search Index </a> --}}
		</div>

		<p class="help-block"><span class="label label-danger">Attention</span> This option will delete the <strong>Search Index</strong>. search will not return any results until a search index in rebuilt.</p>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/search.min.js') }}"></script>
@endsection
