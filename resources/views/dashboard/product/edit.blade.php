@extends('dashboard.master')

@section('pageTitle')
Product - Edit
@endsection

@section('pageHeader')
Product - Edit
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12 margin-bottom-16">
		<table class="table table-bordered">
			<thead>
				<tr class="@if($product->trashed()) warning @else info @endif" >
					<th>ID</th>
					<th>Name</th>
					<th>Slug</th>
					<th>Created At</th>
					<th>Updated At</th>
					<th>Published At</th>
					<th>Trashed</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $product->id }}</td>
					<td>{{ $product->name }}</td>
					<td>{{ $product->slug }}</td>
					<td>{{ $product->created_at }}</td>
					<td>{{ $product->updated_at }}</td>
					<td>
						@if($product->isPublished())
							<span class="label label-success"><i class="fa fa-fw fa-lg fa-eye"></i>{{ $product->published_at }}</span>
						@elseif(is_null($product->published_at))
							<span class="label label-default"><i class="fa fa-fw fa-lg fa-eye-slash"></i> 'DRAFT'</span>
						@else
							<span class="label label-default"><i class="fa fa-fw fa-lg fa-clock-o"></i> {{ $product->published_at }}</span>
						@endif
					</td>

					<td>@if($product->trashed()) <span class="label label-warning">TRASHED</span> @else <span class="label label-default">FALSE</span> @endif</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-12 margin-bottom-32">

	<?php $routeName = Route::currentRouteName();?>

		<ul class="nav nav-tabs" data-active-tab="@yield('activeTab')" >
			<li class="@if($routeName == 'dashboard.product.edit') active @endif">
				<a href="{{ route('dashboard.product.edit', ['product'=>$product->id]) }}"><i class="fa fa-lg fa-fw fa-rocket"></i> Product</a>
			</li>
			<li class="@if($routeName == 'dashboard.product.image.index') active @endif">
				<a href="{{ route('dashboard.product.image.index', ['product'=>$product->id]) }}"><i class="fa fa-lg fa-fw fa-picture-o"></i> Images</a>
			</li>
			<li class="@if($routeName == 'dashboard.product.variant.index') active @endif">
				<a href="{{ route('dashboard.product.variant.index', ['product'=>$product->id]) }}"><i class="fa fa-lg fa-fw fa-cubes"></i> Variants</a>
			</li>
			<li class="@if($routeName == 'dashboard.product.feature.index') active @endif">
				<a href="{{ route('dashboard.product.feature.index', ['product'=>$product->id]) }}"><i class="fa fa-lg fa-fw fa-list-ol"></i> Features</a>
			</li>
			<li class="@if($routeName == 'dashboard.product.category.index') active @endif">
				<a href="{{ route('dashboard.product.category.index', ['product'=>$product->id]) }}"><i class="fa fa-lg fa-fw fa-sitemap"></i> Categories</a>
			</li>
			<li>
				<a href="{{ route('dashboard.product.show', ['slug' => $product->slug]) }}" target="_blank" ><i class="fa fa-lg  fa-fw fa-external-link"></i> View</a>
			</li>
		</ul>
	</div>
</div>

@yield('editContent')

@endsection

@section('scripts')
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/product.min.js') }}"></script>
@endsection
