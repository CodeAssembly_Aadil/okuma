@extends('dashboard.master')

@section('pageTitle')
Variant - Create New
@endsection

@section('pageHeader')
Variant - Create New
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Product ID</th>
					<th>Product Name</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $product->id }}</td>
					<td>{{ $product->name }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		{!! Form::open(['route' => ['dashboard.product.variant.store', $product->id], 'method'=> 'POST']) !!}

		<div class="form-group @if($errors->has('model')) has-error @endif">
			{!! Form::label('model', 'Model: ' . $errors->first('model'), array('class' => 'control-label')) !!}
			{!! Form::text('model', null , ['class'=>'form-control', 'placeholder'=>'Variant Model', 'maxlength' => 32])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Model</strong> name of this Product Variant.</p>
		</div>

		<div class="form-group @if($errors->has('sku')) has-error @endif">
			{!! Form::label('sku', 'SKU: ' . $errors->first('description'), array('class' => 'control-label')) !!}
			{!! Form::text('sku', null , [ 'class'=>'form-control', 'placeholder'=>'Variant SKU', 'maxlength' => 32 ])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>SKU</strong> to which this Product Variant corresponds.</p>
		</div>

		<?php $productSpec = json_decode($product->specification)?>

		<div class="form-group @if($errors->has('specification')) has-error @endif">
			{!! Form::label(null, 'Specifications: ' . $errors->first('specification'), array('class' => 'control-label')) !!}

			@if(empty($productSpec))
				<p class="text-muted"><i>Product Specification is Undefined!</i></p>
			@else
			<table class="table table-bordered">
				<thead>
					<tr>
						@foreach($productSpec as $property)
						<th>
							{{ $property }}
						</th>
						@endforeach
					</tr>
				</thead>
				<tbody>
					<tr>
						@foreach($productSpec as $property)
						<?php $name = str_slug('spec-' . $property);?>
						<td>{!! Form::text($name, old($name), ['class'=>'form-control', 'placeholder'=>$property]) !!}</td>
						@endforeach
					</tr>
				</tbody>
			</table>
			@endif

			<p class="help-block"><span class="label label-default">Optional</span> <strong>Specifications</strong> of this Product Variant.</p>
		</div>

		<div class="form-group">
			<a href="{{ route('dashboard.product.variant.index', ['product'=>$product->id]) }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
			<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-bolt"></i> Create</button>
		</div>

		{!! Form::close() !!}
	</div>
</div>
@endsection
