@extends('dashboard.master')

@section('pageTitle')
Variant - Edit
@endsection

@section('pageHeader')
Variant - Edit
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<table class="table table-bordered">
			<thead >
				<tr class="@if($variant->trashed()) warning @else info @endif" >
					<th>ID</th>
					<th>Model</th>
					<th>SKU</th>
					<th>Product ID</th>
					<th>Product Name</th>
					<th>Trashed</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $variant->id }}</td>
					<td>{{ $variant->model }}</td>
					<td>{{ $variant->sku }}</td>
					<td>{{ $product->id }}</td>
					<td>{{ $product->name }}</td>
					<td>@if($variant->trashed()) <span class="label label-warning">TRUE</span> @else <span class="label label-default">FALSE</span> @endif</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<?php $productSpec = json_decode($product->specification)?>
<?php $variantSpec = json_decode($variant->specification)?>

<div class="row">
	<div class="col-sm-12">
		{!! Form::model($variant, ['route' => ['dashboard.product.variant.update', $product->id, $variant->id], 'method'=> 'PATCH']) !!}

		<div class="form-group @if($errors->has('model')) has-error @endif">
			{!! Form::label('model', 'Model: ' . $errors->first('model'), array('class' => 'control-label')) !!}
			{!! Form::text('model', null , ['class'=>'form-control', 'placeholder'=>'Variant Model', 'maxlength' => 32])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Model</strong> name of this Product Variant.</p>
		</div>

		<div class="form-group @if($errors->has('sku')) has-error @endif">
			{!! Form::label('sku', 'SKU: ' . $errors->first('sku'), array('class' => 'control-label')) !!}
			{!! Form::text('sku', null , ['class'=>'form-control', 'placeholder'=>'Variant SKU', 'maxlength' => 32])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>SKU</strong> to which this Product Variant corresponds.</p>
		</div>

		<div class="form-group @if($errors->has('specification')) has-error @endif">
			{!! Form::label(null, 'Specifications: ' . $errors->first('specification'), array('class' => 'control-label')) !!}

			@if(empty($productSpec))
				<p class="text-muted"><i>Product Specification is Undefined!</i></p>
			@else
			<table class="table table-bordered">
				<thead>
					<tr>
						@foreach($productSpec as $property)
						<th>
							{{ $property }}
						</th>
						@endforeach
					</tr>
				</thead>
				<tbody>
					<tr>
						@foreach($productSpec as $property)
						<?php $name = str_slug('spec-' . $property);?>
						<td>{!! Form::text($name, (old($name) !== null) ? old($name) : isset($variantSpec->$property)? $variantSpec->$property : null  , ['class'=>'form-control', 'placeholder'=>$property]) !!}</td>
						@endforeach
					</tr>
				</tbody>
			</table>
			@endif

			<p class="help-block"><span class="label label-default">Optional</span> <strong>Specifications</strong> of this Product Variant.</p>
		</div>

		<div class="form-group">
			<a href="{{ route('dashboard.product.variant.index', ['product'=>$product->id]) }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
			<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-save"></i> Save Changes</button>
		</div>

		{!! Form::close() !!}

	</div>
</div>

@endsection
