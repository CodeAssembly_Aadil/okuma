@extends('dashboard.product.edit', ['sectionTitle' => ': Categories'])

@section('pageTitle')
@parent
: Categories
@endsection

@section('pageHeader')
@parent
: Categories
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<link rel="stylesheet" href="{{ URL::asset('dashboard_assets/css/vendor/select2-bootstrap.min.css') }}">
@endsection

@section('activeTab', 'categories')

@section('editContent')
<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Product Categories</h3>

		{!! Form::model($product, ['route' => ['dashboard.product.update', $product->id], 'method'=> 'PATCH', 'id'=>'product-category-form']) !!}

		<div class="form-group form-group-lg @if($errors->has('categories')) has-error @endif">
			{!! Form::label('categories', 'Search for Categories: ' . $errors->first('categories', ':message'), ['class' => 'control-label']) !!}

			<div class="input-group select2-bootstrap-append">
				<select id="categories" class="form-control input-lg" data-url="{{ route('dashboard.category.search', ['query' => '', 'page' => 1 ]) }}" multiple="multiple" name="categories[]">
					@foreach ($product->categories as $category) {
					<option value="{{ $category->id }}" selected="selected" data-trashed="{{ $category->trashed() }}">{{ $category->name }}</option>
					@endforeach
				</select>
				<span class="input-group-btn">
				<button class="btn btn-warning" type="button" id="categories-clear">
						<i class="fa fa-fw fa-lg fa-times"></i>
					</button>
				</span>
			</div>

			<p class="help-block"><span class="label label-info">Required</span> Select <strong>Categories</strong> to witch this Product belongs. Can select Multiple Categories. Type <kbd>*</kbd> to see all options. Holddown <kbd>ctrl</kbd> key select multiple Categories at once.</p>
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-save"></i> Save Categories</button>
			<a href="{{ route('dashboard.product.category.index', ['product'=>$product->id]) }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-undo"></i> Reset Categories</a>
		</div>
		{!! Form::close() !!}

	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
@parent
@endsection
