@extends('dashboard.product.edit')

@section('pageTitle')
@parent
: Variants
@endsection

@section('pageHeader')
@parent
: Variants
@endsection

@section('activeTab', 'variants')

@section('editContent')

<?php
$productSpec = json_decode($product->specification);

if (!isset($productSpec)) {
    $productSpec = [];
}
?>
<?php $routeParams = ['product' => $product->id, 'variant' => -1];?>

<div class="row">


	<div class="col-sm-12 margin-bottom-32">

		<h3 class="text-muted">Product Specification</h3>

		{!! Form::model($product, ['route' => ['dashboard.product.update', $product->id], 'method'=> 'PATCH', 'id'=>'product-specification-form' ]) !!}

		<div class="form-group @if($errors->has('specification')) has-error @endif">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<?php $name = "specification[]";?>
						@forelse ($productSpec as $property)
						<td>
							<div class="input-group">
								{!! Form::text($name, $property, ['class'=>'form-control', 'placeholder'=>'Specification']) !!}
								<span class="input-group-btn">
									<button class="btn btn-danger" type="button" data-action="remove" title="Remove"><i class="fa fa-fw fa-times-circle"></i></button>
								</span>
							</div>
						</td>
						@empty
						<td>
							<div class="input-group">
								{!! Form::text($name, null, ['class'=>'form-control', 'placeholder'=>'Specification']) !!}
								<span class="input-group-btn">
									<button class="btn btn-danger" data-action="remove" type="button" title="Remove"><i class="fa fa-fw fa-times-circle"></i></button>
								</span>
							</div>
						</td>
						@endforelse
						<td><button type="button" class="btn btn-success" data-action="add" title="Add Product Specification Field"><i class="fa fa-fw fa-plus-circle"></i></button></td>
					</tr>
				</tbody>
			</table>
			<p class="help-block"><span class="label label-info">Required</span> <strong>Specifications</strong> of this Product. Defines table headers for Variants. Appears on Product page.</p>
		</div>

		<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-save"></i> Save Specification</button>

		<a href="{{ route('dashboard.product.variant.index', ['product'=>$product->id]) }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-undo"></i> Reset Specification</a>
		{!! Form::close() !!}
	</div>
</div>

<div class="row">
	<div class="col-sm-12 margin-bottom-32">

		<h3 class="text-muted">Product Variants</h3>

		<div >
			<a class="btn btn-success" href="{{ route('dashboard.product.variant.create', ['product'=>$product->id]) }}"><i class="fa fa-fw fa-lg fa-plus-circle"></i> New Variant</a>

			<form action="{{ route('dashboard.product.variant.reorder', ['product'=>$product->id]) }}" method="POST" class="inline-block" id="variants-reorder-form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="_method" value="PATCH" />
				<input type="hidden" name="orderables" value=""/>
				<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-save"></i> Save Variants</button>
			</form>

			<a href="{{ route('dashboard.product.variant.index', ['product'=>$product->id]) }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-undo"></i> Reset Variants</a>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12 margin-bottom-16">

		<p class="help-block"><i class="fa fa-fw fa-lg fa-arrows"></i> <em>Drag and drop Variants (rows) to reorder them.</em></p>
		<br/>

		<table class="table table-bordered sortable-table" id="variant-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Model</th>
					<th>SKU</th>
					@foreach($productSpec as $property)
					<th>{{ $property }}</th>
					@endforeach
					<th>Updated At</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($product->variants as $variant)
				<?php $routeParams['variant'] = $variant->id;?>
				<tr class="sortable-row" data-id="{{ $variant->id }}">
					<td>{{ $variant->id }}</td>
					<td>{{ $variant->model }}</td>
					<td>{{ $variant->sku }}</td>

					<?php $variantSpec = json_decode($variant->specification)?>

					@foreach($productSpec as $property)
					<td>{{ $variantSpec->$property or '' }}</td>
					@endforeach

					<td>{{ $variant->updated_at }}</td>
					<td>
						<div class="btn-group btn-group-sm model-actions">

							@include('dashboard.components.buttons.edit', ['url'=> URL::route('dashboard.product.variant.edit', $routeParams)])

							@if($variant->trashed())
							@include('dashboard.components.buttons.destroy', ['url'=> URL::route('dashboard.product.variant.destroy', $routeParams)])
							@include('dashboard.components.buttons.restore', ['url'=> URL::route('dashboard.product.variant.restore', $routeParams)])
							@else
							@include('dashboard.components.buttons.trash', ['url'=> URL::route('dashboard.product.variant.destroy', $routeParams)])
							@endif

						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/vendor/jquery-sortable-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/sortable.table.min.js') }}"></script>
@parent
@endsection