@extends('dashboard.product.edit', ['sectionTitle' => ': Images'])

@section('pageTitle')
@parent
: Images
@endsection

@section('pageHeader')
@parent
: Images
@endsection

@section('activeTab', 'images')

@section('editContent')
<div class="row">
	<div class="col-sm-12 col-md-6">
		<h3 class="text-muted">Product Display Thumbnail</h3>

		<div class="well well-lg">

			@if(isset($product->thumbnail))

			<figure class="thumbnail product-thumb inline-block" data-image-id="{{ $product->thumbnail->id }}">
				<img src="{{ URL::asset($product->thumbnail->path) }}" class="img-responsive" width="{{ $product->thumbnail->width }}"  height="{{ $product->thumbnail->height }}" alt="{{ $product->thumbnail->title }}" >
				<figcaption class="caption">
					<div>
						<span class="label label-primary">#{{ $product->thumbnail->id }}</span>
						<span class="label label-info">{{ $product->thumbnail->updated_at }}</span>
					</div>
					<h5>{{ $product->thumbnail->title }}</h5>
					<div class="btn-group btn-group-sm model-actions">
						@include('dashboard.components.buttons.edit', ['url'=> URL::route('dashboard.image.edit', ['image'=>$product->thumbnail->id])])
						@include('dashboard.components.buttons.destroy', ['url'=> URL::route('dashboard.image.destroy', ['image'=>$product->thumbnail->id])])
					</div>
				</figcaption>
			</figure>
			<br/>
			@else
				<a class="btn btn-primary" href="{{ route('dashboard.product.image.create.thumbnail',['product'=>$product->id]) }}"><i class="fa fa-fw fa-lg fa-picture-o"></i> Set Display Thumbnail</a>
			@endif
		</div>
		<p class="help-block"><span class="label label-default">Recommended</span> <strong>Display Thumbnail</strong> of this Product. Appears as the default thumbnail in Product blocks.</p>
	</div>

	<div class="col-sm-12 col-md-6">
		<h3 class="text-muted">Product Facebook Post Image</h3>

		<div class="well well-lg">

			@if(isset($product->shareImage))

			<figure class="thumbnail product-thumb inline-block" data-image-id="{{ $product->shareImage->id }}">
				<img src="{{ URL::asset($product->shareImage->path) }}" class="img-responsive" width="{{ $product->shareImage->width }}"  height="{{ $product->shareImage->height }}" alt="{{ $product->shareImage->title }}" >
				<figcaption class="caption">
					<div>
						<span class="label label-primary">#{{ $product->shareImage->id }}</span>
						<span class="label label-info">{{ $product->shareImage->updated_at }}</span>
					</div>
					<h5>{{ $product->shareImage->title }}</h5>
					<div class="btn-group btn-group-sm model-actions">
						@include('dashboard.components.buttons.edit', ['url'=> URL::route('dashboard.image.edit', ['image'=>$product->shareImage->id])])
						@include('dashboard.components.buttons.destroy', ['url'=> URL::route('dashboard.image.destroy', ['image'=>$product->shareImage->id])])
					</div>
				</figcaption>
			</figure>
			<br/>
			@else
				<a class="btn btn-primary" href="{{ route('dashboard.product.image.create.shareimage',['product'=>$product->id]) }}"><i class="fa fa-fw fa-lg fa-picture-o"></i> Set Social Share Image</a>
			@endif
		</div>
		<p class="help-block"><span class="label label-default">Recommended</span> <strong>Social Share Image</strong> of this Product. Used when sharing on social networks such as facebook.</p>
	</div>
</div>

<div class="row margin-bottom-32">
	<div class="col-sm-12">

		<h3 class="text-muted">Product Images</h3>

		<div>
			<a class="btn btn-success" href="{{ route('dashboard.product.image.create.large',['product'=>$product->id]) }}"><i class="fa fa-fw fa-lg fa-plus-circle"></i> Add Image</a>

			<form action="{{ route('dashboard.image.reorder') }}" method="POST" class="inline-block" id="images-reorder-form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="_method" value="PATCH" />
				<input type="hidden" name="orderables" value=""/>
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-save"></i> Save Images</button>
			</form>

			<a href="{{ route('dashboard.product.image.index', ['product'=>$product->id]) }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-undo"></i> Reset Images</a>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">

		<p class="help-block"><i class="fa fa-fw fa-lg fa-arrows"></i> <em>Drag and drop Images to reorder them.</em></p>
		<br/>

		<div class="sortable-grid clearfix" id="images-container">

			@foreach ($product->images as $image)
			<?php $imageLarge = $image->relatedImages->first();?>

			<figure class="thumbnail product-thumb sortable-item" data-id="{{ $image->id }}" data-related-id="{{ $imageLarge->id }}">
				<img src="{{ URL::asset($image->path) }}" class="img-responsive" width="{{ $image->width }}"  height="{{ $image->height }}" alt="{{ $image->title }}" >
				<figcaption class="caption">
					<div>
						<span class="label label-primary">#{{ $image->id }}</span>
						<span class="label label-info">{{ $image->updated_at }}</span>
					</div>
					<h5 class="title">{{ $image->title }}</h5>
					<div class="btn-group btn-group-sm model-actions">
						@include('dashboard.components.buttons.large', ['url'=> URL::asset($imageLarge->path)])
						@include('dashboard.components.buttons.edit', ['url'=> URL::route('dashboard.image.edit', ['image'=>$imageLarge->id])])
						@include('dashboard.components.buttons.destroy', ['url'=> URL::route('dashboard.image.destroy', ['image'=>$image->id])])
					</div>
				</figcaption>
			</figure>

			@endforeach
		</div>
	</div>
	<br/>
</div>
@endsection

@section('scripts')
@parent
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/vendor/jquery-sortable-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/sortable.grid.min.js') }}"></script>
@endsection
