@extends('dashboard.product.edit')

@section('pageTitle')
@parent
: Product
@endsection

@section('pageHeader')
@parent
: Product
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.15.35/css/bootstrap-datetimepicker.min.css">
@endsection

@section('activeTab', 'product')

@section('editContent')
<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Product Details</h3>

		{!! Form::model($product, ['route' => ['dashboard.product.update', $product->id], 'method'=> 'PATCH']) !!}

		<div class="form-group @if($errors->has('name')) has-error @endif">
			{!! Form::label('name', 'Name: ' . $errors->first('name', ':message'), array('class' => 'control-label')) !!}
			{!! Form::text('name', null , ['class'=>'form-control', 'placeholder'=>'Product Name', 'maxlength' => 219])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Name</strong> of this Product.</p>
		</div>

		<div class="form-group @if($errors->has('slug')) has-error @endif">
			{!! Form::label('slug', 'Slug: ' . $errors->first('slug', ':message'), array('class' => 'control-label')) !!}
			{!! Form::text('slug', null , ['class'=>'form-control', 'placeholder'=>'Slug', 'maxlength' => 255])!!}
			<p class="help-block">
				<span class="label label-default">Optional</span>  <strong>Slug</strong> of this Product. The slug appears in the browser address bar as a <code>snake-case</code> identifier. Slugs are auto generated on save, but can be customized here.
				<br/>
				<span class="label label-warning">Attention</span> Invalid slugs will be overwritten with the default auto-generated slug. Leave blank to auto-generate
			</p>
		</div>

		<div class="form-group @if($errors->has('description')) has-error @endif">
			{!! Form::label('description', 'Description: ' . $errors->first('description'), array('class' => 'control-label')) !!}
			{!! Form::textArea('description', null , [ 'class'=>'form-control panel-body texteditor-textarea', 'placeholder'=>'Product Description', 'rows'=>15 ])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Description</strong> of this Product. Appears on Product pages. Supports <code>&lt;html&gt;</code> tags.</p>
		</div>

		<div class="form-group @if($errors->has('snippet')) has-error @endif">
			{!! Form::label('snippet', 'Snippet: ' . $errors->first('snippet'), array('class' => 'control-label')) !!}
			{!! Form::textArea('snippet', null , [ 'class'=>'form-control', 'placeholder'=>'Product Snippet', 'rows'=> 3, 'maxlength' => 255 ])!!}
			<p class="help-block"><span class="label label-info">Recommended</span> <strong>Snippet</strong> of this Product. Appears in search results, in Product page metadata and anywhere a short description may be needed.</p>
		</div>

		<div class="form-group @if($errors->has('published_at')) has-error @endif">
			{!! Form::label('published_at', 'Publish Date: ' . $errors->first('published_at'), array('class' => 'control-label')) !!}
			<div class="input-group date" id="published_at_datepicker">
				<span class="input-group-addon"><i class="fa fa-fw fa-calendar"></i></span>
				{!! Form::text('published_at', null , ['class'=>'form-control', 'placeholder'=>'Publish Date'])!!}
			</div>
			<p class="help-block"><span class="label label-default">optional</span> <strong>Publish Date</strong> of this Product. Only Products with a Publish Date set after the current Date will be visible. Leave empty to save as Draft</p>
		</div>

		<div class="form-group">
			<a href="{{ route('dashboard.product.index') }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
			<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-save"></i> Save Changes</button>
		</div>

		{!! Form::close() !!}

	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.15.35/js/bootstrap-datetimepicker.min.js"></script>
<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/texteditor.rules.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/texteditor.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/vendor/word-and-character-counter.min.js') }}"></script>
@parent
@endsection
