@extends('dashboard.product.edit', ['sectionTitle' => ': Features'])

@section('pageTitle')
@parent
: Features
@endsection

@section('pageHeader')
@parent
: Features
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<link rel="stylesheet" href="{{ URL::asset('dashboard_assets/css/vendor/select2-bootstrap.min.css') }}">
@endsection

@section('activeTab', 'features')

@section('editContent')
<div class="row">
	<div class="col-sm-12 margin-bottom-16">

		<h3 class="text-muted">Product Features</h3>

		{!! Form::open() !!}

		<div class="form-group form-group-lg @if($errors->has('features')) has-error @endif">
			{!! Form::label(null, 'Search for Features: ' . $errors->first('feature-ids', ':message'), ['class' => 'control-label']) !!}
			<div class="input-group select2-bootstrap-append">
				{!! Form::select(null, [], null, ['id'=>'feature-search', 'class'=>'form-control input-lg', 'data-url'=> route('dashboard.feature.search', ['query' => '', 'page' => 1 ])] )!!}
				<span class="input-group-btn">
					<button class="btn btn-default" type="button" id="feature-search-open">
						<i class="fa fa-fw fa-lg fa-search"></i>
					</button>
				</span>
			</div>
			<p class="help-block">Search for <strong>Features</strong> witch belong to this Product. Type <kbd>*</kbd> to see all Features. Holddown <kbd>ctrl</kbd> key select multiple Features at once.</p>
		</div>

		{!! Form::close() !!}
	</div>
</div>

<div class="row margin-bottom-32">
	<div class="col-sm-12">
		<form action="{{ route('dashboard.product.update', ['product'=>$product->id]) }}" method="POST" class="inline-block" id="features-reorder-form">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="_method" value="PATCH" />
			<input type="hidden" name="features" value=""/>
			<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-save"></i> Save Features</button>

			<a href="{{ route('dashboard.product.feature.index', ['product'=>$product->id]) }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-undo"></i> Reset Features</a>
		</form>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<p class="help-block"><i class="fa fa-fw fa-lg fa-arrows"></i> <em>Drag and drop Features to reorder them.</em> This is the order in which Features appear on the Product Page</p>
	</div>

	<div class="col-sm-12">

		<div id="feature-list">
			<ol class="sortable-list">
				@foreach($product->features as $feature)

				<li class="sortable-item" data-id="{{ $feature->id }}">
					<div class="sortable-item-wrapper">
						<span class="title"><span class="label @if($feature->trashed())label-warning @else label-success @endif">#{{ $feature->id }}</span> {{ $feature->name }} </span>
						<div class="btn-group btn-group-sm model-actions">
							@include('dashboard.components.buttons.remove')
						</div>
					</div>
				</li>

				@endforeach
			</ol>
		</div>
	</div>
</div>

<div id="feature-template" class="hidden">
	<li class="sortable-item" data-id="{feature_id}">
		<div class="sortable-item-wrapper">
			<span class="title"><span class="label {feature_trashed}">#{feature_id}</span> {feature_name} </span>
			<div class="btn-group btn-group-sm model-actions">
				@include('dashboard.components.buttons.remove')
			</div>
		</div>
	</li>
</div>

@endsection

@section('scripts')
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/vendor/jquery-sortable-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/sortable.list.min.js') }}"></script>
@parent
@endsection
