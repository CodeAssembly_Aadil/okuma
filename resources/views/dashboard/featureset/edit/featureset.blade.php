@extends('dashboard.featureset.edit', ['sectionTitle' => ''])

@section('pageTitle')
@parent
: Feature Set
@endsection

@section('pageHeader')
@parent
: Feature Set
@endsection

@section('editContent')

<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Feature Set Details</h3>

		{!! Form::model($featureSet, ['route' => ['dashboard.featureset.update', $featureSet->id], 'method'=> 'PATCH']) !!}

		<div class="form-group @if($errors->has('name')) has-error @endif">
			{!! Form::label('name', 'Name: ' . $errors->first('name', ':message'), array('class' => 'control-label')) !!}
			{!! Form::text('name', null , ['class'=>'form-control', 'placeholder'=>'Feature Set Name', 'maxlength' => 255])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Name</strong> of this Feature Set. Appears in Filter Menu.</p>
		</div>

		<div class="form-group">
			<a href="@if(Session::has('redirect')) {!! Session::get('redirect') !!} @else {{ route('dashboard.featureset.index') }} @endif" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
			<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-save"></i> Save Changes</button>
		</div>

		{!! Form::close() !!}

	</div>
</div>

@endsection
