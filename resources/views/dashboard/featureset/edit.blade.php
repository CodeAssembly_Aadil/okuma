@extends('dashboard.master')

@section('pageTitle')
Feature Sets - Edit
@endsection

@section('pageHeader')
Feature Sets - Edit
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12 margin-bottom-16">
		<table class="table table-bordered">
			<thead>
				<tr class="@if($featureSet->trashed()) warning @else info @endif" >
					<th>ID</th>
					<th>Name</th>
					<th>Slug</th>
					<th>Position</th>
					<th>Total Features</th>
					<th>Created At</th>
					<th>Updated At</th>
					<th>Published At</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $featureSet->id }}</td>
					<td>{{ $featureSet->name }}</td>
					<td>{{ $featureSet->slug }}</td>
					<td>{{ $featureSet->position }}</td>
					<td>{{ $featureSet->features_count }}</td>
					<td>{{ $featureSet->created_at }}</td>
					<td>{{ $featureSet->updated_at }}</td>
					<td>@if($featureSet->trashed()) <span class="label label-warning">TRUE</span> @else <span class="label label-default">FALSE</span> @endif</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-12 margin-bottom-32">

		<?php $routeName = Route::currentRouteName();?>

		<ul class="nav nav-tabs">
			<li class="@if($routeName == 'dashboard.featureset.edit') active @endif">
				<a href="{{ route('dashboard.featureset.edit', ['featureset'=>$featureSet->id]) }}"><i class="fa fw-lg fa-th-large"></i> Feature Set</a>
			</li>
			<li class="@if($routeName == 'dashboard.featureset.feature.index') active @endif">
				<a href="{{ route('dashboard.featureset.feature.index', ['featureset'=>$featureSet->id]) }}"><i class="fa fa-fw fa-list-ol"></i> Features</a>
			</li>
		</ul>
	</div>
</div>

@yield('editContent')

@endsection