@extends('dashboard.master')

@section('pageTitle')
Feature Sets - Create New
@endsection

@section('pageHeader')
Feature Sets - Create New
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">

			<h3 class="text-muted">Feature Set Details</h3>
			{!! Form::open(['route' => ['dashboard.featureset.store'], 'method'=> 'POST']) !!}

			<div class="form-group @if($errors->has('name')) has-error @endif">
				{!! Form::label('name', 'Name: ' . $errors->first('name'), array('class' => 'control-label')) !!}
				{!! Form::text('name', null , ['class'=>'form-control', 'placeholder'=>'Feature Set Name', 'maxlength' => 219])!!}
			</div>

			<a href="@if(Session::has('redirect')) {!! Session::get('redirect') !!} @else {{ route('dashboard.featureset.index') }} @endif" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
			<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-bolt"></i> Create</button>

			{!! Form::close() !!}
		</div>
	</div>
@endsection
