@extends('dashboard.common.tab.index')

@section('pageTitle')
@parent
- Sort
@endsection

@section('pageHeader')
@parent
- Sort
@endsection

@section('controls')
@parent
<form action="{{ route('dashboard.featureset.reorder') }}" method="POST" class="inline-block" id="featureset-reorder-form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="PATCH" />
    <input type="hidden" name="orderables" value=""/>
    <button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-save"></i> Save Order</button>
</form>

<a href="{{ route('dashboard.featureset.sort') }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-undo"></i> Reset Order</a>
@endsection

@section('tabContent')

<div class="row">

    <div class="col-sm-12">
        <p class="help-block"><i class="fa fa-fw fa-lg fa-arrows"></i> <em>Drag and drop Features Sets to reorder them.</em> This is the order in which Feature Sets appear in the sort menu.</p>
    </div>

    <div class="col-sm-12">

        <ol id="featureset-list" class="sortable-list">
            @foreach($featureSets as $featureSet)
            <?php $routeParams = [$featureSet->id];?>
            <li class="sortable-item" data-id="{{ $featureSet->id }}">
                <div class="sortable-item-wrapper">
                    <span class="title"><span class="label label-default">#{{ $featureSet->id }}</span> {{ $featureSet->name }} </span>

                    <div class="btn-group btn-group-sm model-actions">
                        @include('dashboard.components.buttons.edit', ['url'=> URL::route('dashboard.featureset.edit', $routeParams)])

                        @if($featureSet->trashed())
                        @include('dashboard.components.buttons.destroy', ['url'=> URL::route('dashboard.featureset.destroy', $routeParams)])
                        @include('dashboard.components.buttons.restore', ['url'=> URL::route('dashboard.featureset.restore', $routeParams)])
                        @else
                        @include('dashboard.components.buttons.trash', ['url'=> URL::route('dashboard.featureset.destroy', $routeParams)])
                        @endif
                    </div>
                </div>
            </li>
            @endforeach
        </ol>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/vendor/jquery-sortable-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/sortable.list.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/featureset.min.js') }}"></script>
@endsection