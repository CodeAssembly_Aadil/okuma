@extends('dashboard.master')

@section('pageTitle')
SiteFeature - Create New
@endsection

@section('pageHeader')
Site Feature - Create New
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.15.35/css/bootstrap-datetimepicker.min.css">
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<h3 class="text-muted">Site Feature Details</h3>
		{!! Form::open(['route' => ['dashboard.sitefeature.store'], 'method'=> 'POST']) !!}

		<div class="form-group @if($errors->has('title')) has-error @endif">
			{!! Form::label('title', 'Title: ' . $errors->first('title'), array('class' => 'control-label')) !!}
			{!! Form::text('title', null , ['class'=>'form-control', 'placeholder'=>'Site Feature Title', 'maxlength' => 255])!!}
			<p class="help-block"><span class="label label-default">Optional</span> The <strong>Title</strong> of a Site Feature. Always visible. Supports <code>&lt;em&gt;</code> and <code>&lt;strong&gt;</code> tags.</p>
		</div>

		<div class="form-group @if($errors->has('body')) has-error @endif">
			{!! Form::label('body', 'Body: ' . $errors->first('body'), array('class' => 'control-label')) !!}
			{!! Form::textArea('body', null , [ 'class'=>'form-control', 'placeholder'=>'Site Feature Body', 'rows'=> 10 ])!!}
			<p class="help-block"><span class="label label-default">Optional</span> The <strong>Body</strong> copy that appears when hovering over a Site Feature. Supports <code>&lt;html&gt;</code> tags.</p>
		</div>

		<div class="form-group @if($errors->has('button_label')) has-error @endif">
			{!! Form::label('button_label', 'Button Label: ' . $errors->first('button_label'), array('class' => 'control-label')) !!}
			{!! Form::text('button_label', 'View More' , ['class'=>'form-control', 'placeholder'=>'Site Feature Button Label', 'maxlength' => 64])!!}
			<p class="help-block"><span class="label label-default">Optional</span> The <strong>Button Label</strong>. Defaults to <em>View More</em>.</p>
		</div>

		<div class="form-group @if($errors->has('link')) has-error @endif">
			{!! Form::label('link', 'Link: ' . $errors->first('link'), array('class' => 'control-label')) !!}
			{!! Form::text('link', null , ['class'=>'form-control', 'placeholder'=>'Site Feature Link', 'maxlength' => 2000])!!}
			<p class="help-block"><span class="label label-info">Required</span> The <strong>URL</strong> to navigate to on click.</p>
		</div>

		<div class="form-group @if($errors->has('column_size')) has-error @endif">
			{!! Form::label('column_size', 'Column Size: ' . $errors->first('column_size'), array('class' => 'control-label')) !!}
			{!! Form::select('column_size',  $columnSizes, null, ['id'=>'column_size', 'class'=>'form-control selectpicker', 'title'=>'Select Column Size'])!!}
			<p class="help-block"><span class="label label-info">Required</span> The <strong>Column Size Large</strong>. Determines width of the Site Feature for Large (Desktop) screen sizes. Scales automatically for other screen resolutions</p>
		</div>

		<div class="form-group @if($errors->has('display_class')) has-error @endif">
			{!! Form::label('display_class', 'Display Class: ' . $errors->first('display_class'), array('class' => 'control-label')) !!}
			{!! Form::select('display_class',  $displayClasses, null, ['id'=>'display_class', 'class'=>'form-control selectpicker', 'title'=>'Select Display Class'])!!}
			<p class="help-block"<span class="label label-default">Optional</span> <strong>Display Class</strong>. Changes the appearance of the Site Feature.</p>
		</div>

		<div class="form-group @if($errors->has('published_at')) has-error @endif">
			{!! Form::label('published_at', 'Publish Date: ' . $errors->first('published_at'), array('class' => 'control-label')) !!}
			<div class="input-group date" id="published_at_datepicker">
				<span class="input-group-addon"><i class="fa fa-fw fa-calendar"></i></span>
				{!! Form::text('published_at', null , ['class'=>'form-control', 'placeholder'=>'Publish Date'])!!}
			</div>
			<p class="help-block"><span class="label label-default">Optional</span> <strong>Publish Date</strong> of this Site Feature. Only Site Feature with a Publish Date set after the current Date will be visible. Leave empty to save as Draft</p>
		</div>

		<a href="@if(Session::has('redirect')) {!! Session::get('redirect') !!} @else {{ route('dashboard.sitefeature.index') }} @endif" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
		<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-bolt"></i> Create</button>

		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.15.35/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/sitefeature.min.js') }}"></script>
@endsection
