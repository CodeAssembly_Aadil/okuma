@extends('dashboard.common.tab.index')

@section('pageTitle')
@parent
 - Layout
@endsection

@section('pageHeader')
@parent
- Layout
@endsection

@section('controls')
@parent
<form action="{{ route('dashboard.sitefeature.reorder') }}" method="POST" class="inline-block" id="sitefeatures-reorder-form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="PATCH" />
    <input type="hidden" name="orderables" value=""/>
    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-save"></i> Save Layout</button>
</form>

<a href="{{ route('dashboard.sitefeature.layout') }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-undo"></i> Reset Layout</a>
@endsection

@section('tabContent')

@if (isset($hero))
<?php $routeParams = ['sitefeature' => $hero->id];?>
<div class="row">
    <div class="col-sm-12 hero">
        <div class="site-feature-wrapper">
            <figure class="site-feature {{ $hero->display_class }}">

                @if(isset($hero->background))
                    <div class="site-feature-bg" style="background-image: url({{ URL::asset($hero->background->path) }})"></div>
                @endif

                <figcaption>
                    <div>
                        @if(isset($hero->title))
                            <h2 class="site-feature-title">{{ $hero->title }}</h2>
                        @endif
                        @if(isset($hero->body))
                            <div class="site-feature-body">{{ $hero->body }}</div>
                        @endif
                    </div>

                    @if(isset($hero->link) && isset($hero->button_label))
                        <a href="#" class="site-feature-button">{{ $hero->button_label }}</a>
                    @endif

                </figcaption>
                <div class="btn-group btn-group-sm model-actions">

                    @include('dashboard.components.buttons.edit', ['url'=> URL::route('dashboard.sitefeature.edit', $routeParams)])

                    @if($hero->isPublished())
                        @include('dashboard.components.buttons.unpublish', ['url'=> URL::route('dashboard.sitefeature.togglepublish', $routeParams)])
                    @else
                        @include('dashboard.components.buttons.publish', ['url'=> URL::route('dashboard.sitefeature.togglepublish', $routeParams)])
                    @endif

                    @if($hero->trashed())
                        @include('dashboard.components.buttons.destroy', ['url'=> URL::route('dashboard.sitefeature.destroy', $routeParams)])
                        @include('dashboard.components.buttons.restore', ['url'=> URL::route('dashboard.sitefeature.restore', $routeParams)])
                    @else
                        @include('dashboard.components.buttons.trash', ['url'=> URL::route('dashboard.sitefeature.destroy', $routeParams)])
                    @endif
                </div>
            </figure>
        </div>
    </div>
</div>

@endif

<div class="container" id ="sitefeature-container">
    <div class="row no-gutter sortable-grid">
        @foreach ($siteFeatures as $feature)
        <?php $routeParams = ['sitefeature' => $feature->id];?>
        <div class="{{ $feature->bootstrapColumnSize() }} sortable-item" data-id="{{ $feature->id }}">
            <div class="site-feature-wrapper">
                <figure class="site-feature  {{ $feature->display_class }}">

                    @if(isset($feature->background))
                        <div class="site-feature-bg" style="background-image: url({{ URL::asset($feature->background->path) }})"></div>
                    @endif

                    <figcaption>
                        <div>
                            @if(isset($feature->title))
                                <h2 class="site-feature-title">{{ $feature->title }}</h2>
                            @endif
                            @if(isset($feature->body))
                                <div class="site-feature-body">{{ $feature->body }}</div>
                            @endif
                        </div>

                        @if(isset($feature->link) && isset($feature->button_label))
                            <a href="#" class="site-feature-button">{{ $feature->button_label }}</a>
                        @endif

                    </figcaption>

                    <div class="btn-group btn-group-sm model-actions">
                        @include('dashboard.components.buttons.edit', ['url'=> URL::route('dashboard.sitefeature.edit', $routeParams)])

                        @if($feature->isPublished())
                            @include('dashboard.components.buttons.unpublish', ['url'=> URL::route('dashboard.sitefeature.togglepublish', $routeParams)])
                        @else
                            @include('dashboard.components.buttons.publish', ['url'=> URL::route('dashboard.sitefeature.togglepublish', $routeParams)])
                        @endif

                        @if($feature->trashed())
                            @include('dashboard.components.buttons.destroy', ['url'=> URL::route('dashboard.sitefeature.destroy', $routeParams)])
                            @include('dashboard.components.buttons.restore', ['url'=> URL::route('dashboard.sitefeature.restore', $routeParams)])
                        @else
                            @include('dashboard.components.buttons.trash', ['url'=> URL::route('dashboard.sitefeature.destroy', $routeParams)])
                        @endif
                    </div>
                </figure>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection


@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/vendor/jquery-sortable-min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/sortable.grid.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/sitefeature.min.js') }}"></script>
@endsection
