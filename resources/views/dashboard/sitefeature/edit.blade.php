@extends('dashboard.master')

@section('pageTitle')
SiteFeature - Edit
@endsection

@section('pageHeader')
Site Feature - Edit
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.15.35/css/bootstrap-datetimepicker.min.css">
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<table class="table table-bordered">
			<thead>
				<tr class="@if($siteFeature->trashed()) warning @else info @endif" >
					<th>ID</th>
					<th>Title</th>
					<th>Position</th>
					<th>Created At</th>
					<th>Updated At</th>
					<th>Published On</th>
					<th>Trashed</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $siteFeature->id }}</td>
					<td><span class="truncate" style="max-width:200px">{!! $siteFeature->title !!}</span></td>
					<td>{{ $siteFeature->position }}</td>
					<td>{{ $siteFeature->created_at }}</td>
					<td>{{ $siteFeature->updated_at }}</td>
					<td>@if($siteFeature->isPublished()) <span class="label label-success">{{ $siteFeature->published_at }}</span> @else <span class="label label-default">Unpublished</span> @endif</td>
					<td>@if($siteFeature->trashed()) <span class="label label-warning">TRUE</span> @else <span class="label label-default">FALSE</span> @endif</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="well well-lg">
			@if(isset($siteFeature->background))
			<img src="{{ URL::asset($siteFeature->background->path) }}" class="img-responsive" data-image-id="{{ $siteFeature->background->id }}">
			<br/>
			<div>
				<span class="label label-primary">#{{ $siteFeature->background->id }}</span>
				<span class="label label-info">{{ $siteFeature->background->updated_at }}</span>
			</div>
			<h5>{{ $siteFeature->background->title }}</h5>
			<div class="btn-group btn-group-sm model-actions">
				@include('dashboard.components.buttons.edit', ['url'=> URL::route('dashboard.sitefeature.image.edit.background', ['image'=>$siteFeature->id])])
				@include('dashboard.components.buttons.destroy', ['url'=> URL::route('dashboard.image.destroy', ['image'=>$siteFeature->background->id])])
			</div>
			@else
			<a class="btn btn-primary" href="{{ route('dashboard.sitefeature.image.create.background',['sitefeature'=>$siteFeature->id]) }}"><i class="fa fa-lg fa-fw fa-picture-o"></i> Set Background</a>
			@endif
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<h3 class="text-muted">Site Feature Details</h3>

		{!! Form::model($siteFeature, ['route' => ['dashboard.sitefeature.update', $siteFeature->id], 'method'=> 'PATCH']) !!}

		<div class="form-group @if($errors->has('title')) has-error @endif">
			{!! Form::label('title', 'Title: ' . $errors->first('title'), array('class' => 'control-label')) !!}
			{!! Form::text('title', null , ['class'=>'form-control', 'placeholder'=>'Site Feature Title', 'maxlength' => 255])!!}
			<p class="help-block">The <span class="label label-default">Optional</span> <strong>Title</strong> of a Site Feature. Always visible. Supports <code>&lt;em&gt;</code> and <code>&lt;strong&gt;</code> tags.</p>
		</div>

		<div class="form-group @if($errors->has('body')) has-error @endif">
			{!! Form::label('body', 'Body: ' . $errors->first('body'), array('class' => 'control-label')) !!}
			{!! Form::textArea('body', null , [ 'class'=>'form-control', 'placeholder'=>'Site Feature Body', 'rows'=> 10 ])!!}
			<p class="help-block">The <span class="label label-default">Optional</span> <strong>Body</strong> copy that appears when hovering over a Site Feature. Supports <code>&lt;html&gt;</code> tags.</p>
		</div>

		<div class="form-group @if($errors->has('button_label')) has-error @endif">
			{!! Form::label('button_label', 'Button Label: ' . $errors->first('button_label'), array('class' => 'control-label')) !!}
			{!! Form::text('button_label', null , ['class'=>'form-control', 'placeholder'=>'Site Feature Button Label', 'maxlength' => 64])!!}
			<p class="help-block">The <span class="label label-default">Optional</span> <strong>Button Label</strong>. Defaults to <em>View More</em>.</p>
		</div>

		<div class="form-group @if($errors->has('link')) has-error @endif">
			{!! Form::label('link', 'Link: ' . $errors->first('link'), array('class' => 'control-label')) !!}
			{!! Form::text('link', null , ['class'=>'form-control', 'placeholder'=>'Site Feature Link', 'maxlength' => 2000])!!}
			<p class="help-block">The <span class="label label-info">Required</span> <strong>URL</strong> to navigate to on click.</p>
		</div>

		<?php
$column_size = old('column_size');
if (!isset($column_size)) {
    $column_size = explode(' ', $siteFeature->column_size);
    $column_size = array_pop($column_size);
}
?>

		<div class="form-group @if($errors->has('column_size')) has-error @endif">
			{!! Form::label('column_size', 'Column Size: ' . $errors->first('column_size'), array('class' => 'control-label')) !!}
			{!! Form::select('column_size',  $columnSizes, $column_size, ['id'=>'column_size', 'class'=>'form-control selectpicker', 'title'=>'Select Column Size'])!!}
			<p class="help-block">The <span class="label label-info">Required</span> <strong>Column Size Large</strong>. Determines width of the Site Feature for Large (Desktop) screen sizes. Scales automatically for other screen resolutions <em>{{  $siteFeature->getOriginal('column_size') }}</em></p>
		</div>

		<div class="form-group @if($errors->has('display_class')) has-error @endif">
			{!! Form::label('display_class', 'Display Class: ' . $errors->first('display_class'), array('class' => 'control-label')) !!}
			{!! Form::select('display_class',  $displayClasses, null, ['id'=>'display_class', 'class'=>'form-control selectpicker', 'title'=>'Select Display Class'])!!}
			<p class="help-block"<span class="label label-default">Optional</span> <strong>Display Class</strong>. Changes the appearance of the Site Feature.</p>
		</div>

		<div class="form-group @if($errors->has('published_at')) has-error @endif">
			{!! Form::label('published_at', 'Publish Date: ' . $errors->first('published_at'), array('class' => 'control-label')) !!}
			<div class="input-group date" id="published_at_datepicker">
				<span class="input-group-addon"><i class="fa fa-fw fa-calendar"></i></span>
				{!! Form::text('published_at', null , ['class'=>'form-control', 'placeholder'=>'Publish Date'])!!}
			</div>
			<p class="help-block"><span class="label label-default">optional</span> <strong>Publish Date</strong> of this Site Feature. Only Site Feature with a Publish Date set after the current Date will be visible. Leave empty to save as Draft</p>
		</div>

		<div class="form-group">
			<a href="@if(Session::has('redirect')) {!! Session::get('redirect') !!} @else {{ route('dashboard.sitefeature.index') }} @endif" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
			<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-save"></i> Save Changes</button>
		</div>

		{!! Form::close() !!}

	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.15.35/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/sitefeature.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/sitefeature.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
@endsection
