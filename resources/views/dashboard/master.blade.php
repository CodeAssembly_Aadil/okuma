<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="robots" content="noindex, nofollow, noimageindex">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Okuma Dashboard | @yield('pageTitle')</title>

	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/flatly/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ URL::asset('dashboard_assets/css/okuma/main.css') }}">

	@yield('stylesheets')

</head>

<body>
	<div id="wrapper">
		<header id="top-nav" class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">
						<div>
							<i class="fa fa-lg fa-fw fa-bars"></i> <img clas="logo" alt="Okuma" src="{{ URL::asset('dashboard_assets/images/okuma_logo.png') }}" height="20">
						</div>
					</a>
				</div>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="{{ URL::route('dashboard.administrator.index') }}"><i class="fa fa-fw fa-lg fa-user"></i> {{ Auth::user()->name }}</a>
					</li>
					<li>
						<a href="{{ route('dashboard.logout') }}"><i class="fa fa-fw fa-lg fa-sign-out"></i> Logout</a>
					</li>
				</ul>
			</div>
			<!-- /container -->
		</header>
		<!-- /Header -->
		<aside id="sidebar">
			<nav class="sidebar-menu">
				@include('dashboard.components.sidebar-menu')
			</nav>
		</aside>

		<div id="content">
			<div class="container-fluid">

				<section>
					<div class="row">
						<div class="col-sm-12">
							<h1 class="page-header">@yield('pageHeader')</h1>
						</div>
					</div>

					@yield('content')

				</section>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
	<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/vendor/bootstrap-notify.min.js') }}"></script>

	@yield('scripts')

</body>
</html>
