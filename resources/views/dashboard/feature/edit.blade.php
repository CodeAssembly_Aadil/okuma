@extends('dashboard.master')

@section('pageTitle')
Feature - Edit
@endsection

@section('pageHeader')
Feature - Edit
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<link rel="stylesheet" href="{{ URL::asset('dashboard_assets/css/vendor/select2-bootstrap.min.css') }}">
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<table class="table table-bordered">
			<thead>
				<tr class="@if($feature->trashed()) warning @else info @endif" >
					<th>ID</th>
					<th>Name</th>
					<th>Slug</th>
					<th>Position</th>
					<th>Created At</th>
					<th>Updated At</th>
					<th>Trashed</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $feature->id }}</td>
					<td>{{ $feature->name }}</td>
					<td>{{ $feature->slug }}</td>
					<td>{{ $feature->position }}</td>
					<td>{{ $feature->created_at }}</td>
					<td>{{ $feature->updated_at }}</td>
					<td>@if($feature->trashed()) <span class="label label-warning">TRUE</span> @else <span class="label label-default">FALSE</span> @endif</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Feature Icon</h3>

		<div class="well well-lg">
			@if(isset($feature->icon))
			<figure class="thumbnail product-thumb inline-block" data-image-id="{{ $feature->icon->id }}">
				<img src="{{ URL::asset($feature->icon->path) }}" class="img-responsive" class="img-responsive" width="{{ $feature->icon->width }}"  height="{{ $feature->icon->height }}" alt="{{ $feature->icon->title }}" >
				<figcaption class="caption">
					<div>
						<span class="label label-primary">#{{ $feature->icon->id }}</span>
						<span class="label label-info">{{ $feature->icon->updated_at }}</span>
					</div>
					<h5>{{ $feature->icon->title }}</h5>
					<div class="btn-group btn-group-sm model-actions">
						@include('dashboard.components.buttons.edit', ['url'=> URL::route('dashboard.image.edit', ['image'=>$feature->icon->id])])
						@include('dashboard.components.buttons.destroy', ['url'=> URL::route('dashboard.image.destroy', ['image'=>$feature->icon->id])])
					</div>
				</figcaption>
			</figure>
			<br/>
			@else
			<a class="btn btn-primary" href="{{ route('dashboard.feature.image.create.icon',['feature'=>$feature->id]) }}"><i class="fa fa-fw fa-lg fa-picture-o"></i> Set Icon</a>
			@endif
			<p class="help-block"><span class="label label-default">Recommended</span> <strong>Feature Icon</strong> of this Feature. Appears on product pages.</p>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Feature Details</h3>

		{!! Form::model($feature, ['route' => ['dashboard.feature.update', $feature->id], 'method'=> 'PATCH', 'id'=>'feature-edit-form']) !!}

		<div class="form-group @if($errors->has('name')) has-error @endif">
			{!! Form::label('name', 'Name: ' . $errors->first('name'), array('class' => 'control-label')) !!}
			{!! Form::text('name', null , ['class'=>'form-control', 'placeholder'=>'Feature Name', 'maxlength' => 219])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Name</strong> of this Feature. Appears in the filter menu and on Product pages.</p>
		</div>

		<div class="form-group @if($errors->has('description')) has-error @endif">
			{!! Form::label('description', 'Description: ' . $errors->first('description'), array('class' => 'control-label')) !!}
			{!! Form::textArea('description', null , [ 'class'=>'form-control', 'placeholder'=>'Feature Description', 'rows'=>15 ])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Description</strong> of this Feature. Appears on Product pages. Supports <code>&lt;em&gt;</code> and <code>&lt;strong&gt;</code> tags.</p>
		</div>

		{{-- <div class="form-group form-group-lg @if($errors->has('feature_set_id')) has-error @endif">

			{!! Form::label('feature_set_id', 'Feature Set: ' . $errors->first('feature_set_id', ':message'), ['class' => 'control-label']) !!}

			<div class="input-group select2-bootstrap-append">
				<select id="feature_set_id" class="form-control input-lg" data-url="{{ route('dashboard.featureset.search', ['query' => '', 'page' => 1 ]) }}" name="feature_set_id">
					@if(isset($feature->featureSet))
						<option value="{{ $feature->featureSet->id }}" selected="selected" data-trashed="{{ $feature->featureSet->trashed() }}">{{ $feature->featureSet->name }}</option>
					@endif
				</select>
				<span class="input-group-btn">
				<button class="btn btn-warning" type="button" id="feature-set-clear">
						<i class="fa fa-fw fa-lg fa-times"></i>
					</button>
				</span>
			</div>

			<p class="help-block"><span class="label label-default">Reommended</span> <strong>Feature Set</strong> to witch this Feature belongs Feature that do not belong to Feature Sets will not appear in the filter menu. Start typing to see options. Type <kbd>*</kbd> to see all options.</p>
		</div> --}}


		<div class="form-group form-group-lg @if($errors->has('feature_set_id')) has-error @endif">
			{!! Form::label('feature_set_id', 'Feature Set: ' . $errors->first('feature_set_id', ':message'), ['class' => 'control-label']) !!}

			<div class="input-group select2-bootstrap-append">
				<select id="feature_set_id" class="form-control input-lg" data-url="{{ route('dashboard.featureset.search', ['query' => '', 'page' => 1 ]) }}" name="feature_set_id">
					@if(isset($feature->featureSet))
						<option value="{{ $feature->featureSet->id }}" selected="selected" data-trashed="{{ $feature->featureSet->trashed() }}">{{ $feature->featureSet->name }}</option>
					@endif
				</select>
				<span class="input-group-btn">
				<button class="btn btn-warning" type="button" id="feature-set-clear">
						<i class="fa fa-fw fa-lg fa-times"></i>
					</button>
				</span>
			</div>

			<p class="help-block"><span class="label label-default">Reommended</span> <strong>Feature Set</strong> to witch this Feature belongs Feature that do not belong to Feature Sets will not appear in the filter menu. Start typing to see options. Type <kbd>*</kbd> to see all options.</p>
		</div>

		<div class="form-group">
			<a href="{{ route('dashboard.feature.index') }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
			<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-save"></i> Save Changes</button>
		</div>

		{!! Form::close() !!}

	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
{{-- <script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script> --}}
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/feature.min.js') }}"></script>
@endsection
