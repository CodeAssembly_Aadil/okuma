@extends('dashboard.master')

@section('pageTitle')
Feature - Create new
@endsection

@section('pageHeader')
Feature - Create New
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<link rel="stylesheet" href="{{ URL::asset('dashboard_assets/css/vendor/select2-bootstrap.min.css') }}">
@endsection


@section('content')
<div class="row">
	<div class="col-sm-12">

		<h3 class="text-muted">Feature Details</h3>

		{!! Form::open(['route' => ['dashboard.feature.store'], 'method'=> 'POST', 'id'=>'feature-create-form']) !!}

		<div class="form-group @if($errors->has('name')) has-error @endif">
			{!! Form::label('name', 'Name: ' . $errors->first('name'), array('class' => 'control-label')) !!}
			{!! Form::text('name', null , ['class'=>'form-control', 'placeholder'=>'Feature Name', 'maxlength' => 219])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Name</strong> of this Feature. Appears in filter menu and on product pages.</p>
		</div>

		<div class="form-group @if($errors->has('description')) has-error @endif">
			{!! Form::label('description', 'Description: ' . $errors->first('description'), array('class' => 'control-label')) !!}
			{!! Form::textArea('description', null , [ 'class'=>'form-control', 'placeholder'=>'Feature Description', 'rows'=>15 ])!!}
			<p class="help-block"><span class="label label-info">Required</span> <strong>Description</strong> of this Feature. Appears on product pages. Supports <code>&lt;em&gt;</code> and <code>&lt;strong&gt;</code> tags.</p>
		</div>

		<div class="form-group form-group-lg @if($errors->has('feature_set_id')) has-error @endif">
			{!! Form::label('feature_set_id', 'Feature Set: ' . $errors->first('feature_set_id', ':message'), ['class' => 'control-label']) !!}

			<div class="input-group select2-bootstrap-append">
				<select id="feature_set_id" class="form-control input-lg" data-url="{{ route('dashboard.featureset.search', ['query' => '', 'page' => 1 ]) }}" name="feature_set_id">
				</select>
				<span class="input-group-btn">
				<button class="btn btn-warning" type="button" id="feature-set-clear">
						<i class="fa fa-fw fa-lg fa-times"></i>
					</button>
				</span>
			</div>

			<p class="help-block"><span class="label label-default">Reommended</span> <strong>Feature Set</strong> to witch this Feature belongs Feature that do not belong to Feature Sets will not appear in the filter menu. Start typing to see options. Type <kbd>*</kbd> to see all options.</p>
		</div>

		<a href="{{ route('dashboard.feature.index') }}" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
		<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-bolt"></i> Create</button>

		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('scripts')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
	<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/feature.min.js') }}"></script>
@endsection
