@extends('dashboard.master')

@section('pageTitle')
Image - Edit
@endsection

@section('pageHeader')
Image - Edit
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Type</th>
					<th>Imageable Type</th>
					<th>Imageable ID</th>
					<th>Width <em>(PX)</em></th>
					<th>Height <em>(PX)</em></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $image->id }}</td>
					<td>{{ $image->type }}</td>
					<td>{{ $image->imageable_type }}</td>
					<td>{{ $image->imageable_id }}</td>
					<td>{{ $image->width }}</td>
					<td>{{ $image->height }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="well well-lg">
			<img src="{{ URL::asset($image->path) }}" class="img-responsive" data-image-id="{{ $image->path }}">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">

		{!! Form::model($image, ['route' => ['dashboard.image.update', $image->id], 'method'=>'PATCH', 'files' => true, 'id'=> 'image-update-form']) !!}

		<input type="hidden" name="imageable-type" value="{{ $image->imageable_type }}">
		<input type="hidden" name="imageable-id" value="{{ $image->imageable_id }}">
		<input type="hidden" name="type" value="{{ isset($type) ? $type : $image->type }}">
		{{-- <input type="hidden" name="width" value="{{ isset($width) ? $width : $image->width }}"> --}}
		{{-- <input type="hidden" name="height" value="{{ isset($height) ? $height : $image->height }}"> --}}

		<div class="form-group @if($errors->has('title')) has-error @endif">
			{!! Form::label('title', 'Title: ' . $errors->first('title'), array('class' => 'control-label')) !!}
			{!! Form::text('title', null , ['class'=>'form-control', 'placeholder'=>'Image Title', 'maxlength' => 255])!!}
			<p class="help-block"><span class="label label-info">Required</span> The name of this Image. Used in <code>title</code> and <code>alt</code> attributes and captions.</p>
		</div>

		<div class="form-group @if($errors->has('image-file')) has-error @endif">
			{!! Form::label('image-file', 'Image File: ' . $errors->first('image-file'), array('class' => 'control-label')) !!}
			<input type="file" name="image-file" id="image-file" class="form-control" placeholder="Image File">
			<p class="help-block"><span class="label label-info">Required</span> Select an image file (<code>.png</code> or <code>.jpeg</code>) that is at least {{ isset($width) ? $width : $image->width }}px wide and {{ isset($height) ? $height : $image->height }}px high.</p>
		</div>

		<div class="form-group @if($errors->has('use-original')) has-error @endif">
			<div class="checkbox">
				<label>
					<input type="checkbox" name="use-original" id="use-original" value="1"> Use Original Image File {{ $errors->first('use-original') }}
				</label>
			</div>
			<p class="help-block"><span class="label label-danger">Not Recommended</span> If Checked uploaded image file will be used in its original form, no size validation or resizing will be done.</p>
		</div>

		<div class="form-group">
			<a href="@if(Session::has('redirect')) {!! Session::get('redirect') !!} @else {{ route('dashboard.home') }} @endif" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
			<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-floppy-o"></i> Update</button>
		</div>
		{!! Form::close() !!}

	</div>
</div>

@endsection
