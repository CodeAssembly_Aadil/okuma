@extends('dashboard.master')

@section('pageTitle')
Image - Create New {{ $formTitle }}
@endsection

@section('pageHeader')
Image - Create New {{ $formTitle }}
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		{!! $infoTable !!}
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		@if($errors->has())
		<div class="well text-danger">
			@foreach ($errors->all() as $error)
			<div>{{ $error }}</div>
			@endforeach
		</div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-12">

		{!! Form::open(['route' => ['dashboard.image.store'], 'method'=> 'POST' , 'files' => true, 'id'=>'image-create-form']) !!}

		<input type="hidden" name="imageable-type" value="{{ $imageableType }}">
		<input type="hidden" name="imageable-id" value="{{ $imageableID }}">
		<input type="hidden" name="type" value="{{ $type }}">

		<div class="form-group @if($errors->has('title')) has-error @endif">
			{!! Form::label('title', 'Title: ' . $errors->first('title'), array('class' => 'control-label')) !!}
			{!! Form::text('title', null , ['class'=>'form-control', 'placeholder'=>'Image Title', 'maxlength' => 255])!!}
			<p class="help-block"><span class="label label-info">Required</span> The name of this Image. Used in <code>title</code> and <code>alt</code> attributes and captions.</p>
		</div>

		<div class="form-group @if($errors->has('image-file')) has-error @endif">
			{!! Form::label('image-file', 'Image File: ' . $errors->first('image-file'), array('class' => 'control-label')) !!}
			<input type="file" name="image-file" id="image-file" class="form-control" placeholder="Image File">
			<p class="help-block"><span class="label label-info">Required</span> Select an image file (<code>.png</code> or <code>.jpeg</code>) that is at least {{ $width }}px wide and {{ $height }}px high.</p>
		</div>

		<div class="form-group @if($errors->has('use-original')) has-error @endif">
			<div class="checkbox">
				<label>
					<input type="checkbox" name="use-original" id="use-original" value="1"> Use Original Image File {{ $errors->first('use-original') }}
				</label>
			</div>
			<p class="help-block"><span class="label label-danger">Not Recommended</span> If Checked uploaded image file will be used in its original form, no size validation or resizing will be done.</p>
		</div>

		<a href="@if(Session::has('redirect')) {!! Session::get('redirect') !!} @else {{ route('dashboard.home') }} @endif" class="btn btn-default"><i class="fa fa-fw fa-lg fa-times-circle"></i> Cancel</a>
		<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-upload"></i> Upload</button>

		{!! Form::close() !!}
	</div>
</div>
@endsection
