@extends('dashboard.common.tab.index')

@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">
@endsection

@section('pageTitle')
@parent
 - Datatable
@endsection

@section('pageHeader')
@parent
 - Datatable
@endsection

@section('controls')
@parent
<div class="pull-right">
	<button class="btn btn-info" data-action="datatable-refresh"><i class="fa fa-lg fa-fw fa-refresh"></i> Refresh Data</button>
	<button class="btn btn-default" data-action="datatable-reset"><i class="fa fa-lg fa-fw fa-undo"></i> Reset Table</button>
</div>
@endsection

@section('tabContent')
<div class="row">
	<div class="col-sm-12">
		<div id="datatable-container">
			{!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'dataTableBuilder', 'width'=>'100%' ]) !!}
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.7/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
{!! $dataTable->scripts() !!}
@endsection
