@extends('dashboard.master')

@section('pageTitle')
{{ $pageTitle }}
@endsection

@section('pageHeader')
{{ $pageHeader }}
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12 margin-bottom-32">
	<?php $routeName = Route::currentRouteName();?>
		<ul class="nav nav-tabs">
			<li class="@if($routeName === $baseRoute.'.index') active @endif">
				<a href="{{ route($baseRoute.'.index') }}"><i class="fa fa-fw fa-lg fa-table"></i> Data Table</a>
			</li>

			@include($tabs)
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-sm-12 margin-bottom-32">
		<a class="btn btn-success" href="{{ route($baseRoute.'.create') }}"><i class="fa fa-lg fa-fw fa-plus-circle"></i> New {{ $modelName }}</a>
		@yield('controls')
	</div>
</div>

@yield('tabContent')
@endsection
