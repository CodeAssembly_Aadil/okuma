@extends('dashboard.master')

@section('stylesheets')
{{-- <link rel="stylesheet" href="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css"> --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
@endsection

@section('pageTitle')
{{ $pageTitle }}
@endsection

@section('pageHeader')
{{ $pageHeader }} - Datatable
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12 margin-bottom-32">
		<a class="btn btn-success" href="{{ route($baseRoute.'.create') }}"><i class="fa fa-lg fa-fw fa-plus-circle"></i> New {{ $modelName }}</a>
		@yield('controls')

		<div class="pull-right">
			<button class="btn btn-info" data-action="datatable-refresh"><i class="fa fa-lg fa-fw fa-refresh"></i> Refresh Data</button>
			<button class="btn btn-default" data-action="datatable-reset"><i class="fa fa-lg fa-fw fa-undo"></i> Reset Table</button>
		</div>
	</div>

	<div class="col-sm-12">
		<div id="datatable-container">
			{!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'dataTableBuilder', 'width'=>'100%']) !!}
		</div>
	</div>

</div>
@endsection

@section('scripts')
{{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.7/js/jquery.dataTables.min.js"></script> --}}
<script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
{{-- <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script> --}}
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
{!! $dataTable->scripts() !!}
@endsection
