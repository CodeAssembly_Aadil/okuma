@extends('dashboard.master')

@section('pageTitle')
Backup Manager
@endsection

@section('pageHeader')
Backup Manager
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12 margin-bottom-32">
		<div >
			<form action="{{ route('dashboard.backup.create') }}" method="POST" class="inline-block" id="create-backup-form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<button type="submit" class="btn btn-success" ><i class="fa fa-fw fa-lg fa-database"></i> Create Backup</button>
			</form>
			<form action="{{ route('dashboard.backup.clean') }}" method="POST" class="inline-block" id="create-backup-form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="_method" value="DELETE" />
				<button type="submit" class="btn btn-warning" ><i class="fa fa-fw fa-lg fa-leaf"></i> Clean Old Backups</button>
			</form>
		</div>
	</div>
</div>

@if(Session::has('status'))
<div class="row">
	<div class="col-sm-12 margin-bottom-16">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
			<strong><i class="fa fa-fw fa-lg fa-check-circle-o"></i> Success:</strong> {{ Session::pull('status') }}
		</div>
	</div>
</div>
@endif

<div class="row">
	<div class="col-sm-12 margin-bottom-16">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Filename</th>
					<th>Age</th>
					<th>Size</th>
					<th>Actions</th>
				</tr>
			</thead>

			<tbody>
				<?php $count = 1;?>
				@foreach($backups as $backup)
				<tr>
					<td>{{ $count++ }}</td>
					<td>{{ $backup['filename'] }}</td>
					<td>{{ $backup['age'] }}</td>
					<td>{{ $backup['size'] }}</td>
					<td>
						<div class="btn-group btn-group-sm model-actions">
							@include('dashboard.components.buttons.destroy', ['url'=> URL::route('dashboard.backup.destroy', ['file' => $backup['fullpath']])])
							@include('dashboard.components.buttons.download', ['url'=> URL::route('dashboard.backup.download', ['file' => $backup['fullpath']])])
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ URL::asset('dashboard_assets/js/okuma/model.actions.min.js') }}"></script>
@endsection
