var elixir = require("laravel-elixir");
elixir.config.sourcemaps = false;

/*
|--------------------------------------------------------------------------
| Elixir Asset Management
|--------------------------------------------------------------------------
|
| Elixir provides a clean, fluent API for defining some basic Gulp tasks
| for your Laravel application. By default, we are compiling the Less
| file for our application, as well as publishing vendor resources.
|
*/

var resources = {
    vendor: "resources/assets/bower_components/",
    js: "resources/assets/js/",
    sass: "resources/assets/sass/",
}

var frontend = {
    css: {
        okuma: "public/css/okuma/",
        vendor: "public/css/vendor/",
    },

    js: {
        okuma: "public/js/okuma/",
        vendor: "public/js/vendor/",
    },
}

var dashboard = {

    css: {
        okuma: "public/dashboard_assets/css/okuma/",
        vendor: "public/dashboard_assets/css/vendor/",
    },

    js: {
        okuma: "public/dashboard_assets/js/okuma/",
        vendor: "public/dashboard_assets/js/vendor/",
    },

    fonts: {
        okuma: "public/dashboard_assets/css/fonts/",
        vendor: "public/dashboard_assets/css/fonts/",
    }
}

var compile = {

    frontend: true,
    dashboard: true
}

// Combine Frontend JS
if (compile.frontend) {
    elixir(
        function(mix) {
            // SETUP
            // mix.copy(resources.vendor + "foundation/scss/foundation/_settings.scss", resources.sass + "_settings.scss");
            // mix.copy(resources.vendor + "slick-carousel/slick/slick-theme.scss", resources.sass + "okuma/components/_slick-carousel-theme.scss");

            // JS
            mix.scripts(["foundation.js", "foundation.accordion.js"],
                frontend.js.vendor + "foundation.js",
                resources.vendor + "foundation/js/foundation/");
            mix.scripts(["main.js"], frontend.js.okuma + "main.js");
            mix.scripts(["category.js"], frontend.js.okuma + "category.js");
            mix.scripts(["product.js"], frontend.js.okuma + "product.js");
            mix.scripts(["contact.js"], frontend.js.okuma + "contact.js");
            mix.scripts(["share.js"], frontend.js.okuma + "share.js");

            //  SASS
            mix.sass("main.scss", frontend.css.okuma + "main.css");
            mix.sass("luckycarp.scss", frontend.css.okuma + "luckycarp.css");

            // Versioning
            mix.version([
                frontend.js.vendor + "foundation.js",
                frontend.js.okuma + "main.js",
                frontend.js.okuma + "category.js",
                frontend.js.okuma + "product.js",
                frontend.js.okuma + "contact.js",
                frontend.js.okuma + "share.js",
                frontend.css.okuma + "main.css",
                frontend.css.okuma + "luckycarp.css"
            ]);
        });
}

if (compile.dashboard) {
    elixir(
        function(mix) {

            mix.copy(resources.vendor + "jquery-counter-plugin/word-and-character-counter.min.js", dashboard.js.vendor + "word-and-character-counter.min.js");
            mix.copy(resources.vendor + "remarkable-bootstrap-notify/dist/bootstrap-notify.min.js", dashboard.js.vendor + "bootstrap-notify.min.js");
            mix.copy(resources.vendor + "jquery-sortable/source/js/jquery-sortable-min.js", dashboard.js.vendor + "jquery-sortable-min.js");

            mix.scripts(["dashboard/sortable.grid.js"], dashboard.js.okuma + "sortable.grid.min.js");
            mix.scripts(["dashboard/sortable.list.js"], dashboard.js.okuma + "sortable.list.min.js");
            mix.scripts(["dashboard/sortable.table.js"], dashboard.js.okuma + "sortable.table.min.js");

            mix.scripts(["dashboard/model.actions.js"], dashboard.js.okuma + "model.actions.min.js");

            mix.scripts(["dashboard/category.js"], dashboard.js.okuma + "category.min.js");
            mix.scripts(["dashboard/feature.js"], dashboard.js.okuma + "feature.min.js");
            mix.scripts(["dashboard/featureset.js"], dashboard.js.okuma + "featureset.min.js");
            mix.scripts(["dashboard/product.js"], dashboard.js.okuma + "product.min.js");
            mix.scripts(["dashboard/sitefeature.js"], dashboard.js.okuma + "sitefeature.min.js");

            // SASS
            mix.sass("/dashboard/main.scss", dashboard.css.okuma + "main.css");
            mix.copy(resources.vendor + "select2-bootstrap-theme/dist/select2-bootstrap.min.css", dashboard.css.vendor + "select2-bootstrap.min.css");
        });
}
