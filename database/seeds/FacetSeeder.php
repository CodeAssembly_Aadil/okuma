<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Okuma\Models\Category;

class FacetSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $facets = Category::create(['name' => 'Facet']);

        $freshWater = $facets->addChild(Category::create(['name' => 'Fresh Water']), null, true);
        $estuary = $facets->addChild(Category::create(['name' => 'Estuary']), null, true);
        $rockAndSurf = $facets->addChild(Category::create(['name' => 'Rock and Surf']), null, true);
        $deepSea = $facets->addChild(Category::create(['name' => 'DeepSea']), null, true);

        $tiger = $freshWater->addChild(Category::create(['name' => 'Tiger']), null, true);
        $carp = $freshWater->addChild(Category::create(['name' => 'Bass']), null, true);
        $bass = $freshWater->addChild(Category::create(['name' => 'Carp']), null, true);

        $bigGameFish = $deepSea->addChild(Category::create(['name' => 'Big Game Fish']), null, true);
        $marlin = $deepSea->addChild(Category::create(['name' => 'Marlin']), null, true);
        $tuna = $deepSea->addChild(Category::create(['name' => 'Marlin']), null, true);

        Model::reguard();
    }

}
