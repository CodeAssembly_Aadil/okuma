<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Okuma\Facades\ImageManager;
use Okuma\Models\Image;
use Okuma\Models\Product;

class ProductImageSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productImageDirectory = Config::get('okuma.image_directories.Okuma\Models\Product');
        $publicPath = public_path();

        $large =
        [
            ImageManager::make($publicPath . '/images/product/product_1.png'),
            ImageManager::make($publicPath . '/images/product/product_2.png'),
            ImageManager::make($publicPath . '/images/product/product_3.png'),
            ImageManager::make($publicPath . '/images/product/product_4.png'),
            ImageManager::make($publicPath . '/images/product/product_5.png'),
        ];

        // $thumbs =
        // [
        //     ImageManager::make($publicPath . '/images/product/product_thumbnail_1.png'),
        //     ImageManager::make($publicPath . '/images/product/product_thumbnail_2.png'),
        //     ImageManager::make($publicPath . '/images/product/product_thumbnail_3.png'),
        //     ImageManager::make($publicPath . '/images/product/product_thumbnail_4.png'),
        //     ImageManager::make($publicPath . '/images/product/product_thumbnail_5.png'),
        // ];

        $faker = Factory::create();

        $products = Product::all();

        foreach ($products as $product) {

            $numProductImages = 3; // $faker->numberBetween(1, 6);
            // $productImages = [];

            $title = $product->name;
            // $productID = $product->id;

            $directory = str_replace('{ID}', $product->id, $productImageDirectory);
            // dd($directory);

            if (!Storage::exists('public/' . $directory)) {
                Storage::makeDirectory('public/' . $directory);
            }

            // $productImages = [];

            $imageableType = get_class($product);
            $imageableID = $product->id;

            for ($pi = 1; $pi <= $numProductImages; $pi++) {

                $index = $faker->numberBetween(0, 4);

                $image = Image::create([
                    'imageable_type' => $imageableType,
                    'imageable_id' => $imageableID,
                    'title' => $title,
                    'type' => 'product_large',
                    'width' => 1200,
                    'height' => 740,
                    'position' => $pi,
                ]);

                $fileName = $image->id . '.png';

                $imageFile = $large[$index];
                $imageFile->save($publicPath . '/' . $directory . $fileName);

                $image->path = $directory . $fileName;
                $image->save();
                // array_push($productImages, $image);
            }

            // $product->images()->saveMany($productImages);

            $this->command->info($product->id . ' ' . $product->name);
        }
    }

}
