<?php

use Illuminate\Database\Seeder;
use Okuma\Models\Category;
use Okuma\Models\FeatureSet;

class FeatureSetsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Model::unguard();

        // DB::table('feature_sets')->delete();

        $categories = Category::where('real_depth', '=', 1)->get();

        $features = FeatureSet::create(['name' => 'Features'], null, true);

        $productGroups = ['Reels', 'Rods', 'Line', 'Lures', 'Accesories'];

        foreach ($categories as $category) {

            $productGroup = $category->name;

            $featureSet = $features->addChild(FeatureSet::create(['name' => $productGroup]), null, true);

            $featureSet->addChild(FeatureSet::create(['name' => $productGroup . ' Set A']));
            $featureSet->addChild(FeatureSet::create(['name' => $productGroup . ' Set B']));
            $featureSet->addChild(FeatureSet::create(['name' => $productGroup . ' Set C']));
            $featureSet->addChild(FeatureSet::create(['name' => $productGroup . ' Set D']));
            $featureSet->addChild(FeatureSet::create(['name' => $productGroup . ' Set E']));

            $category->featureSet()->associate($featureSet);
            $category->save();

            foreach ($category->getDescendants() as $subCategory) {
                $subCategory->featureSet()->associate($featureSet);
                $subCategory->save();
            }

        }
    }

}
