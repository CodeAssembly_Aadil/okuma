<?php

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Okuma\Models\Category;
use Okuma\Models\Feature;
use Okuma\Models\FeatureSet;
use Okuma\Models\Product;
use Okuma\Models\Variant;

class ProductsAndVariantsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Factory::create();

        $now = Carbon::now();

        $productTree = Category::where('name', '=', 'Products')->first();
        $productCategories = $productTree->getDescendants();

        $featureGroupID = 0;
        $features = null;
        $featureIDs = [];
        $specSchema = json_encode(['Spec A', 'Spec B', 'Spec C', 'Spec D', 'Spec E']);

        foreach ($productCategories as $category) {

            if ($category->parent_id == $productTree->id) {
                $featureGroupID = FeatureSet::where('name', '=', $category->name)->first()->id;

                $featureIDs = Feature::distinct()
                    ->join('feature_set_closure',
                        function ($join) use ($featureGroupID) {
                            $join->on('feature_set_closure.descendant', '=', 'features.feature_set_id')
                            ->where('feature_set_closure.ancestor', '=', $featureGroupID);
                        })
                    ->lists('features.id')->all();
            }

            $numProducts = 10; //rand(0, 30);

            for ($p = 1; $p <= $numProducts; $p++) {

                $product = Product::create([
                    'name' => 'Product ' . $category->name . ' ' . $p,
                    'description' => '<p>' . implode('</p><p>', $faker->paragraphs(2)) . '</p>',
                    'specification' => $specSchema,
                    'published_at' => $now,
                ]);

                // Variants
                $numVariants = 4; //rand(1, 5);

                $modelNumber = strtoupper($faker->lexify('???00'));

                $variants = [];

                for ($v = 1; $v <= $numVariants; $v++) {
                    $variant = Variant::create([
                        'model' => $modelNumber . $v,
                        'sku' => $faker->ean8,
                        'specification' => json_encode(['Spec A' => rand(1, 500), 'Spec B' => rand(1, 500), 'Spec C' => rand(1, 500), 'Spec D' => rand(1, 500), 'Spec E' => rand(1, 500)]),
                        'product_id' => $product->id,
                        'position' => $v,
                    ]);

                    $variants[$v] = $variant;
                }

                // Features
                $numFeatures = $faker->numberBetween(1, 5);

                $productFeatureIDs = $faker->randomElements($featureIDs, $numFeatures);

                $product->features()->attach($productFeatureIDs);

                $category->products()->attach($product->id, ['product_position' => $p]);
                $category->save();

                $this->command->info($product->id . ' ' . $product->name);
            }
        }

    }

}
