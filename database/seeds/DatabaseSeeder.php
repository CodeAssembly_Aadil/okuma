<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->command->info('SEED: AdminUsers...');
        $this->call('AdminUsersTableSeeder');
        $this->command->info('AdminUsers table seeded!');

        $this->command->info('SEED: Site Features...');
        $this->call('SiteFeaturesTableSeeder');
        $this->command->info('Site Features table seeded!');

        $this->command->info('SEED: Categories...');
        $this->call('CategoriesTableSeeder');
        $this->command->info('Categories table seeded!');

        $this->command->info('SEED: FeatureSets...');
        $this->call('FeatureSetsTableSeeder');
        $this->command->info('Feature Sets table seeded!');

        $this->command->info('SEED: Features...');
        $this->call('FeaturesTableSeeder');
        $this->command->info('Features table seeded!');

        $this->command->info('SEED: Products and Variants...');
        $this->call('ProductsAndVariantsTableSeeder');
        $this->command->info('Products and Variants tables seeded!');

        $this->command->info('SEED: Product Image...');
        $this->call('ProductImageSeeder');
        $this->command->info('Products Images seeded!');

        Model::reguard();
    }

}
