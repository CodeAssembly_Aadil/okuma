<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Okuma\Models\AdminUser;
use Okuma\Models\Category;
use Okuma\Models\FeatureSet;

class ProductionSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->command->info('Create Default Admin');
        AdminUser::create([
            'name' => 'Okuma Admin',
            'email' => 'admin@okuma.co.za',
            'password' => bcrypt('password'),
        ]);

        $this->command->info('Create Root Category');
        Category::create(['name' => 'Products']);
        $this->command->info('Create Root FeatureSet');
        FeatureSet::create(['name' => 'Features']);

        Model::reguard();
    }

}
