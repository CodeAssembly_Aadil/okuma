<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Okuma\Models\Image;
use Okuma\Models\SiteFeature;

class SiteFeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Factory::create();
        // $colors = ['9a8f97', 'c3baba', 'e9e3e6', 'b2b2b2', '736f72'];
        $colors = ['1be7ff', '6eeb83', 'e4ff1a', 'e8aa14', 'ff5714'];
        $colorIndex = 0;

        $siteFeature = factory(SiteFeature::class)->create([
            'column_size' => 'site-hero',
            'position' => 1,
        ]);

        $siteFeature->save();

        $image = new Image([
            'title' => 'Site Feature',
            'path' => 'http://dummyimage.com/1920x714/' . $faker->randomElement($colors) . '/FFFFFF',
            'type' => 'site_feature_hero_bg',
            'width' => 1920,
            'height' => 714,
            'position' => 1,
        ]);

        $siteFeature->background()->save($image);

        $columns = ['small-12 medium-12 large-12', 'small-12 medium-12 large-8', 'small-12 medium-6 large-4', 'small-12 medium-6 large-4', 'small-12 medium-6 large-4', 'small-12 medium-6 large-4', 'small-12 medium-6 large-6', 'small-12 medium-6 large-6'];
        $position = 2;

        foreach ($columns as $column_size) {
            $siteFeature = factory(SiteFeature::class)->create([
                'column_size' => $column_size,
                'position' => $position++,
            ]);

            $siteFeature->save();

            $width = 400;
            $type = 'site_feature_4_bg';

            if (str_contains($column_size, 'large-12')) {
                $width = 1200;
                $type = 'site_feature_12_bg';
            } else if (str_contains($column_size, 'large-8')) {
                $width = 800;
                $type = 'site_feature_8_bg';
            } else if (str_contains($column_size, 'large-6')) {
                $width = 600;
                $type = 'site_feature_6_bg';
            }

            // $color = $faker->randomElement($colors);
            $color = $colors[$colorIndex++];

            $image = new Image([
                'title' => 'Site Feature',
                'path' => "http://dummyimage.com/{$width}x400/{$color}/FFFFFF",
                'type' => $type,
                'width' => $width,
                'height' => 400,
                'position' => 1,
            ]);

            $siteFeature->background()->save($image);

            if ($colorIndex >= count($colors)) {
                $colorIndex = 0;
            }
        }
    }
}
