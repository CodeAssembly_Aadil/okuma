<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Okuma\Facades\ImageManager;
use Okuma\Models\Category;
use Okuma\Models\Image;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Model::unguard();

        $faker = Factory::create();

        // DB::table('categories')->delete();

        $products = Category::create(['name' => 'Products', 'description' => $faker->paragraph(6)]);

        $reels = $products->addChild(Category::create(['name' => 'Reels', 'description' => $faker->paragraph(6)]), null, true);

        $spinningReels = $reels->addChild(Category::create(['name' => 'Spinning Reels', 'description' => $faker->paragraph(6)]), null, true);
        $baitFeederReels = $reels->addChild(Category::create(['name' => 'Baitfeeder Reels', 'description' => $faker->paragraph(6)]));
        $multiplierReels = $reels->addChild(Category::create(['name' => 'Multiplier Reels', 'description' => $faker->paragraph(6)]));
        $baitCasterReels = $reels->addChild(Category::create(['name' => 'Baitcaster Reels', 'description' => $faker->paragraph(6)]));
        $flyReels = $reels->addChild(Category::create(['name' => 'Fly Reels', 'description' => $faker->paragraph(6)]));

        $rods = $products->addChild(Category::create(['name' => 'Rods', 'description' => $faker->paragraph(6)]), null, true);

        $rods->addChild(Category::create(['name' => 'Combos', 'description' => $faker->paragraph(6)]));

        $accesories = $products->addChild(Category::create(['name' => 'Accesories', 'description' => $faker->paragraph(6)]));

        $categories = $products->getDescendants()->add($products);

        $dir = Config::get('okuma.image_directories.Okuma\Models\Category');
        $path = public_path() . '/' . $dir;
        $image = ImageManager::make($path . 'HeroBanner.png');

        foreach ($categories as $category) {

            $heroBanner = new Image([
                'imageable_type' => get_class($category),
                'imageable_id' => $category->id,
                'title' => $category->name,
                'type' => 'category_hero',
                'position' => 1,
                'width' => 1920,
                'height' => 360,
            ]);

            $fileName = $heroBanner->id . '.png';
            $image->save($path . $fileName);

            $heroBanner->path = $dir . $fileName;
            $heroBanner->save();

            // $category = $category->heroBanner()->save($heroBanner);
        }
    }

}
