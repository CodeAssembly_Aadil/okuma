<?php

use Illuminate\Database\Seeder;
use Okuma\Models\AdminUser;

class AdminUsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminUser::create([
            'name' => 'Okuma Admin',
            'email' => 'admin@okuma.co.za',
            'password' => bcrypt('password'),
        ]);
    }

}
