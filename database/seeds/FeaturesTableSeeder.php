<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Okuma\Facades\ImageManager;
use Okuma\Models\Feature;
use Okuma\Models\FeatureSet;
use Okuma\Models\Image;

class FeaturesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Factory::create();

        $productCategories = FeatureSet::where('name', '=', 'Features')->first()->getChildren();

        $dir = Config::get('okuma.image_directories.Okuma\Models\Feature');
        $path = public_path() . '/' . $dir;
        $image = ImageManager::make($path . 'FeatureIcon.png');

        foreach ($productCategories as $featureGroup) {

            foreach ($featureGroup->getChildren() as $featureSet) {

                // for ($i = 0, $numFeatures = rand(2, 6); $i < $numFeatures; $i++) {
                for ($i = 0, $numFeatures = 5; $i < $numFeatures; $i++) {

                    $feature = Feature::create([
                        'name' => $featureSet->name . ' Feature ' . $i,
                        'description' => $faker->paragraph($faker->numberBetween(2, 5)),
                        'position' => $i,
                    ]);

                    $feature->featureSet()->associate($featureSet);
                    $feature->save();

                    $featureIcon = Image::create([
                        'imageable_type' => get_class($feature),
                        'imageable_id' => $feature->id,
                        'title' => $feature->name,
                        'type' => 'feature_icon',
                        'position' => 1,
                        'width' => 320,
                        'height' => 320,
                    ]);

                    $fileName = $featureIcon->id . '.png';
                    $image->save($path . $fileName);

                    $featureIcon->path = $dir . $fileName;
                    $featureIcon->save();

                    // $feature->icon()->save($featureIcon);
                }
            }
        }

    }

}
