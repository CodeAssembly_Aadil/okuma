<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

$factory->define(Okuma\User::class,
    function ($faker) {
        return [
            'name' => $faker->name,
            'email' => $faker->email,
            'password' => str_random(10),
            'remember_token' => str_random(10),
        ];
    });

$factory->defineAs(Okuma\Models\Product::class, 'Product',
    function ($faker) {
        return [
            'name' => $faker->name,
            'description' => '<p>' . implode('</p><p>', $faker->paragraphs($faker->numberBetween(2, 5))) . '</p>',
        ];
    });

$factory->defineAs(Okuma\Models\Variant::class, 'Variant',
    function ($faker) {
        return [
            'model' => 'A1',
            'sku' => $faker->ean,
            'specification' => json_encode(['Spec A' => rand(1, 500), 'Spec B' => rand(1, 500), 'Spec C' => rand(1, 500), 'Spec D' => rand(1, 500)]),
            'position' => 1,
        ];
    });

$factory->defineAs(Okuma\Models\Feature::class, 'Feature',
    function ($faker) {
        return [
            'name' => $faker->numerify('Feature ###'),
            'description' => $faker->paragraph($faker->numberBetween(2, 5)),
            'position' => 1,
        ];
    });

$factory->defineAs(Okuma\Models\FeatureSet::class, 'FeatureSet',
    function ($faker) {
        return [
            'name' => $faker->numerify('Feature Set ##'),
            'position' => 1,
        ];
    });

$factory->defineAs(Okuma\Models\Image::class, 'ImageProductLarge',
    function ($faker) {
        return [
            'path' => 'http://dummyimage.com/1200x740/549A9E/fff.png&text=' . $product->name . ' 0x23' . $pi,
            'title' => 'Product Image',
            'type' => 'product_large',
            'width' => 1200,
            'height' => 740,
            'position' => 1,
        ];
    });

$factory->defineAs(Okuma\Models\Image::class, 'ImageProductThumbnail',
    function ($faker) {
        return [
            'path' => 'http://dummyimage.com/300x185/549A9E/FFFFFF',
            'title' => 'Product Thumbnail',
            'type' => 'product_thumbnail',
            'width' => 300,
            'height' => 185,
            'position' => 1,
        ];
    });

$factory->defineAs(Okuma\Models\Image::class, 'ImageFeatureIcon',
    function ($faker) {
        return [
            'path' => 'http://dummyimage.com/256x256/8abd07/FFFFFF',
            'title' => 'Feature Icon',
            'type' => 'feature_icon',
            'width' => 256,
            'height' => 256,
            'position' => 1,
        ];
    });

$factory->define(Okuma\Models\SiteFeature::class,
    function ($faker) {
        return [
            'title' => 'Site <strong>Feature</strong>',
            'body' => '<p>' . $faker->paragraph($faker->numberBetween(1, 3)) . '</p>',
            'link' => 'http://www.okuma.app',
            'button_label' => 'View More',
            'column_size' => '',
            'position' => 1,
            'published_at' => Carbon\Carbon::now(),
        ];
    });
