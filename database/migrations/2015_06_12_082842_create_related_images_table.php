<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelatedImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('related_images',
            function (Blueprint $table) {

                $table->engine = 'InnoDB';

                $table->increments('id');

                $table->integer('image_id')->unsigned()->nullable();
                $table->integer('related_image_id')->unsigned()->nullable();

                $table->integer('position')->unsigned()->default(1);

                $table->string('relation');

                $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
                $table->foreign('related_image_id')->references('id')->on('images')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('related_images',
            function (Blueprint $table) {
                Schema::dropIfExists('related_images');
            });
    }
}
