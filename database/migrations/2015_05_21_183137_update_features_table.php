<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFeaturesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('features',

            function (Blueprint $table) {
                $table->integer('feature_set_id')->unsigned()->nullable();
                $table->foreign('feature_set_id')->references('id')->on('feature_sets')->onDelete('set null');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('features',
            function (Blueprint $table) {
                $table->dropForeign('features_feature_set_id_foreign');
            }
        );
    }

}
