<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// use Illuminate\Support\Facades\DB;

class CreateProductCategoryTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories',

            function (Blueprint $table) {

                $table->engine = 'InnoDB';

                $table->increments('id');

                $table->integer('product_id')->unsigned();
                $table->integer('category_id')->unsigned();

                $table->integer('product_position')->unsigned()->default(1);

                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Schema::table('product_categories',
            function (Blueprint $table) {
                Schema::dropIfExists('product_categories');
            });

        // DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    }

}
