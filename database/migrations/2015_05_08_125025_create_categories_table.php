<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    public function up()
    {

        Schema::table('categories',

            function (Blueprint $table) {

                $table->engine = 'InnoDB';

                Schema::create('categories',

                    function (Blueprint $table) {

                        $table->increments('id');

                        $table->string('name', 219);
                        $table->text('description');

                        $table->string('slug', 255)->nullable()->index();

                        $table->integer('parent_id')->unsigned()->nullable();
                        $table->integer('position', false, true);
                        $table->integer('real_depth', false, true);

                        $table->timestamps();
                        $table->softDeletes();

                        $table->foreign('parent_id')->references('id')->on('categories')->onDelete('set null');
                    });
            });

    }

    public function down()
    {
        Schema::table('categories',
            function (Blueprint $table) {
                Schema::dropIfExists('categories');
            });
    }
}
