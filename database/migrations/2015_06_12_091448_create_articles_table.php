<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles',

            function (Blueprint $table) {

                $table->engine = 'InnoDB';

                $table->increments('id');

                $table->string('title', 219);
                $table->text('body');
                $table->string('author');

                $table->string('slug', 255)->nullable()->index();

                $table->timestamps();
                $table->softDeletes();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles',
            function (Blueprint $table) {
                Schema::dropIfExists('articles');
            });
    }
}
