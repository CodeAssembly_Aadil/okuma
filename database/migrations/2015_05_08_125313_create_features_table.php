<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeaturesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features',

            function (Blueprint $table) {
                $table->engine = 'InnoDB';

                $table->increments('id');

                $table->string('name', 219);
                $table->text('description');

                $table->string('slug', 255)->nullable()->index();

                $table->integer('position')->unsigned()->default(1);

                $table->timestamps();
                $table->softDeletes();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('features',
            function (Blueprint $table) {
                Schema::dropIfExists('features');
            });
    }

}
