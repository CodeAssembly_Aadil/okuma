<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSiteFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_features',

            function (Blueprint $table) {

                $table->engine = 'InnoDB';

                $table->increments('id');

                $table->string('title', 255);
                $table->text('body');
                $table->string('button_label', 64);

                $table->string('link', 2000);

                $table->string('column_size', 64);
                $table->integer('position')->unsigned()->default(1);

                $table->timestamp('published_at')->nullable()->index();

                $table->timestamps();
                $table->softDeletes();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_features',
            function (Blueprint $table) {
                Schema::dropIfExists('site_features');
            });
    }

}
