<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants',

            function (Blueprint $table) {

                $table->engine = 'InnoDB';

                $table->increments('id');

                $table->string('model', 32)->index();
                $table->string('sku', 32);

                $table->json('specification');

                $table->integer('position')->unsigned()->default(1);

                $table->timestamps();
                $table->softDeletes();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variants',
            function (Blueprint $table) {
                Schema::dropIfExists('variants');
            });

    }

}
