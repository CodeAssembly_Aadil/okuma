<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images',

            function (Blueprint $table) {

                $table->engine = 'InnoDB';

                $table->increments('id');

                $table->integer('imageable_id')->unsigned()->index();
                $table->string('imageable_type', 255)->index();

                $table->string('type', 255)->index();
                $table->string('title', 255);

                // maxlength of varchar is 21844 fo innodb
                $table->string('path', 2000);
                // $table->string('path');

                $table->smallInteger('width')->unsigned();
                $table->smallInteger('height')->unsigned();

                $table->integer('position')->unsigned()->default(1);

                // $table->string('related', 128)->nullable();
                // $table->string('meta', 128)->nullable();

                $table->timestamps();
                // $table->softDeletes();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images',
            function (Blueprint $table) {
                Schema::dropIfExists('images');
            });
    }

}
