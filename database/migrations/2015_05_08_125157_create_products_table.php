<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products',

            function (Blueprint $table) {

                $table->engine = 'InnoDB';

                $table->increments('id');

                // slug 255 - 4 (unique id) - 32 (for dashes)
                $table->string('name', 219);
                $table->text('description');

                $table->json('specification');

                // Optimal index length
                $table->string('slug', 255)->nullable()->index();

                $table->timestamp('published_at')->nullable()->index();

                $table->timestamps();
                $table->softDeletes();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products',
            function (Blueprint $table) {
                Schema::dropIfExists('products');
            });
    }

}
