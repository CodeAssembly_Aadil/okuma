<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductFeatureTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_features',

            function (Blueprint $table) {

                $table->engine = 'InnoDB';

                $table->increments('id');

                $table->integer('product_id')->unsigned();
                $table->integer('feature_id')->unsigned();

                $table->integer('feature_position')->unsigned()->default(1);

                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('product_features',
            function (Blueprint $table) {
                Schema::dropIfExists('product_features');
            });

    }

}
